import { defineConfig, loadEnv } from 'vite'
import react from '@vitejs/plugin-react'
import pathAliases from "vite-tsconfig-paths";

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  const env = loadEnv(mode,  "", "");

  return {
    plugins: [react(), pathAliases()],
    server: {
      port: Number(env.VITE_APP_PORT),
      host: env.VITE_APP_HOST
    }
  }
})
