import { MessagePlace } from "$/components/room/message/place";
import { Avatar, Box, Group, StackProps, Text, Tooltip } from "@mantine/core"

interface MessageReplyProps {
  avatarUrl: string;
  nickname: string;
  shortBody: string;
  containerProps?: StackProps;
}

function MessageReply({ avatarUrl, nickname, shortBody, containerProps }: MessageReplyProps) {
  const containerPropsComputed = containerProps || { bg: 'gray.8' };

  return (
    <MessagePlace>
      <MessagePlace.Inner containerProps={containerPropsComputed}>
        <MessagePlace.Header>
          <Group align="flex-start" wrap="nowrap">
            <Avatar size={30} src={avatarUrl} />
            <Tooltip label={nickname} style={{ overflowWrap: 'anywhere' }}>
              <Text truncate="end" w="100%">{nickname}</Text>
            </Tooltip>
          </Group>
        </MessagePlace.Header>
        <MessagePlace.Body>
          <Box mt={10} w="100%">
            <Text w="100%" style={{ overflowWrap: 'anywhere', whiteSpace: 'pre-wrap' }} lineClamp={3}>{shortBody}</Text>
          </Box>
        </MessagePlace.Body>
      </MessagePlace.Inner>
    </MessagePlace>
  )
}

export { MessageReply }