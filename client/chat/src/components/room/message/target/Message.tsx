import { ImageWithPreview } from "$/components/common/images";
import { Link } from "$/components/common/link";
import { MessagePlace } from "$/components/room/message/place"
import { MessageReply } from "$/components/room/message/reply";
import { ActionIcon, Avatar, Box, Group, Stack, StackProps, Text, Tooltip } from "@mantine/core";

interface Message {
  isMine: boolean;
  avatarUrl: string;
  nickname: string;
  message: string;
  onClickReply: () => void;
}

interface MessageProps {
  messageAuthor: Message;
  messageReply?: Message;
  messageReplyContainerProps?: StackProps;
}

function Message({ messageAuthor, messageReply, messageReplyContainerProps }: MessageProps) {

  const outer = (
    <MessagePlace.Outer>
      <ImageWithPreview url={messageAuthor.avatarUrl}>
        {(url) => (
          <Avatar src={url} />
        )}
      </ImageWithPreview>
    </MessagePlace.Outer>
  );

  const replyMessage = messageReply && (
    <ActionIcon variant="transparent" color="white" onClick={messageReply.onClickReply} style={{ border: 'none' }} w="auto" h="auto">
      <MessageReply
        containerProps={{ bg: messageReply.isMine ? 'gray.8' : 'dark.6', ...messageReplyContainerProps }}
        avatarUrl={messageReply.avatarUrl} 
        nickname={messageReply.nickname}
        shortBody={messageReply.message} />
    </ActionIcon>
  )

  return (
    <MessagePlace placeProps={{ maw: "55vh", align: 'flex-start' }}>
      {!messageAuthor.isMine && (
        <>
          {outer}
        </>
      )}

      <MessagePlace.Inner containerProps={{ bg: messageAuthor.isMine ? 'gray.7' : 'dark.5' }}>
        <MessagePlace.Header>
          <Group wrap="nowrap" w="100%" justify="space-between" align="center">
            <Tooltip label={messageAuthor.nickname} style={{ overflowWrap: "anywhere" }}>
              <Text truncate="end">
                {messageAuthor.nickname}
              </Text>
            </Tooltip>
            <Box miw={70}>
              <Link onClick={messageAuthor.onClickReply}>
                Ответить
              </Link>
            </Box>
          </Group>
        </MessagePlace.Header>
        <MessagePlace.Body>
          <Stack w="100%">
            {replyMessage}
            <Text style={{ overflowWrap: 'anywhere', whiteSpace: 'pre-wrap' }}>{messageAuthor.message}</Text>
          </Stack>
        </MessagePlace.Body>
      </MessagePlace.Inner>

      {messageAuthor.isMine && (
        <>
          {outer}
        </>
      )}
    </MessagePlace>
  )
}

export { Message }