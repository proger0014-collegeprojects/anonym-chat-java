import { Box, Group, Loader, Text } from "@mantine/core";
import { IconCheck, IconX } from "@tabler/icons-react";
import { ReactNode } from "react";

type status = 'processing'|'failed'|'success';

interface MessageSendStatusProps {
  status: status;
} 

// TODO: появляется только тогда, когда отправляется... при перезаходе стейт об этом стирается
function MessageSendStatus({ status }: MessageSendStatusProps) {
  let icon: ReactNode = <></>;
  
  switch (status) {
    case 'success':
      icon = <Box>
        <IconCheck size={12} />
      </Box>
      break;
    case 'failed':
      icon = <Box>
        <IconX size={12} />
      </Box>
      break;
    case 'processing':
      icon = <Box>
        <Loader size={12} />
      </Box>
      break;
  }

  return (
    <Group wrap="nowrap">
      <Text fz={12} style={{  }}>
        Отправляется
      </Text>
      {icon}
    </Group>
  )
}

export { MessageSendStatus }