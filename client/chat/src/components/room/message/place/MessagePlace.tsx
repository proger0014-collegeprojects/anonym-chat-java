import { ChildrenProps } from "$/shared/types";
import { Box, BoxProps, Group, GroupProps, Stack, StackProps } from "@mantine/core";

interface MessageCoreProps extends ChildrenProps {
  placeProps?: GroupProps;
}

function MessagePlace({ children, placeProps }: MessageCoreProps) {
  return (
    <Group wrap="nowrap" { ...placeProps }>
      {children}
    </Group>
  )
}

interface ContainerInnerProps extends ChildrenProps {
  containerProps?: StackProps;
}

function MessagePlaceContainerInner({ children, containerProps }: ContainerInnerProps) {
  return (
    <Stack p={10} w="100%" gap={0} style={{ borderRadius: 'var(--mantine-radius-default)' }} { ...containerProps }>
      {children}
    </Stack>
  )
}

interface ContainerProps extends ChildrenProps {
  containerProps?: BoxProps;
}

function MessagePlaceContainer({ children, containerProps }: ContainerProps) {
  return (
    <Box { ...containerProps }>
      {children}
    </Box>
  )
}

function MessageItem({ children }: ChildrenProps) {
  return (
    <Box w="100%">
      {children}
    </Box>
  )
}

MessagePlace.Outer = MessagePlaceContainer;
MessagePlace.Inner = MessagePlaceContainerInner;
MessagePlace.Header = MessageItem
MessagePlace.Body = MessageItem
MessagePlace.Footer = MessageItem;

export { MessagePlace }