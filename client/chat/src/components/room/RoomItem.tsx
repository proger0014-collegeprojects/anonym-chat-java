import { Room } from "$/shared/api/contracts"
import { Box, Button, ButtonProps, Flex, Group, ScrollArea, Text, Tooltip } from "@mantine/core"
import { IconUser } from "@tabler/icons-react"

interface RoomItemProps extends Room {
  onClick: () => void;
  isConnecting?: boolean;
  buttonProps?: ButtonProps;
  buttonText: string;
}

function RoomItem({ id, countConnectedUsers, name, description, onClick, buttonProps, buttonText, isConnecting = false }: Omit<RoomItemProps, 'userId'>) {
  return (
    <Box>
      <Flex justify="space-between" gap="10px">
        <Tooltip zIndex={4000} label={name} style={{ overflowWrap: 'anywhere' }} multiline={true}>
          <Text fz={18} fw={700} truncate="end">
            {name}
          </Text>
        </Tooltip>
        <Text>
          #{id}
        </Text>
      </Flex>
      {description && (
        <ScrollArea.Autosize mah={100} w="100%" my={5}>
          <Text style={{ overflowWrap: 'anywhere' }}>
            {description}
          </Text>
        </ScrollArea.Autosize>
      )}
      <Group justify="space-between">
        <Box>
          <Button { ...buttonProps } loading={isConnecting} onClick={onClick} size="compact-xs" variant="default">{buttonText}</Button>
        </Box>
        <Group>
          <IconUser size={16} />
          <Text ml={-10}>{countConnectedUsers}</Text>
        </Group>
      </Group>
    </Box>
  )
}

export { RoomItem }