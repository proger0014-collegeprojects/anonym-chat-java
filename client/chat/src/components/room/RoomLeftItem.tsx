import { ButtonIconV2 } from "$/components/common/buttons/button-icon";
import { RoomItemV2 } from "$/components/room/RoomItemV2";
import { Room } from "$/shared/api/contracts";
import { Avatar, Badge, Group, Text } from "@mantine/core";
import { IconMessage } from "@tabler/icons-react";
import { observer } from "mobx-react-lite";

interface LastMessageInfo {
  avatarUrl: string;
  text: string;
}

interface RoomLeftItemProps {
  onClick?: (room: Room) => void;
  room: Room;
  isActive?: boolean;
  newMessagesCount?: number;
  lastMessage?: LastMessageInfo;
}

const RoomItemV2Observalbe = observer(RoomItemV2);

function RoomLeftItem({
  room,
  isActive,
  newMessagesCount,
  lastMessage,
  onClick }: RoomLeftItemProps) {
  const computedIsActive = isActive ?? false;

  const content = lastMessage && (
    <Group wrap="nowrap" align="center">
      <Avatar src={lastMessage.avatarUrl} />
      <Text maw={250} truncate>{lastMessage.text}</Text>
    </Group>
  )

  const footer = newMessagesCount && newMessagesCount > 0 && (
    <Badge 
      py={12} 
      variant="default" 
      leftSection={<IconMessage size={20}/>}>
      {newMessagesCount}
    </Badge>
  );

  return (
    <ButtonIconV2
      onClick={onClick && (() => onClick(room))}
      height="auto" 
      buttonProps={{
        p: 0,
        py: 5,
        px: 10,
        bg: computedIsActive && 'gray.7' || undefined,
        h: 'auto', 
        fullWidth: true,
        styles: {
          'label': {
            'width': '100%'
          }
        } }}>
      <RoomItemV2Observalbe
        headerProps={{ w: '318px' }}
        contentPlaceProps={{ style: { textAlign: 'start' }, py: 5 }}
        content={content && (() => (<>{content}</>)) || undefined} 
        data={room}
        footer={footer && (() => (<>{footer}</>)) || undefined} />
    </ButtonIconV2>
  )
}

export { RoomLeftItem }