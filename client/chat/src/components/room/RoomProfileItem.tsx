import { ImageWithPreview } from "$/components/common/images";
import { ChildrenProps } from "$/shared/types";
import { Avatar as MAvatar, Box, BoxProps, Flex, Stack, Text, Tooltip, TextProps } from "@mantine/core";
import { ReactNode } from "react";

interface AvatarProps {
  url: string;
  size: number|string;
  props?: BoxProps;
}

function RoomProfileAvatar({ url, size, props }: AvatarProps) {
  return (
    <Box mr={20} { ...props } >
      <ImageWithPreview url={url}>
        {(url) => (
          <MAvatar size={size} variant="outline" src={url} />
        )}
      </ImageWithPreview>
    </Box>
  )
}

function RoomProfileBody({ children }: ChildrenProps) {
  return (
    <Stack w="100%">
      {children}
    </Stack>
  )
}

function RoomProfileBodyContent({ children }: ChildrenProps) {
  return (
    <Box h="80%">
      {children}
    </Box>
  )
}

function RoomProfileBodyFooter({ children }: ChildrenProps) {
  return (
    <Box h="30%">
      {children}
    </Box>
  )
}
 
function RoomProfileItem({ children }: ChildrenProps) {
  return (
    <Flex align="start">
      {children}
    </Flex>
  )
}

RoomProfileBody.Content = RoomProfileBodyContent;
RoomProfileBody.Footer = RoomProfileBodyFooter;

RoomProfileItem.Avatar = RoomProfileAvatar;
RoomProfileItem.Body = RoomProfileBody;

interface Avatar {
  size: string|number;
  url: string;
}

interface RoomProfileItemFastProps {
  avatar: Avatar;
  nickname: string;
  footer?: ReactNode;
  nicknameStyleProps?: TextProps;
  avatarProps?: BoxProps;
}

function RoomProfileItemFast({ avatar: { size, url }, nickname, footer, nicknameStyleProps, avatarProps  }: RoomProfileItemFastProps) {
  return (
    <RoomProfileItem>
      <RoomProfileItem.Avatar
        props={avatarProps}
        size={size} 
        url={url} />
      <RoomProfileItem.Body>
        <RoomProfileItem.Body.Content>
          <Tooltip zIndex={3000} label={nickname} style={{ overflowWrap: 'anywhere' }}>
            <Text fz={nicknameStyleProps?.fz ?? 20} truncate="end" { ...nicknameStyleProps }>
              {nickname}
            </Text>
          </Tooltip>
        </RoomProfileItem.Body.Content>
        {footer && (
          <RoomProfileItem.Body.Footer>
            {footer}
          </RoomProfileItem.Body.Footer>
        )}
      </RoomProfileItem.Body>
    </RoomProfileItem>
  )
}

export { RoomProfileItem, RoomProfileItemFast }