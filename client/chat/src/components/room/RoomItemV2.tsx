import { Room } from "$/shared/api/contracts";
import { Box, BoxProps, Flex, Group, Text } from "@mantine/core";
import { IconUser } from "@tabler/icons-react";
import { ReactNode } from "react";

interface RoomItemProps {
  data: Room;
  content?: (data: Room) => ReactNode;
  footer?: (data: Room) => ReactNode;
  headerProps?: BoxProps;
  footerPlaceProps?: BoxProps;
  contentPlaceProps?: BoxProps;
}


function RoomItemV2({ data, footer, content, headerProps, footerPlaceProps, contentPlaceProps }: RoomItemProps) {
  return (
    <Box w="100%">
      <Flex w="100%" justify="space-between" { ...headerProps }>
        <Text fz={18} mr={20} fw={700} truncate="end">
          {data.name}
        </Text>
        <Text>
          #{data.id}
        </Text>
      </Flex>
      {content && (
        <Box w="100%" { ...contentPlaceProps }>
          {content(data)}
        </Box>
      )}
      <Group justify={footer ? 'space-between' : 'end'} { ...footerPlaceProps }>
        {footer && (
          <>
            {footer(data)}
          </>
        )}
        <Group>
          <IconUser size={16} />
          <Text ml={-10}>{data.countConnectedUsers}</Text>
        </Group>
      </Group>
    </Box>
  )
}

export { RoomItemV2 }