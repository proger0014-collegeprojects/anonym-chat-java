import { Cropper } from "$/components/room/profile/avatars/widget/cropper";
import { Avatar, Stack } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { useEffect, useState } from "react";

interface ImageChangerProps {
  setterResult: (blob: Blob, url: string|undefined) =>  void;
  src: string;
}

const ImageChanger = observer(({ setterResult, src }: ImageChangerProps) => {
  const [_, rerenderAvatar] = useState("");

  const [result, setResult] = useState<{
    blob: Blob|undefined;
    url: string|undefined;
  }>({ 
    blob: undefined,
    url: undefined
  });

  useEffect(() => {
    return () => {
      if (!result.blob || !result.url) return;

      setterResult(result.blob, result.url);
    }
  }, [src]);
  
  
  function onCrop(blob: Blob, url: string|undefined) {
    rerenderAvatar(url ?? '');

    setResult(v => {
      v.blob = blob;
      v.url = url;

      return v;
    });
  }

  return (
    <Stack>
      <Avatar src={result?.url ?? ''} bg="black" size="300px" />
      <Cropper
        src={src}
        minSize={50}
        onChange={onCrop}
        containerProps={{ w: 300, h: 300 }} />
    </Stack>
  )
});

export { ImageChanger }