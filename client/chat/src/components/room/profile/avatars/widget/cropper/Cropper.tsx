import { Box, BoxProps} from "@mantine/core";
import { useMove, useWindowEvent } from "@mantine/hooks";
import { RefObject, useState, useRef } from "react";
import RCropper, { ReactCropperElement } from "react-cropper";

interface CropperProps {
  src: string;
  minSize: number;
  onChange: (file: Blob, url: string|undefined) => void;
  containerProps?: BoxProps;
}

function Cropper({ src, minSize, onChange, containerProps }: CropperProps) {
  const [isRightButton, setRightButton] = useState(false);

  const cropperRef = useRef<ReactCropperElement>(null);

  const { ref: mouveMoveRef, active: mouseMoveActive } = useMove(_ => {});

  useWindowEvent('mousedown', (e) => {
    setRightButton(e.button == 2);
  });

  useWindowEvent('mousemove', (e) => {
    const cropper = cropperRef.current?.cropper;

    if (!mouseMoveActive || !isRightButton) return;

    cropper?.move(e.movementX, e.movementY);
  });

  const onCrop = () => {
    const cropper = cropperRef.current?.cropper;

    cropper?.getCroppedCanvas().toBlob((blob => onChange(blob!, cropper?.getCroppedCanvas()?.toDataURL())));
  };

  return (
    <CropperInner 
      containerProps={containerProps} 
      minSize={minSize} 
      mouseMoveRef={mouveMoveRef} 
      src={src} 
      refCropper={cropperRef} 
      onScrop={onCrop} />
  )
}

interface CropperInnerProps  {
  src: string;
  onScrop: () => void;
  refCropper: RefObject<ReactCropperElement>;
  mouseMoveRef: RefObject<HTMLDivElement>;
  minSize: number;
  containerProps?: BoxProps;
}

function CropperInner({ src, onScrop, refCropper, mouseMoveRef, minSize, containerProps }: CropperInnerProps) {

  return (
    <Box ref={mouseMoveRef} w="100%" onContextMenu={(e) => e.preventDefault() } { ...containerProps }>
      <RCropper
        autoCropArea={1}
        minCropBoxWidth={minSize}
        minCropBoxHeight={minSize}
        crossOrigin="use-credentials"
        viewMode={3}
        responsive
        background={false}
        checkOrientation={false}
        ref={refCropper}
        crop={onScrop}
        src={src}
        aspectRatio={4 / 4}
        style={{ width: "100%", height: '100%' }} />
    </Box>
  )
}

export { Cropper }