import { LocalStore } from "$/components/room/profile/avatars/avatars-list";
import { createContext, useContext } from "react";

interface ContextInfo {
  localStore: LocalStore;
}

const Context = createContext<ContextInfo>({
  localStore: {} as LocalStore
});

function useLocalContext() {
  return useContext(Context);
}

export { useLocalContext, Context }