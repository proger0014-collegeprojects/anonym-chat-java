import { getAvatarsSection } from "$/shared/api/avatar";
import { Avatar, Section } from "$/shared/api/contracts";
import { makeAutoObservable } from "mobx";
import { IPromiseBasedObservable, fromPromise } from "mobx-utils";

class LocalStore {
  avatarsList: Avatar[];
  avatarsSection: IPromiseBasedObservable<Section<Avatar>>;
  lastSection: number;
  selectedItemId?: number;

  constructor() {
    makeAutoObservable(this);

    this.avatarsList = [];
    this.avatarsSection = fromPromise(new Promise(_ => {}));
    this.lastSection = 0;
  }

  fetchSection(section: number) {
    const response = getAvatarsSection(section);

    this.avatarsSection = fromPromise(response);

    this.avatarsSection.then(v => {
      if (this.lastSection == v.section) return;

      this.lastSection = v.section;

      this.setAvatarsList([...this.avatarsList, ...v.items]);
    });

  }

  addOneToAvatarsList(avatar: Avatar) {
    this.avatarsList = [...this.avatarsList, avatar];
  }

  setAvatarsList(avatars: Avatar[]) {
    this.avatarsList = avatars;
  }

  updateSelectedItemId(id: number) {
    this.selectedItemId = id;
  }

  resetSelectedItem() {
    if (!this.selectedItemId) return;

    this.selectedItemId = undefined;
  }
}

export { LocalStore }