import { HorizontalScrollArea } from "$/components/common/horizontal-scroll-area";
import { Avatar as MAvatar, Loader, UnstyledButton } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { Context, useLocalContext } from "$/components/room/profile/avatars/avatars-list/context";
import { LocalStore } from "$/components/room/profile/avatars/avatars-list/LocalStore";
import { ReactNode, useEffect, useState } from "react";
import { Avatar, Section } from "$/shared/api/contracts";
import { useStores } from "$/app/stores";
import { IMessage } from "@stomp/stompjs";

interface AvatarsListProps {
  localStore: LocalStore;
}

const AvatarsList = observer(({ localStore }: AvatarsListProps) => {
  const contextValue = {
    localStore: localStore
  }

  return (
    <Context.Provider value={contextValue}>
      <AvatarsListInnerWrapper />
    </Context.Provider>
  )
});

const AvatarsListInnerWrapper = observer(()  => {
  const { localStore } = useLocalContext();

  const { messagingClientStore } = useStores();
  
  useEffect(() => {
    if (messagingClientStore.isConnected) {
      messagingClientStore.subscribe('/concrete-user/avatar/created', (e: IMessage) => {
        if (localStore.lastSection < (localStore.avatarsSection.value as Section<Avatar>).lastSection) return;

        const avatar = JSON.parse(e.body) as Avatar;

        localStore.addOneToAvatarsList(avatar);
      });
    }

    return () => {
      messagingClientStore.unsubscribe('/concrete-user/avatar/created');
    }
  }, [messagingClientStore.isConnected])

  useEffect(() => {
    if (localStore.lastSection > 0 
      && localStore.lastSection >= 1) return;

    localStore.fetchSection(1);
  }, [localStore.avatarsSection.state]);

  function fetchSection(x: number, maxWidth: number) {
    if (x >= (maxWidth - 40)
        && localStore.avatarsSection.value
        && localStore.lastSection < (localStore.avatarsSection.value as Section<Avatar>).lastSection) {
      localStore.fetchSection(localStore.lastSection + 1);
    }
  }

  const items = getItems(localStore);

  return (
    <>
      <AvatarsListInner items={items} fetchSectionHandler={fetchSection} />
    </>
  )
});


function getItemsComponents(items: Avatar[]) {
  return items.map(i => (
    <AvatarsListItem key={i.id} data={i} />
  ));
}

function getItems(localStore: LocalStore) {
  return (
    <>
      <>
        {getItemsComponents(localStore.avatarsList)}
      </>
      {localStore.avatarsSection.case({
        pending: () => (
          <>
            <Loader />
          </>
        ),
        fulfilled: () => null,
        rejected: () => null
      }) }
    </>
  )
}

interface AvatarsListItemProps {
  data: Avatar;
}

const AvatarsListItem = observer(({ data }: AvatarsListItemProps) => {
  const { localStore } = useLocalContext();
  const [isSelected, setIsSelected] = useState(false);

  useEffect(() => {
    if (data.id != localStore.selectedItemId && isSelected) {
      unselected();
    } else if (data.id == localStore.selectedItemId && !isSelected) {
      setIsSelected(true);
    }

    return () => {
      unselected();
    };
  }, [localStore.selectedItemId]);

  function selected() {
    localStore.updateSelectedItemId(data.id);
  }

  function unselected() {
    setIsSelected(false);
  }

  return (
    <UnstyledButton p="4px" style={{ borderRadius: 'var(--mantine-radius-default)' }} bg={isSelected ? 'dark.5' : undefined} onClick={selected}>
      <MAvatar size="60px" src={data.url} />
    </UnstyledButton>
  )
});

interface AvatarsListInnerProps {
  fetchSectionHandler: (x: number, maxWidth: number) => void;
  items: ReactNode;
}

const AvatarsListInner = observer(({ fetchSectionHandler, items }: AvatarsListInnerProps) => {
  return (
    <HorizontalScrollArea innerProps={{ p: '6px' }} scrollbarSize={2} onScrollHandler={fetchSectionHandler}>
      {items}
    </HorizontalScrollArea>
  )
});

export { AvatarsList }