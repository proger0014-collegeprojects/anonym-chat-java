import { RoomProfileItemFast } from "$/components/room";
import { ActionIcon, Box, Group, Indicator, Pagination, ScrollArea, Skeleton } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { ReactNode, createContext, useContext, useEffect } from "react";
import { MessagingClientStore, useStores } from "$/app/stores";
import { IconPlus, IconRefresh } from "@tabler/icons-react";
import { LocalWidgetStore, ModalTabInfoActions } from "$/components/room/profile/widget/LocalTabStore";
import { RoomProfileFull } from "$/shared/api/contracts";
import { useDisclosure } from "@mantine/hooks";
import { RoomProfilesCreateModal, RoomProfilesCreateStore } from "$/components/room/profile/widget/create-modal";

interface ContextProps {
  localWidgetStore?: LocalWidgetStore;
  footerItem?: (data: RoomProfileFull) => ReactNode;
}

const Context = createContext<ContextProps>({});

function useLocalContext() {
  return useContext(Context);
}

interface RoomProfileWidgetProps {
  contextProps: ContextProps;
  footerItem?: (data: RoomProfileFull) => ReactNode;
}

const RoomProfiles = observer(({ contextProps, footerItem }: RoomProfileWidgetProps) => {

  return (
    <Context.Provider value={{ ...contextProps, footerItem }}>
      <RoomProfileInnerWrapper />
    </Context.Provider>
  )
})

function getPagination(localWidgetStore?: LocalWidgetStore): PaginationInfo {
  const currnetPageValue = localWidgetStore?.currentPage.case({
    pending: () => localWidgetStore?.pendingPageInfo.currentPage,
    fulfilled: (v) => v.section,
    rejected: () => -1
  });

  const totalPageInfo = localWidgetStore?.currentPage.case({
    pending: () => localWidgetStore?.pendingPageInfo.totalPages,
    fulfilled: (v) => v.lastSection,
    rejected: () => -1
  });

  function onChangePage(newPage: number) {
    localWidgetStore?.resetCountCreatedNewForRefresh();
    localWidgetStore?.fetchPage(newPage);
  }

  return {
    currentPage: currnetPageValue ?? 0,
    totalPages: totalPageInfo ?? 0,
    onChangePage: onChangePage
  }
}

interface GetModalTabInfoActionsResult extends ModalTabInfoActions {
  unmount: () => void;
}

function getModalTabInfoActions(
    messagingClientStore: MessagingClientStore,
    localWidgetStore?: LocalWidgetStore): GetModalTabInfoActionsResult {
  function onClickRefresh() {
    localWidgetStore?.resetCountCreatedNewForRefresh();
    localWidgetStore?.refreshPage();
  }


  messagingClientStore.subscribe('/concrete-user/room/profile/created', () => {
    localWidgetStore?.incrementCountCreatedNewForRefresh();
  });

  function unmount() {
    messagingClientStore.unsubscribe('/concrete-user/room/profile/created');
  }

  return {
    onClickCreate: () => {},
    refresh: {
      onClick: onClickRefresh,
      count: 0
    },
    unmount: unmount
  }
}

function getItems(localWidgetStore?: LocalWidgetStore, componentFooterItem?: (data: RoomProfileFull) => ReactNode): ReactNode {
  return localWidgetStore?.currentPage.case({
    pending: () => {
      const count = 3;

      const component = (
        <Box mt={15}>
          <Skeleton height={16} radius="xs" />
          <Skeleton height={14} radius="xs" my={15} />
          <Group justify="space-between">
            <Skeleton height={14} width="30%" radius="xs" />
            <Skeleton height={14} width="10%" radius="xs" />
          </Group>
        </Box>
      );

      const components = [];

      for (let i = 0; i < count; i++) {
        components.push(<Box mb={20} key={i}>
          {component}
        </Box>);
      }

      return (
        <>
          {components}
        </>
      )
    },
    fulfilled: (v) => {
      const items = v.items.map(i => {
        const footer = componentFooterItem ? (
          <>
            {componentFooterItem(i)}
          </>
        )
        : <></>;

        return (
          <Box mb={30} key={i.id}>
            <RoomProfileItemFast
              avatar={{ size: '5rem', url: i.avatar.url }}
              nickname={i.nickname}
              footer={footer}
              nicknameStyleProps={{ w: "52vh" }}  />
          </Box>
        )
      });

      return (
        <>
          {items}
        </>
      );
    },
    rejected: () => {
      return (
        <></>
      )
    }
  });
}

const RoomProfileInnerWrapper = observer(() => {
  const { localWidgetStore, footerItem } = useLocalContext();
  const { messagingClientStore } = useStores();

  const roomProfileCreateModalLocalStore = new RoomProfilesCreateStore();

  useEffect(() => {
    localWidgetStore?.fetchPage(1);
    localWidgetStore?.resetCountCreatedNewForRefresh();
    
    const modalTabInfoActions = getModalTabInfoActions(messagingClientStore, localWidgetStore);
    localWidgetStore?.updateModalTabActions(modalTabInfoActions);

    return () => {
      modalTabInfoActions.unmount();
    }
  }, [localWidgetStore])

  const paginationInfo = getPagination(localWidgetStore);
  const items = getItems(localWidgetStore, footerItem);

  const [ openedCreateModal, { open: openCreateModal, close: closeCreateModal } ] = useDisclosure(false);

  const createModalInfo: CreateModalInfo = {
    opened: openedCreateModal,
    open: openCreateModal,
    close: closeCreateModal
  }

  return (
    <RoomProfilesInner
      roomProfileCreateModalLocalStore={roomProfileCreateModalLocalStore}
      pagination={paginationInfo}
      items={items}
      createModalInfo={createModalInfo} />
  )
})

interface PaginationInfo {
  currentPage: number;
  totalPages: number;
  onChangePage: (newPage: number) => void;
}

interface CreateModalInfo {
  opened: boolean;
  open: () => void;
  close: () => void;
}

interface RoomProfilesInnerProps {
  pagination: PaginationInfo;
  items: ReactNode;
  createModalInfo: CreateModalInfo;
  roomProfileCreateModalLocalStore: RoomProfilesCreateStore;
}

function getRefreshAction(onClick?: () => void, count?: number): ReactNode {
  const actionsButton = (
    <ActionIcon onClick={onClick} variant="default">
      <IconRefresh />
    </ActionIcon>
  );

  const actionsButtonWithIndicator = (count ?? 0) > 0
    ? (
      <Box>
        <Indicator zIndex={1000} inline color="red" label={count} size={18}>
          {actionsButton}
        </Indicator>
      </Box>
    )
    : actionsButton;

  return actionsButtonWithIndicator;
}

const RoomProfilesInner = observer(({ 
  pagination: { currentPage, totalPages, onChangePage }, 
  items,
  createModalInfo,
  roomProfileCreateModalLocalStore }: RoomProfilesInnerProps) => {

  const { localWidgetStore } = useLocalContext();
  const { uiStore } = useStores();

  return (
    <Box>
      <Box mb={20}>
        <Group align="start">
          <>
            <RoomProfilesCreateModal
              localStore={roomProfileCreateModalLocalStore}
              opened={createModalInfo.opened}
              close={createModalInfo.close} />
            <ActionIcon onClick={createModalInfo.open} variant="default">
              <IconPlus />
            </ActionIcon>
          </>
          {getRefreshAction(localWidgetStore?.currentModalTabActions?.refresh.onClick, localWidgetStore?.currentModalTabActions?.refresh.count)}
        </Group>
      </Box>
      <ScrollArea h={uiStore.height - 300} scrollbarSize={2}>
        {items}
      </ScrollArea>
      <Box h="6vh">
        <Group h="100%" align="center" justify="center">
          <Pagination 
            total={totalPages}
            value={currentPage}
            onChange={onChangePage}  />
        </Group>
      </Box>
    </Box>
  )
});

export { RoomProfiles as RoomProfilesWidget }