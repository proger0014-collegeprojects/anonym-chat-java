import { RoomProfileFull, Section } from "$/shared/api/contracts";
import { getRoomProfilesPage } from "$/shared/api/room";
import { PendingPageInfo } from "$/shared/types";
import { getPendingPage } from "$/shared/utils";
import { makeAutoObservable } from "mobx";
import { IPromiseBasedObservable, fromPromise } from "mobx-utils";

interface ModalTabInfoActionsRefresh {
  onClick: () => void;
  count: number;
}

interface ModalTabInfoActions {
  onClickCreate: () => void;
  refresh: ModalTabInfoActionsRefresh;
}

class LocalWidgetStore {
  currentPage: IPromiseBasedObservable<Section<RoomProfileFull>>;
  pendingPageInfo: PendingPageInfo;
  currentModalTabActions?: ModalTabInfoActions;

  constructor() {
    makeAutoObservable(this);

    this.currentPage = fromPromise(new Promise(_ => {}));
    this.pendingPageInfo = {
      currentPage: 0,
      totalPages: 0
    }
  }

  updateModalTabActions(actions: ModalTabInfoActions) {
    this.currentModalTabActions = actions;
  }

  incrementCountCreatedNewForRefresh() {
    this.currentModalTabActions!.refresh.count++;
  }

  resetCountCreatedNewForRefresh() {
    if (!this.currentModalTabActions?.refresh) return;
    
    this.currentModalTabActions!.refresh.count = 0;
  }

  fetchPage(page: number) {
    const pendingPageInfo = getPendingPage(page, this.currentPage.value as Section<RoomProfileFull>);

    if (pendingPageInfo) {
      this.pendingPageInfo = pendingPageInfo;
    }

    const response = getRoomProfilesPage(page);

    this.currentPage = fromPromise(response);
  }

  refreshPage() {
    const currentPage = (this.currentPage.value as Section<RoomProfileFull>).section ?? 1;

    this.fetchPage(currentPage);
  }
}

export { LocalWidgetStore, type ModalTabInfoActions }