import { Box, Button, Group, Modal, ScrollArea, Stack, Text, TextInput, UnstyledButton } from "@mantine/core";
import { cn } from "$/shared/styles";
import c from "./RoomProfilesCreateModal.module.scss";
import { AvatarsList } from "$/components/room/profile/avatars/avatars-list";
import { LocalStore as AvatarsListStore } from "$/components/room/profile/avatars/avatars-list";
import { DropzoneLocal } from "$/components/common/drop-zones";
import { FileWithPath, IMAGE_MIME_TYPE } from "@mantine/dropzone";
import { ReactNode, useEffect, useRef, useState } from "react";
import { observer } from "mobx-react-lite";
import { getFileUrl } from "$/shared/utils";
import { RoomProfilesCreateStore } from "$/components/room/profile/widget/create-modal";
import { Context, useLocalStore } from "$/components/room/profile/widget/create-modal/context";
import { useDisclosure } from "@mantine/hooks";
import { ImageChanger } from "$/components/room/profile/avatars/image-changer";
import { UseFormReturnType, useForm } from "@mantine/form";
import { notifySuccess } from "$/shared/lib";
import { AvatarLeftImage } from "$/components/common/avatar-left-image";


interface RoomProfilesCreateModal {
  close: () => void;
  opened: boolean;
  localStore: RoomProfilesCreateStore;
}

const RoomProfilesCreateModal = observer(({ close, opened, localStore }: RoomProfilesCreateModal) => {
  if (!opened) {
    return null;
  }
  
  const avatarsListStore = new AvatarsListStore();

  localStore.setAvatarsListStore(avatarsListStore);

  return (
    <Context.Provider value={localStore}>
      <RoomProfileCreateModalInnerWrapper opened={opened} close={close} />
    </Context.Provider>
  )
});


interface CreateRoomProfileForm {
  nickname: string;
}

interface RoomProfileCreateModalInnerWrapperProps extends Omit<RoomProfilesCreateModal, 'localStore'> { }

const RoomProfileCreateModalInnerWrapper = observer(({ opened, close }: RoomProfileCreateModalInnerWrapperProps) => {
  const localStore = useLocalStore();

  const openFileLoadRef = useRef<() => void>(null);

  const [canCreate, setCanCreate] = useState(false);

  const createRoomProfileForm = useForm<CreateRoomProfileForm>({
    initialValues: {
      nickname: ''
    },

    onValuesChange: (v) => {
      if (v.nickname.length >= 5 && (localStore?.existsAvatarForRoomProfile ?? false)) {
        setCanCreate(true);
      } else if (v.nickname.length < 5) {
        setCanCreate(false)
      }
    }
  });

  useEffect(() => {
    if (localStore?.activeFile && canCreate) return;

    if (createRoomProfileForm.getValues().nickname.length >= 5) {

      setCanCreate(true);
    }
  }, [localStore?.activeFile])

  function onCreate(nickname: string) {
    if (!canCreate) return;

    localStore?.createRoomProfile(nickname);

    localStore?.createRoomProfileStatus.then(_ => {
      notifySuccess({
        message: 'Успешно создан профиль комнаты!'
      });

      close();
    })
  }

  const isLoading = localStore!.createRoomProfileStatus.case({
    pending: () => true,
    rejected: () => false,
    fulfilled: () => false
  });

  function openFileLoad() {
    openFileLoadRef.current?.();
  }

  useEffect(() => {
    if (!localStore?.avatarsListStore) return;
    
    const avatar = localStore?.avatarsListStore?.avatarsList
      .filter(a => a.id == localStore.avatarsListStore?.selectedItemId)[0];

    if (!avatar) return;

    localStore.setActiveFile({
      id: avatar.id,
      url: avatar.url,
    })

  }, [localStore?.avatarsListStore?.selectedItemId])

  function onDropFile(f: FileWithPath[]) {
    getFileUrl(f[0], (res) => {
      localStore?.setLoadFile({
        base64: res!.toString(),
        file: f[0]
      });
    });
  }

  function onCropImage(blob: Blob, url: string|undefined) {
    localStore?.setLoadFile({
      base64: url ?? '',
      file: blob
    })
  }

  return (
    <>
      <RoomProfilesCreateModalInner 
        isLoading={isLoading}
        canCreate={canCreate}
        form={createRoomProfileForm}
        onCreate={onCreate}
        onCrop={onCropImage}
        opened={opened} 
        close={close}
        openFileLoadRef={openFileLoadRef}
        openFileLoad={openFileLoad}
        onDropFile={onDropFile} />
    </>
  )
})

interface WrapperComponentObservable {
  target: ReactNode; 
  active: () => void; 
  inactive: () => void; 
  isActive: boolean;
  localStore: RoomProfilesCreateStore;
  openFileLoad: (fn: () => void) => void;
  form: UseFormReturnType<CreateRoomProfileForm>;
  canCreate: boolean;
  onCreate: (nickname: string) => void;
  isLoading: boolean;
  onCrop: (blob: Blob, url: string|undefined) => void;
}

const WrapperComponentObservable = observer(({ 
  target, 
  active, 
  inactive, 
  isActive, 
  localStore, 
  openFileLoad, 
  onCrop, 
  form, 
  canCreate, 
  onCreate,
  isLoading }: WrapperComponentObservable) => {
  const [openedImageChanger, { toggle: toggleImageChanger }] = useDisclosure(false);

  return (
    <Box 
      pos="relative" 
      onDragEnter={active}  
      onMouseLeave={inactive}>
      <Group justify="center" align="center">
        {
          isActive && (
            <Box p={20} top={0} style={{ zIndex: 2_000 }} w="300px" h="300px" pos="absolute">
              {target}
            </Box>
          )
        }
      </Group>
      <Stack w="100%" align="center" mb={20}>
        {!openedImageChanger && (
          <Group mb={20} align="center" justify="center">
            <UnstyledButton onClick={() => openFileLoad(active)}>
              <AvatarLeftImage
                src={localStore?.activeFile?.url}
                containerProps={{ w: 300 , h: 300}} />
            </UnstyledButton>
          </Group>
        )}

        {openedImageChanger && (
          <ImageChanger
            src={localStore?.activeFile?.url ?? ''}
            setterResult={onCrop} />
        )}

        {localStore?.activeFile?.url && (
          <Button variant={openedImageChanger ? 'filled' : 'default'} onClick={toggleImageChanger}>Изменить картинку</Button>
        )}
      </Stack>

      <Box w="400px" bg="dark.8" className={cn(c.roomProfilesCreateModal)}>
        <AvatarsList localStore={localStore?.avatarsListStore!} />
      </Box>
      <Box mt={20}>
        <form onSubmit={form.onSubmit((v) => onCreate(v.nickname))}>
          <Stack justify="center">

            <TextInput
              label="Никнейм" 
              placeholder="fill kurries"
              key={form.key('nickname')}
              { ...form.getInputProps('nickname') } />
            
            <Button loading={isLoading} variant="default" mt={20} type="submit" disabled={!canCreate}>Создать</Button>
          </Stack>
        </form>
      </Box>
    </Box>
  )
});

interface RoomProfilesCreateModalInnerProps extends Omit<RoomProfilesCreateModal, 'localStore'> {
  openFileLoad: () => void,
  openFileLoadRef: React.RefObject<() => void>;
  onDropFile: (f: FileWithPath[]) => void;
  onCrop: (blob: Blob, url: string|undefined) => void;
  form: UseFormReturnType<CreateRoomProfileForm>;
  canCreate: boolean;
  onCreate: (nickname: string) => void;
  isLoading: boolean;
}

const RoomProfilesCreateModalInner = observer(({ 
  opened, 
  close, 
  openFileLoad,
  openFileLoadRef,
  onDropFile,
  onCrop,
  form,
  canCreate,
  onCreate,
  isLoading }: RoomProfilesCreateModalInnerProps) => {
  const localStore = useLocalStore();

  const title = (
    <Text fz={24}>
      Создание профиля комнаты
    </Text>
  );

  function openFileLoadDecorate(active: () => void) {
    active();
    openFileLoad();
  }

  return (
    <Modal title={title} zIndex={3000} opened={opened} onClose={close}>
      <ScrollArea.Autosize mah={"76vh"} w={400}>
        <DropzoneLocal
        onDrop={onDropFile}
        isMultiple={false}
        openRef={openFileLoadRef}
        radius="50%"
        acceptMimeTypes={IMAGE_MIME_TYPE}
        wrapper={(target, active, inactive, isActive) => {

          return (
            <WrapperComponentObservable
              isLoading={isLoading}
              canCreate={canCreate}
              form={form}
              onCreate={onCreate}
              target={target}
              active={active}
              inactive={inactive}
              isActive={isActive}
              onCrop={onCrop}
              openFileLoad={openFileLoadDecorate}
              localStore={localStore!} />
          )
        }}>
          <Group justify="center" align="center" w="100%" h="100%">
            <DropzoneLocal.Idle>Внести картинку</DropzoneLocal.Idle>
            <DropzoneLocal.Accept>Успешно</DropzoneLocal.Accept>
            <DropzoneLocal.Reject>Не то</DropzoneLocal.Reject>
          </Group>
        </DropzoneLocal>
      </ScrollArea.Autosize>
    </Modal>
  )
});

export { RoomProfilesCreateModal }