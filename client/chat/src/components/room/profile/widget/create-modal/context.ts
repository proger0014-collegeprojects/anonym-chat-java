import { RoomProfilesCreateStore } from "$/components/room/profile/widget/create-modal";
import { createContext, useContext } from "react";

const Context = createContext<RoomProfilesCreateStore|undefined>(undefined);

function useLocalStore() {
  return useContext(Context);
}

export { Context, useLocalStore }