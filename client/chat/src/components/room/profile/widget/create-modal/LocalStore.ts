import { makeAutoObservable } from "mobx";
import { LocalStore as AvatarsListStore } from "$/components/room/profile/avatars/avatars-list";
import { createRoomProfile as apiCreateRoomProfile } from "$/shared/api/room";
import { createAvatar as createAvatarApi } from "$/shared/api/avatar";
import { IPromiseBasedObservable, fromPromise } from "mobx-utils";

interface LoadFileInfo {
  base64: string;
  file: File|Blob;
}

interface ActiveFileInfo {
  url: string;
  id?: number;
}


class LocalStore {
  avatarsListStore?: AvatarsListStore;
  loadFile?: LoadFileInfo;
  activeFile?: ActiveFileInfo;
  createRoomProfileStatus: IPromiseBasedObservable<any>;

  constructor() {
    makeAutoObservable(this);

    this.createRoomProfileStatus = fromPromise(Promise.resolve());
  }

  setAvatarsListStore(avatarsListStore: AvatarsListStore) {
    this.avatarsListStore = avatarsListStore;
  }

  createRoomProfile(nickname: string) {
    if (!this.activeFile) return;

    if (this.activeFile.id) {
      const response = apiCreateRoomProfile({
        avatarId: this.activeFile!.id,
        nickname: nickname
      });

      this.updateCreateRoomProfileStatus(response);
    } else if (this.activeFile.url && this.loadFile?.file) {
      const response = createAvatarApi({
        file: this.loadFile!.file,
        offsetX: 0,
        offsetY: 0,
        size: 100
      }).then(res => {
        return apiCreateRoomProfile({
          avatarId: res.id,
          nickname: nickname
        });
      });

      this.updateCreateRoomProfileStatus(response);
    }
  }

  get existsAvatarForRoomProfile(): boolean {
    return !!this.activeFile || !!this.loadFile;
  }

  updateCreateRoomProfileStatus(promise: Promise<any>) {
    this.createRoomProfileStatus = fromPromise(promise);
  }

  setActiveFile(fileInfo: ActiveFileInfo) {
    if (this.activeFile?.url == fileInfo.url) return;

    this.activeFile = fileInfo;
  }

  setLoadFile(fileInfo: LoadFileInfo) {
    if (this.loadFile?.base64 == fileInfo.base64) return;

    this.resetLoadFile();
    this.avatarsListStore?.resetSelectedItem();

    this.loadFile = fileInfo;
    this.setActiveFile({
      url:fileInfo.base64,
    });
  }

  resetLoadFile() {
    if (!this.loadFile) return;

    this.loadFile = undefined;
  }
}

export { LocalStore as RoomProfilesCreateStore }