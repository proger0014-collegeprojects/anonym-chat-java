import { ChildrenProps } from "$/shared/types";
import { clearText } from "$/shared/utils";
import { ActionIcon, CloseButton, Group, GroupProps, ScrollArea, Stack, StackProps, Textarea } from "@mantine/core";
import { useForm } from "@mantine/form";
import { getHotkeyHandler } from "@mantine/hooks";
import { IconSend } from "@tabler/icons-react";
import { useState } from "react";

interface MessageInputProps extends ChildrenProps {
  containerProps?: StackProps;
  viewportRef?: React.RefObject<HTMLDivElement>;
}

function MessageInput({ children, viewportRef, containerProps }: MessageInputProps) {
  return (
    <Stack ref={viewportRef} { ...containerProps }>
      {children}
    </Stack>
  )
}

interface MessageInputTop extends ChildrenProps {
  containerProps?: GroupProps;
  opened: boolean;
  onClose: () => void;
}

function MessageInputTop({ children, opened, onClose, containerProps }: MessageInputTop) {
  if (!opened) return null;

  return (
    <Group py={5} px={10} h="100%" w="100%" wrap="nowrap" align="center" justify="space-between" { ...containerProps }>
      {children}
      <CloseButton onClick={onClose} />
    </Group>
  )
}

interface MessageInputBottomProps {
  containerProps?: GroupProps;
  onClickSend: (resetForm: () => void, message: string) => void;
}

interface SendMessageForm {
  message: string;
}

function MessageInputBottom({ onClickSend, containerProps }: MessageInputBottomProps) {
  const [lengthMessage, setLengthMessage] = useState(0);

  const form = useForm<SendMessageForm>({
    initialValues: {
      message: ''
    },
    onValuesChange: (v) => {
      setLengthMessage(v.message.length);
    }
  });

  function resetForm() {
    form.reset();
  }

  function onClickSendDecorate(resetForm: () => void, message: string) {
    const clearedMessageText = clearText(message);

    onClickSend(resetForm, clearedMessageText);
  }

  return (
    <form onSubmit={form.onSubmit((values) => onClickSendDecorate(resetForm, values.message))}>
      <Group px={10} align="end" mah="250px" wrap="nowrap" { ...containerProps }>
        <ScrollArea.Autosize py={5} w="100%" mah={250}>
          <Textarea
            placeholder="Введите текст"
            w="100%"
            variant="unstyled"
            autosize
            key={form.key('message')}
            { ...form.getInputProps('message') }
            onKeyDown={getHotkeyHandler([
              ['Enter', form.onSubmit((values) => onClickSendDecorate(resetForm, values.message))],
              ['mod+Enter', () => form.setFieldValue('message', (v) => v + '\r')]
            ])} />
        </ScrollArea.Autosize>
        <ActionIcon
          mb={10}
          color="white"
          bg="none"
          disabled={lengthMessage < 1 || lengthMessage > 500}
          variant="subtle"
          type="submit">
          <IconSend />
        </ActionIcon>
      </Group>
    </form>
  )
}

MessageInput.Top = MessageInputTop;
MessageInput.Bottom = MessageInputBottom;

export { MessageInput }