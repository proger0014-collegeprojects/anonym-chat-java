import { ReactNode } from "react";
import cn from "classnames";
import c from "./ContentPlace.module.scss";
import { Box, MantineStyleProps } from "@mantine/core";

interface ContentPlaceProps {
  children: ReactNode,
  style: MantineStyleProps
}

const defaultStyle: MantineStyleProps = {
  px: '10px',
  py: '10px'
}

function ContentPlace({ children, style }: ContentPlaceProps) {

  return (
    <Box className={cn(c.contentPlace)} { ...defaultStyle } { ...style }>
      {children}
    </Box>
  );
}

export { ContentPlace };