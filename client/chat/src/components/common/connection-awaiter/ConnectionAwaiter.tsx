import { useStores } from "$/app/stores";
import { FilledContainer } from "$/components/common/filled-container";
import { ChildrenProps } from "$/shared/types";
import { Loader, Stack, Text } from "@mantine/core";
import { observer } from "mobx-react-lite";

interface ConnectionAwaiterProps extends ChildrenProps {

}

const ConnectionAwaiter = observer(({ children }: ConnectionAwaiterProps) => {
  const { messagingClientStore } = useStores();
  
  if (!messagingClientStore.isConnected) {
    return (
      <FilledContainer>
        <Stack w="100%" h="100%" justify="center" align="center">
          <Loader type="bars" />
          <Text fz={24} c="white">Подключение к серверу</Text>
        </Stack>
      </FilledContainer>
    )
  }

  return (
    <>
      {children}
    </>
  )
});


export { ConnectionAwaiter }