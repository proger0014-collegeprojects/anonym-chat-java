import { Box, CloseButton, Flex, Group, Image, Modal, UnstyledButton } from "@mantine/core";
import { useClickOutside, useDisclosure } from "@mantine/hooks";
import { ReactNode, useState } from "react";
import { cn } from "$/shared/styles";
import c from "./ImageWithPreview.module.scss";

interface ImageWithPreviewProps {
  url: string;
  children: (url: string) => ReactNode
}

interface ModalPreviewProps {
  opened: boolean;
  close: () => void;
  url: string;
}

function ModalPreview({ opened, close, url }: ModalPreviewProps) {
  const [modalHeader, setModalHeader] = useState<any>();
  const [image, setImage] = useState<any>();

  useClickOutside(close, null, [
    modalHeader,
    image
  ])

  return (
    <Modal className={cn(c.imageWithPreview)} pos="relative" zIndex={10_000} opened={opened} onClose={close} fullScreen withCloseButton={false}>
      <Group w="100%" justify="end">
        <Box ref={setModalHeader}>
          <CloseButton onClick={close} />
        </Box>
      </Group>
      <Flex w="100%" h="80vh" justify="center" align="end">
        <Box ref={setImage}>
          <Image src={url} h="75vh" />
        </Box>
      </Flex>
    </Modal>
  )
}

function ImageWithPreview({ url, children }: ImageWithPreviewProps) {
  const [opened, { open, close }] = useDisclosure(false);

  return (
    <>
      <ModalPreview opened={opened} close={close} url={url} />
      <ImageWithPreviewInner open={open} component={children(url)} />
    </>
  )
}

interface ImageWithPreviewInnerProps {
  component: ReactNode;
  open: () => void;
}

function ImageWithPreviewInner({ component, open }: ImageWithPreviewInnerProps) {
  return (
    <UnstyledButton onClick={open}>
      {component}
    </UnstyledButton>
  )
}

export { ImageWithPreview }