import { getImageSize } from "$/shared/utils";
import { Avatar, Box, BoxProps, Image } from "@mantine/core";
import { useState } from "react";

interface AvatarLeftImageProps {
  src?: string;
  containerProps?: BoxProps;
}

function AvatarLeftImage({ src, containerProps }: AvatarLeftImageProps) {
  const [size, setSize] = useState({
    width: 0,
    height: 0
  });

  getImageSize(src ?? '', setSize);

  const widthString = size.width > size.height
    ? 'auto'
    : '100%';
  
  const heightString = size.height > size.width
    ? 'auto'
    : '100%';

  return (
    <Box pos="relative" style={{ overflow: 'hidden', borderRadius: '50%' }} { ...containerProps }>
      {src
        ? <Image fit="cover" pos="absolute" w={widthString} h={heightString} src={src} />
        : <Avatar bg="black" size={"100%"} />
      }
    </Box>
  )
}

export { AvatarLeftImage }