import { ChildrenProps } from "$/shared/types";
import { Box, Modal } from "@mantine/core";
import { useWindowEvent } from "@mantine/hooks";
import { ReactNode, useState } from "react";

interface ModalProps extends ChildrenProps {
  opened: boolean;
  onClose: () => void;
  title: ReactNode;
}

function MenuModal({ children, opened, onClose, title }: ModalProps) {
  const [height, setHeight] = useState(window.innerHeight);

  useWindowEvent('resize', () => {
    setHeight(window.innerHeight);
  });

  const heightBox = height - 200;

  return (
    <Modal keepMounted={false} zIndex={1000} size="40rem" opened={opened} onClose={onClose} title={title}>
      <Box h={heightBox}>
        {children}
      </Box>
    </Modal>
  )
}

export { MenuModal }