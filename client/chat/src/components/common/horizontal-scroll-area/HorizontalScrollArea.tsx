import { ChildrenProps } from "$/shared/types";
import { Box, BoxProps, Group, ScrollArea } from "@mantine/core";
import { useEventListener, useMergedRef } from "@mantine/hooks";
import React, { useEffect, useRef, useState } from "react";

interface HorizontalScrollAreaProps extends ChildrenProps {
  onScrollHandler?: (x: number, scrollWidth: number) => void;
  scrollbarSize?: number;
  innerProps?: BoxProps;
}

function HorizontalScrollArea({ children, onScrollHandler, scrollbarSize, innerProps }: HorizontalScrollAreaProps) {
  const [offsetX, setOffsetX] = useState(0);

  const viewportWrapperRef = useRef<HTMLDivElement>(null);
  const viewportRef = useRef<HTMLDivElement>(null);

  function getAvailableScrollWidth() {
    const viewportScrollWidth = viewportRef.current?.scrollWidth ?? 0;
    const viewportWrapperScrollWidth = viewportWrapperRef.current?.scrollWidth ?? 0;
    return viewportScrollWidth - viewportWrapperScrollWidth;
  }

  const mwheelEventListenerRef = useEventListener('wheel', (event: WheelEvent) => {
    setOffsetX((v) => {
      if (event.deltaY < 0 && v <= 0) return 0;

      const itemAvailableScrollWidth = getAvailableScrollWidth();

      if (event.deltaY > 0 && offsetX >= itemAvailableScrollWidth) {
        return v;
      }

      return v + event.deltaY;
    })
  });

  // @ts-ignore
  const wrapperRef: React.RefObject<any> = useMergedRef(viewportWrapperRef, mwheelEventListenerRef);

  function scrollPositionChangeHandler({ x }: { x: number, y: number }) {
    setOffsetX(x);

    if (onScrollHandler) {
      const scrollWidth = getAvailableScrollWidth();
      
      onScrollHandler(x, scrollWidth);
    }
  }

  function scrollToX(x: number) {
    viewportRef.current?.scrollTo({ left: x, behavior: 'smooth' });
  }

  useEffect(() => {
    scrollToX(offsetX);
  }, [offsetX]);

  return (
    <HorizontalScrollAreaInner 
      viewportRef={viewportRef}
      scrollPositionChangeHandler={scrollPositionChangeHandler}
      scrollbarSize={scrollbarSize}
      wrapperRef={wrapperRef}
      innerProps={innerProps}>
        {children}
    </HorizontalScrollAreaInner>
  )
}

interface HorizontalScrollAreaInnerProps extends ChildrenProps {
  viewportRef: React.RefObject<HTMLDivElement>;
  scrollPositionChangeHandler: ({ x }: { x: number, y: number }) => void;
  scrollbarSize?: number;
  wrapperRef: React.RefObject<any>;
  innerProps?: BoxProps;
}

function HorizontalScrollAreaInner({ 
  viewportRef, 
  scrollPositionChangeHandler, 
  children,
  scrollbarSize,
  wrapperRef,
  innerProps }: HorizontalScrollAreaInnerProps) {

  return (
    <Box h="100%" w="100%" ref={wrapperRef}>
      <ScrollArea
        onScrollPositionChange={scrollPositionChangeHandler} 
        viewportRef={viewportRef}
        w="100%" 
        h="100%" 
        scrollbars="x" 
        scrollbarSize={scrollbarSize}>
        <Group { ...innerProps } wrap="nowrap" h="100%" w="100%">
          {children}
        </Group>
      </ScrollArea>
    </Box>
  )
}

export { HorizontalScrollArea }