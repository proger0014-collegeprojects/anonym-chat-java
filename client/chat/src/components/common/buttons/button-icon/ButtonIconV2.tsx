import { ChildrenProps } from "$/shared/types";
import { Button, ButtonProps as MButtonProps, MantineStyleProp } from "@mantine/core";
import { ReactNode } from "react";

interface ButtonProps extends ChildrenProps {
  icon?: ReactNode,
  onClick?: () => void;
  height?: number | string;
  buttonProps?: MButtonProps
  style?: MantineStyleProp,
}

function ButtonIconV2({ icon, onClick, children, height, style, buttonProps }: ButtonProps) {
  return (
    <Button
    color="white"
    onClick={onClick}
    h={height}
    fullWidth
    justify="flex-start"
    variant="subtle"
    leftSection={icon}
    { ...buttonProps }
    style={style}>
      {children}
    </Button>
  )
}

export { ButtonIconV2 }