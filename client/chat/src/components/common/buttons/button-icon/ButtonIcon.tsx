import { ActionIcon } from "@mantine/core"
import { ReactNode, forwardRef } from "react"

interface ButtonProps {
  icon: ReactNode,
  onClick?: () => void;
  innerRef?: any;
}

function ButtonIcon({ icon, onClick, innerRef  }: ButtonProps) {
  return (
    <ActionIcon
      ref={innerRef}
      w="100%" 
      h="100%" 
      color="none" 
      variant="transparent"
      onClick={onClick}>
      {icon}
    </ActionIcon>
  )
}

const ForwardRefBtn = forwardRef<HTMLDivElement, ButtonProps>((props, ref) => (
  <ButtonIcon innerRef={ref} {...props} />
))

export { ForwardRefBtn as ButtonIcon }