import { Button, ButtonProps, MantineStyleProp, Stack, Text, TextProps } from "@mantine/core";
import { ReactNode } from "react";

interface ButtonIconV3Props {
  buttonProps?: ButtonProps;
  style?: MantineStyleProp;
  icon?: ReactNode;
  textProps?: TextProps;
  children: string;
  isActive?: boolean;
  onClick?: () => void;
}

function ButtonIconV3({ children, isActive, icon, onClick, buttonProps, style, textProps }: ButtonIconV3Props) {
  const isActiveComputed = isActive ?? false;

  let buttonBg;

  if (isActiveComputed) {
    buttonBg = 'dark.5';
  }

  return (
    <Button onClick={onClick} bg={buttonBg} m={0} p={0} py={2} px={2} color="white" variant="subtle" h="auto" {...buttonProps} style={style}>
      <Stack m={0} p={0} align="center" w="100%">
        {icon}
        <Text fz={10} { ...textProps }>{children}</Text>
      </Stack>
    </Button>
  )
}

export { ButtonIconV3 }