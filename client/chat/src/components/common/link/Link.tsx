import { Button, MantineStyleProps } from "@mantine/core";
import { cn } from "$/shared/styles";
import c from "./Link.module.scss";
import { ChildrenProps } from "$/shared/types";
import { useNavigate } from "react-router-dom";

const defaultStyle: MantineStyleProps = {
  m: 0,
  p: 0
}

interface LinkProps extends ChildrenProps {
  href?: string;
  onClick?: () => void;
  style?: MantineStyleProps
}

function Link({ children, onClick, href, style }: LinkProps) {
  const navigate = useNavigate();

  const handleClick = onClick
    ? onClick
    : href
    ? () => { navigate(href); }
    : () => {}; 

  return (
    <Button {...defaultStyle} { ...style }
      variant="transparent" 
      className={cn(c.link)}
      onClick={handleClick}>
      {children}
    </Button>
  )
}

export { Link }