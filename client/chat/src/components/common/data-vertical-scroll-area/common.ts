import { ReactNode } from "react";

interface CanScrollAction {
  up: boolean;
  down: boolean;
}

interface Item {
  key: number|string;
  component: ReactNode;
  onVisible?: () => void;
}

export type { CanScrollAction, Item }