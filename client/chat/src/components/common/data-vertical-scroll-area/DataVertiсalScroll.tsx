import { Box, Group, Loader, ScrollArea, ScrollAreaProps } from "@mantine/core";
import { useIntersection, useMergedRef } from "@mantine/hooks";
import React, { ReactNode, useEffect, useRef, useState } from "react";
import { CanScrollAction, Item } from "$/components/common/data-vertical-scroll-area/common";


interface DateItemProps {
  item: Item;
  scrollAreaRef: React.RefObject<any>;
}

function DataItem({item, scrollAreaRef}: DateItemProps) {
  const { ref, entry } = useIntersection({
    root: scrollAreaRef.current,
    threshold: 1
  });

  const refItem = useRef<HTMLDivElement>(null);

  // @ts-ignore
  const mergedRefItem = useMergedRef(ref, refItem) as React.RefObject<any>;

  useEffect(() => {
    if (entry?.isIntersecting != undefined && entry.isIntersecting) {
      item.onVisible && item.onVisible();
    }
  }, [entry?.isIntersecting]);

  return (
    <Box ref={mergedRefItem} key={item.key}>
      {item.component}
    </Box>
  )
}

interface DataVertialScrollProps {
  viewportRef?: React.RefObject<HTMLDivElement>;
  onScrolledUp: (resetScrolled: () => void) => void;
  onScrolledDown: (resetScrolled: () => void) => void;
  items: Item[];
  canScrollAction: CanScrollAction;
  containerProps?: ScrollAreaProps;
  onScroll?: (scrollHeight: number, scroll: number) => void;
}



function DataVertiсalScroll({ items, onScrolledUp, onScrolledDown, canScrollAction, viewportRef, containerProps, onScroll }: DataVertialScrollProps) {
  const [isScrolled, setScrolled] = useState(false);
  const [lastMaxHeight, setLastMaxHeight] = useState(0);
  const [loadingState, setLoadingState] = useState<{
    isLoading: boolean;
    position?: 'top'|'bottom';
  }>({
    isLoading: false,
  });

  function setLoadingStateOnTop() {
    setLoadingState({
      isLoading: true,
      position: 'top'
    });
  }

  function setLoadingStateOnBottom() {
    setLoadingState({
      isLoading: true,
      position: 'bottom'
    });
  }

  function resetScrolled() {
    setScrolled(false);
    setLoadingState({
      isLoading: false
    })
  }

  const scrollAreaRef = useRef<HTMLDivElement>(null);

  // @ts-ignore
  const mergedRef = useMergedRef(scrollAreaRef, viewportRef) as React.RefObject<any>;

  useEffect(() => {
    if (!scrollAreaRef.current) return;

    const maxHeight = scrollAreaRef.current.scrollHeight - scrollAreaRef.current.clientHeight;

    if (scrollAreaRef.current.scrollTop >= maxHeight - 200) {
      scrollAreaRef?.current.scrollTo({ top: scrollAreaRef.current.scrollHeight, behavior: 'smooth' });
    } else if (scrollAreaRef.current.scrollTop <= 100 && lastMaxHeight < maxHeight) {
      const lastPos = maxHeight - lastMaxHeight;

      scrollAreaRef?.current.scrollTo({ top: lastPos, behavior: 'smooth' });
    }
  }, [items.length, scrollAreaRef.current?.scrollHeight]);

  function onWheel(e: React.WheelEvent<HTMLDivElement>) {
    if (!scrollAreaRef?.current || isScrolled) return;

    const maxHeight = scrollAreaRef.current.scrollHeight - scrollAreaRef.current.clientHeight;

    onScroll && onScroll(maxHeight, scrollAreaRef.current.scrollTop);

    if (e.deltaY < 0 && canScrollAction.up) {
      if (scrollAreaRef.current?.scrollTop <= 50) {
        setScrolled(true);
        setLoadingStateOnTop();
        onScrolledUp(resetScrolled);
        setLastMaxHeight(scrollAreaRef.current.scrollHeight - scrollAreaRef.current.clientHeight)
      }
    } else if (e.deltaY > 0 && canScrollAction.down) {
      if (scrollAreaRef.current?.scrollTop >= maxHeight - 50) {
        setScrolled(true);
        setLoadingStateOnBottom();
        onScrolledDown(resetScrolled);
      }
    }
  }

  const components = items.map(c => (
    <DataItem key={c.key} item={c} scrollAreaRef={scrollAreaRef} />
  ))

  return (
    <DataVerticalScrollInner containerProps={containerProps} scrollAreaRef={mergedRef} onWheel={onWheel}>
      {loadingState.isLoading && loadingState.position && loadingState.position == 'top' && (
        <Group w="100%" justify="center" py={10}>
          <Loader />
        </Group>
      )}
      {components}
      {loadingState.isLoading && loadingState.position && loadingState.position == 'bottom' && (
        <Group w="100%" justify="center" py={10}>
          <Loader />
        </Group>
      )}
    </DataVerticalScrollInner>
  )
}

interface DataVerticalScrollInnerProps {
  scrollAreaRef: React.RefObject<HTMLDivElement>;
  onWheel: (e: React.WheelEvent<HTMLDivElement>) => void;
  children: ReactNode[];
  containerProps?: ScrollAreaProps;
}

function DataVerticalScrollInner({ scrollAreaRef, children, onWheel, containerProps }: DataVerticalScrollInnerProps) {
  
  return (
    <ScrollArea viewportRef={scrollAreaRef} onWheel={onWheel} { ...containerProps }>
      {children}
    </ScrollArea>
  )
}

export { DataVertiсalScroll }