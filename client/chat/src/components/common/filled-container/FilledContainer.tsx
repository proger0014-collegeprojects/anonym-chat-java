import { ChildrenProps } from "$/shared/types";
import { Box } from "@mantine/core";

function FilledContainer({ children }: ChildrenProps) {
  return (
    <Box h="100vh" w="100%">
      {children}
    </Box>
  )
}

export { FilledContainer }