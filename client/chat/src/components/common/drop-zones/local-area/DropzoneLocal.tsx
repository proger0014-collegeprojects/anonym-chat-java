import { ChildrenProps } from "$/shared/types";
import { Dropzone, FileRejection, FileWithPath } from "@mantine/dropzone";
import React, { ReactNode, useState } from "react";

interface DropzoneLocalProps extends ChildrenProps {
  acceptMimeTypes: string[];
  onDrop?: (files: FileWithPath[]) => void;
  onReject?: (rejections: FileRejection[]) => void;
  openRef?: React.RefObject<() => void>;
  invisibleIfInactive?: boolean;
  radius?: string|number;
  wrapper: (target: ReactNode, active: () => void, inactive: () => void, isActive: boolean) => ReactNode;
  isMultiple: boolean;
}

function DropzoneLocal({ 
  acceptMimeTypes,
  onDrop,
  onReject,
  children,
  openRef,
  radius,
  wrapper,
  isMultiple }: DropzoneLocalProps) {
    const [isActive, setActive] = useState(false);

    function handleInactive() {
      if (isActive) {
        setActive(false);
      }
    }

    function onDropDecorate(files: FileWithPath[]) {
      handleInactive();

      onDrop && onDrop(files);
    }

    function handleActive() {
      if (!isActive) {
        setActive(true);
      }
    }

    return (
      <DropzoneLocalInner
        isActive={isActive}
        acceptMimeTypes={acceptMimeTypes}
        onDrop={onDropDecorate}
        onReject={onReject}
        children={children}
        openRef={openRef}
        handleActive={handleActive}
        handleInactive={handleInactive}
        radius={radius}
        wrapper={wrapper}
        isMultiple={isMultiple} />
    )
}

interface DropzoneLocalInnerProps extends ChildrenProps {
  acceptMimeTypes: string[];
  onDrop?: (files: FileWithPath[]) => void;
  onReject?: (rejections: FileRejection[]) => void;
  isActive: boolean;
  openRef?: React.RefObject<() => void>;
  handleActive: () => void;
  handleInactive: () => void;
  radius?: string|number;
  wrapper: (target: ReactNode, active: () => void, inactive: () => void, isActive: boolean) => ReactNode;
  isMultiple: boolean;
}

function DropzoneLocalInner({ 
  acceptMimeTypes,
  onDrop,
  onReject,
  children,
  openRef,
  isActive,
  handleActive,
  handleInactive,
  radius,
  wrapper,
  isMultiple }: DropzoneLocalInnerProps) {

  const component = (
    <Dropzone
      multiple={isMultiple}
      radius={radius}
      w="100%" 
      h="100%" 
      openRef={openRef}
      onDrop={onDrop!} 
      onReject={onReject} 
      accept={acceptMimeTypes}>
      {children}
    </Dropzone>
  );

  return (
    <>
      {wrapper(component, handleActive, handleInactive, isActive)}
    </>
  )
}

DropzoneLocal.Accept = Dropzone.Accept;
DropzoneLocal.Reject = Dropzone.Reject;
DropzoneLocal.Idle = Dropzone.Idle;

export { DropzoneLocal }