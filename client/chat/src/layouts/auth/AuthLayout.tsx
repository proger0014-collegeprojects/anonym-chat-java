import { ChildrenProps } from "$/shared/types";
import { Center, Container, Flex } from "@mantine/core";
import { cn, textScss } from "$/shared/styles";
import { ContentPlace } from "$/components/common/content-place";
import c from "./AuthLayout.module.scss";


interface HeaderProps {
  children: string;
}

function Header({ children }: HeaderProps) {
  return (
    <Center>
      <h2 className={cn(textScss.reset, textScss.fz42)}>
        {children}
      </h2>
    </Center>
  )
}

function Body({ children }: ChildrenProps) {
  return (
    <div className={cn(c.body)}>
      <Flex justify={ "center" }>
        <ContentPlace style={{
          w: '490px',
          h: '550px',
        }}>
          {children}
        </ContentPlace>
      </Flex>
    </div>
  )
}


function AuthLayout({ children }: ChildrenProps) {
  return (
    <Container>
      {children}
    </Container>
  )
}

AuthLayout.Header = Header;
AuthLayout.Body = Body;

export { AuthLayout };