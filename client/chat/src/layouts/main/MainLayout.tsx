import { FilledContainer } from "$/components/common/filled-container";
import { ChildrenProps } from "$/shared/types";
import { Box, Flex } from "@mantine/core";
import { ReactNode } from "react";

function Left1({ children }: ChildrenProps) {
  return (
    <Box h="100%" w={90} bg="gray.9">
      {children}
    </Box>
  )
}

function Left2({ children }: ChildrenProps) {
  return (
    <Box h="100%" w={340} bg="gray.8">
      {children}
    </Box>
  )
}

function LeftBody({ children }: ChildrenProps) {
  return (
    <Flex>
      {children}
    </Flex>
  )
}

function ItemHeader({ children }: ChildrenProps) {
  return (
    <Box h={54} bg="dark.7">
      {children}
    </Box>
  )
}

function Left({ children }: ChildrenProps) {
  return (
    <Flex>
      {children}
    </Flex>
  )
}

interface RightProps {
  children?: ReactNode;
}

function Right({ children }: RightProps) {
  return (
    <Box h="100%" bg="gray.9" w="100%">
      {children}
    </Box>
  )
}

function RightBody({ children }: ChildrenProps) {
  return (
    <Box>
      {children}
    </Box>
  )
}

function MainLayout({ children }: ChildrenProps) {
  return (
    <FilledContainer>
      <Flex h="100%" w="100%">
        {children}
      </Flex>
    </FilledContainer>
  )
}

Left.Header = ItemHeader;
Left.Body = LeftBody;

Left.Left1 = Left1;
Left.Left2 = Left2;

Right.Header = ItemHeader;
Right.Body = RightBody;

MainLayout.Left = Left;
MainLayout.Right = Right;

export { MainLayout }