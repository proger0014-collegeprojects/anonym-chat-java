import { BACKEND_SERVER_IS_SECURE, BACKEND_SERVER, IS_DEV, IS_WEBSOCKET_DISABLED } from "$/shared/constants";
import { getAccessToken } from "$/shared/utils";
import { Client, IStompSocket, StompHeaders, Versions } from "@stomp/stompjs";
import { getTokensStore } from "$/app/stores";
import SockJS from "sockjs-client";

const tokensStore = getTokensStore();

function getConnectionUrl(protocol: "ws" | "http", endpoint: "ws" | "sock") {
  return `${protocol + (BACKEND_SERVER_IS_SECURE ? "s" : "")}://${BACKEND_SERVER}/${endpoint}`;
}

function getCommonHeaders(): StompHeaders {
  const object: StompHeaders = { };

  const accessToken = getAccessToken();

  if (accessToken != null) {
    object.accessToken = accessToken;
  }

  return object;
}

function refreshToken() {
  tokensStore.refreshTokens();
  tokensStore.accessToken.then(_ => {
    client.connectHeaders = getCommonHeaders();
  })
}

const RECONNECT_DELAY = 100;

const client = new Client({
  brokerURL: getConnectionUrl('ws', 'ws'),
  logRawCommunication: IS_DEV,
  stompVersions: new Versions([Versions.V1_0, Versions.V1_1]),
  reconnectDelay: RECONNECT_DELAY
});

client.onStompError = (_) => {
  refreshToken();
}

function getWebSocketFactory() {
  return () => {
    return new SockJS(getConnectionUrl('http', 'sock')) as IStompSocket;
  }
}

if (IS_WEBSOCKET_DISABLED) {
  client.webSocketFactory = getWebSocketFactory();
}

client.onWebSocketError = () => {
  client.webSocketFactory = getWebSocketFactory();
}

if (IS_DEV) {
  client.debug = (msg) => {
    console.log("=== MESSAGING ===");
    
    console.log(msg);

    console.log("== END MESSAGING ===");
  }
}

export default client;