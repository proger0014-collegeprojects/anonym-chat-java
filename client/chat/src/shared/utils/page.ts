import { Section } from "$/shared/api/contracts";
import { SECTION_COUNT } from "$/shared/constants";
import { PendingPageInfo } from "$/shared/types";

function getPendingPage<TSection>(newPage: number, lastPage: Section<TSection>): PendingPageInfo|undefined {
  if (lastPage) {
    return {
      currentPage: newPage > lastPage.lastSection
        ? lastPage.lastSection
        : newPage,
      totalPages: lastPage.lastSection
    }
  }

  return undefined;
}

function predicateToNewSection(currentMessageCount: number) {
  if (currentMessageCount > SECTION_COUNT 
      && ((currentMessageCount - SECTION_COUNT) % SECTION_COUNT) == 0) return true;

  return false;
}

export { getPendingPage, predicateToNewSection }