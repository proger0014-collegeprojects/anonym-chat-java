function getFileUrl(file: File|Blob, setterResult: (result: string|ArrayBuffer|null|undefined) => void) {
  const fileReader = new FileReader();
  fileReader.readAsDataURL(file);
  fileReader.onload = (res) => setterResult(res.target?.result);
}

export { getFileUrl }