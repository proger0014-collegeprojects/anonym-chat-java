interface GetImageSizeResult {
  width: number;
  height: number;
}

function getImageSize(src: string, setterResult: (result: GetImageSizeResult) => void) {
  const image = new Image();
  
  image.onload = (_) => {
    setterResult({
      width: image.width,
      height: image.height
    });
  }

  image.src = src;
}

export { getImageSize }