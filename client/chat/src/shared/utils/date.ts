function getSeconds(target: Date): number {
  return target.getTime() / 1000;
}

function getMinutes(target: Date): number {
  return getSeconds(target) / 60;
}

function minutesAgo(from: Date, to: Date): number {
  return getMinutes(to) - getMinutes(from);
}

function minutesAgoFromNow(target: Date): number {
  return minutesAgo(target, new Date(Date.now()));
}

function getUnixTime(target: Date): number {
  return Math.floor(target.getTime() / 1000);
}

function getDateNowInUnixTime(): number {
  return getUnixTime(new Date(Date.now()));
}

function getLocalDateFromString(date: string): Date {
  const utcDate = new Date(date);
  const localeDateString = utcDate.toLocaleString();

  return new Date(localeDateString);
}

function getLocalDateFromUnixTime(time: number): Date {
  const utcDate = new Date(time * 1000);
  const utcDateString = utcDate.toUTCString();

  return getLocalDateFromString(utcDateString);
}

export {
  getSeconds,
  getMinutes,
  minutesAgo,
  minutesAgoFromNow,
  getUnixTime,
  getDateNowInUnixTime,
  getLocalDateFromString,
  getLocalDateFromUnixTime
}