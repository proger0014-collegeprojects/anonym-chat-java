function clearText(text: string): string {
  return text.trim().split(/ +/g).join(' ').split(/[\n]+/g).join('\n');
}

export { clearText }