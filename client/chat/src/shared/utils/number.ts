function numberOrZero(number: number|undefined|null) {
  return number ?? 0;
}

export { numberOrZero }