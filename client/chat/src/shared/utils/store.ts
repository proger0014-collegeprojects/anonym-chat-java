import { getTokensStore } from "$/app/stores";
import { FULFILLED, IPromiseBasedObservable, fromPromise } from "mobx-utils";

type Func<TInput, TResult> = (input: TInput) => TResult;

interface FullProperty<TValue, TSerializedValue = any> {
  key: string,
  serialize: Func<TValue, TSerializedValue>; 
  deserialize: Func<TSerializedValue, TValue>;
}

function getObservablePromiseFullProperty<T, S extends T = any>(key: string): FullProperty<IPromiseBasedObservable<T>, S|null> {
  return {
    key,
    serialize: (v) => {
      switch (v.state) {
        case FULFILLED:
          return v.value as S;
      }

      return null
    },
    deserialize: (v) => {
      if (!v) {
        return fromPromise(Promise.reject());
      }

      return fromPromise(Promise.resolve(v));
    } 
  };
}

function getFullProperty<T, S extends T = any>({
    key, 
    serialize = (v) => {
      return v as S;
    },
    deserialize = (v) => {
      return v
    }
  }: FullProperty<T|null, S|null>): FullProperty<T|null, S|null> {
  return {
    key: key,
    serialize: serialize,
    deserialize: deserialize
  }
}

function getAccessToken(): string|null {
  const tokensStore = getTokensStore();

  if (tokensStore.accessToken.state != FULFILLED) {
    return null;
  }

  return tokensStore.accessToken.value;
}

export { getObservablePromiseFullProperty, getFullProperty, getAccessToken }