function validateMinLength(value: string, minLength: number): string|null {
  return value.length < minLength 
    ? `Значение должно быть не меньше ${minLength} символов`
    : null;
}

export { validateMinLength }