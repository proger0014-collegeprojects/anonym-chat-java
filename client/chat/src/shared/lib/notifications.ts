import { notifications } from "@mantine/notifications";
import { ReactNode } from "react";

interface GenericNotifyProps {
  title?: ReactNode,
  message: ReactNode
}

function notifyError({ title, message }: GenericNotifyProps) {
  notifications.show({ title, message, color: "red" })
}

function notifySuccess({ title, message }: GenericNotifyProps) {
  notifications.show({ title, message, color: "green" });
}

function notifyDanger({ title, message }: GenericNotifyProps) {
  notifications.show({ title, message, color: "yellow" });
}

export { notifyError, notifyDanger, notifySuccess }