import c from "./text.module.scss";

interface StyleProps extends CSSModuleClasses {
  readonly reset: string;

  readonly fz24: string;
  readonly fz42: string;
}

const style: StyleProps = c as StyleProps;

export { style as textScss }

