import { AuthStore, useStores } from "$/app/stores";
import { useEffect, useState } from "react";

interface HookResult {
  isLoading: boolean;
  handleAnonymLoginCallback: () => void;
}

function handleLogin(login: string, authStore: AuthStore) {
  authStore.login(login, null);
}

function handleRegister(authStore: AuthStore, loading: () => void) {
  loading();

  authStore.register(null, null);
}

function useAnonymLogin(): HookResult {
  const [isLoading, setIsLoading] = useState(false);

  const { authStore } = useStores();

  function loading() {
    setIsLoading(true);
  }

  useEffect(() => {
    authStore.registerResponse?.case({
      fulfilled: (r) => {
        handleLogin(r.login, authStore);
      }
    })
  }, [authStore.registerResponse?.state])

  const handleAnonymLoginCallback = () => {
    handleRegister(authStore, loading)
  };

  return {
    isLoading: isLoading,
    handleAnonymLoginCallback: handleAnonymLoginCallback,
  };
}

export { useAnonymLogin }