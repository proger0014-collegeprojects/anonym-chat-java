enum Role {
  USER = 2,
  ADMIN = 0
}

interface User {
  id: number;
  login?: string;
  role: Role;
  authType: 'anonym' | 'auth';
}

export type { Role, User }