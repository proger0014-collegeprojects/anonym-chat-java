interface Callbacks {
  onSuccess?: () => void;
  onError?: () => void;
}

interface CallbacksWithInput<TSuccess = any, TError = any> {
  onSuccess?: (data?: TSuccess) => void;
  onError?: (error?: TError) => void;
}

export type { Callbacks, CallbacksWithInput }