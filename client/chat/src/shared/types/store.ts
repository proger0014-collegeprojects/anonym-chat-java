import { IPromiseBasedObservable } from "mobx-utils";

export type ConnectionStatus = 'connection'|'connected'|'already-connected'|'not-selected-room-profile'|'internal-server-error'|'disconnected'|'processing';

export interface ConnectionToRoomInfo {
  connectionResponse?: IPromiseBasedObservable<void>;
  status: ConnectionStatus;
}