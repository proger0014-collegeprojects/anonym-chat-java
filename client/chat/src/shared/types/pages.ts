interface PendingPageInfo {
  totalPages: number;
  currentPage: number;
}

export type { PendingPageInfo }