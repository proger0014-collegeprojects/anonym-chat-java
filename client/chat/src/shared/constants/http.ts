export const STATUS_INTERNAL_SERVER_ERROR = 500;
export const STATUS_FORBIDDEN = 403;
export const STATUS_UNAUTHORIZED = 401;

export const HEADER_WWW_AUTHENTICATE = "WWW-Authenticate";
