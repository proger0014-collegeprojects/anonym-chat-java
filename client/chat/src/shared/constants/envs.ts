export const ENV = import.meta.env.MODE;
export const BACKEND_SERVER_HOST = import.meta.env.VITE_BACKEND_SERVER_HOST;
export const BACKEND_SERVER_PORT = new Number(import.meta.env.VITE_BACKEND_SERVER_PORT) as number;
export const BACKEND_SERVER_IS_SECURE = import.meta.env.VITE_BACKEND_SERVER_IS_SECURE == 'true';
export const AUTH_TYPE = import.meta.env.VITE_AUTH_TYPE;
export const MODE = import.meta.env.MODE;
export const ACCESS_TOKEN_EXPIRATION = new Number(import.meta.env.VITE_ACCESS_TOKEN_EXPIRATION) as number;
export const SECTION_COUNT = new Number(import.meta.env.VITE_SECTION_COUNT) as number;
export const WEBSOCKET_DISABLED = import.meta.env.VITE_WEBSOCKET_DISABLED == 'true';