import { RoomProfileFull } from "$/shared/api/contracts/room";

interface RoomPrivateMessageInfo {
  destinationUserId: number;
}

interface MessagePreview {
  userId: number;
  messageId: number;
  shortBody: string;
}

interface MessagePreviewV2 {
  userId: number;
  messageId: number;
  shortBody: string;
  roomProfile: RoomProfileFull;
}

interface RoomMessage {
  id: number;
  userId: number;
  reply: MessagePreview;
  asPrivateMessageInfo: RoomPrivateMessageInfo;
  body: string;
  createdAt: Date;
}



interface RoomMessageV2 extends Omit<RoomMessage, 'reply'> {
  roomProfile: RoomProfileFull;
  reply: MessagePreviewV2;
}

interface CreateNewMessageRequest {
  body: string;
}

interface CreateNewMessageResponse {
  id: number;
  createdAt: Date;
}

export type { RoomPrivateMessageInfo, MessagePreview, RoomMessage, CreateNewMessageRequest, CreateNewMessageResponse, RoomMessageV2 }