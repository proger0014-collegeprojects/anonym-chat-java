interface TokensResponse {
  accessToken: string;
}

interface LoginRequest {
  login: string;
  password: string|null;
}

interface RegisterRequest {
  role: number;
  login: string|null;
  password: string|null;
}

interface RegisterResponse {
  id: number;
  role: number;
  login: string;
}

interface UpdateAuthUserRequest {
  login: string;
  password: string;
}

interface loginResponse extends TokensResponse {  }

export type { TokensResponse, LoginRequest, loginResponse, RegisterRequest, RegisterResponse, UpdateAuthUserRequest }