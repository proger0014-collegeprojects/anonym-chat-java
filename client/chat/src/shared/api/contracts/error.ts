interface Error {
  type: string;
  title: string;
  status: number;
  description: number;
}

interface ListError {
  type: string;
  title: string;
  status: number;
  errors: Map<string, Array<string>>
}

type GenericError = Error | ListError;

export type { Error, ListError, GenericError }