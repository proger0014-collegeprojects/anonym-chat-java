import { Role } from "$/shared/types";

interface Section<TItem> {
  section: number;
  lastSection: number;
  perSection: number;
  total: number;
  items: TItem[];
}

interface User {
  id: number;
  role: number;
  login: Role;
  createdAt: Date;
}

export type { Section, User }