interface CreateAvatarRequest {
  offsetX: number;
  offsetY: number;
  size: number;
  file: File|Blob;
}

interface Avatar {
  id: number;
  size: number;
  url: string;
}

export type { CreateAvatarRequest, Avatar }