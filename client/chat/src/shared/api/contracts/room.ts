import { Avatar, User } from "$/shared/api/contracts";

interface Room {
  id: number;
  userId: number;
  countConnectedUsers: number;
  name: string;
  description?: string;
}

interface CreateRoomRequest {
  name: string;
  description?: string;
}

interface RoomProfileFull {
  id: number;
  nickname: string;
  avatar: Avatar;
}

interface CreateNewRoomProfileRequest {
  avatarId: number;
  nickname: string;
}

interface SetRoomProfileRequest {
  roomId: number;
}

interface RoomProfileWithUserFull {
  roomProfile: RoomProfileFull;
  user: User;
}

interface InstallRoomProfile {
  userId: number;
  roomProfile: RoomProfileFull;
}

export type { Room, RoomProfileFull, SetRoomProfileRequest, RoomProfileWithUserFull, InstallRoomProfile, CreateNewRoomProfileRequest, CreateRoomRequest }