import { apiClient } from "$/shared/api";
import { CreateNewMessageRequest, CreateNewMessageResponse, RoomMessage, RoomMessageV2, Section } from "$/shared/api/contracts";

const BASE_URL = '/rooms/{}/messages';

async function getMessagesSection(roomId: number, section: number): Promise<Section<RoomMessageV2>> {
  const url = BASE_URL.replace('{}', roomId.toString());

  return apiClient.get(url, {
    params: {
      section: section
    }
  }).then(res => res.data);
}

async function createRoomMessage(roomId: number, request: CreateNewMessageRequest): Promise<CreateNewMessageResponse> {
  const url = BASE_URL.replace('{}', roomId.toString());

  return apiClient.post(url, request).then(res => res.data);
}

async function replyRoomMessage(roomId: number, messageId: number, request: CreateNewMessageRequest): Promise<RoomMessage> {
  const url = `${BASE_URL.replace('{}', roomId.toString())}/${messageId}/replies`;

  return apiClient.post(url, request).then(res => res.data);
}

export { getMessagesSection, createRoomMessage, replyRoomMessage }