import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { baseUrl } from "$/shared/api/constants";
import { getTokensStore } from "$/app/stores";
import { GenericError } from "$/shared/api/contracts";
import { notifyError } from "$/shared/lib";
import { 
  STATUS_FORBIDDEN, 
  STATUS_UNAUTHORIZED, 
  STATUS_INTERNAL_SERVER_ERROR } from "$/shared/constants";

const tokensStore = getTokensStore();

const client = axios.create({
  baseURL: baseUrl,
  withCredentials: true,
  headers: {
    "Content-Type": "application/json",
  }
});

client.interceptors.request.use(async (config) => {
  tokensStore.refreshTokens();

  await tokensStore.accessToken?.then(token => {
    config.headers.Authorization = `Bearer ${token}`
  });

  return config;
});

interface AxiosRequestConfigDecorate extends AxiosRequestConfig {
  _retry: boolean;
}

client.interceptors.response.use(
  (response) => response,
  async (error: AxiosError<GenericError>) => {
    const originalRequest = error.config as AxiosRequestConfigDecorate;

    if (error.response?.status == undefined || error.response?.status >= STATUS_INTERNAL_SERVER_ERROR) {
      notifyError({
        title: "Ошибка на сервере", 
        message: "Неизвестная ошибка"
      });
    } else if (error.response?.status == STATUS_FORBIDDEN) {
      notifyError({
        title: "Ошибка доступа", 
        message: "Отказано в доступе"
      });
    } else if (error.response?.status == STATUS_UNAUTHORIZED 
      && !originalRequest._retry) {
      originalRequest._retry = true;

      tokensStore.refreshTokens();

      await tokensStore.accessToken?.then((_) => {
        return client.request(originalRequest)
            .then(res => res.data);
      });

      return originalRequest;
    }

    return Promise.reject(error);
  }
)

export default client;