import { defaultUnauthClient } from "$/shared/api/auth/config";
import { baseUrl } from "$/shared/api/constants";
import { LoginRequest, RegisterRequest, RegisterResponse, TokensResponse } from "$/shared/api/contracts";
import { AxiosResponse } from "axios";

async function login(req: LoginRequest): Promise<TokensResponse> {
  return defaultUnauthClient.post<
    TokensResponse, 
    AxiosResponse<TokensResponse>, 
    LoginRequest>("/auth/login", req)
    .then(res => res.data);
}

async function register(req: RegisterRequest): Promise<RegisterResponse> {
  return defaultUnauthClient.post<
    RegisterResponse, 
    AxiosResponse<RegisterResponse>, 
    RegisterRequest>("/auth/register", req)
    .then(res => res.data);
}

async function logout(): Promise<void> {
  const BASE_URL = `${baseUrl}/auth/logout`;

  return defaultUnauthClient.post(BASE_URL);
}

export { login, register, logout };