import { apiClient } from "$/shared/api";
import { baseUrl } from "$/shared/api/constants";
import { UpdateAuthUserRequest } from "$/shared/api/contracts";

const BASE_URL = `${baseUrl}/auth/auth-user`;

async function updateAuthUser(request: UpdateAuthUserRequest): Promise<void> {
  return apiClient.post(BASE_URL, request);
}

export { updateAuthUser }