import { TokensResponse } from "$/shared/api/contracts";
import { defaultUnauthClient } from "$/shared/api/auth/config";
import { AxiosResponse } from "axios";

async function postRefreshTokens(): Promise<TokensResponse> {
  return defaultUnauthClient
    .post<
      TokensResponse, 
      AxiosResponse<TokensResponse>>("/auth/refresh-tokens")
    .then(response => response.data);
}

export { postRefreshTokens }