import { baseUrl } from "$/shared/api/constants";
import axios from "axios";

const defaultUnauthClient = axios.create({
  baseURL: baseUrl,
  withCredentials: true,
  headers: {
    "Content-Type": "application/json"
  }
});

export { defaultUnauthClient }