import { BACKEND_SERVER_IS_SECURE } from "$/shared/constants";
import { BACKEND_SERVER } from "$/shared/constants"

export const baseUrl = `${"http" + (BACKEND_SERVER_IS_SECURE ? "s" : "")}://${BACKEND_SERVER}/api`