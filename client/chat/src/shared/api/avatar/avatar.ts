import { apiClient } from "$/shared/api";
import { Avatar, CreateAvatarRequest, Section } from "$/shared/api/contracts";
import { AxiosResponse } from "axios";

const BASE_URL = '/avatars';

async function getAvatarsSection(section: number): Promise<Section<Avatar>> {
  return apiClient.get<Section<Avatar>, AxiosResponse<Section<Avatar>>>(`${BASE_URL}`, {
    params: {
      section: section
    }
  }).then(res => res.data);
}

async function createAvatar(request: CreateAvatarRequest): Promise<Avatar> {
  const formData = new FormData();

  formData.append('file', request.file);
  formData.append('offsetX', request.offsetX.toString());
  formData.append('offsetY', request.offsetY.toString());
  formData.append('size', request.size.toString());

  return apiClient.post(`${BASE_URL}`, request, {
    headers: {
      'Content-Type': 'multipart/form-data',
      'Accept': '*/*'
    }
  }).then(res => res.data);
}

export{ getAvatarsSection, createAvatar }