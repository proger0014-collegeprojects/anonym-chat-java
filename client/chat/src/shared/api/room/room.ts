import { CreateRoomRequest, Room, RoomProfileWithUserFull, Section } from "$/shared/api/contracts";
import { apiClient } from "$/shared/api";
import { AxiosResponse } from "axios";

const BASE_ROUTE = '/rooms';

async function getRoomsPage(page: number): Promise<Section<Room>> {
  return apiClient.get<Section<Room>, AxiosResponse<Section<Room>>>(`${BASE_ROUTE}`, {
    params: {
      page: page
    }
  }).then(res => res.data);
}

async function connectToRoom(id: number): Promise<void> {
  return apiClient.post<void, AxiosResponse<void>>(`${BASE_ROUTE}/${id}/connections`, null, { 
    headers: {
      'Content-Type': 'application/json'
    }
   })
    .then(res => res.data);
}

async function disconnecFromRoom(id: number): Promise<void> {
  return apiClient.delete(`${BASE_ROUTE}/${id}/connections`).then(res => res.data);
}

async function getConnections(roomId: number, section: number): Promise<Section<RoomProfileWithUserFull>> {
  const url = `${BASE_ROUTE}/${roomId}/connections`;

  return apiClient.get(url, {
    params: {
      section: section
    }
  }).then(res => res.data);
}

async function createRoom(request: CreateRoomRequest): Promise<Room> {
  const url = `${BASE_ROUTE}`;

  return apiClient.post(url, request).then(res => res.data);
}

async function getRoomById(id: number): Promise<Room> {
  const url = `${BASE_ROUTE}/{}`.replace('{}', id.toString());

  return apiClient.get(url).then(res => res.data);
}

export { getRoomsPage, connectToRoom, getConnections, disconnecFromRoom, createRoom, getRoomById }