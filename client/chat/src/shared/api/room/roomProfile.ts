import { apiClient } from "$/shared/api";
import { CreateNewRoomProfileRequest, RoomProfileFull, Section, SetRoomProfileRequest } from "$/shared/api/contracts";
import { AxiosResponse } from "axios";

const BASE_ROUTE = '/room-profiles';

async function getRoomProfilesPage(page: number): Promise<Section<RoomProfileFull>> {
  return apiClient.get<Section<RoomProfileFull>, AxiosResponse<Section<RoomProfileFull>>>(`${BASE_ROUTE}`, {
    params: {
      page: page
    }
  }).then(res => res.data);
}

async function installRoomProfile(roomProfileId: number, request: SetRoomProfileRequest): Promise<void> {
  return apiClient.post<void, AxiosResponse<void>, SetRoomProfileRequest>(`${BASE_ROUTE}/${roomProfileId}/installs`, request)
    .then(res => res.data);
    
}

async function createRoomProfile(request: CreateNewRoomProfileRequest): Promise<RoomProfileFull> {
  return apiClient.post(`${BASE_ROUTE}`, request).then(res => res.data);
}

export { getRoomProfilesPage, installRoomProfile, createRoomProfile }