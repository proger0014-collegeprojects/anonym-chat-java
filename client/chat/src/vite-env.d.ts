/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly VITE_BACKEND_SERVER_HOST: string;
  readonly VITE_BACKEND_SERVER_PORT: number;
  readonly VITE_BACKEND_SERVER_IS_SECURE: string;
  readonly VITE_AUTH_TYPE: string;
  readonly VITE_ACCESS_TOKEN_EXPIRATION: number;
  readonly VITE_SECTION_COUNT: number;
  readonly VITE_WEBSOCKET_DISABLED: string;
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}