import { AuthLayout } from "$/layouts/auth";
import { Box, Button, Flex, Text } from "@mantine/core";
import { Link } from "$/components/common/link";
import { LOGIN_PAGE_WITH_LOGIN_AND_PASSWORD_ROUTE } from "$/pages/auth/login/with-login-and-password";
import { observer } from "mobx-react-lite";
import { Auth } from "$/pages/auth/Auth";
import { useAnonymLogin } from "$/shared/hooks/auth";

function LoginPage() {
  const LoginPageInnerWrapperObservable = observer(LoginPageInnerWrapper);

  return (
    <Auth.ToDoAuthCheckerWrapper>
      <LoginPageInnerWrapperObservable />
    </Auth.ToDoAuthCheckerWrapper>
  )
}

function LoginPageInnerWrapper() {
  const { isLoading, handleAnonymLoginCallback } = useAnonymLogin();

  return (
    <LoginPageInner
      isLoading={isLoading}
      handleAnonymLoginCallback={handleAnonymLoginCallback} />
  )
}

interface LoginPageInnerProps {
  isLoading: boolean;
  handleAnonymLoginCallback: () => void;
}

function LoginPageInner({ isLoading, handleAnonymLoginCallback }: LoginPageInnerProps) {

  return (
    <AuthLayout>
      <AuthLayout.Header>Anonym Chat</AuthLayout.Header>
      <AuthLayout.Body>

        <Flex align="center" direction="column">
          <Text fw={700} fz={24}>Вход</Text>

          <Box w={300}>

            <Button 
              onClick={handleAnonymLoginCallback} 
              loading={isLoading} 
              mt="96px" 
              color="dark" 
              variant="default"
              fullWidth>Низвергнуться во тьму</Button>

            <Box mt={'76px'}>
              <Text>
                Есть аккаунт с логином и паролем?
              </Text>
              <Link href={LOGIN_PAGE_WITH_LOGIN_AND_PASSWORD_ROUTE} style={{ p: '0px', m: '0px' }}>
                Войти в такой аккаунт
              </Link>
            </Box>

          </Box>

        </Flex>
        
      </AuthLayout.Body>
    </AuthLayout>
  )
}

export { LoginPage };