import { LOGIN_PAGE_ROUTE } from "$/pages/auth/login/constants";

const LOGIN_PAGE_WITH_LOGIN_AND_PASSWORD_ROUTE = `${LOGIN_PAGE_ROUTE}/with-login-and-password`;

export { LOGIN_PAGE_WITH_LOGIN_AND_PASSWORD_ROUTE }