import { AuthStore } from "$/app/stores";

function handleLogin(loading: () => void, authStore: AuthStore, login: string, password: string) {
  loading();

  authStore.login(login, password);
}

export { handleLogin }