import { AuthLayout } from "$/layouts/auth";
import { Auth } from "$/pages/auth/Auth";
import { observer } from "mobx-react-lite";
import { Box, Button, Flex, Group, PasswordInput, Text, TextInput } from "@mantine/core";
import { Link } from "$/components/common/link";
import { UseFormReturnType, useForm } from "@mantine/form";
import { validateMinLength } from "$/shared/lib";
import { REGISTER_PAGE_ROUTE } from "$/pages/auth/register";
import { useAnonymLogin } from "$/shared/hooks/auth";
import { useEffect, useState } from "react";
import { handleLogin } from "$/pages/auth/login/with-login-and-password/model";
import { useStores } from "$/app/stores";
import { Error as ApiError } from "$/shared/api/contracts";
import { AxiosError } from "axios";

function LoginPageWithLoginAndPassword() {
  const LoginPageWithLoginAndPasswordInnerWrapperObservable = observer(LoginPageWithLoginAndPasswordInnerWrapper);

  return (
    <Auth.ToDoAuthCheckerWrapper>
      <LoginPageWithLoginAndPasswordInnerWrapperObservable />
    </Auth.ToDoAuthCheckerWrapper>
  )
}

interface FormProps {
  login: string;
  password: string;
}

function LoginPageWithLoginAndPasswordInnerWrapper() {
  const [isLoading, setIsLoading] = useState(false);
  const { authStore } = useStores();

  function loading() {
    setIsLoading(true);
  }

  function disableLoading() {
    setIsLoading(false);
  }

  useEffect(() => {
    authStore.loginResponse?.case({
      rejected: (r: AxiosError<ApiError>) => {
        if (r.response?.data.type == "/errors/auth") {
          form.setErrors({ 'password': r.response?.data.description })
        }

        disableLoading();
      }
    })
  }, [authStore.loginResponse?.state])

  const form = useForm<FormProps>({
    initialValues: {
      login: '',
      password: ''
    },
    validate: {
      login: (value) => validateMinLength(value, 5),
      password: (value) => validateMinLength(value, 5) 
    }
  });

  const { handleAnonymLoginCallback } = useAnonymLogin();

  function handleFormSubmit(values: FormProps) {
    handleLogin(loading, authStore, values.login, values.password);
  }

  return (
    <LoginPageWithLoginAndPasswordInner
      formConfig={form}
      handleAnonymLoginCallback={handleAnonymLoginCallback}
      isLoading={isLoading}
      handleLoginFormSubmit={handleFormSubmit} />
  )
}

interface LoginPageWithLoginAndPasswordInnerProps {
  formConfig: UseFormReturnType<FormProps>;
  handleAnonymLoginCallback: () => void;
  isLoading: boolean;
  handleLoginFormSubmit: (values: FormProps) => void;
}

function LoginPageWithLoginAndPasswordInner({ 
  formConfig, 
  handleAnonymLoginCallback, 
  isLoading, 
  handleLoginFormSubmit }: LoginPageWithLoginAndPasswordInnerProps) {

  return (
    <AuthLayout>
      <AuthLayout.Header>Anonym Chat</AuthLayout.Header>
      <AuthLayout.Body>
        <Flex align="center" direction="column">
          <Text fw={700} fz={24}>Вход</Text>

          <Box mt={30} mih={385}>
            <form onSubmit={formConfig.onSubmit(handleLoginFormSubmit)}>
              <TextInput
                mb={25}
                w={300}
                label="Логин"
                placeholder="super_login@228"
                key={formConfig.key('login')}
                { ...formConfig.getInputProps('login') } />

              <PasswordInput
                mb={10}
                w={300}
                label="Пароль"
                key={formConfig.key('password')}
                { ...formConfig.getInputProps('password') } />

              <Box mb={20}>
                <Text>Нет аккаунта с логином и паролем?</Text>
                <Link href={REGISTER_PAGE_ROUTE}>Создать такой аккаунт</Link>
              </Box>

              <Button loading={isLoading} type="submit" variant="default">Войти</Button>
            </form>
          </Box>  

          <Group w={300} align="end" mt={40}>
            <Link onClick={handleAnonymLoginCallback}>Войти как аноним</Link>
          </Group>
        </Flex>

      </AuthLayout.Body>
    </AuthLayout>
  )
}

export { LoginPageWithLoginAndPassword }