import { AUTH_ROUTE } from "$/pages/auth/constants";

const LOGIN_PAGE_ROUTE = `${AUTH_ROUTE}/login`;

export { LOGIN_PAGE_ROUTE }