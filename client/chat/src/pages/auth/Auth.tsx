import { useStores } from "$/app/stores";
import { MAIN_PAGE_ROUTE } from "$/pages/main";
import { ChildrenProps } from "$/shared/types";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

function Auth() {  }

function ToDoAuthChecker({ children }: ChildrenProps) {
  const { authStore, messagingClientStore } = useStores();
  const navigate = useNavigate();

  useEffect(() => {
    if (authStore.isAuthorize) {
      navigate(MAIN_PAGE_ROUTE);
    } else {
      if (messagingClientStore.isActive) {
        messagingClientStore.client.deactivate();
      }
    }
  }, [authStore.isAuthorize])

  return (
    <>
      {children}
    </>
  )
}

function ToAuthRequiredChecker({ children }: ChildrenProps) {
  const { authStore, messagingClientStore } = useStores();

  function logout() {
    authStore.logout();
  }

  useEffect(() => {
    if (!authStore.isAuthorize) {
      logout();
    } else {
      if (!messagingClientStore.isActive) {
        messagingClientStore.client.activate();
      }
    }
  }, [authStore.isAuthorize])

  return (
    <>
      {children}
    </>
  )
}

Auth.ToDoAuthCheckerWrapper = observer(ToDoAuthChecker);
Auth.ToAuthRequiredCheckerWrapper = observer(ToAuthRequiredChecker);

export { Auth }