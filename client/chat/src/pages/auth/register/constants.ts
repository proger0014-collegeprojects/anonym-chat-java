import { AUTH_ROUTE } from "$/pages/auth/constants";

const REGISTER_PAGE_ROUTE = `${AUTH_ROUTE}/register`;

export { REGISTER_PAGE_ROUTE };