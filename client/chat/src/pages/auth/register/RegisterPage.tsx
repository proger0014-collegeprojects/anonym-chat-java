import { useStores } from "$/app/stores";
import { Link } from "$/components/common/link";
import { AuthLayout } from "$/layouts/auth";
import { Auth } from "$/pages/auth/Auth";
import { LOGIN_PAGE_WITH_LOGIN_AND_PASSWORD_ROUTE } from "$/pages/auth/login/with-login-and-password";
import { Form, handleRegister, validateConfirmPassword } from "$/pages/auth/register/model";
import { useAnonymLogin } from "$/shared/hooks/auth";
import { notifySuccess, validateMinLength } from "$/shared/lib";
import { Box, Button, Flex, Group, PasswordInput, Text, TextInput } from "@mantine/core";
import { UseFormReturnType, useForm } from "@mantine/form";
import { AxiosError } from "axios";
import { observer } from "mobx-react-lite"
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Error as ApiError } from "$/shared/api/contracts";

function RegisterPage() {
  const RegisterPageInnerWrapperObservable = observer(RegisterPageInnerWrapper);

  return (
    <Auth.ToDoAuthCheckerWrapper>
      <RegisterPageInnerWrapperObservable />
    </Auth.ToDoAuthCheckerWrapper>
  )
}

function RegisterPageInnerWrapper() {
  const [isLoading, setIsLoading] = useState(false);
  const { authStore } = useStores();
  const navigate = useNavigate();

  function loading() {
    setIsLoading(true);
  }

  function disableLoading() {
    setIsLoading(false);
  }

  const form = useForm<Form>({
    initialValues: {
      login: '',
      password: '',
      confirmPassword: ''
    },

    validate: {
      login: (value) => validateMinLength(value, 5),
      password: (value) => validateMinLength(value, 5),
      confirmPassword: validateConfirmPassword
    }
  });

  useEffect(() => {
    if (isLoading) {
      authStore.registerResponse?.case({
        fulfilled: () => {
          authStore.clearRegisterResponse();
          notifySuccess({ message: "Аккаунт был успешно создан" });
          navigate(LOGIN_PAGE_WITH_LOGIN_AND_PASSWORD_ROUTE);
        },
        rejected: (e: AxiosError<ApiError>) => {
          disableLoading();
          authStore.clearRegisterResponse();
          
          if (e.response?.data.type == "/errors/models/exists") {
            form.setErrors({ 'login': "пользователь с таким логином уже существует" });
          }
        }
      });
    }
  }, [authStore.registerResponse?.state])

  const { handleAnonymLoginCallback } = useAnonymLogin();

  function handleFormSubmit(values: Form) {
    handleRegister(loading, authStore, values.login, values.password);
  }

  return (
    <RegisterPageInner 
      isLoading={isLoading}
      handleAnonymLoginCallback={handleAnonymLoginCallback}
      formConfig={form}
      handleFormSubmit={handleFormSubmit} />
  )
}

interface RegisterPageInnerProps {
  isLoading: boolean;
  handleAnonymLoginCallback: () => void;
  formConfig: UseFormReturnType<Form>;
  handleFormSubmit: (values: Form) => void;
}

function RegisterPageInner({ 
  isLoading, 
  handleAnonymLoginCallback, 
  formConfig,
  handleFormSubmit  }: RegisterPageInnerProps) {
  return (
    <AuthLayout>
      <AuthLayout.Header>Anonym Chat</AuthLayout.Header>
      <AuthLayout.Body>
        <Flex align="center" direction="column">
          <Text fw={700} fz={24}>Регистрация</Text>

          <Box mt={30} mih={385}>
            <form onSubmit={formConfig.onSubmit(handleFormSubmit)}>
              <TextInput
                mb={25}
                w={300}
                label="Логин"
                placeholder="super_login@228"
                key={formConfig.key('login')}
                { ...formConfig.getInputProps('login') } />

              <PasswordInput
                mb={25}
                w={300}
                label="Пароль"
                key={formConfig.key('password')}
                { ...formConfig.getInputProps('password') } />

              <PasswordInput
                mb={10}
                w={300}
                label="Повторить пароль"
                key={formConfig.key('confirmPassword')}
                { ...formConfig.getInputProps('confirmPassword') } />

              <Box mb={20}>
                <Link href={LOGIN_PAGE_WITH_LOGIN_AND_PASSWORD_ROUTE}>Вернуться ко входу</Link>
              </Box>

              <Button loading={isLoading} type="submit" variant="default">Зарегистрироваться</Button>
            </form>
          </Box>  

          <Group w={300} mt={40}>
            <Link onClick={handleAnonymLoginCallback}>Войти как аноним</Link>
          </Group>
        </Flex>

      </AuthLayout.Body>
    </AuthLayout>
  )
}

export { RegisterPage }