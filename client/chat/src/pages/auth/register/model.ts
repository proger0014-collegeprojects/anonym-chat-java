import { AuthStore } from "$/app/stores";
import { validateMinLength } from "$/shared/lib";

interface Form {
  login: string;
  password: string;
  confirmPassword: string;
}

function validateConfirmPassword(value: string, values: Form): string|null {
  return validateMinLength(value, 5)
    || value !== values.password
    ? 'Пароли не совпадают'
    : null;
}

function handleRegister(loading: () => void, authStore: AuthStore, login: string, password: string) {
  loading();

  authStore.register(login, password);
}

export { type Form, validateConfirmPassword, handleRegister }