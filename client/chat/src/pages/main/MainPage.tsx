import { Auth } from "$/pages/auth";
import { MainLayout } from "$/layouts/main";
import { Left } from "$/pages/main/left";
import { Right } from "$/pages/main/right";
import { ConnectionAwaiter } from "$/components/common/connection-awaiter";

function MainPage() {
  return (
    <Auth.ToAuthRequiredCheckerWrapper>
      <ConnectionAwaiter>
        <MainPageInner />
      </ConnectionAwaiter>
    </Auth.ToAuthRequiredCheckerWrapper>
  )
}

function MainPageInner() {

  return (
    <MainLayout>
      <Left />
      <Right />
    </MainLayout>
  )
}

export { MainPage }