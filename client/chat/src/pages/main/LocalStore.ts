import { ModalTabInfo, rootTab } from "$/pages/main";
import { makeAutoObservable } from "mobx";

class LocalStore {
  currentTabLeft1: ModalTabInfo;
  activeRoomIdLeft2?: number;

  constructor() {
    makeAutoObservable(this);

    this.currentTabLeft1 = rootTab;
  }

  updateTabLeft1(tab: ModalTabInfo) {
    if (this.currentTabLeft1.tab == tab.tab) return;

    this.currentTabLeft1 = this.currentTabLeft1;
  }

  resetTabLeft1() {
    this.updateTabLeft1(rootTab);
  }

  updateActiveRoomIdLeft2(id: number) {
    if (this.activeRoomIdLeft2 == id) return;

    this.activeRoomIdLeft2 = id;
  }

  resetActiveRoomIdLeft2() {
    if (!this.activeRoomIdLeft2) return;
    
    this.activeRoomIdLeft2 = undefined;
  }
}

const mainStore = new LocalStore();

export { mainStore }