import { AuthStore, useStores } from "$/app/stores";
import { RoomStore } from "$/app/stores/rooms";
import { MessagesStore } from "$/app/stores/rooms/messages/MessagesStore";
import { CanScrollAction, DataVertiсalScroll, Item } from "$/components/common/data-vertical-scroll-area";
import { MessageInput } from "$/components/common/message-input";
import { Message } from "$/components/room/message";
import { MessageReply } from "$/components/room/message/reply";
import { mainStore } from "$/pages/main/LocalStore";
import { Group, Stack } from "@mantine/core";
import { useDisclosure, useWindowEvent } from "@mantine/hooks";
import { observer } from "mobx-react-lite";
import { useEffect, useMemo, useState } from "react";

interface MessageReplyInfo {
  id: number;
  nickname: string;
  avatarUrl: string;
  shortBody: string;
}

function getItems(roomStore: RoomStore, messagesStore: MessagesStore, authStore: AuthStore, onActiveMessageToReply: (message: MessageReplyInfo) => void) {
  return messagesStore?.messages.map((m) => {
    const roomProfile = roomStore.roomProfiles[m.message.userId];
    const roomProfileReply = m.message.reply && roomStore.roomProfiles[m.message.reply.userId];

    const messageReply = m.message.reply && {
      isMine: m.message.reply.userId == authStore.user?.id,
      avatarUrl: roomProfileReply.avatar.url,
      nickname: roomProfileReply.nickname,
      message: m.message.reply.shortBody,
      onClickReply: () => {  }
    };

    const isMine = m.message.userId == authStore.user?.id;

    const MessageObservable = observer(() => (
      <Group justify={isMine ? 'end' : 'start'} mb={15} px={80}>
        <Message
          messageAuthor={{
            isMine: isMine,
            avatarUrl: roomProfile.avatar.url,
            nickname: roomProfile.nickname,
            message: m.message.body,
            onClickReply: () => { onActiveMessageToReply({
              id: m.message.id,
              nickname: roomProfile.nickname,
              avatarUrl: roomProfile.avatar.url,
              shortBody: m.message.body
            }) }
          }}
          messageReply={messageReply}
          messageReplyContainerProps={{ maw: "52vh" }} />
      </Group>
    ))

    return {
      key: m.message.id,
      component: (
        <MessageObservable />
      ),
      onVisible: () => {
        m.read();
      }
    }
  })
}

const Content = observer(() => {
  const { rootRoomStores} = useStores();

  if (!mainStore.activeRoomIdLeft2) return null;
  
  const roomStore = rootRoomStores.find(mainStore.activeRoomIdLeft2);

  useEffect(() => {
    return () => {
      roomStore.messagesStore.notReadingOngoint();
    }
  }, [mainStore.activeRoomIdLeft2])

  roomStore.messagesStore?.fetchLastMessageSection();

  const canScrollUp = roomStore.messagesStore.lastSection > 1;
  const canScrollDown = roomStore.messagesStore.lastSection != roomStore.messagesStore.totalSections;

  function onScrolledUp(reset: () => void) {
    roomStore.messagesStore.fetchMessageSection(roomStore.messagesStore.lastSection - 1);

    roomStore.messagesStore.pendingMessageInfo.then(_ => reset());
  }

  function onScrolledDown(reset: () => void) {
    roomStore.messagesStore.fetchMessageSection(roomStore.messagesStore.lastSection + 1);

    roomStore.messagesStore.pendingMessageInfo.then(_ => reset());
  }

  return (
    <>
      <ConteintInnerWrapper
        roomStore={roomStore}
        messagesStore={roomStore.messagesStore}
        canScrollAction={{ up: canScrollUp, down: canScrollDown }}
        onScrolledUp={onScrolledUp}
        onScrolledDown={onScrolledDown} />
    </>
  )
})

interface ConteintInnerWrapperProps {
  messagesStore: MessagesStore;
  roomStore: RoomStore;
  canScrollAction: CanScrollAction;
  onScrolledUp: (reset: () => void) => void;
  onScrolledDown: (reset: () => void) => void;
}

const ConteintInnerWrapper = observer(({ 
  messagesStore, 
  roomStore, 
  canScrollAction, 
  onScrolledDown, 
  onScrolledUp }: ConteintInnerWrapperProps) => {
  const [maxHeight, setMaxHeight] = useState(window.innerHeight - 54);
  const [messageToReply, setMessageToReply] = useState<MessageReplyInfo|undefined>(undefined);
  const [openedMessageInputTop, { open: openMessageInputTop, close: closeMessageInputTop }] = useDisclosure(false);

  useWindowEvent('resize', (_) => {
    setMaxHeight(window.innerHeight - 54);
  });

  function activeMessageToReply(roomMessage: MessageReplyInfo) {
    if (messageToReply && messageToReply.id == roomMessage.id) return;

    setMessageToReply(roomMessage);
    openMessageInputTop();
  }

  function resetMessageToReply() {
    if (!messageToReply) return;

    setMessageToReply(undefined);
    closeMessageInputTop();
  }

  function onClickSend(reset: () => void, message: string) {
    if (messageToReply) {
      const messageStore = roomStore.messagesStore.findMessageById(messageToReply.id);

      if (messageStore) {
        messageStore.reply(message);
      }
    } else {
      roomStore.messagesStore.sendMessage(message);
    }

    resetMessageToReply();

    reset();
  }

  function onScroll(maxHeight: number, scroll: number) {
    if (maxHeight <= scroll - 200) {
      roomStore.messagesStore.readingOngoing();
    } else {
      roomStore.messagesStore.notReadingOngoint();
    }
  }

  const { authStore } = useStores();

  const items = useMemo(
    () => getItems(roomStore, messagesStore, authStore, activeMessageToReply),
    [roomStore.messagesStore.messages.length]
  );

  return (
    <>
      <ContentInner
        onScroll={onScroll}
        messageToReply={messageToReply}
        openedMessageInputTop={openedMessageInputTop}
        onCloseMessageInputTop={resetMessageToReply}
        onClickSend={onClickSend}
        maxHeight={maxHeight}
        items={items}
        canScrollAction={canScrollAction}
        onScrolledUp={onScrolledUp}
        onScrolledDown={onScrolledDown} />
    </>
  )
});

interface ContentInnerProps {
  messageToReply?: MessageReplyInfo;
  openedMessageInputTop: boolean;
  onCloseMessageInputTop: () => void;
  canScrollAction: CanScrollAction;
  onScrolledUp: (reset: () => void) => void;
  onScrolledDown: (reset: () => void) => void;
  items: Item[];
  maxHeight: number;
  onClickSend: (reset: () => void, message: string) => void;
  onScroll?: (scrollHeight: number, scroll: number) => void;
}

const ContentInner = observer(({ 
  onScroll,
  canScrollAction, 
  items, 
  onScrolledDown, 
  onScrolledUp, 
  maxHeight, 
  onClickSend, 
  onCloseMessageInputTop, 
  openedMessageInputTop, 
  messageToReply }: ContentInnerProps) => {
  return (
    <Stack justify="space-between" gap={0} w="100%" h={maxHeight}>
      <DataVertiсalScroll
        containerProps={{ w: '100%', h: 'auto', px: 0 }}
        canScrollAction={canScrollAction} 
        onScrolledUp={onScrolledUp}
        onScrolledDown={onScrolledDown}
        items={items}
        onScroll={onScroll} />
      <MessageInput containerProps={{ bg: 'gray.8', h: 'auto' }}>
        <MessageInput.Top containerProps={{ bg: 'gray.8' }} opened={openedMessageInputTop} onClose={onCloseMessageInputTop}>
          <MessageReply
            containerProps={{ maw: "60vh" }}
            avatarUrl={messageToReply?.avatarUrl ?? ''}
            nickname={messageToReply?.nickname ?? ''}
            shortBody={messageToReply?.shortBody ?? ''} /> 
        </MessageInput.Top>
        <MessageInput.Bottom containerProps={{ bg: 'gray.8' }} onClickSend={onClickSend} />
      </MessageInput>
    </Stack>
  )
})

export { Content }