import { RoomStore } from "$/app/stores/rooms";
import { LocalWidgetStore, RoomProfilesWidget } from "$/components/room/profile/widget";
import { localInfoStore } from "$/pages/main/right/room-content/header/menu/info/LocalStore";
import { RoomProfileFull } from "$/shared/api/contracts";
import { Box, Button, Text } from "@mantine/core";
import { openConfirmModal } from "@mantine/modals";
import { observer } from "mobx-react-lite";

interface ChangeRoomProfileTabProps {
  widgetStore: LocalWidgetStore;
  roomStore: RoomStore;
}

const FooterItemObservable = observer(({ data, onClick }: { data: RoomProfileFull, onClick: (roomProfileId: number) => void}) => {
  return (
    <Box>
      <Button onClick={() => {
        onClick(data.id);
      }} variant="default" size="compact-xs">Выбрать</Button>
    </Box>
  )
})

const ChangeRoomProfileTab = observer(({ widgetStore, roomStore }: ChangeRoomProfileTabProps) => {
  function installRoomProfile(roomProfileId: number) {
    roomStore.installRoomProfile(roomProfileId);

    if (roomStore.installRoomProfileInfo && roomStore.installRoomProfileInfo.status) {
      if (roomStore.installRoomProfileInfo.status = 'finished-ok') {
        localInfoStore.resetTab();
      }
    }
  }

  function onClick(roomProfileId: number) {
    openConfirmModal({
      zIndex: 5_000,
      title: "Подтверждение действия",
      children: (
        <Text>Вы уверены, что хотите сменить профиль комнаты?</Text>
      ),
      labels: {
        confirm: 'Да',
        cancel: 'Нет'
      },
      onConfirm: () => {
        installRoomProfile(roomProfileId);
      }
    })
  }

  return (
    <RoomProfilesWidget contextProps={{
      localWidgetStore: widgetStore
    }}
    footerItem={(data) => (
      <FooterItemObservable data={data} onClick={onClick} />
    )}/>
  )
});

export { ChangeRoomProfileTab }