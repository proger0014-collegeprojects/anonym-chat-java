import { RoomStore } from "$/app/stores/rooms";
import { ButtonIcon } from "$/components/common/buttons/button-icon";
import { mainStore } from "$/pages/main/LocalStore";
import { Menu } from "$/pages/main/right/room-content/header/menu";
import { Room } from "$/shared/api/contracts";
import { Box, Group, Text } from "@mantine/core";
import { IconArrowLeft, IconDotsVertical } from "@tabler/icons-react";
import { observer } from "mobx-react-lite";
import { useState } from "react";

interface HeaderProps {
  roomStore: RoomStore;
}

const Header = observer(({ roomStore }: HeaderProps) => {
  const [openedMenu, setOpenedMenu] = useState(false);

  function onClickDots() {
    setOpenedMenu(true);
  }

  function onClickBack() {
    mainStore.resetActiveRoomIdLeft2();
  }

  return (
    <HeaderInner 
      roomStore={roomStore}
      onClickDots={onClickDots}
      openedMenu={openedMenu}
      setOpenedMenu={setOpenedMenu}
      roomActive={roomStore}
      onClickBack={onClickBack} />
  )
});

interface HeaderInnerProps {
  roomActive?: Room;
  onClickBack: () => void;
  openedMenu: boolean;
  setOpenedMenu: (opened: boolean) => void;
  onClickDots: () => void;
  roomStore: RoomStore;
}

const HeaderInner = observer(({ roomActive, onClickBack, openedMenu, setOpenedMenu, onClickDots, roomStore }: HeaderInnerProps) => {
  return (
    <Group w="100%" h="100%" align="center" justify="space-between" px={10} py={5}>
      <Group w="80%" h="100%" wrap="nowrap" justify="start" align="center">
        <Box h="100%">
          <ButtonIcon onClick={onClickBack} icon={<IconArrowLeft size={30} />} />
        </Box>
        <Text w="100%" truncate style={{ whiteSpace: 'none' }} fz={20} fw={700} size="xs">{roomActive?.name}</Text>
      </Group>
      <Box h="100%" pos="relative">
        <Menu 
          roomStore={roomStore}
          containerProps={{ position: 'bottom-end', width: 250 }}
          target={
            <ButtonIcon icon={<IconDotsVertical size={30} />} onClick={onClickDots} />
          }
          opened={openedMenu} 
          onChange={setOpenedMenu} />
      </Box>
    </Group>
  )
})

export { Header }