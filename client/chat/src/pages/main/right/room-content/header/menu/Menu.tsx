import { observer } from "mobx-react-lite";
import { Menu as MMenu, MenuProps as MMenuProps, Text } from "@mantine/core";
import { IconInfoCircle, IconLogout } from "@tabler/icons-react";
import { ReactNode } from "react";
import { Info } from "$/pages/main/right/room-content/header/menu/info";
import { localMenuStore } from "$/pages/main/right/room-content/header/menu";
import { RoomStore } from "$/app/stores/rooms";
import { mainStore } from "$/pages/main/LocalStore";
import { openConfirmModal } from "@mantine/modals";

interface MenuProps {
  opened: boolean;
  onChange: (opened: boolean) => void;
  containerProps?: MMenuProps;
  target: ReactNode;
  roomStore: RoomStore;
}

const Menu = observer(({ opened, onChange, containerProps, target, roomStore }: MenuProps) => {
  return (
    <>
      <Info roomStore={roomStore} opened={localMenuStore.openedInfo} close={() => localMenuStore.closeInfo()} />
      <MMenu opened={opened} onChange={onChange} { ...containerProps }>
        <MMenu.Target>{target}</MMenu.Target>
        <MenuDropdown opened={opened} roomStore={roomStore} />
      </MMenu>
    </>
  )
});

interface MenuDropdownProps {
  opened: boolean;
  roomStore: RoomStore;
}

const MenuDropdown = observer(({ opened, roomStore }: MenuDropdownProps ) => {
  if (!opened) return null;

  return (
    <MMenu.Dropdown py={10}>
      <MMenu.Item leftSection={<IconInfoCircle />} onClick={() => localMenuStore.openInfo()}>
        Информация
      </MMenu.Item>
      <MMenu.Item c="red" leftSection={<IconLogout />} onClick={() => {
        openConfirmModal({
          title: 'Подтверждение действия',
          children: (
            <Text c="red">
              Вы действительно хотите выйти из комнаты?
            </Text>
          ),
          labels: {
            confirm: 'Да',
            cancel: 'Нет'
          },
          onConfirm: () => roomStore.disconnectFromRoom({ onSuccess: () => mainStore.resetActiveRoomIdLeft2() })
        })
      }}>
        Выйти
      </MMenu.Item>
    </MMenu.Dropdown>
  )
})

export { Menu }