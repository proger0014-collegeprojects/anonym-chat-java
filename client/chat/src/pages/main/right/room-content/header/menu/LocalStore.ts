import { makeAutoObservable } from "mobx";

class LocalStore {
  openedInfo: boolean;

  constructor() {
    makeAutoObservable(this);

    this.openedInfo = false;
  }

  openInfo() {
    this.openedInfo = true;
  }

  closeInfo() {
    this.openedInfo = false;
  }
}

const localStore = new LocalStore();

export { localStore as localMenuStore }