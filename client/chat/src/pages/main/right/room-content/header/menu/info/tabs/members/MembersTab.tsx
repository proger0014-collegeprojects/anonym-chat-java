import { DataVertiсalScroll, Item } from "$/components/common/data-vertical-scroll-area";
import { RoomProfileItemFast } from "$/components/room";
import { LocalStore } from "$/pages/main/right/room-content/header/menu/info/tabs/members/LocalStore";
import { RoomProfileFull } from "$/shared/api/contracts";
import { Box } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { createContext, useContext, useEffect, useMemo } from "react";

interface MembersTabProps {
  localStore: LocalStore;
}

function getItems(roomProfiles: Map<number, RoomProfileFull>): Item[] {
  const arr = [...roomProfiles];

  const items = arr.map((v) => {
    const userId = v[0];
    const roomProfile = v[1];

    return {
      key: userId,
      component: (
        <Box h={100} w="100%">
          <RoomProfileItemFast
            nicknameStyleProps={{ w: '50vh' }}
            avatarProps={{ w: 100, h: 100 }}
            avatar={{ url: roomProfile.avatar.url, size: 80 }}
            nickname={roomProfile.nickname} />
        </Box>
      )
    }
  }) as Item[];

  return items;
}

const Context = createContext<LocalStore|undefined>(undefined);

function useLocalStore() {
  return useContext(Context);
}

const MembersTab = observer(({ localStore }: MembersTabProps) => {
  return (
    <Context.Provider value={localStore}>
      <MembersTabWrapper />
    </Context.Provider>
  )
})

const MembersTabWrapper = observer(() => {
  const localStore = useLocalStore();

  const canScrollDown = localStore!.connectionsInfo.lastSection < localStore!.connectionsInfo.totalSections;

  useEffect(() => {
    localStore!.fetchConnections(1);
  }, [localStore]);

  function onScrolledDown(reset: () => void) {
    localStore!.fetchConnections(localStore!.connectionsInfo.lastSection + 1);

    localStore!.connectionsInfo.fetchStatus.then(_ => {
      localStore!.connectionsInfo.fetchStatus.case({
        fulfilled: () => reset(),
        rejected: () => reset(),
      });
    });
  }

  const items = useMemo(
    () => getItems(localStore!.connections),
    [localStore!.connections.size]
  )

  return (
    <MembersTabInner
      onScrolledDown={onScrolledDown}
      canScrollDown={canScrollDown}
      items={items} />
  )
});

interface MembersTabInnerProps {
  items: Item[];
  canScrollDown: boolean;
  onScrolledDown: (reset: () => void) => void;
}

const MembersTabInner = observer(({ items, canScrollDown, onScrolledDown }: MembersTabInnerProps) => {
  return (
    <DataVertiсalScroll 
      containerProps={{ h: '100%' }}
      items={items} 
      onScrolledUp={() => {}}
      onScrolledDown={onScrolledDown}
      canScrollAction={{ up: false, down: canScrollDown }}
       />
  )
})

export { MembersTab }