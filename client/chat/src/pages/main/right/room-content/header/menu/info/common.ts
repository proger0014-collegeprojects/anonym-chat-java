import { ModalTabInfo } from "$/pages/main/common";


const tabs: {
  main: ModalTabInfo,
  members: ModalTabInfo,
  changeRoomProfile: ModalTabInfo
} = {
  main: {
    tab: 'main',
    title: 'Информация о комнате'
  },
  members: {
    tab: 'members',
    title: 'Участники'
  },
  changeRoomProfile: {
    tab: 'change-room-profile',
    title: 'Смена профиля комнаты'
  }
}

const rootTab = tabs.main;

export { tabs, rootTab }