import { ModalTabInfo } from "$/pages/main/common";
import { rootTab } from "$/pages/main/right/room-content/header/menu/info/common";
import { makeAutoObservable } from "mobx";

class LocalStore {
  currentTab: ModalTabInfo;

  constructor() {
    makeAutoObservable(this);

    this.currentTab = rootTab;
  }

  updateTab(tab: ModalTabInfo) {
    if (this.currentTab.tab == tab.tab) return;

    this.currentTab = tab;
  }

  resetTab() {
    if (this.currentTab.tab == rootTab.tab) return;

    this.currentTab = rootTab;
  }
}

const localInfoStore = new LocalStore();

export { localInfoStore }