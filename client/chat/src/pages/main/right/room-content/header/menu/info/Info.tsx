import { RoomStore } from "$/app/stores/rooms";
import { ButtonIcon } from "$/components/common/buttons/button-icon";
import { MenuModal } from "$/components/common/modals";
import { LocalWidgetStore } from "$/components/room/profile/widget";
import { localInfoStore, rootTab, tabs } from "$/pages/main/right/room-content/header/menu/info";
import { ChangeRoomProfileTab } from "$/pages/main/right/room-content/header/menu/info/tabs/change-room-profile/ChangeRoomProfileTab";
import { MainTab } from "$/pages/main/right/room-content/header/menu/info/tabs/main";
import { MembersTab } from "$/pages/main/right/room-content/header/menu/info/tabs/members";
import { LocalStore } from "$/pages/main/right/room-content/header/menu/info/tabs/members/LocalStore";
import { Box, Group, Tabs, Text } from "@mantine/core";
import { IconArrowLeft } from "@tabler/icons-react";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";

interface InfoProps {
  opened: boolean;
  close: () => void;
  roomStore: RoomStore;
}

const Info = observer(({ opened, close, roomStore }: InfoProps) => {
  useEffect(() => {
    return () => {
      localInfoStore.resetTab();
    }
  }, [opened])
  
  const isNotRootTab = localInfoStore.currentTab.tab != rootTab.tab;

  const title = (
    <Group wrap="nowrap">
      {isNotRootTab && (
        <Box>
          <ButtonIcon 
            icon={<IconArrowLeft />}
            onClick={() => localInfoStore.resetTab()} />
        </Box>
      )}
      <Text fz={24}>{localInfoStore.currentTab.title}</Text>
    </Group>
  )

  const membersTabStore = new LocalStore(roomStore);
  const localWidgetStore = new LocalWidgetStore();

  return (
    <MenuModal opened={opened} onClose={close} title={title}>
      <Tabs h="100%" value={localInfoStore.currentTab.tab}>
        <Tabs.Panel h="100%" value={tabs.main.tab}>
          <MainTab />
        </Tabs.Panel>
        <Tabs.Panel h="100%" value={tabs.members.tab}>
          <MembersTab localStore={membersTabStore} />
        </Tabs.Panel>
        <Tabs.Panel h="100%" value={tabs.changeRoomProfile.tab}>
          <ChangeRoomProfileTab widgetStore={localWidgetStore} roomStore={roomStore} />
        </Tabs.Panel>
      </Tabs>
    </MenuModal>
  )
})

export { Info }