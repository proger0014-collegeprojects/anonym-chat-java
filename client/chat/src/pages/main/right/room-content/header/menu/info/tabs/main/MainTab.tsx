import { useStores } from "$/app/stores";
import { RoomStore } from "$/app/stores/rooms";
import { ButtonIconV2 } from "$/components/common/buttons/button-icon";
import { RoomProfileItemFast } from "$/components/room";
import { mainStore } from "$/pages/main/LocalStore";
import { localInfoStore } from "$/pages/main/right/room-content/header/menu/info/LocalStore";
import { tabs } from "$/pages/main/right/room-content/header/menu/info/common";
import { RoomProfileFull } from "$/shared/api/contracts";
import { Box, Button, ScrollArea, Stack, Table, Text } from "@mantine/core";
import { observer } from "mobx-react-lite";

const MainTab = observer(() => {
  const { rootRoomStores } = useStores();

  const room = rootRoomStores.find(mainStore.activeRoomIdLeft2 ?? -1);

  if (!room) return null;

  const myRoomProfile = room.myRoomProfile;

  return (
    <MainTabInner
      roomStore={room}
      myRoomProfile={myRoomProfile} />
  )
});

interface MainTabInnerProps {
  roomStore: RoomStore;
  myRoomProfile: RoomProfileFull;
}

const MainTabInner = observer(({ roomStore, myRoomProfile }: MainTabInnerProps) => {
  return (
    <ScrollArea.Autosize h="100%">
      <Stack h="100%">
        <Box>
          <Table>
            <Table.Tbody>
              <Table.Tr>
                <Table.Th>Id</Table.Th>
                <Table.Td>{roomStore.id}</Table.Td>
              </Table.Tr>
              <Table.Tr>
                <Table.Th>Название</Table.Th>
                <Table.Td>
                  <Text style={{ overflowWrap: 'anywhere' }}>{roomStore.name}</Text>
                </Table.Td>
              </Table.Tr>
              <Table.Tr>
                <Table.Th>Описание</Table.Th>
                <Table.Td>{<Text style={{ overflowWrap: 'anywhere' }}>{roomStore.description}</Text> ?? <Text fz={14} fs="italic" c="gray.6">Отсутствует</Text>}</Table.Td>
              </Table.Tr>
              <Table.Tr>
                <Table.Th>Кол-во активных участников</Table.Th>
                <Table.Td>{roomStore.countConnectedUsers}</Table.Td>
              </Table.Tr>
            </Table.Tbody>
          </Table>
        </Box>
        <Box my={10}>
          <Text fz={18} mb={10}>Текущий профиль комнаты</Text>

          <Box>
            <RoomProfileItemFast 
              avatar={{
                url: myRoomProfile.avatar.url,
                size: 80
              }}
              nickname={myRoomProfile.nickname}
              nicknameStyleProps={{ w: '52vh' }}
              footer={(
                <Box>
                  <Button variant="default" size="compact-md" onClick={() => {
                    localInfoStore.updateTab(tabs.changeRoomProfile)
                  }}>Сменить</Button>
                </Box>
              )} />
          </Box>
        </Box>
        <Box mt={10}>
          <Text fz={18} mb={20}>Прочее</Text>

          <Box>
            <ButtonIconV2 onClick={() => localInfoStore.updateTab(tabs.members)}>Участники</ButtonIconV2>
          </Box>
        </Box>
      </Stack>
    </ScrollArea.Autosize>
  )
})

export { MainTab }