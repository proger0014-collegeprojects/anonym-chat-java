import { RoomStore } from "$/app/stores/rooms";
import { RoomProfileFull, RoomProfileWithUserFull } from "$/shared/api/contracts";
import { getConnections } from "$/shared/api/room";
import { makeAutoObservable } from "mobx";
import { IPromiseBasedObservable, fromPromise } from "mobx-utils";

interface ConnectionsInfo {
  lastSection: number;
  fetchStatus: IPromiseBasedObservable<any>;
  totalSections: number;
}

class LocalStore {
  roomStore: RoomStore;
  connections: Map<number, RoomProfileFull>;
  connectionsInfo: ConnectionsInfo;
  
  constructor(roomStore: RoomStore) {
    makeAutoObservable(this);

    this.roomStore = roomStore;
    this.connections = new Map();
    this.connectionsInfo = {
      lastSection: 0,
      fetchStatus: fromPromise(new Promise(_ => {})),
      totalSections: 0
    };
  }

  fetchConnections(section: number) {
    if (this.connectionsInfo.lastSection == section) return;

    this.updateConnectionsInfo({
      ...this.connectionsInfo,
      lastSection: section
    });
    
    const response = getConnections(this.roomStore.id, section).then(res => {
      this.updateConnectionsInfo({
        ...this.connectionsInfo,
        totalSections: res.lastSection
      });

      this.addToConnectionsList(res.items);
      this.roomStore.addToRoomProfilesFromList(res.items);

      return res;
    }).catch(_ => {
      this.updateConnectionsInfo({
        ...this.connectionsInfo,
        lastSection: section - 1
      });
    });

    this.updateConnectionsInfo({
      ...this.connectionsInfo,
      fetchStatus: fromPromise(response),
    })
  }

  updateConnectionsInfo(info: ConnectionsInfo) {
    this.connectionsInfo = info;
  }

  addToConnectionsList(connections: RoomProfileWithUserFull[]) {
    connections.forEach(v => {
      this.connections.set(v.user.id, v.roomProfile);
    });
  } 
}

export { LocalStore }