import { useStores } from "$/app/stores";
import { RoomStore } from "$/app/stores/rooms";
import { MainLayout } from "$/layouts/main";
import { mainStore } from "$/pages/main/LocalStore";
import { Content, Header } from "$/pages/main/right/room-content";
import { Badge, Flex } from "@mantine/core";
import { observer } from "mobx-react-lite";

const Right = observer(() => {
  return (
    <MainLayout.Right>
      <RightInner />
    </MainLayout.Right>
  )
});

const RightInner = observer(() => {
  const { rootRoomStores } = useStores();

  const activeRoom = mainStore.activeRoomIdLeft2;

  const roomStore = rootRoomStores.find(activeRoom || -1);

  return (
    <>
      <RightInnerContent isActive={!!roomStore} roomStore={roomStore} />
      <RightInnerNullContent isActive={!!!roomStore} />
    </>
  )
});

interface RightInnerItemProps {
  isActive: boolean;
}

interface RightInnerContentProps extends RightInnerItemProps {
  roomStore: RoomStore;
}

const RightInnerContent = observer(({ isActive, roomStore }: RightInnerContentProps) => {
  if (!isActive) return null;

  return (
    <>
      <MainLayout.Right.Header>
        <Header roomStore={roomStore} />
      </MainLayout.Right.Header>
      <MainLayout.Right.Body>
        <Content />
      </MainLayout.Right.Body>
    </>
  )
});

function RightInnerNullContent({ isActive }: RightInnerItemProps) {
  if (!isActive) return null;

  return (
    <Flex justify="center" align="center" w="100%" h="100%">
      <Badge variant="default">Выберите чат, чтобы начать общение</Badge>
    </Flex>
  )
} 

export { Right }