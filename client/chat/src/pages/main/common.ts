interface ModalTabInfo {
  tab: string;
  title: string;
}

const tabsInfo: { 
  connected: ModalTabInfo,
 } = {
  connected: {
    tab: "connected",
    title: "Подключенные",
  },
}

const rootTab = tabsInfo.connected;

export { tabsInfo, rootTab, type ModalTabInfo }