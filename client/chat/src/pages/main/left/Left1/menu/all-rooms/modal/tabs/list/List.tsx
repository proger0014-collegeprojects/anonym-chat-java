import { useStores } from "$/app/stores";
import { Box, Group, Pagination, ScrollArea, Stack } from "@mantine/core";
import { observer } from "mobx-react-lite"
import { ReactNode } from "react";

interface ListProps {
  header: ReactNode;
  items: ReactNode;
  total: number;
  currentPage: number;
  onChangePage: (page: number) => void;
}

const List = observer(({ 
  header,
  items,
  total,
  currentPage,
  onChangePage }: ListProps) => {
  const { uiStore } = useStores();

  return (
    <Box>
      <Group mb={20} align="top" wrap="nowrap">
        {header}
      </Group>
      <Stack justify="space-between">
        <ScrollArea pr={10} pb={20} h={uiStore.height - 310}>
          {items}
        </ScrollArea>
        <Group h="50px" justify="center" pb={20}>
          <Pagination total={total} value={currentPage} onChange={onChangePage} />
        </Group>
      </Stack>
    </Box>
  )
});

export { List }