import { useStores } from "$/app/stores";
import { getAuthText, getAuthTextFragment } from "$/pages/main/left/Left1/menu/settings/modal/common";
import { Error as ApiError, UpdateAuthUserRequest } from "$/shared/api/contracts";
import { notifySuccess } from "$/shared/lib";
import { Blockquote, Box, Button, Group, Modal, Text, TextInput, Tooltip } from "@mantine/core";
import { UseFormReturnType, useForm } from "@mantine/form";
import { IconInfoCircle } from "@tabler/icons-react";
import { AxiosError } from "axios";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";

interface AuthProps {
  opened: boolean;
  close: () => void;
}

const Auth = observer(({ opened, close }: AuthProps) => {
  if (!opened) return null;

  return (
    <AuthInnerWrapper 
      opened={opened}
      close={close} />
  )
});

interface AuthInnerWrapperProps extends AuthProps { }

const AuthInnerWrapper = observer(({ opened, close  }: AuthInnerWrapperProps) => {
  const { authStore } = useStores();
  
  useEffect(() => {
    return () => {
      authStore.clearUpdateAuthRequest();
    }
  }, [close]);

  const form = useForm<UpdateAuthUserRequest>({
    initialValues: {
      login: '',
      password: ''
    },

    validate: {
      login: (val) => {
        if (val.length <= 0) {
          return 'Поле обязательно';
        } else if (val.length < 5) {
          return 'Поле должно быть не меньше 5 символов';
        } else if (val.length > 255) {
          return 'Поле должно быть не больше 255 символов';
        }
        
        return null;
      },
      password: (val) => {
        if (val.length <= 0) {
          return 'Поле обязательно';
        } else if (val.length < 5) {
          return 'Поле должно быть не меньше 5 символов';
        } else if (val.length > 255) {
          return 'Поле должно быть не больше 255 символов';
        }
        
        return null;
      },
    }
  });

  const login = authStore.user!.login!;

  function onClick(values: UpdateAuthUserRequest) {
    authStore.updateAuthUser(values);

    authStore.updateAuthUserRequest.then(_ => {
      notifySuccess({ message: 'Успешно!' });
      close();
    });

  }
  
  authStore.updateAuthUserRequest.case({
    rejected: (e: AxiosError<ApiError>) => {
      if (e.response?.data.type == '/errors/users/login-is-already-used') {
        form.setFieldError('login', e.response?.data.title);
      }
    }
  });

  const isLoading = authStore.updateAuthUserRequest.case({
    fulfilled: () => false,
    rejected: () => false,
    pending: () => true
  });
  
  return (
    <AuthInner
      opened={opened}
      close={close}
      login={login}
      form={form}
      onClick={onClick}
      loading={isLoading} />
  );
});


interface AuthInnerProps {
  opened: boolean;
  close: () => void;
  login: string;
  form: UseFormReturnType<UpdateAuthUserRequest>;
  onClick: (values: UpdateAuthUserRequest) => void;
  loading: boolean;
}

const AuthInner = observer(({ opened, close, login, form, onClick, loading }: AuthInnerProps) => {
  const { authStore } = useStores();

  const title = (
    <Text fz={24}>{getAuthText(authStore.user!.authType)}</Text>
  )

  return (
    <Modal zIndex={3000} title={title} opened={opened} onClose={close}>
      <Box>
        <Blockquote my={20} py={5} icon={<IconInfoCircle />}>
          Изменение отобразятся здесь спустя 20 минут или меньше
        </Blockquote>
        <Group wrap="nowrap">
          <Text w="35%" fw={700}>Текущий логин: </Text>
          {isNaN(new Number(login) as number) 
            ? (
              <Tooltip label={login} zIndex={4000} style={{ overflowWrap: 'anywhere' }}>
                <Text maw="65%" truncate="end">{login}</Text>
              </Tooltip>
            ) 
            : (
              <Text maw="65%" truncate="end" fz={14} c="gray" fs="italic">Отсутствует</Text>
            )
          }
        </Group>

        <form onSubmit={form.onSubmit(onClick)}>
          <Box my={20}>
            <TextInput 
              withAsterisk
              label="Логин"
              key={form.key('login')}
              { ...form.getInputProps('login') } />
          </Box>

          <Box mb={20}>
            <TextInput
              type="password"
              withAsterisk
              label="Пароль"
              key={form.key('password')}
              { ...form.getInputProps('password') } />
          </Box>

          <Group w="100%" justify="center">
            <Button loading={loading} variant="default" type="submit">{getAuthTextFragment(authStore.user!.authType)}</Button>
          </Group>
        </form>
      </Box>
    </Modal>
  )
});

export { Auth }