import { AllRooms, Appearance } from "$/pages/main/left/Left1/menu";
import { Logout } from "$/pages/main/left/Left1/menu/logout";
import { Settings } from "$/pages/main/left/Left1/menu/settings";
import { Box, BoxComponentProps, Center, Drawer, Stack } from "@mantine/core";
import { IconX } from "@tabler/icons-react";

interface MenuProps {
  opened: boolean;
  onClose: () => void;
  title?: string;
}

function Menu({ opened, onClose, title }: MenuProps) {
  return (
    <Drawer
      keepMounted={false}
      opened={opened}
      onClose={onClose}
      title={
        <Center w="19.5rem" fz="24">
          {title}
        </Center>
      }
      size="23rem"
      overlayProps={{ backgroundOpacity: 0.5, blur: 4 }}
      padding={0}
      closeButtonProps={{ icon: <IconX />, w: "25px", m: "0 auto" }}
    >
      <MenuInner style={{ mt: '25' }} />
    </Drawer>
  )
}

interface MenuInnerProps {
  style: BoxComponentProps;

}

function MenuInner({ style }: MenuInnerProps) {
  return (
    <Box h="100%" { ...style }>
      <Stack h="100%">
        <Box mb={100}>
          <AllRooms />
          <Appearance />
          <Settings />
        </Box>
        <Box>
          <Logout />
        </Box>
      </Stack>
    </Box>
  )
}

export { Menu }