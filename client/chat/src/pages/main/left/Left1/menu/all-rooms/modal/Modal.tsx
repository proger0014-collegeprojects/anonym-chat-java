import { useStores } from "$/app/stores";
import { RoomItem } from "$/components/room";
import { localModalStore } from "$/pages/main/left/Left1/menu/all-rooms/modal/LocalModalStore";
import { Room } from "$/shared/api/contracts";
import { ActionIcon, Box, ButtonProps, Flex, Group, Indicator, Skeleton, Tabs, Text } from "@mantine/core";
import { IconArrowLeft, IconPlus, IconRefresh } from "@tabler/icons-react";
import { observer } from "mobx-react-lite";
import { ReactNode, useEffect, useState } from "react";
import { ModalMainProps } from "$/pages/main/left/Left1/menu/common";
import { MenuModal } from "$/components/common/modals";
import { modals } from "@mantine/modals";
import { ModalTabInfo, rootTab, tabsInfo } from "$/pages/main/left/Left1/menu/all-rooms/modal/common";
import { List, SelectRoomProfile } from "$/pages/main/left/Left1/menu/all-rooms/modal/tabs";
import { ButtonIcon } from "$/components/common/buttons/button-icon";
import { useDisclosure } from "@mantine/hooks";
import { CreateRoomModal } from "$/pages/main/left/Left1/menu/all-rooms/modal/create-room";

function Modal({ opened, close }: ModalMainProps) {

  useEffect(() => {
    return () => {
      localModalStore.resetTab();
    }
  }, [localModalStore.modalTabInfo.tab])

  if (!opened) {
    return null;
  }

  return (
    <ModalWrapper
      opened={opened}
      close={close} />
  )
}

const ModalWrapper = observer(({ opened, close }: ModalMainProps) => {
  const { messagingClientStore, rootRoomStores } = useStores();

  const [openedCreateRoomModal, { open: openCreateRoomModal, close: closeCreateRoomModal }] = useDisclosure(false);

  function handleCreatedNew() {
    localModalStore.created();
  }

  function handleRefresh() {
    localModalStore.refreshPage();
  }

  useEffect(() => {
    localModalStore.fetchPage(1);
    localModalStore.resetCreated();

    if (messagingClientStore.isConnected) {
      messagingClientStore.subscribe('/room/created', handleCreatedNew);
    }

    return () => {
      if (messagingClientStore.isConnected) {
        messagingClientStore.unsubscribe('/room/created');
      }
    }
  }, [messagingClientStore.isConnected])

  function handleChangePage(page: number) {
    localModalStore.resetCreated();
    localModalStore.fetchPage(page);
  }

  const items: ReactNode = localModalStore.currentPage.case({
    fulfilled: (v) => {
      const components = v.items.map(i => (
        <Box mb={20} key={i.id} my={10} w="37rem">
          <RoomListItem 
            room={i} />
        </Box>
      ));

      return (
        <>
          {components}
        </>
      );
    },
    pending: () => {
      const count = 3;

      const component = (
        <Box mt={15}>
          <Skeleton height={16} radius="xs" />
          <Skeleton height={14} radius="xs" my={15} />
          <Group justify="space-between">
            <Skeleton height={14} width="30%" radius="xs" />
            <Skeleton height={14} width="10%" radius="xs" />
          </Group>
        </Box>
      );

      const components = [];

      for (let i = 0; i < count; i++) {
        components.push(<Box mb={20} key={i}>
          {component}
        </Box>);
      }

      return (
        <>
          {components}
        </>
      )
    },
    rejected: () => {
      return (
        <></>
      )
    }
  });

  function onRoomProfileInstalled(roomId: number) {
    localModalStore.resetTab();

    rootRoomStores.find(roomId).connectToRoom();
  }

  const total = localModalStore.currentPage.case({
    pending: () => localModalStore.pendingPageInfo.totalPages,
    fulfilled: (v) => v.lastSection,
    rejected: () => -1
  })

  const currentPage = localModalStore.currentPage.case({
    pending: () => localModalStore.pendingPageInfo.currentPage,
    fulfilled: (v) => v.section,
    rejected: () => -1
  })

  const headerModal = getModalHeader(localModalStore.createdNew, handleRefresh, openCreateRoomModal);

  return (
    <>
      <CreateRoomModal 
        opened={openedCreateRoomModal}
        close={closeCreateRoomModal} />
      <ModalWrapperInner
        onRoomProfileInstalled={onRoomProfileInstalled}
        modalTab={localModalStore.modalTabInfo}
        opened={opened}
        close={close}
        items={items}
        total={total}
        currentPage={currentPage}
        handleChangePage={handleChangePage}
        headerModal={headerModal} />
    </>
  )
});

interface ListItemProps {
  room: Room;
}

const RoomListItem = observer(({ room }: ListItemProps) => {
  const [countConnectedUsersState, setCountConnectedUsersState] = useState(room.countConnectedUsers);
  
  const { messagingClientStore, rootRoomStores } = useStores();

  useEffect(() => {
    rootRoomStores.addRoom(room);
  }, [rootRoomStores]);

  function handleConnectedUsers() {
    setCountConnectedUsersState(v => v + 1);
  }

  function handleDisconnectedUsers() {
    setCountConnectedUsersState(v => v - 1);
  }

  useEffect(() => {
    if (messagingClientStore.isConnected) {
      messagingClientStore.subscribe(`/room/${room.id}/connected-user`, handleConnectedUsers);
      messagingClientStore.subscribe(`/room/${room.id}/disconnected-user`, handleDisconnectedUsers);
    }
    
    return () => {
      if (messagingClientStore.isConnected) {
        messagingClientStore.unsubscribe(`/room/${room.id}/connected-user`);
        messagingClientStore.unsubscribe(`/room/${room.id}/disconnected-user`);
      }
    }
  }, [messagingClientStore.isConnected])

  const roomStore = rootRoomStores.find(room.id);

  function onClickHandle() {
    roomStore?.connectToRoom({ 
      onError: () => {
        if (roomStore.connectionInfo?.status == 'not-selected-room-profile' || false)
        modals.openConfirmModal({
          zIndex: 10_000,
          title: "Подключение к комнате",
          children: (
            <Text>
              Не был выбран профиль комнаты. Желаете выбрать?
            </Text>
          ),
          labels: { confirm: 'Да', cancel: 'Нет' },
          onConfirm: () => {
            localModalStore.setSelectRoomId(room.id);
            localModalStore.updateTab(tabsInfo.selectRoomProfile);
          }
        })
      } 
    });
  }
  
  
  let buttonInfo: {
    buttonProps: ButtonProps,
    buttonText: string
  } = {
    buttonProps: { variant: 'default', miw: 150, c: 'white' },
    buttonText: 'Подключиться'
  }

  switch (roomStore?.connectionInfo?.status) {
    case 'already-connected':
      buttonInfo = {
        buttonProps: { ...buttonInfo.buttonProps, bg: 'orange', disabled: true },
        buttonText: 'Уже подключен'
      }
      break;
    case 'connected':
      buttonInfo = {
        buttonProps: { ...buttonInfo.buttonProps, bg: 'green', disabled: true },
        buttonText: 'Подключено'
      };
      break;
    case 'internal-server-error':
      buttonInfo = {
        buttonProps: { ...buttonInfo.buttonProps, bg: 'red' },
        buttonText: 'Неизвестная ошибка'
      }
      break;
    case 'not-selected-room-profile':
      buttonInfo = {
        buttonProps: { ...buttonInfo.buttonProps, bg: 'orange' },
        buttonText: 'Не выбран профиль комнаты'
      }
      break;
  }

  return (
    <RoomItem
      buttonText={buttonInfo.buttonText}
      buttonProps={buttonInfo.buttonProps}
      isConnecting={roomStore?.connectionInfo && roomStore.connectionInfo?.status == 'connection'}
      {...room}
      countConnectedUsers={countConnectedUsersState}
      onClick={onClickHandle} />
  )
});

function getModalHeader(createdNew: number, handleRefreshPage: () => void, onClickNew: () => void) {
  const buttonNew = (
    <>
      <ActionIcon onClick={onClickNew} variant="default">
        <IconPlus />
      </ActionIcon>
    </>
  );

  const buttonRefresh = createdNew > 0
  ? (
    <Indicator zIndex={2000} inline color="red" label={createdNew} size={18}>
      <ActionIcon onClick={handleRefreshPage} variant="default">
        <IconRefresh />
      </ActionIcon>
    </Indicator>
  )
  : (
    <ActionIcon onClick={handleRefreshPage} variant="default">
      <IconRefresh />
    </ActionIcon>
  );

  return (
    <>
      {buttonNew}
      {buttonRefresh}
    </>
  )
}

interface ModalWrapperInnerProps extends ModalMainProps {
  items: ReactNode[] | ReactNode
  total: number;
  currentPage: number;
  handleChangePage: (page: number) => void;
  modalTab: ModalTabInfo;
  onRoomProfileInstalled: (roomId: number) => void;
  headerModal: ReactNode;
}

const ModalWrapperInner = observer(({ 
  modalTab,
  opened, 
  close, 
  items, 
  total, 
  currentPage, 
  headerModal,
  handleChangePage, 
  onRoomProfileInstalled }: ModalWrapperInnerProps) => {

  const title = (
    <Box mb={10}>
      <Flex mb={10} align="center">
        {localModalStore.modalTabInfo.tab != rootTab.tab && (
          <Box mr={10}>
            <ButtonIcon
              icon={<IconArrowLeft />}
              onClick={() => localModalStore.resetTab()} />
          </Box>
        )}
        <Text fz={24}>{modalTab.title}</Text>
      </Flex>
    </Box>
  )

  return (
    <MenuModal opened={opened} onClose={close} title={title}>
      <Tabs value={modalTab.tab}>
        <Tabs.Panel value={tabsInfo.list.tab}>
          <List
            header={headerModal}
            items={items}
            total={total}
            currentPage={currentPage}
            onChangePage={handleChangePage} />
        </Tabs.Panel>
        <Tabs.Panel value={tabsInfo.selectRoomProfile.tab}>
          <SelectRoomProfile 
            onRoomProfileInstalled={onRoomProfileInstalled} />
        </Tabs.Panel>
      </Tabs>
    </MenuModal>
  )
});

export { Modal }