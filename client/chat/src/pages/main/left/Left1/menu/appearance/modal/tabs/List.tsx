import { Box } from "@mantine/core"
import { ModalTabInfo, tabsInfo } from "$/pages/main/left/Left1/menu/appearance/modal/tabs/common"
import { ButtonIconV2 } from "$/components/common/buttons/button-icon"
import { localModalStore } from "$/pages/main/left/Left1/menu/appearance/modal/LocalModalStore"
import { observer } from "mobx-react-lite"

const List = observer(() => {
  function clickButton(tab: ModalTabInfo) {
    localModalStore.updateModalTab(tab);
  }

  return (
    <Box>
      <ButtonIconV2 height="60px" onClick={() => clickButton(tabsInfo.roomProfiles)}>Профили комнат</ButtonIconV2>
      <ButtonIconV2 height="60px" onClick={() => clickButton(tabsInfo.avatars)}>Аватарки</ButtonIconV2>
    </Box>
  )
});

export { List }