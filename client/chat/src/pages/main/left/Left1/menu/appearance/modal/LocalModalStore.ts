import { makeAutoObservable } from "mobx";
import { ModalTabInfo, rootTab } from "$/pages/main/left/Left1/menu/appearance/modal/tabs";

class LocalModalStore {
  currentModalTab: ModalTabInfo;

  constructor() {
    makeAutoObservable(this);

    this.currentModalTab = rootTab;
  }

  updateModalTab(tab: ModalTabInfo) {
    this.currentModalTab = tab;
  }

  resetModalTab() {
    this.currentModalTab = rootTab;
  }
}

const localModalStore = new LocalModalStore();

export { localModalStore, type ModalTabInfo }