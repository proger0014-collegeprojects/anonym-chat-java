import { ButtonIcon } from "$/components/common/buttons/button-icon";
import { MenuModal } from "$/components/common/modals";
import { LocalStore } from "$/pages/main/left/Left1/menu/settings/modal/LocalStore";
import { rootTab, tabs, Context, useStore } from "$/pages/main/left/Left1/menu/settings/modal/common";
import { List } from "$/pages/main/left/Left1/menu/settings/modal/tabs/list";
import { Box, Flex, Tabs, Text } from "@mantine/core";
import { IconArrowLeft } from "@tabler/icons-react";
import { observer } from "mobx-react-lite";



interface ModalProps {
  opened: boolean;
  close: () => void;
}

const Modal = observer(({ opened, close }: ModalProps) => {
  if (!opened) return null;

  const localStore = new LocalStore();

  return (
    <Context.Provider value={localStore}>
      <ModalInnerWrapper 
        opened={opened}
        close={close} />
    </Context.Provider>
  )
});

interface ModalInnerWrapperProps {
  opened: boolean;
  close: () => void;
}

const ModalInnerWrapper = observer(({ opened, close }: ModalInnerWrapperProps) => {


  return (
    <ModalInner
      opened={opened}
      close={close} />
  )
});

interface ModalInnerProps {
  opened: boolean;
  close: () => void;
}

const ModalInner = observer(({ opened, close }: ModalInnerProps) => {
  const localModalStore = useStore();

  const title = (
    <Box>
      <Flex mb={10} align="center">
        {localModalStore!.currentTab.tab != rootTab.tab && (
          <Box mr={10}>
            <ButtonIcon 
              icon={<IconArrowLeft />}
              onClick={() => localModalStore!.resetTabs()} />
          </Box>
        )}
        <Text fz={24}>
          {localModalStore!.currentTab.title}
        </Text>
      </Flex>
    </Box>
  )

  return (
    <MenuModal title={title} opened={opened} onClose={close}>
      <Tabs value={localModalStore!.currentTab.tab}>
        <Tabs.Panel value={tabs.list.tab}>
          <List />
        </Tabs.Panel>
      </Tabs>
    </MenuModal>
  )
});

export { Modal }