import { ModalTabInfo } from "$/pages/main/common";
import { LocalStore } from "$/pages/main/left/Left1/menu/settings/modal/LocalStore";
import { createContext, useContext } from "react";

const Context = createContext<LocalStore|undefined>(undefined);

function useStore() {
  return useContext(Context);
}

const tabs: {
  list: ModalTabInfo
} = {
  list: {
    tab: 'list',
    title: 'Настройки'
  }
}

function getAuthTextFragment(authType: string) {
  return authType == 'anonym'
  ? 'Установить'
  : 'Обновить';

}

function getAuthText(authType: string) {
  const textFragment = getAuthTextFragment(authType);

  return '{} логин и пароль'.replace('{}', textFragment);
}

const rootTab = tabs.list;

export { tabs, rootTab, useStore, Context, getAuthText, getAuthTextFragment }