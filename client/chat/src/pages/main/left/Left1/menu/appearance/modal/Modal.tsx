import { MenuModal } from "$/components/common/modals";
import { ModalMainProps } from "$/pages/main/left/Left1/menu/common"
import { Box, Flex, Tabs, Text } from "@mantine/core";
import { observer } from "mobx-react-lite"
import { ModalTabInfo, localModalStore } from "$/pages/main/left/Left1/menu/appearance/modal/LocalModalStore";
import { List, RoomProfiles, rootTab, tabsInfo } from "$/pages/main/left/Left1/menu/appearance/modal/tabs";
import { useEffect } from "react";
import { ButtonIcon } from "$/components/common/buttons/button-icon";
import { IconArrowLeft } from "@tabler/icons-react";
import { Avatars, LocalStore as AvatarsLocalStore } from "$/pages/main/left/Left1/menu/appearance/modal/tabs/avatars";
import { useStores } from "$/app/stores";

const Modal = observer(({ opened, close }: ModalMainProps) => {
  if (!opened) {
    return null;
  }

  return (
    <ModalWrapper
      opened={opened}
      close={close} />
  )
})

const ModalWrapper = observer(({ opened, close }: ModalMainProps) => {
  const { messagingClientStore } = useStores();

  useEffect(() => {
    localModalStore.resetModalTab();
  }, [opened])

  const localAvatarsStore = new AvatarsLocalStore(messagingClientStore);

  return (
    <ModalWrapperInner
      localAvatarsStore={localAvatarsStore}
      opened={opened}
      close={close}
      currentTab={localModalStore.currentModalTab} />
  )
});

interface ModalWrapperInnerProps extends ModalMainProps {
  currentTab: ModalTabInfo;
  localAvatarsStore: AvatarsLocalStore;
}

const ModalWrapperInner = observer(({ 
  opened,
  close,
  currentTab,
  localAvatarsStore
 }: ModalWrapperInnerProps) => {

  const title = (
    <Box>
      <Flex mb={10} align="center">
        {localModalStore.currentModalTab.tab != rootTab.tab && (
          <Box mr={10}>
            <ButtonIcon 
              icon={<IconArrowLeft />}
              onClick={() => localModalStore.resetModalTab()} />
          </Box>
        )}
        <Text fz={24}>
          {currentTab.title}
        </Text>
      </Flex>
    </Box>
  )

  return (
    <MenuModal opened={opened} onClose={close} title={title}>
      <Tabs h="100%" value={currentTab.tab}>
        <Tabs.Panel value={tabsInfo.list.tab}>
          <List />
        </Tabs.Panel>
        <Tabs.Panel value={tabsInfo.roomProfiles.tab}>
          <RoomProfiles />
        </Tabs.Panel>
        <Tabs.Panel h="100%" value={tabsInfo.avatars.tab}>
          <Avatars store={localAvatarsStore} />
        </Tabs.Panel>
      </Tabs>
    </MenuModal>
  )
});

export { Modal }