import { useStores } from "$/app/stores";
import { RoomProfilesWidget, LocalWidgetStore } from "$/components/room/profile/widget"
import { localModalStore } from "$/pages/main/left/Left1/menu/all-rooms/modal/LocalModalStore";
import { RoomProfileFull } from "$/shared/api/contracts";
import { Button, Group } from "@mantine/core";
import { observer } from "mobx-react-lite";
import { useState } from "react";

interface SelectRoomProfileProps {
  onRoomProfileInstalled: (roomId: number) => void;
}

const SelectRoomProfile = observer(({ onRoomProfileInstalled }: SelectRoomProfileProps) => {
  const localWidgetStore = new LocalWidgetStore();

  return (
    <SelectRoomProfileInner 
      localWidgetStore={localWidgetStore}
      onRoomProfileInstalled={onRoomProfileInstalled} />
  )
});

interface SelectRoomProfileInnerProps {
  localWidgetStore: LocalWidgetStore;
  onRoomProfileInstalled: (roomId: number) => void;
}

const SelectRoomProfileInner = observer(({ 
  localWidgetStore, 
  onRoomProfileInstalled }: SelectRoomProfileInnerProps) => {
    
  return (
    <RoomProfilesWidget 
      contextProps={{ localWidgetStore: localWidgetStore }}
      footerItem={(data) => {
        const Component = observer(({ data }: { data: RoomProfileFull }) => {
          const [isLoading, setLoading] = useState(false);
          const { rootRoomStores } = useStores();

          const roomStore = rootRoomStores.find(localModalStore?.selectRoomId ?? -1);

          function onClick() {
            setLoading(true);

            roomStore.installRoomProfile(data.id, {
              onSuccess: () => {
                onRoomProfileInstalled(roomStore.id);
                setLoading(false);
              },
              onError: () => {
                setLoading(false);
              }
            });

            setLoading(roomStore.installRoomProfileInfo?.status == 'processing');
          }

          return (
            <Group>
              <Button onClick={onClick} loading={isLoading} variant="default" size="compact-xs">
                Выбрать
              </Button>
            </Group>
          )
        });

        return (
          <>
            <Component data={data} />
          </>
        )
      }} />
  )
});

export { SelectRoomProfile }