import { LocalStore } from "$/pages/main/left/Left1/menu/all-rooms/modal/create-room/LocalStore";
import { CreateRoomRequest } from "$/shared/api/contracts";
import { Box, Button, Group, Modal, Text, TextInput, Textarea } from "@mantine/core";
import { UseFormReturnType, useForm } from "@mantine/form";
import { observer } from "mobx-react-lite";
import { ReactNode, createContext, useContext } from "react";

interface CreateRoomModalProps {
  opened: boolean;
  close: () => void;
}

const Context = createContext<LocalStore|undefined>(undefined);

function useStore() {
  return useContext(Context);
}

const CreateRoomModal = observer(({ opened, close }: CreateRoomModalProps) => {
  if (!opened) return null;

  const localStore = new LocalStore();

  return (
    <Context.Provider value={localStore}>
      <CreateRoomModalInnerWrapper 
        opened={opened}
        close={close} />
    </Context.Provider>
  )
});

interface CreateRoomModalInnerWrapperProps {
  opened: boolean;
  close: () => void;
}

const CreateRoomModalInnerWrapper = observer(({ opened, close }: CreateRoomModalInnerWrapperProps) => {
  const localStore = useStore()!;

  const form = useForm<CreateRoomRequest>({
    initialValues: {
      name: "",
      description: undefined
    },
    validate: {
      name: (val) => {
        if (val.length <= 0) {
          return "Поле обязательно";
        } else if (val.length < 5) {
          return "Поле должно содержать не менее 5 символов";
        } else if (val.length > 300) {
          return "Поле не должно превышать длину 300 символов"
        }

        return null;
      },
      description: (val) => {
        if (!val) return null;

        if (val!.length < 5) {
          return "Поле должно содержать не менее 5 символов";
        } else if (val!.length > 600) {
          return 'Поле не должно превышать длину 600 символов';
        }

        return null;
      }
    }
  });

  const onClickCreate = (val: CreateRoomRequest) => {
    console.log('hui');
    

    localStore.createRoom(val);

    localStore.createRoomRequest.then(_ => {
      close();
    });
  }

  const title = (
    <Text fz={24}>
      Создание комнаты
    </Text>
  )

  const isLoading = localStore.createRoomRequest.case({
    pending: () => true,
    rejected: () => false,
    fulfilled: () => false
  })

  return (
    <CreateRoomModalInner 
      opened={opened}
      close={close}
      title={title}
      form={form}
      onClickCreate={onClickCreate}
      isLoading={isLoading} />
  )
});

interface CreateRoomModalInnerProps {
  opened: boolean;
  close: () => void;
  title: ReactNode;
  onClickCreate: (val: CreateRoomRequest) => void;
  form: UseFormReturnType<CreateRoomRequest>;
  isLoading: boolean;
}

const CreateRoomModalInner = observer(({ opened, close, title, onClickCreate, form, isLoading }: CreateRoomModalInnerProps) => {

  return (
    <Modal zIndex={3000} opened={opened} onClose={close} title={title}>
      <Box my={20}>
        <form onSubmit={form.onSubmit(onClickCreate)}>
          <TextInput
            withAsterisk
            mb={20}
            label="Название"
            key={form.key('name')}
            { ...form.getInputProps('name') } />

          <Textarea
            mb={40}
            label="Описание"
            key={form.key('description')}
            { ...form.getInputProps('description') }
            rows={5} />

          <Group w="100%" justify="center">
            <Button loading={isLoading} type="submit" w={200} variant="default">
              Создать
            </Button>
          </Group>
        </form>
      </Box>
    </Modal>
  )
});

export { CreateRoomModal }