import { makeAutoObservable } from "mobx";
import { IPromiseBasedObservable, fromPromise } from "mobx-utils";
import { createAvatar as apiCreateAvatar } from "$/shared/api/avatar";
import { CreateAvatarRequest } from "$/shared/api/contracts";

interface LoadFileInfo {
  data: Blob|File;
  url: string|undefined;
}

class LocalStore {
  createAvatarRequestStatus: IPromiseBasedObservable<any>;
  loadFile?: LoadFileInfo;

  constructor() {
    makeAutoObservable(this);

    this.createAvatarRequestStatus = fromPromise(Promise.resolve());
  }

  createAvatarFromInnerData() {
    if (!this.loadFile) return;

    this.createAvatar({
      file: this.loadFile?.data,
      offsetX: 0,
      offsetY: 0,
      size: 100
    })
  }

  createAvatar(req: CreateAvatarRequest) {
    const response = apiCreateAvatar(req);

    this.updateRequestStatus(response);
  }

  updateRequestStatus(promise: Promise<any>) {
    this.createAvatarRequestStatus = fromPromise(promise);
  }

  updateLoadFile(file: LoadFileInfo) {
    if (this.loadFile?.url == file.url) return;

    this.loadFile = file;
  }
}

export { LocalStore as CreateAvatarLocalStore }