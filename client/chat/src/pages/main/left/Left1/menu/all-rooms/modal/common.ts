interface ModalTabInfo {
  tab: string;
  title: string;
}

const tabsInfo: { 
  list: ModalTabInfo,
  selectRoomProfile: ModalTabInfo,
 } = {
  list: {
    tab: "list",
    title: "Все комнаты",
  },
  selectRoomProfile: {
    tab: "select-room-profile",
    title: "Выбор профиля комнаты",
  },
}

const rootTab = tabsInfo.list;

export { tabsInfo, rootTab, type ModalTabInfo }