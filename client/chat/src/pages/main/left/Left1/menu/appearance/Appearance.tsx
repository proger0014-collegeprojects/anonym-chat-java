import { ButtonIconV2 } from "$/components/common/buttons/button-icon"
import { IconUserCircle } from "@tabler/icons-react"
import { Modal } from "$/pages/main/left/Left1/menu/appearance/modal";
import { useDisclosure } from "@mantine/hooks";

function Appearance() {
  const [opened, { open, close }] = useDisclosure(false);

  return (
    <>
      <Modal opened={opened} close={close} />
      <AppearanceInner onClick={open} />
    </>
  )
}

interface AppearanceInnerProps {
  onClick: () => void;
}

function AppearanceInner({ onClick }: AppearanceInnerProps) {
  return (
    <ButtonIconV2
      icon={<IconUserCircle size={30} />}
      height="60px"
      onClick={onClick}>
        Внешность
    </ButtonIconV2>
  )
}

export { Appearance }