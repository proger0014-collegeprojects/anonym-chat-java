import { AvatarLeftImage } from "$/components/common/avatar-left-image";
import { DropzoneLocal } from "$/components/common/drop-zones";
import { ImageChanger } from "$/components/room/profile/avatars/image-changer";
import { CreateAvatarLocalStore } from "$/pages/main/left/Left1/menu/appearance/modal/tabs/avatars/create-avatar-modal/LocalStore";
import { notifySuccess } from "$/shared/lib";
import { getFileUrl } from "$/shared/utils";
import { Box, Button, Group, Modal, ScrollArea, Stack, Text, UnstyledButton } from "@mantine/core";
import { FileWithPath, IMAGE_MIME_TYPE } from "@mantine/dropzone";
import { useDisclosure } from "@mantine/hooks";
import { observer } from "mobx-react-lite";
import { ReactNode, createContext, useContext, useRef } from "react";

interface CreateAvatarModalProps {
  isOpened: boolean;
  onClose: () => void;
}

const Context = createContext<CreateAvatarLocalStore|undefined>(undefined);

function useLocalStore() {
  return useContext(Context);
}

const CreateAvatarModal = observer(({ isOpened, onClose }: CreateAvatarModalProps) => {
  if (!isOpened) return null;

  const localStore = new CreateAvatarLocalStore();

  return (
    <Context.Provider value={localStore}>
      <CreateAvatarModalInnerWrapper 
        isOpened={isOpened}
        onClose={onClose} />
    </Context.Provider>
  )
})

interface CreateAvatarModalInnerWrapperProps {
  isOpened: boolean;
  onClose: () => void;
}

const CreateAvatarModalInnerWrapper = observer(({ isOpened, onClose }: CreateAvatarModalInnerWrapperProps) => {
  const localCreateAvatarStore = useLocalStore()!;
  
  const openFileLoadRef = useRef<() =>  void>(null);
  
  function openFileLoad() {
    openFileLoadRef.current?.();
  }

  function onDropFile(f: FileWithPath[]) {
    getFileUrl(f[0], (res) => {
      localCreateAvatarStore.updateLoadFile({
        url: res!.toString(),
        data: f[0]
      });
    });
  }

  function onCropImage(blob: Blob, url: string|undefined) {
    localCreateAvatarStore.updateLoadFile({
      url: url ?? '',
      data: blob
    })
  }

  const canClick = localCreateAvatarStore.loadFile != undefined;

  function onClickSend() {
    if (!canClick) return;

    localCreateAvatarStore.createAvatarFromInnerData();

    localCreateAvatarStore.createAvatarRequestStatus.then(_ => {
      notifySuccess({
        message: 'Аватар создан успешно!'
      });

      onClose();
    })
  }

  const isLoading = localCreateAvatarStore.createAvatarRequestStatus.case({
    pending: () => true,
    fulfilled: () => false,
    rejected: () => false
  });

  return (
    <CreateAvatarModalInner
      isLoading={isLoading}
      onClickSend={onClickSend}
      canClickSend={canClick}
      openFileLoadRef={openFileLoadRef}
      openFileLoad={openFileLoad}
      onDropFile={onDropFile}
      onCrop={onCropImage}
      close={onClose}
      opened={isOpened} />
  )
});

const WrapperComponentObservable = observer(({ target, active, inactive, isActive, openFileLoad, onCrop, onClickSend, canClickSend, isLoading }: {
  target: ReactNode; 
  active: () => void; 
  inactive: () => void; 
  isActive: boolean;
  openFileLoad: (fn: () => void) => void;
  onCrop: (blob: Blob, url: string|undefined) => void;
  onClickSend: () => void;
  canClickSend: boolean;
  isLoading: boolean;
}) => {
  const localCreateAvatarStore = useLocalStore()!;

  const [openedImageChanger, { toggle: toggleImageChanger }] = useDisclosure(false);

  return (
    <Box 
      pos="relative" 
      onDragEnter={active}  
      onMouseLeave={inactive}>
      <Group justify="center" align="center">
        {
          isActive && (
            <Box p={20} top={0} style={{ zIndex: 2_000 }} w="300px" h="300px" pos="absolute">
              {target}
            </Box>
          )
        }
      </Group>
      <Stack w="100%" align="center" mb={20}>
        {!openedImageChanger && (
          <Group mb={20} align="center" justify="center">
            <UnstyledButton onClick={() => openFileLoad(active)}>
              <AvatarLeftImage 
                src={localCreateAvatarStore?.loadFile?.url}
                containerProps={{ w: 300, h: 300 }} />
            </UnstyledButton>
          </Group>
        )}

        {openedImageChanger && (
          <ImageChanger
            src={localCreateAvatarStore?.loadFile?.url ?? ''}
            setterResult={onCrop} />
        )}

        {localCreateAvatarStore?.loadFile?.url && (
          <Button variant={openedImageChanger ? 'filled' : 'default'} onClick={toggleImageChanger}>Изменить картинку</Button>
        )}
      </Stack>

      <Box mt={20}>
        <form>
          <Stack justify="center">
            <Button loading={isLoading} variant="default" mt={20} disabled={!canClickSend} onClick={onClickSend}>Создать</Button>
          </Stack>
        </form>
      </Box>
    </Box>
  )
});

interface CreateAvatarModalInnerProps {
  openFileLoad: () => void,
  openFileLoadRef: React.RefObject<() => void>;
  onDropFile: (f: FileWithPath[]) => void;
  onCrop: (blob: Blob, url: string|undefined) => void;
  close: () => void;
  opened: boolean;
  onClickSend: () => void;
  canClickSend: boolean;
  isLoading: boolean;
}

const CreateAvatarModalInner = observer(({ 
  opened, 
  close, 
  openFileLoad,
  openFileLoadRef,
  onDropFile,
  onCrop,
  onClickSend,
  canClickSend,
  isLoading }: CreateAvatarModalInnerProps) => {
  const title = (
    <Text fz={24}>
      Создание аватарки
    </Text>
  );

  function openFileLoadDecorate(active: () => void) {
    active();
    openFileLoad();
  }

  return (
    <Modal title={title} zIndex={3000} opened={opened} onClose={close}>
      <ScrollArea.Autosize mah="76vh" w={400}>
        <DropzoneLocal
        onDrop={onDropFile}
        isMultiple={false}
        openRef={openFileLoadRef}
        radius="50%"
        acceptMimeTypes={IMAGE_MIME_TYPE}
        wrapper={(target, active, inactive, isActive) => {

          return (
            <WrapperComponentObservable
              isLoading={isLoading}
              canClickSend={canClickSend}
              onClickSend={onClickSend}
              target={target}
              active={active}
              inactive={inactive}
              isActive={isActive}
              onCrop={onCrop}
              openFileLoad={openFileLoadDecorate} />
          )
        }}>
          <Group justify="center" align="center" w="100%" h="100%">
            <DropzoneLocal.Idle>Внести картинку</DropzoneLocal.Idle>
            <DropzoneLocal.Accept>Успешно</DropzoneLocal.Accept>
            <DropzoneLocal.Reject>Не то</DropzoneLocal.Reject>
          </Group>
        </DropzoneLocal>
      </ScrollArea.Autosize>
    </Modal>
  )
});

export { CreateAvatarModal }