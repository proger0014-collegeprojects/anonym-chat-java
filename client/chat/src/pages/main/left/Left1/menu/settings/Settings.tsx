import { ButtonIconV2 } from "$/components/common/buttons/button-icon";
import { Modal } from "$/pages/main/left/Left1/menu/settings/modal";
import { useDisclosure } from "@mantine/hooks";
import { IconSettings } from "@tabler/icons-react";
import { observer } from "mobx-react-lite";

const Settings = observer(() => {
  const [opened, { open, close }] = useDisclosure(false);

  return (
    <>
      <Modal 
        opened={opened}
        close={close} />
      <SettingsInner onClick={open}/>
    </>
  )
});

interface SettingsInnerProps {
  onClick: () => void;
}

const SettingsInner = observer(({ onClick }: SettingsInnerProps) => {

  return (
    <ButtonIconV2
      icon={<IconSettings size={30} />}
      height="60px"
      onClick={onClick}>
      Настройки
    </ButtonIconV2>
  )
});

export { Settings }