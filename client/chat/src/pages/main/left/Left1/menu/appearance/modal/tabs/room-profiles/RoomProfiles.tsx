import { localWidgetStore } from "$/pages/main/left/Left1/menu/appearance/modal/tabs/room-profiles/common";
import { RoomProfilesWidget } from "$/components/room/profile/widget";
import { observer } from "mobx-react-lite";

function RoomProfiles() {
  const RoomProfilesInnerObservable = observer(RoomProfilesInner);

  return (
    <>
      <RoomProfilesInnerObservable  />
    </>
  )
}

function RoomProfilesInner() {

  return (
    <RoomProfilesWidget 
      contextProps={{ localWidgetStore: localWidgetStore }}
    />
  )
}

export { RoomProfiles }