import { useStores } from "$/app/stores";
import { ButtonIconV2 } from "$/components/common/buttons/button-icon";
import { getAuthText } from "$/pages/main/left/Left1/menu/settings/modal/common";
import { Auth } from "$/pages/main/left/Left1/menu/settings/modal/modals/auth";
import { Box } from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import { observer } from "mobx-react-lite";

const List = observer(() => {
  const { authStore } = useStores();
  const [openedAuth, {open: openAuth, close: closeAuth}] = useDisclosure(false);


  return (
    <Box>
      <>
        <Auth opened={openedAuth} close={closeAuth} />
        <ButtonIconV2 height={60} onClick={openAuth}>
          {getAuthText(authStore.user!.authType)}
        </ButtonIconV2>
      </>
    </Box>
  );
})

export { List }