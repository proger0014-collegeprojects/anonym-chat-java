import { useStores } from "$/app/stores";
import { ButtonIconV2 } from "$/components/common/buttons/button-icon";
import { Text } from "@mantine/core";
import { openConfirmModal } from "@mantine/modals";
import { IconLogout } from "@tabler/icons-react";
import { observer } from "mobx-react-lite";

const Logout = observer(() => {
  const { authStore } = useStores();

  function onClick() {
    openConfirmModal({
      title: 'Выход из системы',
      children: (
        <Text>Вы действительно хотите выйти из системы?</Text>
      ),
      labels: { confirm: 'Да', cancel: 'Нет' },
      onConfirm: () => authStore.logout()
    });
  }

  return (
    <>
      <LogoutInner onClick={onClick} />
    </>
  )
});

interface LogoutInnerProps {
  onClick: () => void;
}

const LogoutInner = observer(({ onClick }: LogoutInnerProps) => {
  return (
    <ButtonIconV2
      icon={<IconLogout size={30} />}
      height={60}
      onClick={onClick}
      buttonProps={{ c: "red.6" }}>
      Выйти
    </ButtonIconV2>
  )
});

export { Logout }