import { MessagingClientStore } from "$/app/stores";
import { getAvatarsSection } from "$/shared/api/avatar";
import { Avatar } from "$/shared/api/contracts";
import { predicateToNewSection } from "$/shared/utils";
import { makeAutoObservable, observe } from "mobx";
import { IPromiseBasedObservable, fromPromise } from "mobx-utils";

function predicateIsExists(source: Avatar[], target: Avatar) {
  if (source.filter(s => s.id == target.id).length > 0) return true;

  return false;
}

class LocalStore {
  avatars: Avatar[];
  fetchStatus: IPromiseBasedObservable<void>;
  lastSection: number;
  totalSections: number;
  messagingClientStore: MessagingClientStore;

  constructor(messagingClientStore: MessagingClientStore) {
    makeAutoObservable(this);

    this.avatars = [];
    this.fetchStatus = fromPromise(new Promise(_ => {}));
    this.lastSection = 0;
    this.totalSections = 0;
    this.messagingClientStore = messagingClientStore;

    this.subscribe();
  }

  subscribe() {
    observe(this, () => {
      if (!this.messagingClientStore.isConnected) return;

      this.messagingClientStore.subscribe('/concrete-user/avatar/created', (m) => {
        if (this.lastSection < this.totalSections) return;

        if (predicateToNewSection(this.avatars.length)) {
          this.updateLastSection(this.lastSection + 1);
          this.updateTotalSections(this.totalSections + 1);
        }
          
        const avatar = JSON.parse(m.body) as Avatar;

        this.updateAvatarsList([avatar]);
      }, 'local-avatars-store-modal');
    });
  }

  updateAvatarsList(avatars: Avatar[]) {
    const avatarsToAppend = avatars.filter(a => !predicateIsExists(this.avatars, a));

    if (avatarsToAppend.length <= 0) return;
    
    this.avatars = [...this.avatars, ...avatarsToAppend];
  }

  fetchSection(section: number) {
    if (this.lastSection == section) return;

    const response = getAvatarsSection(section).then(data => {
      this.updateLastSection(section);
      this.updateTotalSections(data.lastSection);
      this.updateAvatarsList(data.items);

      return data;
    });

    this.updateFetchStatus(response);
  }

  updateFetchStatus(promise: Promise<any>) {
    this.fetchStatus = fromPromise(promise);
  }

  updateLastSection(section: number) {
    if (this.lastSection == section) return;

    this.lastSection = section;
  }

  updateTotalSections(total: number) {
    if (this.totalSections == total) return;

    this.totalSections = total;
  }

  unuseMessagingClientStore() {
    this.messagingClientStore.unsubscribe('/concrete-user/avatar/created', 'local-avatars-store-modal');
  }
}

export { LocalStore }