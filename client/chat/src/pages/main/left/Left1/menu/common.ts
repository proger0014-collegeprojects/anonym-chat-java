interface ModalMainProps {
  opened: boolean;
  close: () => void;
}

export type { ModalMainProps }