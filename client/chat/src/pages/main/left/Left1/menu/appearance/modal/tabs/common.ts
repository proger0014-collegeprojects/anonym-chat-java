interface ModalTabInfo {
  tab: string;
  title: string;
}

const tabsInfo: { 
  list: ModalTabInfo,
  roomProfiles: ModalTabInfo,
  avatars: ModalTabInfo
 } = {
  list: {
    tab: "list",
    title: "Внешность",
  },
  roomProfiles: {
    tab: "room-profiles",
    title: "Профили комнат",
  },
  avatars: {
    tab: "avatars",
    title: "Аватарки"
  }
}

const rootTab = tabsInfo.list;

export { tabsInfo, rootTab, type ModalTabInfo }