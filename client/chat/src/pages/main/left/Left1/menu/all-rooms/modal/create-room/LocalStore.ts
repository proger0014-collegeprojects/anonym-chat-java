import { CreateRoomRequest } from "$/shared/api/contracts";
import { makeAutoObservable } from "mobx";
import { IPromiseBasedObservable, fromPromise } from "mobx-utils";
import { createRoom as apiCreateRoom } from "$/shared/api/room";

class LocalStore {
  createRoomRequest: IPromiseBasedObservable<void>;

  constructor() {
    makeAutoObservable(this);

    this.createRoomRequest = fromPromise(Promise.resolve());
  }

  createRoom(request: CreateRoomRequest) {
    const response = apiCreateRoom(request).then(_ => {});

    this.createRoomRequest = fromPromise(response);
  }
}

export { LocalStore }