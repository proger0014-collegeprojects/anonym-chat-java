import { ButtonIconV2 } from "$/components/common/buttons/button-icon";
import { Modal } from "$/pages/main/left/Left1/menu/all-rooms/modal";
import { useDisclosure } from "@mantine/hooks";
import { IconMessages } from "@tabler/icons-react";

function AllRooms() {
  const [opened, { open, close }] = useDisclosure(false);

  return (
    <>
      <Modal opened={opened} close={close} />
      <AllRoomsInner onClick={open} />
    </>
  )
}

interface AllRoomsInnerProps {
  onClick: () => void;
}

function AllRoomsInner({ onClick }: AllRoomsInnerProps) {
  return (
    <ButtonIconV2
        icon={<IconMessages size={30} />}
        height="60px"
        onClick={onClick}
      >
          Все комнаты
    </ButtonIconV2>
  )
}

export { AllRooms }