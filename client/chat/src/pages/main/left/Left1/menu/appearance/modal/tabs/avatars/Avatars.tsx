import { useStores } from "$/app/stores";
import { DataVertiсalScroll, Item } from "$/components/common/data-vertical-scroll-area";
import { ImageWithPreview } from "$/components/common/images";
import { LocalStore } from "$/pages/main/left/Left1/menu/appearance/modal/tabs/avatars";
import { CreateAvatarModal } from "$/pages/main/left/Left1/menu/appearance/modal/tabs/avatars/create-avatar-modal";
import { Avatar } from "$/shared/api/contracts";
import { ActionIcon, Avatar as MAvatar, Box, Group, Grid } from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import { IconPlus } from "@tabler/icons-react";
import { observer } from "mobx-react-lite";
import { ReactNode, createContext, useContext, useEffect } from "react";

function getItemsToRow(items: Avatar[]): ReactNode[] {
  return items.map(i => (
    <Grid.Col key={i.id} span={4}>
      <Group w="100%" justify="center">
        <ImageWithPreview key={i.id} url={i.url}>
          {(url) => (
            <MAvatar size={120} src={url} />
          )}
        </ImageWithPreview>
      </Group>
    </Grid.Col>
  ))
}

const DataVertiсalScrollObservable = observer(DataVertiсalScroll);

function getItems(items: Avatar[]): Item[] {
  if (items.length == 0) return [];

  let itemsResult: Item[] = [];

  const itemsTemp = Array.from(items);

  while (itemsTemp.length > 0) {
    const itemsArray = itemsTemp.slice(0, 3);
    itemsTemp.splice(0, 3);

    const itemRow = getItemsToRow(itemsArray);

    const key = itemsArray[0].id;

    if (itemsResult.filter(i => i.key == key).length > 0) continue;

    itemsResult.push({
      key: key,
      component:
        <Grid key={key} align="center" w="98%" mb={30}>
          {itemRow}
        </Grid>,
    });
  }

  return itemsResult;
}

interface AvatarsProps {
  store: LocalStore;
}

const Context = createContext<LocalStore|undefined>(undefined);

function useStore() {
  return useContext(Context);
}

const Avatars = observer(({ store }: AvatarsProps) => {
  return (
    <Context.Provider value={store}>
      <AvatarsInnerWrapper />
    </Context.Provider>
  )
});

const AvatarsInnerWrapper = observer(() => {
  const localAvatarsStore = useStore()!;

  const [openedCreateAvatarModal, { open: openCreateAvatarModal, close: closeCreateAvatarModal }] = useDisclosure(false);

  useEffect(() => {
    localAvatarsStore.fetchSection(1);

    return () => {
      localAvatarsStore.unuseMessagingClientStore();
    }
  }, [localAvatarsStore]);

  const canScrollDown = localAvatarsStore.lastSection < localAvatarsStore.totalSections;

  function onScrolledDown(reset: () => void) {
    localAvatarsStore.fetchSection(localAvatarsStore.lastSection + 1);

    localAvatarsStore.fetchStatus.then(_ => {
      reset();
    })
  }

  const items = getItems(localAvatarsStore.avatars);

  return (
    <AvatarsInner
      openCreateAvatarModal={openCreateAvatarModal}
      closeCreateAvatarModal={closeCreateAvatarModal}
      openedCreateAvatarModal={openedCreateAvatarModal}
      items={items}
      canScrollDown={canScrollDown}
      onScrolledDown={onScrolledDown} />
  )
});

interface AvatarsInnerProps {
  openedCreateAvatarModal: boolean;
  openCreateAvatarModal: () => void;
  closeCreateAvatarModal: () => void;
  items: Item[];
  canScrollDown: boolean;
  onScrolledDown: (reset: () => void) => void;
}

const AvatarsInner = observer(({ 
  canScrollDown, 
  onScrolledDown, 
  items,
  openCreateAvatarModal,
 closeCreateAvatarModal,
  openedCreateAvatarModal }: AvatarsInnerProps) => {
  const { uiStore } = useStores();

  return (
    <Box w="100%" h="100%">
      <Box mb={20}>
        <Group wrap="nowrap">
          <CreateAvatarModal 
            isOpened={openedCreateAvatarModal}
            onClose={closeCreateAvatarModal} />
          <ActionIcon variant="default" onClick={openCreateAvatarModal}>
            <IconPlus />
          </ActionIcon>
        </Group>
      </Box>
      <DataVertiсalScrollObservable 
        containerProps={{ h: uiStore.height - 300, w: '100%' }}
        items={items}
        onScrolledUp={() => {}}
        onScrolledDown={onScrolledDown}
        canScrollAction={{ up: false, down: canScrollDown }} />
    </Box>
  )
});

export { Avatars }