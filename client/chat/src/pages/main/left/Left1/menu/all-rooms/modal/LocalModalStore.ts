import { ModalTabInfo, rootTab } from "$/pages/main/left/Left1/menu/all-rooms/modal/common";
import { Room, Section } from "$/shared/api/contracts";
import { getRoomsPage } from "$/shared/api/room";
import { PendingPageInfo } from "$/shared/types";
import { getPendingPage } from "$/shared/utils";
import { makeAutoObservable } from "mobx";
import { IPromiseBasedObservable, fromPromise } from "mobx-utils";

class LocalModalStore {
  currentPage: IPromiseBasedObservable<Section<Room>>;
  createdNew: number;
  pendingPageInfo: PendingPageInfo;
  modalTabInfo: ModalTabInfo;
  selectRoomId?: number;

  constructor() {
    makeAutoObservable(this);

    this.currentPage = fromPromise(new Promise(_ => {}));
    this.createdNew = 0;
    this.pendingPageInfo = {
      currentPage: 0,
      totalPages: 0,
    };
    this.modalTabInfo = rootTab;
  }

  setSelectRoomId(id: number) {
    this.selectRoomId = id;
  }

  resetSelectRoomId() {
    this.selectRoomId = undefined;
  }

  updateTab(tab: ModalTabInfo) {
    this.modalTabInfo = tab;
  }

  resetTab() {
    if (this.modalTabInfo.tab == rootTab.tab) return;

    this.modalTabInfo = rootTab;
  }

  fetchPage(page: number) {
    const pendingPageInfo = getPendingPage(page, this.currentPage.value as Section<Room>);

    if (pendingPageInfo) {
      this.pendingPageInfo = pendingPageInfo;
    }

    const response = getRoomsPage(page);

    this.currentPage = fromPromise(response);
  }

  created() {
    this.createdNew++;
  }

  refreshPage() {
    this.createdNew = 0;

    const currentPage = (this.currentPage?.value as Section<Room>).section ?? 1;

    this.fetchPage(currentPage);
  }

  resetCreated() {
    this.createdNew = 0;
  }
}

const localModalStore = new LocalModalStore();

export { localModalStore }