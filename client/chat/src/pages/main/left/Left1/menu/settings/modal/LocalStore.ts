import { ModalTabInfo } from "$/pages/main/common";
import { rootTab } from "$/pages/main/left/Left1/menu/settings/modal/common";
import { makeAutoObservable } from "mobx";

class LocalStore {
  currentTab: ModalTabInfo;

  constructor() {
    makeAutoObservable(this);

    this.currentTab = rootTab;
  }

  resetTabs() {
    this.currentTab = rootTab;
  }

  updateTab(tab: ModalTabInfo) {
    this.currentTab = tab;
  }
}

export { LocalStore }