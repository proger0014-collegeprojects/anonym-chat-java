import { ButtonIcon } from "$/components/common/buttons/button-icon";
import { MainLayout } from "$/layouts/main"
import { IconMenu2 } from "@tabler/icons-react";
import { Menu } from "$/pages/main/left/Left1/menu";
import { Body } from "$/pages/main/left/Left1/body";
import { useDisclosure } from "@mantine/hooks";

function Left1() {
  const [openedMenu, { open, close }] = useDisclosure(false);

  return (
    <MainLayout.Left.Left1>
      <MainLayout.Left.Header>
        <Header menuOnClick={open} />
        <Menu
          title="ANONYM CHAT"
          opened={openedMenu}
          onClose={close} />
      </MainLayout.Left.Header>
      <MainLayout.Left.Body>
        <Body />
      </MainLayout.Left.Body>
    </MainLayout.Left.Left1>
  )
}

interface HeaderProps {
  menuOnClick: () => void;
}

function Header({ menuOnClick }: HeaderProps) {
  return (
    <ButtonIcon 
      icon={<IconMenu2 
        width={30} 
        height={30} 
      />}
      onClick={menuOnClick} 
    />
  )
}

export { Left1 }