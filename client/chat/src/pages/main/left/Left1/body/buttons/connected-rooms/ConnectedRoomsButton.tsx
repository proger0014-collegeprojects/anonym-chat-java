import { ButtonIconV3 } from "$/components/common/buttons/button-icon";
import { mainStore } from "$/pages/main";
import { rootTab } from "$/pages/main/common";
import { IconMessages } from "@tabler/icons-react";
import { observer } from "mobx-react-lite";

const ConnectedRoomsButton = observer(() => {
  return (
    <ButtonIconV3
      isActive={mainStore.currentTabLeft1.tab == rootTab.tab}
      buttonProps={{ fullWidth: true }}
      icon={ <IconMessages size={30}/>}>
      Подключенные
    </ButtonIconV3>
  )
});

export { ConnectedRoomsButton }