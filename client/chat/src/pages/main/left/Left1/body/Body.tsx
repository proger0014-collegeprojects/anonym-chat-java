import { observer } from "mobx-react-lite"
import { ConnectedRoomsButton } from "$/pages/main/left/Left1/body/buttons";
import { Stack } from "@mantine/core";

const Body = observer(() => {
  return (
    <>
      <BodyInner />
    </>
  )
});

const BodyInner = observer(() => {
  return (
    <Stack w="100%">
      <ConnectedRoomsButton />
    </Stack>
  )
})

export { Body }