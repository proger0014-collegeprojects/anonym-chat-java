import { useStores } from "$/app/stores";
import { RoomLeftItem } from "$/components/room/RoomLeftItem";
import { mainStore } from "$/pages/main/LocalStore";
import { Room } from "$/shared/api/contracts";
import { getUnixTime } from "$/shared/utils";
import { ScrollArea } from "@mantine/core";
import { useWindowEvent } from "@mantine/hooks";
import { observer } from "mobx-react-lite";
import { useState } from "react";

function getSize(size: number) {
  return size - 54;
}

const Rooms = observer(() => {
  const { rootRoomStores } = useStores();
  const [size, setSize] = useState(() => getSize(window.innerHeight));

  useWindowEvent("resize", (_) => {
    setSize(getSize(window.innerHeight));
  });

  const roomsId = rootRoomStores.roomStores
    .filter(r => r.connectionInfo?.status == 'connected' 
      || r.connectionInfo?.status == 'already-connected')
    .sort((a, b) => {
      if (!a.messagesStore.lastMessage) {
        return -1
      } else if (!b.messagesStore.lastMessage) {
        return 1;
      }

      return getUnixTime(new Date(a.messagesStore.lastMessage.message.createdAt)) - getUnixTime(new Date(b.messagesStore.lastMessage.message.createdAt));
    })
    .reverse()
    .map(rs => rs.id);

  function onClick(r: Room) {
    mainStore.updateActiveRoomIdLeft2(r.id);
  }

  function getIsActive(r: Room) {
    return mainStore?.activeRoomIdLeft2 == r.id || false;
  }

  return (
    <>
      <RoomsInner 
        scrollAreaSize={size}
        roomsId={roomsId}
        onClick={onClick}
        getIsActive={getIsActive} />
    </>
  )
})

interface RoomsInnerProps {
  roomsId: number[];
  onClick: (r: Room) => void;
  getIsActive: (r: Room) => boolean;
  scrollAreaSize: number;
}

const RoomsInner = observer(({
  scrollAreaSize, 
  roomsId,
  onClick,
  getIsActive }: RoomsInnerProps) => {
  const { rootRoomStores } = useStores();
  
  const components = roomsId.map(r => {
    const roomStore = rootRoomStores.find(r);

    const lastMessage = roomStore.messagesStore.lastMessage;

 
    let lastMessageInfo = undefined;

    if (lastMessage) {
      const roomProfile = roomStore.roomProfiles[lastMessage.message.userId];

      lastMessageInfo = {
        avatarUrl: roomProfile.avatar.url,
        text: lastMessage.message.body
      }
    }

    return (
      <RoomLeftItem
        lastMessage={lastMessageInfo}
        newMessagesCount={roomStore.messagesStore.newCount}
        key={r}
        room={roomStore}
        isActive={getIsActive(roomStore)}
        onClick={onClick} />
    )
  });

  return (
    <ScrollArea h={scrollAreaSize} scrollbarSize={2} w="100%">
      {components}
    </ScrollArea>
  )
})

export { Rooms }