import { MainLayout } from "$/layouts/main"
import { Rooms } from "$/pages/main/left/Left2/rooms"

function Left2() {
  return (
    <MainLayout.Left.Left2>
      <MainLayout.Left.Header>{""}</MainLayout.Left.Header>
      <MainLayout.Left.Body>
        <Rooms />
      </MainLayout.Left.Body>
    </MainLayout.Left.Left2>
  )
}

export { Left2 }