import { MainLayout } from "$/layouts/main";
import { Left1 } from "$/pages/main/left/Left1";
import { Left2 } from "$/pages/main/left/Left2";

function Left() {
  return (
    <MainLayout.Left>
      <Left1 />
      <Left2 />
    </MainLayout.Left>
  )
}

export { Left }