import { action, makeAutoObservable, observable } from "mobx";
import { ActivationState, IMessage, Client as MessagingClient } from "@stomp/stompjs";
import { messagingClient } from "$/shared/messaging";
import { IS_DEV } from "$/shared/constants";

interface SubscripionInfo {
  key?: string;
  destination: string;
  unsubscribeCallback: () => void;
}

function predicate(source: SubscripionInfo[], destination: string, key?: string) {
  return source.filter(s => {
    if (key && s.key) {
      return s.destination == destination && key == s.key;
    }

    if (!key && !s.key) {
      return s.destination == destination;
    }

    return false;
  }).length > 0;
}

function predicateItem(source: SubscripionInfo, destination: string, key?: string) {
  if (key && source.key) {
    return source.destination == destination && key == source.key;
  }

  if (!key && !source.key) {
    return source.destination == destination;
  }

  return false;
}

class MessagingClientStore {
  isConnected: boolean = false;
  isActive: boolean = false;
  client: MessagingClient;
  subscriptions: SubscripionInfo[];

  constructor() {
    makeAutoObservable(this, {
      client: observable,
      isConnected: observable,
      isActive: observable,
      subscriptions: observable,

      updateActive: action,
      updateConnected: action,
      subscribe: action
    });
    
    this.client = messagingClient;

    this.subscriptions = [];

    this.client.onChangeState = (state) => {
      switch(state) {
        case ActivationState.ACTIVE:
          this.updateActive(true)
          break;
        default:
          this.updateActive(false);
          break;
      }
    }

    this.client.onConnect = () => {
      this.updateConnected(true);
    }

    this.client.onDisconnect = () => {
      this.updateConnected(false);
    }

    this.client.onWebSocketClose = () => {
      this.updateConnected(false);
    }
  }

  subscribe(destination: string, callback: (message: IMessage) => void, key?: string) {
    if (predicate(this.subscriptions, destination, key)) {
      return;
    }

    const subscriptionInfo = this.client.subscribe(destination, callback);

    if (key && IS_DEV) {
      console.log("key: ", key);
    }

    this.subscriptions.push({
      destination: destination,
      unsubscribeCallback: subscriptionInfo.unsubscribe,
      key: key
    });
  }

  unsubscribe(destination: string, key?: string) {
    if (!predicate(this.subscriptions, destination, key)) {
      return;
    }

    const subscription = this.subscriptions.filter(s => predicateItem(s, destination, key))[0];

    subscription.unsubscribeCallback();

    if (key && IS_DEV) {
      console.log("key: ", key);
    }

    this.subscriptions = this.subscriptions.filter(s => !predicateItem(s, destination, key));
  }

  updateActive(isActive: boolean) {
    this.isActive = isActive;
  }

  updateConnected(isConnected: boolean) {
    this.isConnected = isConnected;
  }
}

export default MessagingClientStore;