import TokensStore from "$/app/stores/TokensStore";
import { action, makeAutoObservable, observable } from "mobx";
import { login as apiLogin, register as apiRegister, logout as apiLogout } from "$/shared/api/auth";
import { updateAuthUser as apiUpdateAuthUser } from "$/shared/api/auth/user";
import { IPromiseBasedObservable, fromPromise } from "mobx-utils";
import { RegisterResponse, UpdateAuthUserRequest, loginResponse } from "$/shared/api/contracts";
import { makePersistable } from "mobx-persist-store";
import { getFullProperty } from "$/shared/utils";
import { User } from "$/shared/types";

class AuthStore {
  tokensStore?: TokensStore;
  registerResponse?: IPromiseBasedObservable<RegisterResponse>;
  loginResponse?: IPromiseBasedObservable<loginResponse>;
  isAuthorize: boolean;
  user?: User; // TODO: ANONYMCHAT-76
  logoutRequest: IPromiseBasedObservable<void>;
  updateAuthUserRequest: IPromiseBasedObservable<void>;

  constructor() {
    makeAutoObservable(this, {
      registerResponse: observable,
      isAuthorize: observable,
      user: observable,
      loginResponse: observable,
      logoutRequest: observable,
      updateAuthUserRequest: observable,

      updateAuthorize: action,
      updateUser: action,
      login: action,
      register: action,
      logout: action,
      updateAuthUser: action
    });

    this.isAuthorize = false;
    this.logoutRequest = fromPromise(Promise.resolve());
    this.updateAuthUserRequest = fromPromise(Promise.resolve());

    makePersistable(this, {
      name: "AuthStore",
      properties: [
        // @ts-ignore
        getFullProperty<boolean>({key: 'isAuthorize'}),
        // @ts-ignore
        getFullProperty<User>({key: 'user'})
      ],
      storage: window.localStorage
    })
  }

  updateAuthorize(isAuthorize: boolean) {
    this.isAuthorize = isAuthorize;
  }

  updateUser(user: User) {
    this.user = user;
  }

  setTokensStore(tokensStore: TokensStore) {
    this.tokensStore = tokensStore;
  }

  login(login: string, password: string|null) {
    const response = apiLogin({login, password});

    this.loginResponse = fromPromise(response);

    this.tokensStore?.updateFromExistsTokens(response);
  }

  logout() {
    const response = apiLogout().then(_ => {
      localStorage.clear();

      window.location.replace('/auth/login');
    });

    this.logoutRequest = fromPromise(response);
  }

  register(login: string|null, password: string|null) {
    const response = apiRegister({
      role: 0,
      login: login,
      password: password
    });

    this.registerResponse = fromPromise(response);
  }

  updateAuthUser(request: UpdateAuthUserRequest) {
    const response = apiUpdateAuthUser(request).then(_ => {
      this.tokensStore?.refreshTokens();
    });

    this.updateAuthUserRequest = fromPromise(response);
  }

  clearRegisterResponse() {
    this.registerResponse = undefined;
  }

  clearUpdateAuthRequest() {
    this.updateAuthUserRequest = fromPromise(Promise.resolve());
  }
}

export { AuthStore }