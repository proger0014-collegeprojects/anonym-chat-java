import { createContext, useContext } from "react";
import { 
  AuthStore,
  MessagingClientStore, 
  TokensStore, 
  UiStore, 
  getTokensStore } from "$/app/stores";
import { RootRoomsStore } from "$/app/stores/rooms";

interface StoresContextProps {
  tokensStore: TokensStore;
  messagingClientStore: MessagingClientStore;
  authStore: AuthStore;
  rootRoomStores: RootRoomsStore;
  uiStore: UiStore;
}

// @ts-ignore
const stores: StoresContextProps = { };

stores.uiStore = new UiStore();
stores.tokensStore = getTokensStore();
stores.messagingClientStore = new MessagingClientStore();
stores.authStore = new AuthStore();
stores.rootRoomStores = new RootRoomsStore(stores.messagingClientStore, stores.authStore);

stores.tokensStore.setAuthStore(stores.authStore);
stores.authStore.setTokensStore(stores.tokensStore);

const StoresContext = createContext<StoresContextProps>(stores);

function useStores() {
  const storesContext = useContext(StoresContext);

  return storesContext;
}

function getStores(): StoresContextProps {
  return stores;
}

export { stores, useStores, getStores, StoresContext };