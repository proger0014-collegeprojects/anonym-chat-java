import { makeAutoObservable } from "mobx";

class UiStore {
  height: number;
  width: number;

  constructor() {
    makeAutoObservable(this);

    this.height = window.innerHeight;
    this.width = window.innerWidth;

    const update = () =>  {
      this.updateHeight(window.innerHeight);
      this.updateWidth(window.innerWidth);
    }

    window.addEventListener('resize', update);
  }

  updateHeight(height: number) {
    this.height = height;
  }

  updateWidth(width: number) {
    this.width = width;
  }
}

export { UiStore }