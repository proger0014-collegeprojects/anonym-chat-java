import { postRefreshTokens } from "$/shared/api/auth";
import { TokensResponse } from "$/shared/api/contracts";
import { action, makeAutoObservable, observable } from "mobx";
import { makePersistable } from "mobx-persist-store";
import { FULFILLED, IPromiseBasedObservable, fromPromise } from "mobx-utils";
import { getObservablePromiseFullProperty, getFullProperty, getDateNowInUnixTime, getLocalDateFromUnixTime, getLocalDateFromString, getUnixTime } from "$/shared/utils";
import { AuthStore } from "$/app/stores/AuthStore";
import { JwtPayload, jwtDecode } from "jwt-decode";
import { Role } from "$/shared/types";

interface JwtPayloadActual extends JwtPayload {
  id: number;
  role: Role;
  authType: 'anonym' | 'auth';
}

class TokensStore {
  authStore?: AuthStore;
  accessToken: IPromiseBasedObservable<string>;
  updatedAt: Date;
  payload?: JwtPayloadActual;

  constructor() {
    makeAutoObservable(this, {
      accessToken: observable,
      updatedAt: observable,

      refreshTokens: action,
      updateUpdatedAt: action,
      updateFromExistsTokens: action
    });

    this.accessToken = fromPromise(new Promise(_ => {}));
    this.updatedAt = new Date(Date.now());
    
    makePersistable(this, { 
      name: "TokensStore",
      properties: [
        // @ts-ignore
        getObservablePromiseFullProperty<string>('accessToken'),
        // @ts-ignore
        getFullProperty<Date>({
          key: 'updatedAt', 
          deserialize: (v) => {
            return getLocalDateFromString(v);
          },
          serialize: (v) => {
            return v?.toISOString();
          }
        }),
        // @ts-ignore
        getFullProperty<JwtPayloadActual>({ key: 'payload' })
      ],
      storage: window.localStorage, });
  }

  setAuthStore(authStore: AuthStore) {
    this.authStore = authStore;
  }

  refreshTokens() {
    if (this.accessToken.state != FULFILLED) return;
    
    const dateNow = getDateNowInUnixTime();
    const dateExpiration = getUnixTime(getLocalDateFromUnixTime(this.payload?.exp ?? dateNow));
    
    if (dateNow >= (dateExpiration ?? dateNow)) {
      this.updateFromExistsTokens(postRefreshTokens());
    }
  }

  updateUpdatedAt(date: Date) {
    this.updatedAt = date;
  }

  updatePayload(payoad: JwtPayloadActual) {
    this.payload = payoad;
  }

  togglePayloadToAuth() {
    if (this.authStore!.user!.authType == 'auth') return;

    this.updatePayload({
      ...this.payload!,
      authType: 'auth'
    });
  }

  updateFromExistsTokens(accessTokenInput: Promise<TokensResponse>) {
    const response = accessTokenInput.then(res => {
      if (this.authStore && !this.authStore.isAuthorize) {
        this.authStore?.updateAuthorize(true);
      }

      const jwtPayload = jwtDecode<JwtPayloadActual>(res.accessToken);

      this.updateUpdatedAt(getLocalDateFromUnixTime(jwtPayload.iat!));

      this.updatePayload(jwtPayload);

      this.authStore?.updateUser({
        id: jwtPayload.id,
        login: jwtPayload.sub,
        role: jwtPayload.role,
        authType: jwtPayload.authType
      });

      return res.accessToken
    }).catch(_ => {
      this.authStore?.updateAuthorize(false);

      return null!;
    });

    this.accessToken = fromPromise(response);
  }
}

const tokensStore = new TokensStore();

function getTokensStore() {
  return tokensStore;
}

export { getTokensStore }

export default TokensStore;