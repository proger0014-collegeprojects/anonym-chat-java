import { AuthStore } from "$/app/stores/AuthStore";
import MessagingClientStore from "$/app/stores/MessagingClientStore";
import { RoomStore } from "$/app/stores/rooms/RoomStore";
import { Room, RoomProfileFull } from "$/shared/api/contracts";
import { getMessagesSection } from "$/shared/api/message";
import { getRoomById } from "$/shared/api/room";
import { ConnectionToRoomInfo } from "$/shared/types";
import { getFullProperty } from "$/shared/utils";
import { makeAutoObservable } from "mobx";
import { makePersistable } from "mobx-persist-store";

class RootRoomsStore {
  authStore: AuthStore;
  roomStores: RoomStore[];
  messagingClientStore: MessagingClientStore;

  constructor(store: MessagingClientStore, authStore: AuthStore) {
    makeAutoObservable(this);

    this.authStore = authStore;
    this.roomStores = [];
    this.messagingClientStore = store;

    makePersistable(this, {
      name: 'RootRoomsStore',
      properties: [
        // @ts-ignore
        getFullProperty<RoomStore[], { room: Room, connectionInfo: ConnectionToRoomInfo, roomProfiles: { [userId: number]:  RoomProfileFull } }[]>({ 
          key: 'roomStores',
          serialize: (stores) => {
            return stores?.map((s) => {
              return {
                room: { 
                  id: s.id,
                  userId: s.userId,
                  countConnectedUsers: s.countConnectedUsers,
                  name: s.name,
                  description: s.description
                },
                roomProfiles: s.roomProfiles,
                connectionInfo: {
                  status: s.connectionInfo?.status
                }
              } as { room: Room, connectionInfo: ConnectionToRoomInfo, roomProfiles: { [userId: number]:  RoomProfileFull } };
            }).filter(s => s.connectionInfo?.status == 'already-connected' || s.connectionInfo?.status == 'connected');
          },
          deserialize: (stores) => {
            return stores && stores?.map(s => {
              const rs = new RoomStore(s.room, this.messagingClientStore, authStore);
              
              getRoomById(s.room.id).then(data => {
                rs.update(data);
              });

              getMessagesSection(s.room.id, 1).catch(_ => {
                this.deleteRoom(s.room);
              });

              rs.setConnecectionInfo(s.connectionInfo);
              rs.setRoomProfilesList(s.roomProfiles);

              return rs;
            });
          } 
        })
      ],
      storage: window.localStorage
    });
    
  }

  addStore(roomStore: RoomStore) {
    if (this.roomStores.filter(r => r.id == roomStore.id).length > 0) return;

    this.roomStores.push(roomStore);
  }

  addRoom(room: Room) {
    if (this.roomStores.filter(r => r.id == room.id).length > 0) return;

    this.addStore(new RoomStore(room, this.messagingClientStore, this.authStore));
  }

  deleteRoom(room: Room) {
    if (this.roomStores.filter(r => r.id == room.id).length <= 0) return;

    this.roomStores = this.roomStores.filter(r => r.id != room.id);
  }

  find(id: number): RoomStore {
    return this.roomStores.filter(rs => rs.id == id)[0];
  }
}

export { RootRoomsStore }