import { AuthStore } from "$/app/stores/AuthStore";
import MessagingClientStore from "$/app/stores/MessagingClientStore";
import { RoomStore } from "$/app/stores/rooms/RoomStore";
import { MessageStore } from "$/app/stores/rooms/messages/MessageStore";
import { RoomMessage, RoomMessageV2, Section } from "$/shared/api/contracts";
import { createRoomMessage, getMessagesSection } from "$/shared/api/message";
import { getUnixTime } from "$/shared/utils";
import { makeAutoObservable, observe } from "mobx";
import { IPromiseBasedObservable, fromPromise } from "mobx-utils";
import { predicateToNewSection } from "$/shared/utils";

function predicateExistsMessages(source: MessageStore[], target: RoomMessage[]) {
  let result = false;

  for (let i = 0; i < target.length; i++) {
    const targetItem = target[i];

    if (source.filter(s => s.message.id == targetItem.id).length > 0) {
      result = true;
      break;
    }
  }

  return result;
}

class MessagesStore {
  firstSection: number;
  lastSection: number;
  totalSections: number;
  pendingMessageInfo: IPromiseBasedObservable<Section<RoomMessage>>;
  messages: MessageStore[];
  newCount: number;
  messagingClientStore: MessagingClientStore;
  inReadingOngoing: boolean;
  authStore: AuthStore;
  roomStore: RoomStore;

  constructor(messagingClientStore: MessagingClientStore, authStore: AuthStore, roomStore: RoomStore) {
    this.pendingMessageInfo = fromPromise(new Promise(_ => {}));
    this.authStore = authStore;
    this.roomStore = roomStore;

    observe(messagingClientStore, () => {
      if (this.messagingClientStore.isConnected) {
        this.messagingClientStore.subscribe(`/room/${roomStore.id}/message/created`, (m) => {
          if (predicateToNewSection(this.messages.length)) {
            this.updateLastSection(this.lastSection + 1);
            this.updateTotalSections(this.totalSections + 1);
          }
          
          const message = JSON.parse(m.body) as RoomMessage;

          if (!this.inReadingOngoing && message.userId != this.authStore.user?.id) {
            this.incrementNewCount();
          }
    
          this.addOneMessageToList(message);
        }, 'message-store');
      }
    });

    makeAutoObservable(this);

    this.messagingClientStore = messagingClientStore;

    this.inReadingOngoing = false;
    this.messages = [];
    this.totalSections = 0;
    this.firstSection = 0;
    this.lastSection = 0;
    this.newCount = 0;
  }

  readingOngoing() {
    if (this.inReadingOngoing) return;

    this.inReadingOngoing = true;
  }

  notReadingOngoint() {
    if (!this.inReadingOngoing) return;

    this.inReadingOngoing = false;
  }

  fetchLastMessageSection() {
    if (this.lastSection > 0) return;

    getMessagesSection(this.roomStore.id, 1).then(res => {
      this.updateFirstSection(res.lastSection);
      this.fetchMessageSection(res.lastSection);

      return res;
    }).then(res => {
      this.pendingMessageInfo.then(res2 => {
        if (res2.total <= 10 && res.lastSection > 1) {
          this.fetchMessageSection(res.lastSection - 1);
        }
      });
    });
  }

  updateRoomProfilesFromMessages(messages: RoomMessageV2[]) {
    messages.forEach(m => {
      this.roomStore.updateRoomProfiles(m.userId, m.roomProfile);

      if (m.reply) {
        this.roomStore.updateRoomProfiles(m.reply.userId, m.reply.roomProfile);
      }
    });
  }

  fetchMessageSection(section: number) {
    if ((this.lastSection > 0 && this.totalSections > 0) 
      && (this.lastSection == section || section < 1 || section > this.totalSections)) return;

    const response = getMessagesSection(this.roomStore.id, section).then(data => {
      this.updateRoomProfilesFromMessages(data.items);
      this.updateLastSection(section);
      this.updateTotalSections(data.lastSection);
      this.updateMessagesList(data.items);

      return data;
    });

    this.pendingMessageInfo = fromPromise(response);
  }

  get lastMessage() {
    return this.messages[this.messages.length - 1];
  }

  updateTotalSections(section: number) {
    if (this.totalSections == section) return;

    this.totalSections = section;
  }

  updateFirstSection(section: number) {
    if (this.firstSection == section) return;

    this.firstSection = section;
  }

  updateLastSection(section: number) {
    if (this.lastSection == section) return;

    this.lastSection = section;
  }

  addOneMessageToList(message: RoomMessage) {
    this.updateMessagesList([message]);
  }

  incrementNewCount() {
    this.newCount++;
  }

  decrementNewCount() {
    if (this.newCount <= 0) return;

    this.newCount--;
  }

  updateMessagesList(messages: RoomMessage[]) {
    if (predicateExistsMessages(this.messages, messages)) return;

    const newMessages = messages.map(m => new MessageStore(m, this));

    this.messages = ([...this.messages, ...newMessages]).sort((a, b) => {
      return getUnixTime(new Date(a.message.createdAt)) - getUnixTime(new Date(b.message.createdAt));
    });
  }

  findMessageById(id: number) {
    return this.messages.filter(m => m.message.id == id)[0];
  }

  sendMessage(message: string) {
    createRoomMessage(this.roomStore.id, {
      body: message
    });
  }
}

export { MessagesStore }