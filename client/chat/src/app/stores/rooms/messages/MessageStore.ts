import { MessagesStore } from "$/app/stores/rooms/messages/MessagesStore";
import { RoomMessage } from "$/shared/api/contracts";
import { replyRoomMessage } from "$/shared/api/message";
import { makeAutoObservable } from "mobx";

class MessageStore {
  messagesStore: MessagesStore;
  message: RoomMessage;
  isRead: boolean;

  constructor(roomMessage: RoomMessage, store: MessagesStore) {
    makeAutoObservable(this);

    this.message = roomMessage;
    this.isRead = false;
    this.messagesStore = store;
  }

  read() {
    if (this.isRead) return;

    this.isRead = true;
    this.messagesStore.decrementNewCount();
  }

  reply(message: string) {
    replyRoomMessage(this.messagesStore.roomStore.id, this.message.id, {
      body: message
    });
  }
}

export { MessageStore }