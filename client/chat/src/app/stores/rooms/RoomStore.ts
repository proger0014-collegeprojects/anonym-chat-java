import { AuthStore } from "$/app/stores/AuthStore";
import MessagingClientStore from "$/app/stores/MessagingClientStore";
import { MessagesStore } from "$/app/stores/rooms/messages/MessagesStore";
import { Error, GenericError, InstallRoomProfile, Room, RoomProfileFull, RoomProfileWithUserFull } from "$/shared/api/contracts";
import { connectToRoom, disconnecFromRoom, installRoomProfile } from "$/shared/api/room";
import { notifyDanger, notifyError } from "$/shared/lib";
import { Callbacks } from "$/shared/types";
import { AxiosError } from "axios";
import { makeAutoObservable, observe } from "mobx";
import { IPromiseBasedObservable, fromPromise } from "mobx-utils";
import { ConnectionToRoomInfo } from "$/shared/types";

function predicateIsExistsRoomProfile(userId: number, roomProfile: RoomProfileFull, roomProfiles:  { [userId: number]: RoomProfileFull }) {
  if (!roomProfiles[userId]) return false;

  return roomProfiles[userId].id == roomProfile.id;
}

export type InstallStatus = 'processing'|'finished-ok'|'internal-server-error';

interface InstallRoomProfileInfo {
  installResponse?: IPromiseBasedObservable<void>;
  status: InstallStatus;
}

class RoomStore implements Room {
  id: number;
  userId: number;
  countConnectedUsers: number;
  name: string;
  description?: string;
  connectionInfo?: ConnectionToRoomInfo;
  installRoomProfileInfo?: InstallRoomProfileInfo;
  authStore?: AuthStore;
  messagingClientStore?: MessagingClientStore;
  messagesStore: MessagesStore;
  roomProfiles: { [userId: number]:  RoomProfileFull };

  constructor(room: Room, messagingClientStore: MessagingClientStore, authStore: AuthStore) {
    makeAutoObservable(this);

    this.authStore = authStore;
    this.roomProfiles = [];
    this.id = room.id;
    this.userId = room.userId;
    this.countConnectedUsers = room.countConnectedUsers;
    this.name = room.name;
    this.description = room.description;
    this.messagesStore = new MessagesStore(messagingClientStore, authStore, this);

    observe(messagingClientStore, () => {
      if (!messagingClientStore.isConnected) return;

      messagingClientStore?.subscribe(`/room/${this.id}/connected-user`, (data) => {
        this.update({
          ...this,
          countConnectedUsers: this.countConnectedUsers + 1
        });

        const roomProfile = JSON.parse(data.body) as RoomProfileWithUserFull;

        this.updateRoomProfiles(roomProfile.user.id, roomProfile.roomProfile);
      }, 'room-store');

      messagingClientStore?.subscribe(`/room/${this.id}/disconnected-user`, (_) => {
        this.update({
          ...this,
          countConnectedUsers: this.countConnectedUsers - 1
        });
      }, 'room-store');

      messagingClientStore?.subscribe(`/room/${this.id}/profile/installed`, (e) => {
        const roomProfileOfUser = JSON.parse(e.body) as InstallRoomProfile;

        this.updateRoomProfiles(roomProfileOfUser.userId, roomProfileOfUser.roomProfile);
      }, 'room-store');
    });
  }

  setRoomProfilesList(roomProfiles: { [userId: number]:  RoomProfileFull }) {
    this.roomProfiles = roomProfiles;
  }

  addToRoomProfilesFromList(list: RoomProfileWithUserFull[]) {
    list.forEach(i => this.updateRoomProfiles(i.user.id, i.roomProfile));
  }

  updateRoomProfiles(userId: number, roomProfile: RoomProfileFull) {
    if (predicateIsExistsRoomProfile(userId, roomProfile, this.roomProfiles)) {
      this.roomProfiles[userId] = roomProfile;
    } else {
      this.roomProfiles =  { ...this.roomProfiles, [userId]: roomProfile };
    }
  }

  get myRoomProfile(): RoomProfileFull {
    return this.roomProfiles[this.authStore?.user?.id ?? -1];
  }

  connectToRoom(callbacks?: Callbacks) {
    this.setConnecectionInfo({
      status: 'connection',
    })

    const response = connectToRoom(this.id).then(res => {
      this.setConnecectionInfo({
        status: 'connected',
        connectionResponse: this.connectionInfo?.connectionResponse
      });

      callbacks && callbacks.onSuccess && callbacks.onSuccess();

      return res;
    }).catch((err: AxiosError<GenericError>) => {
      if (err.response?.data as Error) {
        const error = err.response?.data as Error;

        if (error.type == '/errors/rooms/connect-is-already-exists') {
          notifyDanger({
            message: 'Подключение уже существует'
          })

          this.setConnecectionInfo({
            status: 'already-connected',
            connectionResponse: this.connectionInfo?.connectionResponse
          });
        } else if (error.type == '/errors/rooms/not-selected-room-profile-for-room') {
          notifyDanger({
            message: 'Не установлен профиль команыт'
          })

          this.setConnecectionInfo({
            status: 'not-selected-room-profile',
            connectionResponse: this.connectionInfo?.connectionResponse
          });
        } else {
          notifyError({ message: 'Ошибка подключения' })

          this.setConnecectionInfo({
            status: 'internal-server-error',
            connectionResponse: this.connectionInfo?.connectionResponse
          })
        }

        callbacks && callbacks.onError && callbacks.onError();
      }
    });

    this.setConnecectionInfo({
      status: this.connectionInfo?.status ?? 'connection',
      connectionResponse: fromPromise(response)
    })
  }

  disconnectFromRoom(callbacks?: Callbacks) {
    const response = disconnecFromRoom(this.id).then(_ => {
      this.setConnecectionInfo({
        status: 'disconnected',
        connectionResponse: fromPromise(Promise.resolve())
      });

      callbacks && callbacks.onSuccess && callbacks.onSuccess();

      return _;
    });

    this.setConnecectionInfo({
      status: this.connectionInfo?.status ?? 'processing',
      connectionResponse: fromPromise(response)
    });
  }

  setConnecectionInfo(info: ConnectionToRoomInfo) {
    this.connectionInfo = info;
  }

  installRoomProfile(roomProfileId: number, callbacks?: Callbacks) {
    this.updateInstallRoomProfileInfo({
      status: 'processing',
    });

    const response = installRoomProfile(roomProfileId, {
      roomId: this.id
    }).then(res => {
      this.updateInstallRoomProfileInfo({
        status: 'finished-ok',
        installResponse: this.installRoomProfileInfo?.installResponse
      });

      callbacks && callbacks.onSuccess && callbacks.onSuccess();

      return res;
    }).catch((_) => {
      notifyError({ message: 'Неизвестная ошибка' });

      this.updateInstallRoomProfileInfo({
        status: 'internal-server-error',
        installResponse: this.installRoomProfileInfo?.installResponse
      });

      callbacks && callbacks.onError && callbacks.onError();
    });

    this.updateInstallRoomProfileInfo({
      status: this.installRoomProfileInfo?.status ?? 'processing',
      installResponse: fromPromise(response)
    });
  }

  updateInstallRoomProfileInfo(info: InstallRoomProfileInfo) {
    this.installRoomProfileInfo = info;
  }

  update(room: Room) {
    this.id = room.id;
    this.userId = room.userId;
    this.countConnectedUsers = room.countConnectedUsers;
    this.name = room.name;
    this.description = this.description;
  }
}

export { RoomStore }