import { RouterProvider } from "react-router-dom";
import { routes } from "$/app/config";

function RoutingProvider() {
  return (
    <RouterProvider router={routes} />
  )
}

export default RoutingProvider;