import { ChildrenProps } from "$/shared/types";
import { stores, StoresContext } from "$/app/stores";
import { configurePersistable } from "mobx-persist-store";
import { IS_DEV } from "$/shared/constants"

function StoresProvider({ children }: ChildrenProps) {
  configurePersistable({
    debugMode: IS_DEV
  })

  return (
    <StoresContext.Provider value={stores}>
      {children}
    </StoresContext.Provider>
  )
}

export default StoresProvider;