import { 
  ModalsManagerProvider, 
  NotificationsProvider, 
  OtherStylesProvider, 
  RoutingProvider, 
  StoresProvider, 
  ThemeProvider } from "$/app/providers";

function CommonProvider() {
  return (
    <StoresProvider>
      <ThemeProvider>
        <OtherStylesProvider>
          <ModalsManagerProvider>
            <NotificationsProvider />
            <RoutingProvider />
          </ModalsManagerProvider>
        </OtherStylesProvider>
      </ThemeProvider>
    </StoresProvider>
  )
}

export default CommonProvider;