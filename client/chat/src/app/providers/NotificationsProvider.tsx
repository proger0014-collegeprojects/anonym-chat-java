import { Notifications } from "@mantine/notifications";

function NotificationsProvider() {
  return (
    <Notifications zIndex={10_000} />
  )
}

export default NotificationsProvider;