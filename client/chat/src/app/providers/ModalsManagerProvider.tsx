import { ChildrenProps } from "$/shared/types";
import { ModalsProvider as MantineModalsProvider } from "@mantine/modals";

function ModalsManagerProvider({ children }: ChildrenProps) {
  return (
    <MantineModalsProvider>
      {children}
    </MantineModalsProvider>
  )
}

export default ModalsManagerProvider;