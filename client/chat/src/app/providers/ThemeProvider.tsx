import { MantineProvider } from "@mantine/core";
import { theme } from "$/app/config";
import { ChildrenProps } from "$/shared/types";

import "@mantine/core/styles.css";
import "@mantine/dropzone/styles.css";
import "@mantine/notifications/styles.css";

function ThemeProvider({ children }: ChildrenProps) {
  return (
    <MantineProvider theme={theme} defaultColorScheme="dark">
      {children}
    </MantineProvider>
  )
}

export default ThemeProvider;