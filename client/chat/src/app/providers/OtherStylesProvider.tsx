import { ChildrenProps } from "$/shared/types";

import "cropperjs/dist/cropper.css";

function OtherStylesProvider({ children }: ChildrenProps) {
  return (
    <>
      {children}
    </>
  )
}

export { OtherStylesProvider }