import { Navigate, createBrowserRouter } from "react-router-dom";
import { LOGIN_PAGE_ROUTE, LoginPage } from "$/pages/auth/login";
import { LOGIN_PAGE_WITH_LOGIN_AND_PASSWORD_ROUTE, LoginPageWithLoginAndPassword } from "$/pages/auth/login/with-login-and-password";
import { REGISTER_PAGE_ROUTE, RegisterPage } from "$/pages/auth/register";
import { MAIN_PAGE_ROUTE } from "$/pages/main";
import { MainPage } from "$/pages/main/MainPage";

const routes = createBrowserRouter([
  {
    path: LOGIN_PAGE_ROUTE,
    element: <LoginPage />
  },
  {
    path: LOGIN_PAGE_WITH_LOGIN_AND_PASSWORD_ROUTE,
    element: <LoginPageWithLoginAndPassword />
  },
  {
    path: REGISTER_PAGE_ROUTE,
    element: <RegisterPage />
  },
  {
    path: MAIN_PAGE_ROUTE,
    element: <MainPage />
  },
  {
    path: '*',
    element: (
      <Navigate to={MAIN_PAGE_ROUTE} />
    )
  }
]);

export default routes;