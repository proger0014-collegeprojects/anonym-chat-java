import { CommonProvider } from "$/app/providers";

function App() {
  return (
    <CommonProvider />
  )
}

export default App;