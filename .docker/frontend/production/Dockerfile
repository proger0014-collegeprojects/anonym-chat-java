FROM node:alpine as build-app

WORKDIR /chat

COPY /chat .

ARG PROJECT_ENVIRONMENT=production

RUN npm install

RUN npx tsc &&  \
    npx vite build --mode ${PROJECT_ENVIRONMENT}

FROM node:alpine as build-info

WORKDIR /info

COPY /info .

ARG PROJECT_ENVIRONMENT=production

RUN npm install

RUN npx vite build --mode ${PROJECT_ENVIRONMENT}

FROM nginx:alpine as run

WORKDIR /app/chat

COPY --from=build-app /chat/dist .

WORKDIR /app/info

COPY --from=build-info /info/dist .

WORKDIR /

RUN chown -R nginx:www-data app/ && \
    chmod -R 755 app/

ENTRYPOINT [ "nginx", "-g", "daemon off;" ]