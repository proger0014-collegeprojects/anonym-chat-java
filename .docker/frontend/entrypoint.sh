#!/bin/sh

echo "RUN ENTRYPOINT";

./docker-entrypoint.d/20-envsubst-on-templates.sh;

nginx -g "daemon off;";

echo "END ENTRYPOINT";
