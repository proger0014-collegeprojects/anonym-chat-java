#!/bin/sh

alias="$ANONYMCHAT_MINIO_ALIAS"
default_bucket="$ANONYMCHAT_MINIO_DEFAULT_BUCKET"

echo "RUN INIT MINIO"

mc alias set "$alias" "$ANONYMCHAT_MINIO_URL" "$ANONYMCHAT_MINIO_USER" "$ANONYMCHAT_MINIO_PASSWORD"

mc mb -p "$alias"/"$default_bucket"
mc anonymous set download "$alias"/"$default_bucket"

echo "END INIT MINIO"
