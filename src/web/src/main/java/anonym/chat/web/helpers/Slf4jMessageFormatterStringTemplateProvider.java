package anonym.chat.web.helpers;

import anonym.chat.web.abstractions.StringTemplateProvider;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class Slf4jMessageFormatterStringTemplateProvider implements StringTemplateProvider {
    @Override
    public String getFromTemplate(String template, Object... args) {
        String targetResult = template;
        Object[] forUseArgs = args;
        String previousFormatting = "";

        while (!targetResult.equals(previousFormatting)) {
            previousFormatting = targetResult;

            FormattingTuple formattingResult = MessageFormatter.arrayFormat(targetResult, forUseArgs);

            targetResult = formattingResult.getMessage();

            forUseArgs = getArgsForUse(targetResult, forUseArgs);
        }

        return targetResult;
    }

    private Object[] getArgsForUse(String template, Object[] currentForUseArgs) {
        Pattern templateArgsPattern = Pattern.compile("\\{}");
        Matcher templateArgsMatcher = templateArgsPattern.matcher(template);

        int templateArgsCount = (int)templateArgsMatcher.results().count();

        return Arrays.copyOfRange(
                currentForUseArgs,
                currentForUseArgs.length - templateArgsCount,
                currentForUseArgs.length);
    }
}
