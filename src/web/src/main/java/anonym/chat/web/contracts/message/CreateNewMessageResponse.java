package anonym.chat.web.contracts.message;

import java.time.LocalDateTime;

public record CreateNewMessageResponse(
    long id,
    LocalDateTime createdAt
) { }
