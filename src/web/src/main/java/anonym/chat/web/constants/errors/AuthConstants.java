package anonym.chat.web.constants.errors;

public final class AuthConstants {
    public static final String BEARER = "Bearer";
}
