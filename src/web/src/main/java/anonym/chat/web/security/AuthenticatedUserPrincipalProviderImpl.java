package anonym.chat.web.security;

import anonym.chat.web.abstractions.auth.AuthenticatedUserPrincipalProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AuthenticatedUserPrincipalProviderImpl implements AuthenticatedUserPrincipalProvider {
    @Override
    public Optional<Authentication> getPrincipal() {
        return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication());
    }
}
