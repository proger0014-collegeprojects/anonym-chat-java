package anonym.chat.web.events.publishers.room;

import anonym.chat.core.models.message.room.RoomMessageModel;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.web.abstractions.events.EventsPublisher;
import anonym.chat.web.abstractions.events.RoomPrivateMessageEventsPublisher;
import anonym.chat.web.events.contracts.room.message.CreatedMessageEvent;
import anonym.chat.web.events.contracts.room.message.RepliedMessageEvent;
import anonym.chat.web.helpers.events.RoomMessageHelperEvent;
import org.springframework.stereotype.Component;

@Component
public class RoomPrivateMessageEventsPublisherImpl implements RoomPrivateMessageEventsPublisher {
    private final EventsPublisher eventsPublisher;
    private final RoomMessageHelperEvent roomMessageHelperEvent;

    public RoomPrivateMessageEventsPublisherImpl(
            EventsPublisher eventsPublisher,
            RoomMessageHelperEvent roomMessageHelperEvent) {
        this.eventsPublisher = eventsPublisher;
        this.roomMessageHelperEvent = roomMessageHelperEvent;
    }

    @Override
    public ResultT<Boolean> createdNewMessage(RoomMessageModel message, UserModel authUser, UserModel userDestination) {
        CreatedMessageEvent event = roomMessageHelperEvent.toEventFrom(message, authUser.getId());

        return eventsPublisher.publishToUser(
            userDestination,
            event,
            "{}/created",
            USER_PATH,
            message.getRoomId()
        );
    }

    @Override
    public ResultT<Boolean> repliedToMessage(RoomMessageModel replyMessage, UserModel userDestination) {
        RepliedMessageEvent event = roomMessageHelperEvent.toEventFrom(replyMessage);

        return eventsPublisher.publishToUser(
            userDestination,
            event,
            "{}/replied",
            USER_PATH,
            replyMessage.getRoomId()
        );
    }
}
