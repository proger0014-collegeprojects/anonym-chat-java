package anonym.chat.web.abstractions;

import java.util.List;
import java.util.function.Function;

public interface ConfigSplitter {
    <TItemType> List<TItemType> split(String source, Function<String, TItemType> converter);
    List<String> split(String source);
}
