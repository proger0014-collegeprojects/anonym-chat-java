package anonym.chat.web.helpers.events;

import anonym.chat.core.abstractions.room.profile.RoomProfileService;
import anonym.chat.core.models.room.profile.RoomProfileModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.web.contracts.room.profile.RoomProfileFullResponse;
import anonym.chat.web.events.contracts.room.profile.CreatedRoomProfileEvent;
import anonym.chat.web.events.contracts.room.profile.InstalledRoomProfileEvent;
import anonym.chat.web.helpers.contracts.RoomProfileHelperContract;
import anonym.chat.web.mappers.events.RoomProfileEventsMapper;
import org.springframework.stereotype.Component;

@Component
public class RoomProfileHelperEvent {
    private final RoomProfileHelperContract roomProfileHelperContract;
    private final RoomProfileEventsMapper roomProfileEventsMapper;
    private final RoomProfileService roomProfileService;

    public RoomProfileHelperEvent(
            RoomProfileHelperContract roomProfileHelperContract,
            RoomProfileEventsMapper roomProfileEventsMapper,
            RoomProfileService roomProfileService) {
        this.roomProfileHelperContract = roomProfileHelperContract;
        this.roomProfileEventsMapper = roomProfileEventsMapper;
        this.roomProfileService = roomProfileService;
    }

    public InstalledRoomProfileEvent toEventFrom(long userId, long roomId) {
        ResultT<RoomProfileModel> roomProfileResult = roomProfileService.getByRoomIdAndUserId(roomId, userId);

        RoomProfileModel roomProfile = roomProfileResult.getData();

        RoomProfileFullResponse roomProfileFullResponse = roomProfileHelperContract
                .toRoomProfileFullResponseFromRoomProfile(roomProfile);

        return roomProfileEventsMapper.toEventFrom(roomProfileFullResponse, userId);
    }

    public CreatedRoomProfileEvent toEventFrom(RoomProfileModel model) {
        RoomProfileFullResponse roomProfileFullResponse = roomProfileHelperContract
                .toRoomProfileFullResponseFromRoomProfile(model);

        return roomProfileEventsMapper.toEventFrom(roomProfileFullResponse);
    }
}
