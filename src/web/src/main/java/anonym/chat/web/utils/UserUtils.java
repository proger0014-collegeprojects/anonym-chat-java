package anonym.chat.web.utils;

import anonym.chat.core.models.user.UserModel;
import org.apache.commons.lang3.StringUtils;

public final class UserUtils {
    public static String getUserPrincipal(UserModel user) {
        return StringUtils.isEmpty(user.getLogin())
                ? String.valueOf(user.getId())
                : user.getLogin();
    }
}
