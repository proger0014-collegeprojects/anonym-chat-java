package anonym.chat.web.abstractions.auth;

import anonym.chat.core.utils.result.ResultT;
import jakarta.servlet.http.HttpServletRequest;

public interface LoginProvider {
    ResultT<Boolean> login(String principal, HttpServletRequest request);
    ResultT<Boolean> login(String principal);
}
