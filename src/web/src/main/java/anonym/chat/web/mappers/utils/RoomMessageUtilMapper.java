package anonym.chat.web.mappers.utils;

import anonym.chat.core.models.message.room.RoomMessageModel;
import anonym.chat.web.utils.dto.message.reply.ShortMessageReplyInfo;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(
    componentModel = MappingConstants.ComponentModel.SPRING,
    implementationName = "RoomMessageUtilMapperImplWeb"
)
public interface RoomMessageUtilMapper {
    ShortMessageReplyInfo toShortMessageReplyInfoFrom(RoomMessageModel message);
}
