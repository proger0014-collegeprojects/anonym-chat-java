package anonym.chat.web.contracts.room.profile;

public record IsInstalledResponse(
    boolean isInstalled
) { }
