package anonym.chat.web.mappers;

import anonym.chat.core.constants.ResultConstants;
import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.utils.result.ResultError;
import anonym.chat.web.contracts.error.Error;
import anonym.chat.web.contracts.error.ListError;
import anonym.chat.web.contracts.section.SectionResponse;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", implementationName = "CommonMapperWeb")
public interface CommonMapper {
    ListError toListErrorFromResultError(ResultError resultError);

    default Error toErrorFromResultError(ResultError resultError) {
        try {
            var errors = resultError.getErrors();

            String singleError = errors.get(ResultConstants.SINGLE_ERROR_KEY).getFirst();

            return new Error(
                resultError.getType(),
                resultError.getTitle(),
                resultError.getStatus(),
                singleError
            );
        } catch (Exception ignored) { }

        return null;
    }

    default <TItemModel, TItemResponse>
    SectionResponse<TItemResponse> toSectionResponseFromSectionCollectionAndResponseList(
            SectionCollection<TItemModel> sectionCollection,
            List<TItemResponse> responseList) {

        return new SectionResponse<>(
            sectionCollection.section(),
            sectionCollection.lastSection(),
            sectionCollection.perSection(),
            sectionCollection.total(),
            responseList
        );
    }
}
