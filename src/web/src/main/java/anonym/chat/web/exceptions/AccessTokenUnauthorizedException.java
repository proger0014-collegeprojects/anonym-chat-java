package anonym.chat.web.exceptions;

import anonym.chat.web.constants.errors.AuthConstants;
import anonym.chat.web.utils.HttpResponseEntityBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class AccessTokenUnauthorizedException extends HttpBaseException {
    public AccessTokenUnauthorizedException() {
        super(getResponseException());
    }

    private static ResponseEntity<?> getResponseException() {
        return HttpResponseEntityBuilder.start()
                .statusCode(HttpStatus.UNAUTHORIZED.value())
                .header(HttpHeaders.WWW_AUTHENTICATE, AuthConstants.BEARER)
                .build();
    }
}
