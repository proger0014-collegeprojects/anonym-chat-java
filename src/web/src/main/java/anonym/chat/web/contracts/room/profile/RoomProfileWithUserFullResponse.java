package anonym.chat.web.contracts.room.profile;

import anonym.chat.web.contracts.user.UserResponse;

public record RoomProfileWithUserFullResponse(
    RoomProfileFullResponse roomProfile,
    UserResponse user
) { }
