package anonym.chat.web.mappers.contracts;

import anonym.chat.core.models.user.UserModel;
import anonym.chat.web.contracts.auth.register.RegisterResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", implementationName = "UserMapperImplWeb")
public interface UserMapper {
    @Mapping(target = "role", expression = "java(userModel.getRole().getValue())")
    @Mapping(target = "login", expression = "java(userModel.getLogin() == null ? Long.toString(userModel.getId()) : userModel.getLogin())")
    RegisterResponse toRegisterResponseFromUserModel(UserModel userModel);
}
