package anonym.chat.web.events.publishers;

import anonym.chat.core.models.avatar.AvatarModel;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.web.abstractions.events.AvatarEventsPublisher;
import anonym.chat.web.abstractions.events.EventsPublisher;
import anonym.chat.web.events.contracts.CreatedAvatarEvent;
import anonym.chat.web.helpers.events.AvatarHelperEvent;
import org.springframework.stereotype.Component;

@Component
public class AvatarEventsPublisherImpl implements AvatarEventsPublisher {
    private final EventsPublisher eventsPublisher;
    private final AvatarHelperEvent avatarHelperEvent;

    public AvatarEventsPublisherImpl(
            EventsPublisher eventsPublisher,
            AvatarHelperEvent avatarHelperEvent) {
        this.eventsPublisher = eventsPublisher;
        this.avatarHelperEvent = avatarHelperEvent;
    }

    @Override
    public ResultT<Boolean> createdNewAvatar(AvatarModel avatar, UserModel userDestination) {
        CreatedAvatarEvent event = avatarHelperEvent.toEventFrom(avatar);

        return eventsPublisher.publishToUser(
            userDestination,
            event,
            "{}/created",
            USER_PATH
        );
    }
}
