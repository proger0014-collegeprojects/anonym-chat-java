package anonym.chat.web.abstractions.events;

import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;

public interface EventsPublisher {
    String USER_PATH = "/concrete-user";

    <TData> ResultT<Boolean> publish(TData data, String path, Object... args);
    <TData> ResultT<Boolean> publishToUser(UserModel userDestination, TData data, String path, Object... args);
}
