package anonym.chat.web.security;

import anonym.chat.core.abstractions.user.UserRepository;
import anonym.chat.core.models.user.UserModel;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public UserDetailsServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        boolean usernameHasId = false;

        Optional<UserModel> existsUserModel;

        try {
            long id = Long.parseLong(username);
            usernameHasId = true;

            existsUserModel = Optional.ofNullable(userRepository.getById(id));
        } catch(Exception ignored) {
            existsUserModel = Optional.ofNullable(userRepository.getByLogin(username));
        }

        if (existsUserModel.isPresent()) {
            String existsPasswordHash = userRepository.getPasswordHashById(existsUserModel.get().getId());
            String passwordHash = existsPasswordHash == null
                    ? passwordEncoder.encode(Long.toString(existsUserModel.get().getId()))
                    : existsPasswordHash;

            return new UserDetailsImpl(existsUserModel.get(), passwordHash);
        }

        String errorDetails = usernameHasId
                ? "id " + username
                : "логином " + username;

        throw new UsernameNotFoundException("Пользователь с " + errorDetails + " не найден");
    }
}
