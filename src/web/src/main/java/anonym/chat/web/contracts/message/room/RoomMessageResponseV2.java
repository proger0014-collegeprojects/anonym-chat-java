package anonym.chat.web.contracts.message.room;

import anonym.chat.web.contracts.message.MessagePreviewResponseV2;
import anonym.chat.web.contracts.room.profile.RoomProfileFullResponse;

import java.time.LocalDateTime;

public record RoomMessageResponseV2(
    long id,
    long userId,
    MessagePreviewResponseV2 reply,
    RoomPrivateMessageInfoResponse asPrivateMessageInfo,
    String body,
    LocalDateTime createdAt,
    RoomProfileFullResponse roomProfile
) { }
