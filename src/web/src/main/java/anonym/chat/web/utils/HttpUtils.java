package anonym.chat.web.utils;

import anonym.chat.core.utils.result.ResultError;
import anonym.chat.core.utils.result.ResultUtils;
import anonym.chat.web.contracts.error.Error;
import anonym.chat.web.contracts.error.ListError;
import anonym.chat.web.mappers.CommonMapper;
import org.springframework.http.ResponseEntity;

public final class HttpUtils {
    public static ResponseEntity<?> responseError(ResultError resultError, CommonMapper commonMapper) {
        if (ResultUtils.isSingleError(resultError)) {
            Error error = commonMapper.toErrorFromResultError(resultError);
            return ResponseEntity.status(error.status()).body(error);
        } else {
            ListError error = commonMapper.toListErrorFromResultError(resultError);
            return ResponseEntity.status(error.status()).body(error);
        }
    }
}
