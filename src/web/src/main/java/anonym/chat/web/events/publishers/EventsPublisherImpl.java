package anonym.chat.web.events.publishers;

import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.core.utils.result.ResultUtils;
import anonym.chat.web.abstractions.StringTemplateProvider;
import anonym.chat.web.abstractions.events.EventsPublisher;
import anonym.chat.web.utils.UserUtils;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
public class EventsPublisherImpl implements EventsPublisher {
    private final StringTemplateProvider stringTemplateProvider;
    private final SimpMessagingTemplate simpMessagingTemplate;

    public EventsPublisherImpl(
            StringTemplateProvider stringTemplateProvider,
            SimpMessagingTemplate simpMessagingTemplate) {
        this.stringTemplateProvider = stringTemplateProvider;
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @Override
    public <TData> ResultT<Boolean> publish(TData data, String path, Object... args) {
        String pathFormatted = stringTemplateProvider.getFromTemplate(path, args);
        simpMessagingTemplate.convertAndSend(pathFormatted, data);

        return ResultUtils.fromTData(true);
    }

    @Override
    public <TData> ResultT<Boolean> publishToUser(
            UserModel userDestination,
            TData data,
            String path,
            Object... args) {
        String userPrincipal = UserUtils.getUserPrincipal(userDestination);

        String pathFormatted = stringTemplateProvider.getFromTemplate(path, args);
        simpMessagingTemplate.convertAndSendToUser(userPrincipal, pathFormatted, data);

        return ResultUtils.fromTData(true);
    }
}
