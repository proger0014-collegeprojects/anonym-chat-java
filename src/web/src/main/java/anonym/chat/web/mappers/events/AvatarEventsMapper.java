package anonym.chat.web.mappers.events;

import anonym.chat.web.contracts.avatar.AvatarResponse;
import anonym.chat.web.events.contracts.CreatedAvatarEvent;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(
    componentModel = MappingConstants.ComponentModel.SPRING,
    implementationName = "AvatarEventsMapperImplWeb"
)
public interface AvatarEventsMapper {
    CreatedAvatarEvent toEventFrom(AvatarResponse source);
}
