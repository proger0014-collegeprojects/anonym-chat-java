package anonym.chat.web.helpers.events;

import anonym.chat.core.models.avatar.AvatarModel;
import anonym.chat.web.contracts.avatar.AvatarResponse;
import anonym.chat.web.events.contracts.CreatedAvatarEvent;
import anonym.chat.web.helpers.contracts.AvatarHelperContract;
import anonym.chat.web.mappers.events.AvatarEventsMapper;
import org.springframework.stereotype.Component;

@Component
public class AvatarHelperEvent {
    private final AvatarHelperContract avatarHelperContract;
    private final AvatarEventsMapper avatarEventsMapper;

    public AvatarHelperEvent(
            AvatarHelperContract avatarHelperContract,
            AvatarEventsMapper avatarEventsMapper) {
        this.avatarHelperContract = avatarHelperContract;
        this.avatarEventsMapper = avatarEventsMapper;
    }

    public CreatedAvatarEvent toEventFrom(AvatarModel avatar) {
        AvatarResponse contract = avatarHelperContract
                .toAvatarResponseFromAvatarModel(avatar);

        return avatarEventsMapper.toEventFrom(contract);
    }
}
