package anonym.chat.web.configs.providers;

import anonym.chat.core.abstractions.providers.config.RefreshTokenConfigProvider;
import anonym.chat.core.utils.token.RefreshTokenConfig;
import anonym.chat.web.mappers.configs.RefreshTokenProviderConfigMapper;
import org.springframework.stereotype.Service;

@Service
public class RefreshTokenConfigProviderImpl implements RefreshTokenConfigProvider {
    private final anonym.chat.web.configs.RefreshTokenConfig refreshTokenConfig;
    private final RefreshTokenProviderConfigMapper refreshTokenProviderConfigMapper;

    public RefreshTokenConfigProviderImpl(
            anonym.chat.web.configs.RefreshTokenConfig refreshTokenConfig,
            RefreshTokenProviderConfigMapper refreshTokenProviderConfigMapper) {
        this.refreshTokenConfig = refreshTokenConfig;
        this.refreshTokenProviderConfigMapper = refreshTokenProviderConfigMapper;
    }

    @Override
    public RefreshTokenConfig get() {
        return refreshTokenProviderConfigMapper.toCoreDtoFromWebDto(refreshTokenConfig);
    }
}
