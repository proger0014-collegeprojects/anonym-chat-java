package anonym.chat.web.contracts.error;

public record Error(
    String type,
    String title,
    int status,
    String description
) { }
