package anonym.chat.web.abstractions.events;

import anonym.chat.core.models.room.profile.RoomProfileModel;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;

public interface RoomProfileEventsPublisher {
    String USER_PATH = "/room/profile";
    String PATH = "/room/{}/profile";

    ResultT<Boolean> installedProfileToRoom(long userId, long roomId);
    ResultT<Boolean> createdRoomProfile(RoomProfileModel roomProfile, UserModel userDestination);
}
