package anonym.chat.web.contracts.auth.login;

public record LoginResponse(
    long userId
) { }
