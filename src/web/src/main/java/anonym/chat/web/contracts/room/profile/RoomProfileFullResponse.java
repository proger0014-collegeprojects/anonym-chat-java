package anonym.chat.web.contracts.room.profile;

import anonym.chat.web.contracts.avatar.AvatarResponse;

public record RoomProfileFullResponse(
    long id,
    String nickname,
    AvatarResponse avatar
) { }
