package anonym.chat.web.validations.auth;

import anonym.chat.core.constants.validations.CommonValidationMessages;
import anonym.chat.core.models.ValidationModel;
import jakarta.validation.constraints.NotEmpty;

public record LoginValidator(
    @NotEmpty(message = CommonValidationMessages.FIELD_EMPTY)
    String login,
    String password
) implements ValidationModel { }
