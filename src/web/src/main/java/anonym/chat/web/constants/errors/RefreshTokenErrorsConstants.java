package anonym.chat.web.constants.errors;

public final class RefreshTokenErrorsConstants {
    public static final String TYPE = AuthErrorsConstants.TYPE + "/refresh-token";

    public static final String NOT_EXISTS_COOKIE_TYPE = TYPE + "/not-exists-cookie";
    public static final String NOT_EXISTS_COOKIE_TITLE = TYPE + "/not-exists-cookie";
    public static final int NOT_EXISTS_COOKIE_STATUS = 400;
    public static final String NOT_EXISTS_COOKIE_DESCRIPTION = TYPE + "/not-exists-cookie";
}
