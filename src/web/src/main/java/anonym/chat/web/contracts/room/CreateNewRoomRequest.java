package anonym.chat.web.contracts.room;

import org.springframework.web.bind.annotation.RequestParam;

public record CreateNewRoomRequest(
    String name,
    @RequestParam(required = false) String description
) { }
