package anonym.chat.web.contracts.message.room;

public record CreateNewPrivateMessageRequest(
    long destinationUserId,
    String body
) { }
