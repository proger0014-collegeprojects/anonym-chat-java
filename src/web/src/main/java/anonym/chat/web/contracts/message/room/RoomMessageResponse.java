package anonym.chat.web.contracts.message.room;

import anonym.chat.web.contracts.message.MessagePreviewResponse;

import java.time.LocalDateTime;

public record RoomMessageResponse(
    long id,
    long userId,
    MessagePreviewResponse reply,
    RoomPrivateMessageInfoResponse asPrivateMessageInfo,
    String body,
    LocalDateTime createdAt
) { }
