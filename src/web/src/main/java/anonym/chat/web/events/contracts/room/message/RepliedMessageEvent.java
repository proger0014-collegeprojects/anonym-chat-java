package anonym.chat.web.events.contracts.room.message;

import anonym.chat.web.utils.dto.message.reply.ReplyInfo;
import anonym.chat.web.utils.dto.message.reply.ShortMessageReplyInfo;

public record RepliedMessageEvent(
    ReplyInfo reply,
    ShortMessageReplyInfo parentMessage
) { }
