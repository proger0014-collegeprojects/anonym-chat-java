package anonym.chat.web.configs;

import anonym.chat.web.exceptions.handlers.FilterExceptionHandler;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

import java.util.logging.Filter;

@Configuration
public class FiltersConfig {
    private final FilterExceptionHandler filterExceptionHandler;

    public FiltersConfig(FilterExceptionHandler filterExceptionHandler) {
        this.filterExceptionHandler = filterExceptionHandler;
    }

    @Bean
    public FilterRegistrationBean<FilterExceptionHandler> filterExceptionHandlerRegistration() {
        FilterRegistrationBean<FilterExceptionHandler> filterRegistration = new FilterRegistrationBean<>();

        filterRegistration.setFilter(filterExceptionHandler);
        filterRegistration.setOrder(Ordered.HIGHEST_PRECEDENCE);

        return filterRegistration;
    }
}
