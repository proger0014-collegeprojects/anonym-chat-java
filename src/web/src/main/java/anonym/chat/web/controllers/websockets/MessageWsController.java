package anonym.chat.web.controllers.websockets;

import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class MessageWsController {
    private final SimpMessagingTemplate simpMessagingTemplate;

    public MessageWsController(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }
}
