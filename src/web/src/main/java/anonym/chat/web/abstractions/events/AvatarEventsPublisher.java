package anonym.chat.web.abstractions.events;

import anonym.chat.core.models.avatar.AvatarModel;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;

public interface AvatarEventsPublisher {
    String USER_PATH = "/avatar";

    ResultT<Boolean> createdNewAvatar(AvatarModel avatar, UserModel userDestination);
}
