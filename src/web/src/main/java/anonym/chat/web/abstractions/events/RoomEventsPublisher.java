package anonym.chat.web.abstractions.events;

import anonym.chat.core.models.room.RoomModel;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;

public interface RoomEventsPublisher {
    String PATH = "/room";

    ResultT<Boolean> connectedToRoom(UserModel user, long roomId);
    ResultT<Boolean> disconnectedFromRoom(long userId, long roomId);
    ResultT<Boolean> createdRoom(RoomModel room);
}
