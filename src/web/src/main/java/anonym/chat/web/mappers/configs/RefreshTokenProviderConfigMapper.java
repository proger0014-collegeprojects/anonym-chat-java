package anonym.chat.web.mappers.configs;

import anonym.chat.core.utils.token.RefreshTokenConfig;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(
        componentModel = MappingConstants.ComponentModel.SPRING,
        implementationName = "RefreshTokenProviderConfigMapperImplWeb")
public interface RefreshTokenProviderConfigMapper {
    RefreshTokenConfig toCoreDtoFromWebDto(anonym.chat.web.configs.RefreshTokenConfig source);
}
