package anonym.chat.web.mappers.contracts;

import anonym.chat.core.models.avatar.CreateNewAvatar;
import anonym.chat.core.models.file.FileModel;
import anonym.chat.web.contracts.avatar.AvatarResponse;
import anonym.chat.web.contracts.avatar.CreateNewAvatarRequest;
import anonym.chat.web.contracts.file.FileResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.io.File;
import java.io.InputStream;

@Mapper(componentModel = "spring", implementationName = "AvatarMapperImplWeb")
public interface AvatarMapper {
    @Mapping(target = "userId", source = "userId")
    @Mapping(target = "file", source = "file")
    @Mapping(target = "offsetX", source = "source.offsetX")
    @Mapping(target = "offsetY", source = "source.offsetY")
    @Mapping(target = "size", source = "source.size")
    CreateNewAvatar toCreateNewAvatarFromCreateNewAvatarRequest(CreateNewAvatarRequest source, long userId, InputStream file);

    @Mapping(target = "id", source = "avatarId")
    @Mapping(target = "size", source = "fileModel.size")
    @Mapping(target = "fileType", expression = "java(fileModel.getType().getValue())")
    @Mapping(target = "url", source = "url")
    FileResponse toFileResponseFromFileModelAndAvatarIdAndUrl(FileModel fileModel, long avatarId, String url);

    @Mapping(target = "id", source = "avatarId")
    @Mapping(target = "size", source = "fileModel.size")
    @Mapping(target = "url", source = "url")
    AvatarResponse toAvatarResponseFromFileModelAndAvatarIdAndUrl(FileModel fileModel, long avatarId, String url);
}
