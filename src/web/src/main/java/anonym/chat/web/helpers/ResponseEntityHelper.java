package anonym.chat.web.helpers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class ResponseEntityHelper {
    private final ObjectMapper objectMapper;

    public ResponseEntityHelper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public void addToServletResponse(ResponseEntity<?> source, HttpServletResponse response) throws IOException {
        Optional<String> jsonBody = Optional.empty();
        try {
            String jsonBodyActual = objectMapper.writeValueAsString(source.getBody());

            jsonBody = Optional.of(jsonBodyActual);
        } catch (JsonProcessingException ignore) { }

        response.setStatus(source.getStatusCode().value());

        for (Map.Entry<String, List<String>> header : source.getHeaders().entrySet()) {
            String headerKey = header.getKey();
            for (String subHeaderValue : header.getValue()) {
                response.addHeader(headerKey, subHeaderValue);
            }
        }

        if (jsonBody.isPresent()) {
            response.getWriter().write(jsonBody.get());
            response.flushBuffer();
        }
    }
}
