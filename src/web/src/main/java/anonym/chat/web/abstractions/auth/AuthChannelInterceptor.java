package anonym.chat.web.abstractions.auth;

import org.springframework.messaging.support.ChannelInterceptor;

public interface AuthChannelInterceptor extends ChannelInterceptor { }
