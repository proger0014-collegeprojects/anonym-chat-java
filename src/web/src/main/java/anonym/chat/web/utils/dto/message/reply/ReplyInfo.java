package anonym.chat.web.utils.dto.message.reply;

import anonym.chat.web.contracts.room.profile.RoomProfileWithUserFullResponse;

public record ReplyInfo(
    ShortMessageReplyInfo replyMessage,
    RoomProfileWithUserFullResponse user
) { }
