package anonym.chat.web.contracts.room.profile;

public record CreateNewRoomProfileRequest(
    Long avatarId,
    String nickname
) { }
