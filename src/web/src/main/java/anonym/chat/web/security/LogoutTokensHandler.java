package anonym.chat.web.security;

import anonym.chat.web.abstractions.auth.RefreshTokenCookieProvider;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

@Primary
@Component
public class LogoutTokensHandler implements LogoutHandler {
    private final RefreshTokenCookieProvider refreshTokenCookieProvider;

    public LogoutTokensHandler(RefreshTokenCookieProvider refreshTokenCookieProvider) {
        this.refreshTokenCookieProvider = refreshTokenCookieProvider;
    }

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        refreshTokenCookieProvider.removeRefreshToken(response);
    }
}
