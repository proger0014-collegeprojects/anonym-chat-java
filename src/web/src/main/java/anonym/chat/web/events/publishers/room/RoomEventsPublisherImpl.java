package anonym.chat.web.events.publishers.room;

import anonym.chat.core.models.room.RoomModel;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.web.abstractions.events.EventsPublisher;
import anonym.chat.web.abstractions.events.RoomEventsPublisher;
import anonym.chat.web.events.contracts.room.ConnectedToRoomEvent;
import anonym.chat.web.events.contracts.room.CreatedRoomEvent;
import anonym.chat.web.events.contracts.room.DisconnectedFromRoomEvent;
import anonym.chat.web.helpers.events.RoomHelperEvent;
import org.springframework.stereotype.Component;

@Component
public class RoomEventsPublisherImpl implements RoomEventsPublisher {
    private final EventsPublisher eventsPublisher;
    private final RoomHelperEvent roomHelperEvent;

    public RoomEventsPublisherImpl(
            EventsPublisher eventsPublisher,
            RoomHelperEvent roomHelperEvent) {
        this.eventsPublisher = eventsPublisher;
        this.roomHelperEvent = roomHelperEvent;
    }

    @Override
    public ResultT<Boolean> connectedToRoom(UserModel user, long roomId) {
        ConnectedToRoomEvent event = roomHelperEvent.toEventFrom(user, roomId);

        return eventsPublisher.publish(
            event,
            "{}/{}/connected-user",
            PATH,
            roomId
        );
    }

    @Override
    public ResultT<Boolean> disconnectedFromRoom(long userId, long roomId) {
        DisconnectedFromRoomEvent event = new DisconnectedFromRoomEvent(userId);

        return eventsPublisher.publish(
            event,
            "{}/{}/disconnected-user",
            PATH,
            roomId
        );
    }

    @Override
    public ResultT<Boolean> createdRoom(RoomModel room) {
        CreatedRoomEvent event = roomHelperEvent.toEventFrom(room);

        return eventsPublisher.publish(
            event,
            "{}/created",
            PATH
        );
    }
}
