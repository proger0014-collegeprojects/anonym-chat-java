package anonym.chat.web.abstractions.auth;

import anonym.chat.core.utils.result.ResultT;

public interface AuthenticationProvider {
    ResultT<Boolean> authenticate(String principal);
    ResultT<Boolean> authenticate(String login, String password);
}
