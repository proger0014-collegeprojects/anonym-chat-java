package anonym.chat.web.contracts.error;

import java.util.List;
import java.util.Map;

public record ListError(
    String type,
    String title,
    int status,
    Map<String, List<String>> errors
) { }
