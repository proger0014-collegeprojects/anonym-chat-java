package anonym.chat.web.helpers.contracts;

import anonym.chat.core.abstractions.room.RoomService;
import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.models.room.RoomModel;
import anonym.chat.web.contracts.room.RoomResponse;
import anonym.chat.web.contracts.section.SectionResponse;
import anonym.chat.web.mappers.contracts.RoomMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RoomHelperContract {
    private final RoomMapper roomMapper;

    private final RoomService roomService;

    public RoomHelperContract(
            RoomMapper roomMapper,
            RoomService roomService) {
        this.roomMapper = roomMapper;
        this.roomService = roomService;
    }

    public RoomResponse toRoomResponseFromRoomModel(RoomModel roomModel) {
        int countConnectedUsers = roomService.getConnectedUsersCountInRoom(roomModel.getId()).getData();

        return roomMapper.toRoomResponseFromRoomModel(roomModel, countConnectedUsers);
    }

    public SectionResponse<RoomResponse> toSectionResponseRoomsFromSection(SectionCollection<RoomModel> section) {
        List<RoomResponse> convertedItems = section.items().stream()
                .map(this::toRoomResponseFromRoomModel)
                .toList();

        return new SectionResponse<>(
            section.section(),
            section.lastSection(),
            section.perSection(),
            section.total(),
            convertedItems
        );
    }
}
