package anonym.chat.web.contracts.avatar;

public record AvatarResponse(
    long id,
    long size,
    String url
) { }
