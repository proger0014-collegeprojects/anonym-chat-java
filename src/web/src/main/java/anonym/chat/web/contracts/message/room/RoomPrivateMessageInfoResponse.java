package anonym.chat.web.contracts.message.room;

public record RoomPrivateMessageInfoResponse(
    long destinationUserId
) { }
