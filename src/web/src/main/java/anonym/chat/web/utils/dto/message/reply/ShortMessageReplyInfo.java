package anonym.chat.web.utils.dto.message.reply;

public record ShortMessageReplyInfo(
    long id,
    String body
) { }
