package anonym.chat.web.constants.errors;

public final class CommonErrorsConstants {
    public static final String TYPE = anonym.chat.core.constants.errors.CommonErrorsConstants.TYPE + "/web";

    public static final String CANNOT_INSTALL_COOKIE_TYPE = TYPE + "/cannot-install-cookie";
    public static final String CANNOT_INSTALL_COOKIE_TITLE = "Не удалось установить куки!";
    public static final int CANNOT_INSTALL_COOKIE_STATUS = 400;
    public static final String CANNOT_INSTALL_COOKIE_DESCRIPTION = "Возможно, вы совершаете запрос не с браузера!";
}
