package anonym.chat.web.abstractions.auth;

import org.springframework.security.core.Authentication;

import java.util.Optional;

public interface AuthenticatedUserPrincipalProvider {
    Optional<Authentication> getPrincipal();
}
