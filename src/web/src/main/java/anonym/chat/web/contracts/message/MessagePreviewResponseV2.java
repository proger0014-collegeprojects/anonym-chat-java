package anonym.chat.web.contracts.message;

import anonym.chat.web.contracts.room.profile.RoomProfileFullResponse;

public record MessagePreviewResponseV2(
    long userId,
    long messageId,
    String shortBody,
    RoomProfileFullResponse roomProfile
) { }
