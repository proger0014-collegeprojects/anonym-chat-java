package anonym.chat.web.exceptions;

import org.springframework.http.ResponseEntity;

public class HttpBaseException extends RuntimeException {
    private final ResponseEntity<?> response;

    public HttpBaseException(ResponseEntity<?> response) {
        this.response = response;
    }

    public ResponseEntity<?> getResponse() {
        return this.response;
    }
}
