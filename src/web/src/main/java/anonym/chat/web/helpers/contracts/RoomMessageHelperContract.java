package anonym.chat.web.helpers.contracts;

import anonym.chat.core.abstractions.message.room.RoomMessageService;
import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.models.message.room.GetInRoomById;
import anonym.chat.core.models.message.room.RoomMessageModel;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.web.contracts.message.CreateNewMessageResponse;
import anonym.chat.web.contracts.message.MessagePreviewResponseV2;
import anonym.chat.web.contracts.message.room.RoomMessageResponse;
import anonym.chat.web.contracts.message.room.RoomMessageResponseV2;
import anonym.chat.web.contracts.message.user.UserIdResponse;
import anonym.chat.web.contracts.room.profile.RoomProfileFullResponse;
import anonym.chat.web.contracts.section.SectionResponse;
import anonym.chat.web.mappers.contracts.RoomMessageMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RoomMessageHelperContract {
    private final RoomMessageMapper roomMessageMapper;

    private final RoomMessageService roomMessageService;
    private final RoomProfileHelperContract roomProfileHelperContract;

    public RoomMessageHelperContract(
            RoomMessageMapper roomMessageMapper,
            RoomMessageService roomMessageService,
            RoomProfileHelperContract roomProfileHelperContract) {
        this.roomMessageMapper = roomMessageMapper;
        this.roomMessageService = roomMessageService;
        this.roomProfileHelperContract = roomProfileHelperContract;
    }


    public CreateNewMessageResponse toCreateNewMessageResponse(RoomMessageModel target) {
        return new CreateNewMessageResponse(target.getId(), target.getCreatedAt());
    }

    public RoomMessageResponse toRoomMessageResponse(RoomMessageModel target, long userIdForPrivates) {
        RoomMessageModel replyModel = target.getReply() == null
                ? null
                : roomMessageService.getInRoomById(new GetInRoomById(target.getRoomId(), userIdForPrivates, target.getReply()))
                .getData();


        return roomMessageMapper.toRoomMessageResponseFrom(target, replyModel);
    }

    public SectionResponse<RoomMessageResponse> toSectionResponseRoomMessage(SectionCollection<RoomMessageModel> target, long userIdForPrivates) {
        List<RoomMessageModel> list = target.items();

        List<RoomMessageResponse> convertedItems = list.stream().map(rm -> toRoomMessageResponse(rm, userIdForPrivates)).toList();

        return new SectionResponse<>(
            target.section(),
            target.lastSection(),
            target.perSection(),
            target.total(),
            convertedItems
        );
    }

    public SectionResponse<UserIdResponse> toSectionResponseDestinationUserIdFromSectionCollectionUserModel(SectionCollection<UserModel> target) {
        List<UserIdResponse> convertedList = target.items().stream()
                .map(u -> new UserIdResponse(u.getId()))
                .toList();

        return new SectionResponse<>(
            target.section(),
            target.lastSection(),
            target.perSection(),
            target.total(),
            convertedList
        );
    }

    public SectionResponse<RoomMessageResponseV2> toSectionResponseMessagesV2(SectionCollection<RoomMessageModel> target, long userIdForPrivates) {
        List<RoomMessageModel> targetItems = target.items();

        List<RoomMessageResponseV2> responseV2List = targetItems.stream().map(rm -> {
            RoomProfileFullResponse roomProfileFullResponse = roomProfileHelperContract
                    .toRoomProfileFullResponseFromUserIdAndRoomId(rm.getUserId(), rm.getRoomId());

            RoomMessageResponse messageResponse = toRoomMessageResponse(rm, userIdForPrivates);

            MessagePreviewResponseV2 reply = null;

            if (messageResponse.reply() != null) {
                RoomProfileFullResponse roomProfileFullResponseOfReply = roomProfileHelperContract
                    .toRoomProfileFullResponseFromUserIdAndRoomId(messageResponse.reply().userId(), rm.getRoomId());

                 reply = new MessagePreviewResponseV2(
                    messageResponse.reply().userId(),
                    messageResponse.reply().messageId(),
                    messageResponse.reply().shortBody(),
                    roomProfileFullResponseOfReply
                );
            }

            return new RoomMessageResponseV2(
                messageResponse.id(),
                messageResponse.userId(),
                reply,
                messageResponse.asPrivateMessageInfo(),
                messageResponse.body(),
                messageResponse.createdAt(),
                roomProfileFullResponse
            );
        }).toList();

        return new SectionResponse<>(
            target.section(),
            target.lastSection(),
            target.perSection(),
            target.total(),
            responseV2List
        );
    }
}
