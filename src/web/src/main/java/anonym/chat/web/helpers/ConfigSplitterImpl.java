package anonym.chat.web.helpers;

import anonym.chat.web.abstractions.ConfigSplitter;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

@Component
public class ConfigSplitterImpl implements ConfigSplitter {
    private final String SEPARATOR = ",";

    @Override
    public <TItemType> List<TItemType> split(String source, Function<String, TItemType> converter) {
        return Arrays.stream(source.split(SEPARATOR))
                .map(converter)
                .toList();
    }

    @Override
    public List<String> split(String source) {
        return Arrays.asList(source.split(SEPARATOR));
    }
}
