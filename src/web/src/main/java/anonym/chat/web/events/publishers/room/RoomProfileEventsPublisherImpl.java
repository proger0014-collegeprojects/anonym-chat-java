package anonym.chat.web.events.publishers.room;

import anonym.chat.core.models.room.profile.RoomProfileModel;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.web.abstractions.events.EventsPublisher;
import anonym.chat.web.abstractions.events.RoomProfileEventsPublisher;
import anonym.chat.web.events.contracts.room.profile.CreatedRoomProfileEvent;
import anonym.chat.web.events.contracts.room.profile.InstalledRoomProfileEvent;
import anonym.chat.web.helpers.events.RoomProfileHelperEvent;
import org.springframework.stereotype.Component;

@Component
public class RoomProfileEventsPublisherImpl implements RoomProfileEventsPublisher {
    private final EventsPublisher eventsPublisher;
    private final RoomProfileHelperEvent roomProfileHelperEvent;

    public RoomProfileEventsPublisherImpl(
            EventsPublisher eventsPublisher,
            RoomProfileHelperEvent roomProfileHelperEvent) {
        this.eventsPublisher = eventsPublisher;
        this.roomProfileHelperEvent = roomProfileHelperEvent;
    }

    @Override
    public ResultT<Boolean> installedProfileToRoom(long userId, long roomId) {
        InstalledRoomProfileEvent event = roomProfileHelperEvent
                .toEventFrom(userId, roomId);

        return eventsPublisher.publish(
            event,
            "{}/installed",
            PATH,
            roomId
        );
    }

    @Override
    public ResultT<Boolean> createdRoomProfile(RoomProfileModel roomProfile, UserModel userDestination) {
        CreatedRoomProfileEvent event = roomProfileHelperEvent.toEventFrom(roomProfile);

        return eventsPublisher.publishToUser(
            userDestination,
            event,
            "{}/created",
            USER_PATH);
    }
}
