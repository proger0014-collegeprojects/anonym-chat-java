package anonym.chat.web.security;

import anonym.chat.core.constants.errors.CommonErrorsConstants;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.core.utils.result.ResultUtils;
import anonym.chat.web.abstractions.auth.LoginProvider;
import anonym.chat.web.constants.errors.AuthErrorsConstants;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;

@Service
public class LoginProviderImpl implements LoginProvider {
    private final UserDetailsService userDetailsService;

    public LoginProviderImpl(
            UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    public ResultT<Boolean> login(String principal, HttpServletRequest request) {
        ResultT<UsernamePasswordAuthenticationToken> authenticationResult = getAuthentication(principal);

        if (authenticationResult.isFailure()) {
            return ResultUtils.fromTSingleError(authenticationResult.getError());
        }

        UsernamePasswordAuthenticationToken authentication = authenticationResult.getData();

        SecurityContext securityContext = SecurityContextHolder.createEmptyContext();

        try {
            WebAuthenticationDetails httpRequestAuthDetails = new WebAuthenticationDetailsSource().buildDetails(request);
            authentication.setDetails(httpRequestAuthDetails);
        } catch (IllegalStateException ignored) {
            return ResultUtils.fromTSingleError(
                AuthErrorsConstants.CANNOT_LOGIN_WITH_HTTP_REQUEST_TYPE,
                AuthErrorsConstants.CANNOT_LOGIN_WITH_HTTP_REQUEST_TITLE,
                AuthErrorsConstants.CANNOT_LOGIN_WITH_HTTP_REQUEST_STATUS,
                AuthErrorsConstants.CANNOT_LOGIN_WITH_HTTP_REQUEST_DESCRIPTION
            );
        }

        securityContext.setAuthentication(authentication);
        SecurityContextHolder.setContext(securityContext);

        return ResultUtils.fromTData(true);
    }

    @Override
    public ResultT<Boolean> login(String principal) {
        ResultT<UsernamePasswordAuthenticationToken> authenticationResult = getAuthentication(principal);

        if (authenticationResult.isFailure()) {
            return ResultUtils.fromTSingleError(authenticationResult.getError());
        }

        SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
        securityContext.setAuthentication(authenticationResult.getData());
        SecurityContextHolder.setContext(securityContext);

        return ResultUtils.fromTData(true);
    }

    private ResultT<UsernamePasswordAuthenticationToken> getAuthentication(String principal) {
        UserDetails existsUserDetails = null;

        try {
            existsUserDetails = userDetailsService.loadUserByUsername(principal);
        } catch (UsernameNotFoundException usernameNotFoundException) {
            return ResultUtils.fromTSingleError(
                CommonErrorsConstants.AUTH_TYPE,
                CommonErrorsConstants.AUTH_TITLE,
                CommonErrorsConstants.AUTH_STATUS,
                CommonErrorsConstants.AUTH_DESCRIPTION_INVALID_PASSWORD_OR_LOGIN
            );
        }

        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
            existsUserDetails, null, existsUserDetails.getAuthorities()
        );

        return ResultUtils.fromTData(authentication);
    }
}
