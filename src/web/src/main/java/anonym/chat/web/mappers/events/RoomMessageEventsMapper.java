package anonym.chat.web.mappers.events;

import anonym.chat.web.contracts.message.room.RoomMessageResponse;
import anonym.chat.web.events.contracts.room.message.CreatedMessageEvent;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(
    componentModel = MappingConstants.ComponentModel.SPRING,
    implementationName = "RoomMessageEventsMapperImplWeb"
)
public interface RoomMessageEventsMapper {
    CreatedMessageEvent toEventFrom(RoomMessageResponse source);
}
