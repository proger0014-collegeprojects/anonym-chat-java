package anonym.chat.web.contracts.auth.user;

public record SetAuthUserRequest(
    String login,
    String password
) { }
