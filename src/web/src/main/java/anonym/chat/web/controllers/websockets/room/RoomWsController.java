package anonym.chat.web.controllers.websockets.room;

import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class RoomWsController {
    private final SimpMessagingTemplate simpMessagingTemplate;

    public RoomWsController(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }
}
