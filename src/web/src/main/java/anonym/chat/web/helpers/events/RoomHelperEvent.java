package anonym.chat.web.helpers.events;

import anonym.chat.core.models.room.RoomModel;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.web.contracts.room.RoomResponse;
import anonym.chat.web.contracts.room.profile.RoomProfileWithUserFullResponse;
import anonym.chat.web.events.contracts.room.ConnectedToRoomEvent;
import anonym.chat.web.events.contracts.room.CreatedRoomEvent;
import anonym.chat.web.helpers.contracts.RoomHelperContract;
import anonym.chat.web.helpers.contracts.RoomProfileHelperContract;
import anonym.chat.web.mappers.events.RoomEventsMapper;
import org.springframework.stereotype.Component;

@Component
public class RoomHelperEvent {
    private final RoomProfileHelperContract roomProfileHelperContract;
    private final RoomHelperContract roomHelperContract;
    private final RoomEventsMapper roomEventsMapper;

    public RoomHelperEvent(
            RoomProfileHelperContract roomProfileHelperContract,
            RoomHelperContract roomHelperContract,
            RoomEventsMapper roomEventsMapper) {
        this.roomProfileHelperContract = roomProfileHelperContract;
        this.roomHelperContract = roomHelperContract;
        this.roomEventsMapper = roomEventsMapper;
    }

    public ConnectedToRoomEvent toEventFrom(UserModel user, long roomId) {
        RoomProfileWithUserFullResponse contract = roomProfileHelperContract
                .generateRoomProfileWithUserFullResponse(user, roomId);

        return roomEventsMapper.toConnectedToRoomEventFromContract(contract);
    }

    public CreatedRoomEvent toEventFrom(RoomModel room) {
        RoomResponse contract = roomHelperContract.toRoomResponseFromRoomModel(room);

        return roomEventsMapper.toCreatedRoomEventFromContract(contract);
    }
}
