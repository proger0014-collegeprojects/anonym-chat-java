package anonym.chat.web.contracts.auth.token;

public record TokensResponse(
    String accessToken
) { }
