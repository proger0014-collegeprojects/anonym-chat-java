package anonym.chat.web.contracts.message.room;

public record RoomPrivateMessageResponse(
    long destinationUserId
) { }
