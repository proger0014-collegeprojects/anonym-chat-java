package anonym.chat.web.contracts.room.profile;

public record SetRoomProfileRequest(
    long roomId
) { }
