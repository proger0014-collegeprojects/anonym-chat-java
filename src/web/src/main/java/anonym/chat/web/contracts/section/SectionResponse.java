package anonym.chat.web.contracts.section;

import java.util.List;

public record SectionResponse<TItem>(
    long section,
    long lastSection,
    long perSection,
    long total,
    List<TItem> items
) { }
