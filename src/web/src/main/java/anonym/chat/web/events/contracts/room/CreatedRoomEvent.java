package anonym.chat.web.events.contracts.room;

import anonym.chat.web.events.contracts.Event;

public record CreatedRoomEvent(
    long id,
    long userId,
    long countConnectedUsers,
    String name,
    String description
) implements Event { }
