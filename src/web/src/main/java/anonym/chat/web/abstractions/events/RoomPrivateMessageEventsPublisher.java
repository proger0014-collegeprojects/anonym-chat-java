package anonym.chat.web.abstractions.events;

import anonym.chat.core.models.message.room.RoomMessageModel;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;

public interface RoomPrivateMessageEventsPublisher {
    String USER_PATH = RoomEventsPublisher.PATH + "/{}/private/message";

    ResultT<Boolean> createdNewMessage(RoomMessageModel message, UserModel authUser, UserModel userDestination);
    ResultT<Boolean> repliedToMessage(RoomMessageModel replyMessage, UserModel userDestination);
}
