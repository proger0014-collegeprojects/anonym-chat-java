package anonym.chat.web.utils;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public final class HttpResponseEntityBuilder {
    private final HttpHeaders httpHeaders;
    private int statusCode;
    private Object body;

    private HttpResponseEntityBuilder() {
        this(new HttpHeaders());
    }

    private HttpResponseEntityBuilder(HttpHeaders headers) {
        this.httpHeaders = headers;
        this.statusCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
    }

    public static HttpResponseEntityBuilder start() {
        return new HttpResponseEntityBuilder();
    }

    public static HttpResponseEntityBuilder start(HttpHeaders headers) {
        return new HttpResponseEntityBuilder(headers);
    }

    public HttpResponseEntityBuilder header(String key, String value) {
        this.httpHeaders.add(key, value);

        return this;
    }

    public HttpResponseEntityBuilder headers(HttpHeaders headers) {
        this.httpHeaders.addAll(headers);

        return this;
    }

    public HttpResponseEntityBuilder statusCode(int statusCode) {
        this.statusCode = statusCode;

        return this;
    }

    public HttpResponseEntityBuilder body(Object body) {
        this.body = body;

        return this;
    }

    public ResponseEntity<?> build() {
        return new ResponseEntity<>(body, httpHeaders, statusCode);
    }
}
