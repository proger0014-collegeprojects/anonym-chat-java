package anonym.chat.web.events.publishers.room;

import anonym.chat.core.models.message.room.RoomMessageModel;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.web.abstractions.auth.AuthenticatedUserProvider;
import anonym.chat.web.abstractions.events.EventsPublisher;
import anonym.chat.web.abstractions.events.RoomMessageEventsPublisher;
import anonym.chat.web.events.contracts.room.message.CreatedMessageEvent;
import anonym.chat.web.events.contracts.room.message.RepliedMessageEvent;
import anonym.chat.web.helpers.events.RoomMessageHelperEvent;
import org.springframework.stereotype.Component;

@Component
public class RoomMessageEventsPublisherImpl implements RoomMessageEventsPublisher {
    private final EventsPublisher eventsPublisher;
    private final RoomMessageHelperEvent roomMessageHelperEvent;

    public RoomMessageEventsPublisherImpl(
            EventsPublisher eventsPublisher,
            RoomMessageHelperEvent roomMessageHelperEvent,
            AuthenticatedUserProvider authenticatedUserProvider) {
        this.eventsPublisher = eventsPublisher;
        this.roomMessageHelperEvent = roomMessageHelperEvent;
    }

    @Override
    public ResultT<Boolean> createdNewMessage(RoomMessageModel message, UserModel authUser) {
        CreatedMessageEvent event = roomMessageHelperEvent.toEventFrom(message, authUser.getId());

        return eventsPublisher.publish(
            event,
            "{}/created",
            PATH,
            message.getRoomId()
        );
    }

    @Override
    public ResultT<Boolean> repliedToMessage(RoomMessageModel replyMessage, UserModel userDestination) {
        RepliedMessageEvent event = roomMessageHelperEvent.toEventFrom(replyMessage);

        return eventsPublisher.publishToUser(
            userDestination,
            event,
            "{}/replied",
            PATH,
            replyMessage.getRoomId()
        );
    }
}
