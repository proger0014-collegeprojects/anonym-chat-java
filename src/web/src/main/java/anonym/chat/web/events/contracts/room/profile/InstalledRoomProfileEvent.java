package anonym.chat.web.events.contracts.room.profile;

import anonym.chat.web.contracts.room.profile.RoomProfileFullResponse;

public record InstalledRoomProfileEvent(
    long userId,
    RoomProfileFullResponse roomProfile
) { }
