package anonym.chat.web.events.contracts.room.message;

import anonym.chat.web.contracts.message.MessagePreviewResponse;
import anonym.chat.web.contracts.message.room.RoomPrivateMessageInfoResponse;
import anonym.chat.web.events.contracts.Event;

import java.time.LocalDateTime;

public record CreatedMessageEvent(
    long id,
    long userId,
    MessagePreviewResponse reply,
    RoomPrivateMessageInfoResponse asPrivateMessageInfo,
    String body,
    LocalDateTime createdAt
) implements Event { }
