package anonym.chat.web.abstractions;

public interface StringTemplateProvider {
    String getFromTemplate(String template, Object... args);
}
