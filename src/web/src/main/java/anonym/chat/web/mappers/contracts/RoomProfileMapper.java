package anonym.chat.web.mappers.contracts;

import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.models.avatar.AvatarModel;
import anonym.chat.core.models.file.FileModel;
import anonym.chat.core.models.room.profile.CreateNewRoomProfile;
import anonym.chat.core.models.room.profile.RoomProfileModel;
import anonym.chat.core.models.room.profile.SetRoomProfileToRoom;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.web.contracts.avatar.AvatarResponse;
import anonym.chat.web.contracts.room.profile.*;
import anonym.chat.web.contracts.section.SectionResponse;
import anonym.chat.web.contracts.user.UserResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;


@Mapper(componentModel = "spring", implementationName = "RoomProfileMapperImplWeb")
public interface RoomProfileMapper {
    @Mapping(target = "userId", source = "userId")
    @Mapping(target = "avatarId", source = "source.avatarId")
    @Mapping(target = "nickname", source = "source.nickname")
    CreateNewRoomProfile toCreateNewRoomProfileFromCreateNewRoomProfileRequest(CreateNewRoomProfileRequest source, long userId);
    RoomProfileResponse toRoomProfileResponseFromRoomProfileModel(RoomProfileModel source);


    default SectionResponse<RoomProfileResponse> toSectionResponseRoomProfileResponseFromSectionCollectionRoomProfileModel(SectionCollection<RoomProfileModel> source) {
        List<RoomProfileResponse> roomProfileResponses = source.items().stream().map(this::toRoomProfileResponseFromRoomProfileModel).toList();

        return new SectionResponse<>(
            source.section(),
            source.lastSection(),
            source.perSection(),
            source.total(),
            roomProfileResponses
        );
    }

    @Mapping(target = "roomId", source = "source.roomId")
    @Mapping(target = "roomProfileId", source = "roomProfileId")
    @Mapping(target = "userIdTarget", source = "userIdTarget")
    SetRoomProfileToRoom toSetRoomProfileToRoomFromSetRoomProfileRequest(SetRoomProfileRequest source, long roomProfileId, long userIdTarget);

    default RoomProfileWithUserFullResponse toRoomProfileWithUserFullResponseFrom(RoomProfileModel roomProfile, AvatarModel avatar, FileModel avatarFile, UserModel user, String imageUrl) {
        return new RoomProfileWithUserFullResponse(
            new RoomProfileFullResponse(
                roomProfile.getId(),
                roomProfile.getNickname(),
                new AvatarResponse(
                    avatar.getId(),
                    avatarFile.getSize(),
                    imageUrl
                )
            ),
            new UserResponse(
                user.getId(),
                user.getRole().getValue(),
                user.getLogin(),
                user.getCreatedAt()
            )
        );
    }

    default RoomProfileFullResponse toRoomProfileFullResponseFrom(RoomProfileModel roomProfileModel, AvatarResponse avatarResponse) {
        return new RoomProfileFullResponse(
            roomProfileModel.getId(),
            roomProfileModel.getNickname(),
            avatarResponse
        );
    }
}
