package anonym.chat.web.events.contracts.room;

import anonym.chat.web.events.contracts.Event;

public record DisconnectedFromRoomEvent(
    long userId
) implements Event { }
