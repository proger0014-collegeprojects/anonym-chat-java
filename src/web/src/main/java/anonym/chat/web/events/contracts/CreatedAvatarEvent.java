package anonym.chat.web.events.contracts;

public record CreatedAvatarEvent(
    long id,
    long size,
    String url
) implements Event { }
