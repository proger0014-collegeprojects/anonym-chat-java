package anonym.chat.web.mappers.contracts;

import anonym.chat.core.models.message.room.CreateNewRoomMessage;
import anonym.chat.core.models.message.room.RoomMessageModel;
import anonym.chat.core.models.message.room.RoomPrivateMessageInfo;
import anonym.chat.web.contracts.message.CreateNewMessageRequest;
import anonym.chat.web.contracts.message.MessagePreviewResponse;
import anonym.chat.web.contracts.message.room.RoomMessageResponse;
import anonym.chat.web.contracts.message.room.RoomPrivateMessageInfoResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", implementationName = "RoomMessageMapperImplWeb")
public interface RoomMessageMapper {
    RoomPrivateMessageInfoResponse toRoomPrivateMessageInfoResponseFromRoomPrivateMessageInfo(RoomPrivateMessageInfo source);

    @Mapping(target = "userId", source = "userId")
    @Mapping(target = "roomId", source = "roomId")
    @Mapping(target = "body", source = "source.body")
    CreateNewRoomMessage toCreateNewRoomMessageFromCreateNewRoomMessageRequest(CreateNewMessageRequest source, long userId, long roomId);

    default RoomMessageResponse toRoomMessageResponseFrom(RoomMessageModel source, RoomMessageModel reply) {
        MessagePreviewResponse replyResponse = null;

        if (reply != null) {
            String shortBody = reply.getBody().length() <= 25
                    ? reply.getBody()
                    : reply.getBody().substring(0, 25);

            replyResponse = new MessagePreviewResponse(reply.getUserId(), reply.getId(), shortBody);
        }

        RoomPrivateMessageInfoResponse asPrivateInfo = source.privateMessageInfo == null
                ? null
                : toRoomPrivateMessageInfoResponseFromRoomPrivateMessageInfo(source.getPrivateMessageInfo());

        return new RoomMessageResponse(
                source.getId(),
                source.getUserId(),
                replyResponse,
                asPrivateInfo,
                source.getBody(),
                source.getCreatedAt());
    }
}
