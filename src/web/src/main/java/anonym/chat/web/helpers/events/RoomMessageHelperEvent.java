package anonym.chat.web.helpers.events;

import anonym.chat.core.abstractions.message.room.RoomMessageService;
import anonym.chat.core.abstractions.user.UserService;
import anonym.chat.core.models.message.room.GetInRoomById;
import anonym.chat.core.models.message.room.GetInRoomByIdPublic;
import anonym.chat.core.models.message.room.RoomMessageModel;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.web.contracts.message.room.RoomMessageResponse;
import anonym.chat.web.contracts.room.profile.RoomProfileWithUserFullResponse;
import anonym.chat.web.events.contracts.room.message.CreatedMessageEvent;
import anonym.chat.web.events.contracts.room.message.RepliedMessageEvent;
import anonym.chat.web.helpers.contracts.RoomMessageHelperContract;
import anonym.chat.web.helpers.contracts.RoomProfileHelperContract;
import anonym.chat.web.mappers.events.RoomMessageEventsMapper;
import anonym.chat.web.mappers.utils.RoomMessageUtilMapper;
import anonym.chat.web.utils.dto.message.reply.ReplyInfo;
import anonym.chat.web.utils.dto.message.reply.ShortMessageReplyInfo;
import org.springframework.stereotype.Component;

@Component
public class RoomMessageHelperEvent {
    private final RoomMessageEventsMapper roomMessageEventsMapper;
    private final RoomMessageHelperContract roomMessageHelperContract;
    private final RoomProfileHelperContract roomProfileHelperContract;
    private final RoomMessageUtilMapper roomMessageUtilMapper;
    private final RoomMessageService roomMessageService;
    private final UserService userService;

    public RoomMessageHelperEvent(
            RoomMessageEventsMapper roomMessageEventsMapper,
            RoomMessageHelperContract roomMessageHelperContract,
            RoomProfileHelperContract roomProfileHelperContract,
            RoomMessageUtilMapper roomMessageUtilMapper,
            RoomMessageService roomMessageService,
            UserService userService) {
        this.roomMessageEventsMapper = roomMessageEventsMapper;
        this.roomMessageHelperContract = roomMessageHelperContract;
        this.roomProfileHelperContract = roomProfileHelperContract;
        this.roomMessageUtilMapper = roomMessageUtilMapper;
        this.roomMessageService = roomMessageService;
        this.userService = userService;
    }

    public CreatedMessageEvent toEventFrom(RoomMessageModel message, long authAuthId) {
        RoomMessageResponse contract = roomMessageHelperContract
                .toRoomMessageResponse(message, authAuthId);

        return roomMessageEventsMapper.toEventFrom(contract);
    }

    public RepliedMessageEvent toEventFrom(RoomMessageModel replyMessage) {
        RoomMessageModel parentMessage = roomMessageService.getInRoomByIdPublic(new GetInRoomByIdPublic(
            replyMessage.getRoomId(),
            replyMessage.getReply()
        )).getData();

        ShortMessageReplyInfo shortParentMessage = roomMessageUtilMapper.toShortMessageReplyInfoFrom(parentMessage);
        ShortMessageReplyInfo shortReplyMessage = roomMessageUtilMapper.toShortMessageReplyInfoFrom(replyMessage);
        UserModel ownerReplyMessage = userService.getById(replyMessage.getUserId()).getData();


        RoomProfileWithUserFullResponse ownerReplyMessageContract = roomProfileHelperContract
                .generateRoomProfileWithUserFullResponse(ownerReplyMessage, replyMessage.getRoomId());

        ReplyInfo replyInfo = new ReplyInfo(
            shortReplyMessage,
            ownerReplyMessageContract
        );

        return new RepliedMessageEvent(
            replyInfo,
            shortParentMessage
        );
    }
}
