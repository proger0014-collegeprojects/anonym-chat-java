package anonym.chat.web.security;

import org.springframework.stereotype.Service;

@Service("PasswordEncoderCore")
public class PasswordEncoder implements anonym.chat.core.abstractions.PasswordEncoder {
    private final org.springframework.security.crypto.password.PasswordEncoder passwordEncoder;

    public PasswordEncoder(org.springframework.security.crypto.password.PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public String encode(String password) {
        return passwordEncoder.encode(password);
    }
}
