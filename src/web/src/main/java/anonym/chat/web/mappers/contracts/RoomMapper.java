package anonym.chat.web.mappers.contracts;

import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.models.room.RoomModel;
import anonym.chat.web.contracts.room.RoomResponse;
import anonym.chat.web.contracts.section.SectionResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", implementationName = "RoomMapperImplWeb")
public interface RoomMapper {
    @Mapping(target = "id", source = "roomModel.id")
    @Mapping(target = "userId", source = "roomModel.userId")
    @Mapping(target = "countConnectedUsers", source = "countConnectedUsers")
    @Mapping(target = "name", source = "roomModel.name")
    @Mapping(target = "description", source = "roomModel.description")
    RoomResponse toRoomResponseFromRoomModel(RoomModel roomModel, int countConnectedUsers);
}
