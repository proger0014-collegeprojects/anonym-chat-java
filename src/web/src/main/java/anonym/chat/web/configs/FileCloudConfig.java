package anonym.chat.web.configs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FileCloudConfig {
    @Value("${files.storage.cloud.url}")
    private String baseUrlOfFileStorage;

    public String getBaseUrlOfFileStorage() {
        return baseUrlOfFileStorage;
    }
}
