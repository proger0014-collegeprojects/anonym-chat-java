package anonym.chat.web.exceptions.handlers;

import anonym.chat.web.exceptions.HttpBaseException;
import anonym.chat.web.helpers.ResponseEntityHelper;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class FilterExceptionHandler implements Filter {
    private final ResponseEntityHelper responseEntityHelper;

    public FilterExceptionHandler(ResponseEntityHelper responseEntityHelper) {
        this.responseEntityHelper = responseEntityHelper;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws
            IOException,
            ServletException {

        try {
            filterChain.doFilter(servletRequest, servletResponse);
        } catch (HttpBaseException e) {
            HttpServletResponse httpServletResponse = (HttpServletResponse)servletResponse;

            responseEntityHelper.addToServletResponse(e.getResponse(), httpServletResponse);
        }
    }
}
