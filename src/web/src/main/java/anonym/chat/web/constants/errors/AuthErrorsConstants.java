package anonym.chat.web.constants.errors;

public final class AuthErrorsConstants {
    public static final String TYPE = CommonErrorsConstants.TYPE + "/auth";

    public static final String CANNOT_LOGIN_WITH_HTTP_REQUEST_TYPE = TYPE + "/cannot-login-with-http-request";
    public static final String CANNOT_LOGIN_WITH_HTTP_REQUEST_TITLE = "Не удалось войти в систему с http запросом";
    public static final int CANNOT_LOGIN_WITH_HTTP_REQUEST_STATUS = 400;
    public static final String CANNOT_LOGIN_WITH_HTTP_REQUEST_DESCRIPTION = anonym.chat.core.constants.errors.CommonErrorsConstants.INTERNAL_SERVER_ERROR_DESCRIPTION;
}
