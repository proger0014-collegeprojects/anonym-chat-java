package anonym.chat.web.events.contracts.room;

import anonym.chat.web.contracts.room.profile.RoomProfileFullResponse;
import anonym.chat.web.contracts.user.UserResponse;
import anonym.chat.web.events.contracts.Event;

public record ConnectedToRoomEvent(
    RoomProfileFullResponse roomProfile,
    UserResponse user
) implements Event { }
