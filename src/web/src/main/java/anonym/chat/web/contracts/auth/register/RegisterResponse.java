package anonym.chat.web.contracts.auth.register;


import io.swagger.v3.oas.annotations.Parameter;

public record RegisterResponse(
    @Parameter(required = true) long id,
    int role,
    String login
) { }
