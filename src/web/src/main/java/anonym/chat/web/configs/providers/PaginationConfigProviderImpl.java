package anonym.chat.web.configs.providers;

import anonym.chat.core.abstractions.providers.config.PaginationConfigProvider;
import anonym.chat.core.utils.paginations.PaginationConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class PaginationConfigProviderImpl implements PaginationConfigProvider {
    @Value("${pagination.defaults.per-page}")
    private Long perPageDefault;

    @Override
    public PaginationConfig get() {
        return new PaginationConfig(perPageDefault);
    }
}
