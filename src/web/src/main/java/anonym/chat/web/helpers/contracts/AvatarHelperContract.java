package anonym.chat.web.helpers.contracts;

import anonym.chat.core.abstractions.file.FileService;
import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.models.avatar.AvatarModel;
import anonym.chat.core.models.file.FileModel;
import anonym.chat.web.configs.FileCloudConfig;
import anonym.chat.web.contracts.avatar.AvatarResponse;
import anonym.chat.web.contracts.section.SectionResponse;
import anonym.chat.web.mappers.contracts.AvatarMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AvatarHelperContract {
    private final AvatarMapper avatarMapper;

    private final FileService fileService;
    private final FileCloudConfig fileCloudConfig;

    public AvatarHelperContract(
            AvatarMapper avatarMapper,
            FileService fileService,
            FileCloudConfig fileCloudConfig) {
        this.avatarMapper = avatarMapper;
        this.fileService = fileService;
        this.fileCloudConfig = fileCloudConfig;
    }

    public AvatarResponse toAvatarResponseFromAvatarModel(AvatarModel avatar) {
        FileModel avatarFile = fileService.getById(avatar.getFileId()).getData();
        String url = fileCloudConfig.getBaseUrlOfFileStorage() + "/" + avatarFile.getUrl();

        return avatarMapper.toAvatarResponseFromFileModelAndAvatarIdAndUrl(avatarFile, avatar.getId(), url);
    }

    public SectionResponse<AvatarResponse> toSectionResponseAvatarFromSectionCollectionAvatar(SectionCollection<AvatarModel> section) {
        List<AvatarResponse> convertedList = section.items().stream()
                .map(this::toAvatarResponseFromAvatarModel)
                .toList();

        return new SectionResponse<>(
            section.section(),
            section.lastSection(),
            section.perSection(),
            section.total(),
            convertedList
        );
    }
}
