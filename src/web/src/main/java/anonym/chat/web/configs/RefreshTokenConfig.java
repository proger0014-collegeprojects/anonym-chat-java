package anonym.chat.web.configs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RefreshTokenConfig {
    @Value("${security.refresh-token.expiration}")
    private String expiresInDays;

    public int getExpiresInDays() {
        return Integer.parseInt(expiresInDays);
    }
}
