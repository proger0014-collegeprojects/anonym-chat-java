package anonym.chat.web.security;

import anonym.chat.core.utils.result.ResultT;
import anonym.chat.web.abstractions.auth.AccessTokenLoginProvider;
import anonym.chat.web.abstractions.auth.AuthChannelInterceptor;
import anonym.chat.web.abstractions.auth.AuthenticatedUserPrincipalProvider;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AccessTokenChannelInterceptor implements AuthChannelInterceptor {
    private final String ACCESS_TOKEN_HEADER = "accessToken";

    private final AccessTokenLoginProvider accessTokenLoginProvider;
    private final AuthenticatedUserPrincipalProvider authenticatedUserPrincipalProvider;

    public AccessTokenChannelInterceptor(
            AccessTokenLoginProvider accessTokenLoginProvider,
            AuthenticatedUserPrincipalProvider authenticatedUserPrincipalProvider) {
        this.accessTokenLoginProvider = accessTokenLoginProvider;
        this.authenticatedUserPrincipalProvider = authenticatedUserPrincipalProvider;
    }

    // TODO: ANONYMCHAT-44
    @Override
    public Message<?> preSend(@NonNull Message<?> message, @NonNull MessageChannel channel) {
        StompHeaderAccessor stompHeaderAccessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);

        if (StompCommand.CONNECT.equals(stompHeaderAccessor.getCommand())) {
            String accessToken = stompHeaderAccessor.getFirstNativeHeader(ACCESS_TOKEN_HEADER);

            if (StringUtils.isEmpty(accessToken)) {
                throw new BadCredentialsException("Not found access token header");
            }

            ResultT<Boolean> loginResult = accessTokenLoginProvider.login(accessToken);

            if (loginResult.isFailure()) {
                throw new BadCredentialsException("Login is failed");
            }

            Optional<Authentication> principal = authenticatedUserPrincipalProvider.getPrincipal();

            principal.ifPresent(stompHeaderAccessor::setUser);
        }

        return message;
    }
}
