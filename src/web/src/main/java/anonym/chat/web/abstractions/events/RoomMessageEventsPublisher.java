package anonym.chat.web.abstractions.events;

import anonym.chat.core.models.message.room.RoomMessageModel;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;

public interface RoomMessageEventsPublisher {
    String PATH = RoomEventsPublisher.PATH + "/{}/message";

    ResultT<Boolean> createdNewMessage(RoomMessageModel message, UserModel authUser);
    ResultT<Boolean> repliedToMessage(RoomMessageModel replyMessage, UserModel userDestination);
}
