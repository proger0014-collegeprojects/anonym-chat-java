package anonym.chat.web.mappers.events;

import anonym.chat.web.contracts.room.profile.RoomProfileFullResponse;
import anonym.chat.web.events.contracts.room.profile.CreatedRoomProfileEvent;
import anonym.chat.web.events.contracts.room.profile.InstalledRoomProfileEvent;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

@Mapper(
    componentModel = MappingConstants.ComponentModel.SPRING,
    implementationName = "RoomProfileEventsMapperImplWeb"
)
public interface RoomProfileEventsMapper {
    @Mapping(target = "userId", source = "userId")
    @Mapping(target = "roomProfile.id", source = "contract.id")
    @Mapping(target = "roomProfile.nickname", source = "contract.nickname")
    @Mapping(target = "roomProfile.avatar", source = "contract.avatar")
    InstalledRoomProfileEvent toEventFrom(RoomProfileFullResponse contract, long userId);

    CreatedRoomProfileEvent toEventFrom(RoomProfileFullResponse source);
}
