package anonym.chat.web.events.contracts.room.profile;

import anonym.chat.web.contracts.avatar.AvatarResponse;

public record CreatedRoomProfileEvent(
    long id,
    String nickname,
    AvatarResponse avatar
) { }
