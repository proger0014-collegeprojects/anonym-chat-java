package anonym.chat.web.mappers.events;

import anonym.chat.web.contracts.room.RoomResponse;
import anonym.chat.web.contracts.room.profile.RoomProfileWithUserFullResponse;
import anonym.chat.web.events.contracts.room.ConnectedToRoomEvent;
import anonym.chat.web.events.contracts.room.CreatedRoomEvent;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(
        componentModel = MappingConstants.ComponentModel.SPRING,
        implementationName = "RoomEventsMapperImplWeb"
)
public interface RoomEventsMapper {
    ConnectedToRoomEvent toConnectedToRoomEventFromContract(RoomProfileWithUserFullResponse contract);
    CreatedRoomEvent toCreatedRoomEventFromContract(RoomResponse contract);
}
