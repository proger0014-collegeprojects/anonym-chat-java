package anonym.chat.application.services.file;

import anonym.chat.application.mappers.FileMapper;
import anonym.chat.core.abstractions.Validator;
import anonym.chat.core.abstractions.file.FileRepository;
import anonym.chat.core.abstractions.file.FileService;
import anonym.chat.core.abstractions.file.temp.FileTempStorage;
import anonym.chat.core.constants.errors.CommonErrorsConstants;
import anonym.chat.core.constants.errors.ModelsErrorConstants;
import anonym.chat.core.models.file.FileModel;
import anonym.chat.core.models.file.temp.FileTempModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.core.utils.result.ResultUtils;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.Optional;

@Service
public class FileServiceImpl implements FileService {
    private final FileRepository fileRepository;
    private final FileTempStorage fileTempStorage;

    private final FileMapper fileMapper;
    private final Validator validator;

    public FileServiceImpl(
            FileRepository fileRepository,
            FileTempStorage fileTempStorage,
            FileMapper fileMapper,
            Validator validator) {
        this.fileRepository = fileRepository;
        this.fileTempStorage = fileTempStorage;
        this.fileMapper = fileMapper;
        this.validator = validator;
    }

    @Override
    public ResultT<FileModel> add(InputStream file) {
        Optional<FileTempModel> savedTempFile = fileTempStorage.saveTempFileByStream(file);

        if (savedTempFile.isEmpty()) {
            return ResultUtils.fromTSingleError(
                CommonErrorsConstants.INTERNAL_SERVER_ERROR_TYPE,
                CommonErrorsConstants.INTERNAL_SERVER_ERROR_TITLE,
                CommonErrorsConstants.INTERNAL_SERVER_ERROR_STATUS,
                CommonErrorsConstants.INTERNAL_SERVER_ERROR_DESCRIPTION
            );
        }

        FileModel createdFile = fileRepository.add(savedTempFile.get());

        if (createdFile == null) {
            return ResultUtils.fromTSingleError(
                CommonErrorsConstants.INTERNAL_SERVER_ERROR_TYPE,
                CommonErrorsConstants.INTERNAL_SERVER_ERROR_TITLE,
                CommonErrorsConstants.INTERNAL_SERVER_ERROR_STATUS,
                CommonErrorsConstants.INTERNAL_SERVER_ERROR_DESCRIPTION
            );
        }

        fileTempStorage.deleteTempFile(savedTempFile.get().getFile());

        return ResultUtils.fromTData(createdFile);
    }

    @Override
    public ResultT<FileModel> getById(long id) {
        FileModel existsFile = fileRepository.getById(id);

        if (existsFile == null) {
            return ResultUtils.fromTSingleError(
                ModelsErrorConstants.NOT_EXISTS_TYPE,
                ModelsErrorConstants.NOT_EXISTS_TITLE,
                ModelsErrorConstants.NOT_EXISTS_STATUS,
                ModelsErrorConstants.NOT_EXISTS_DESCRIPTION
            );
        }

        return ResultUtils.fromTData(existsFile);
    }

    @Override
    public ResultT<FileModel> addExistsFile(FileTempModel existsFile) {
        FileModel createdFile = fileRepository.add(existsFile);

        if (createdFile == null) {
            return ResultUtils.fromTSingleError(
                    CommonErrorsConstants.INTERNAL_SERVER_ERROR_TYPE,
                    CommonErrorsConstants.INTERNAL_SERVER_ERROR_TITLE,
                    CommonErrorsConstants.INTERNAL_SERVER_ERROR_STATUS,
                    CommonErrorsConstants.INTERNAL_SERVER_ERROR_DESCRIPTION
            );
        }

        fileTempStorage.deleteTempFile(existsFile.getFile());

        return ResultUtils.fromTData(createdFile);
    }
}
