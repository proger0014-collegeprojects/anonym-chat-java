package anonym.chat.application.validators.user;

import anonym.chat.core.models.ValidationModel;
import anonym.chat.core.models.user.Role;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;

public record UpdateUserAuthValidator(
    @Valid
    UserAuthValidator userAuth,
    Role role,
    @Min(value = 1, message = "Значение не должно быть меньше, чем 1")
    long userId
) implements ValidationModel { }
