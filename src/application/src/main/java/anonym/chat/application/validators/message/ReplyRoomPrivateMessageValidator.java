package anonym.chat.application.validators.message;

import anonym.chat.core.constants.validations.CommonValidationMessages;
import anonym.chat.core.models.ValidationModel;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

public record ReplyRoomPrivateMessageValidator(
    @Min(value = 1, message = "Значение не должно быть меньше, чем 1")
    long userId,

    @Min(value = 1, message = "Значение не должно быть меньше, чем 1")
    long destinationUserId,

    @Min(value = 1, message = "Значение не должно быть меньше, чем 1")
    long roomId,

    @Min(value = 1, message = "Значение не должно быть меньше, чем 1")
    long roomMessageId,

    @NotEmpty(message = CommonValidationMessages.FIELD_EMPTY)
    @Size(min = 1, message = "Сообщение не должно быть пустым")
    @Size(max = 500, message = "Сообщение не должно быть больше 500 символов")
    String body
) implements ValidationModel { }
