package anonym.chat.application.mappers;

import anonym.chat.application.validators.message.CreateNewRoomMessageValidator;
import anonym.chat.core.models.message.room.CreateNewRoomMessage;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationName = "MessageMapperImplApplication")
public interface MessageMapper {
    CreateNewRoomMessageValidator toCreateNewRoomMessageValidatorFromCreateNewRoomMessage(CreateNewRoomMessage source);
}
