package anonym.chat.application.validators.room.profile;

import anonym.chat.core.constants.RoomProfileConstants;
import anonym.chat.core.constants.validations.CommonValidationMessages;
import anonym.chat.core.constants.validations.RoomProfileValidationMessages;
import anonym.chat.core.models.ValidationModel;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

public record NicknameValidator(
    @NotEmpty(message = CommonValidationMessages.FIELD_EMPTY)
    @Size(min = RoomProfileConstants.NICKNAME_MIN_LENGTH, message = RoomProfileValidationMessages.NICKNAME_MIN)
    @Size(max = RoomProfileConstants.NICKNAME_MAX_LENGTH, message = RoomProfileValidationMessages.NICKNAME_MAX)
    String nickname
) implements ValidationModel {
}
