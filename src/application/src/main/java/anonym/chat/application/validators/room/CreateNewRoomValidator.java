package anonym.chat.application.validators.room;

import anonym.chat.core.constants.validations.CommonValidationMessages;
import anonym.chat.core.models.ValidationModel;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;

public record CreateNewRoomValidator(
    @Min(value = 1, message = "Значение не должно быть меньше, чем 1")
    long userId,

    @NotEmpty(message = CommonValidationMessages.FIELD_EMPTY)
    String name,

    String description
) implements ValidationModel { }
