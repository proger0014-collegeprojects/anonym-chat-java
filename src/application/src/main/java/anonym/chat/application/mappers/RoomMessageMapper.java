package anonym.chat.application.mappers;

import anonym.chat.application.validators.message.*;
import anonym.chat.application.validators.message.GetInRoomByIdPublicValidator;
import anonym.chat.core.models.message.room.*;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationName = "RoomMessageMapperImplApplication")
public interface RoomMessageMapper {
    GetAllInRoomSectionValidator toGetAllInRoomSectionValidatorFromGetAllInRoomSection(GetAllInRoomSection source);
    GetInRoomByIdValidator toGetInRoomByIdValidatorFromGetInRoomById(GetInRoomById source);
    GetInRoomByIdPublicValidator toGetInRoomByIdPublicValidatorFromGetInRoomByIdPublic(GetInRoomByIdPublic source);
    ReplyRoomMessageValidator toReplyRoomMessageValidatorFromReplyRoomMessage(ReplyRoomMessage source);
    ReplyRoomPrivateMessageValidator toReplyRoomPrivateMessageValidatorFromReplyToRoomPrivateMessage(ReplyToRoomPrivateMessage source);
    CreateNewPrivateRoomMessageValidator toCreateNewPrivateRoomMessageValidatorFromCreateNewPrivateRoomMessages(CreateNewPrivateRoomMessage source);
    GetAllPrivateMessageDestinationUserSectionValidator toGetAllPrivateMessageDestinationUserSectionValidatorFromModel(GetAllPrivateMessageDestinationUserSection source);
    GetAllPrivateMessageByDestinationValidator toGetAllPrivateMessageByDestinationValidatorFromModel(GetAllPrivateMessageByDestination source);
}
