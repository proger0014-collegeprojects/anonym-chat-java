package anonym.chat.application.validators.common;

import anonym.chat.core.models.ValidationModel;
import jakarta.validation.constraints.Min;

public record PageValidator(
    @Min(value = 1, message = "Значение не должно быть меньше, чем 1")
    long page
) implements ValidationModel {
}
