package anonym.chat.application.mappers;

import anonym.chat.application.validators.room.CreateNewRoomValidator;
import anonym.chat.core.models.room.CreateNewRoom;
import anonym.chat.core.models.room.RoomModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", implementationName = "RoomMapperImplApplication")
public interface RoomMapper {
    CreateNewRoomValidator toCreateNewRoomValidatorFromCreateNewRoom(CreateNewRoom createNewRoom);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    RoomModel toRoomModelFromCreateNewRoom(CreateNewRoom createNewRoom);
}
