package anonym.chat.application.validators.user;

import anonym.chat.core.models.ValidationModel;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

public record UserAuthValidator(
    @Size(min = 5, message = "Поле не может быть меньше 5 символов")
    @Size(max = 255, message = "Поле не может быть больше 255 символов")
    @NotEmpty(message = "Значение обязательно")
    String login,

    @Size(min = 5, message = "Поле не может быть меньше 5 символов")
    @Size(max = 255, message = "Поле не может быть больше 255 символов")
    @NotEmpty(message = "Значение обязательно")
    String password
) implements ValidationModel { }
