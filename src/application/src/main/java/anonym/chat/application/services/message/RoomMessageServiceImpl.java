package anonym.chat.application.services.message;

import anonym.chat.application.mappers.MessageMapper;
import anonym.chat.application.mappers.RoomMessageMapper;
import anonym.chat.application.utils.PaginationUtils;
import anonym.chat.application.validators.message.*;
import anonym.chat.core.abstractions.providers.DateTimeProvider;
import anonym.chat.core.abstractions.providers.config.PaginationConfigProvider;
import anonym.chat.core.abstractions.Validator;
import anonym.chat.core.abstractions.message.room.RoomMessageRepository;
import anonym.chat.core.abstractions.message.room.RoomMessageService;
import anonym.chat.core.abstractions.room.RoomService;
import anonym.chat.core.abstractions.user.UserService;
import anonym.chat.core.constants.errors.MessageErrorConstants;
import anonym.chat.core.constants.errors.ModelsErrorConstants;
import anonym.chat.core.constants.errors.RoomPrivateMessageErrorsConstants;
import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.models.message.room.GetInRoomByIdPublic;
import anonym.chat.core.models.message.room.ReplyRoomMessage;
import anonym.chat.core.models.message.room.*;
import anonym.chat.core.models.room.RoomModel;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.paginations.PaginationConfig;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.core.utils.result.ResultUtils;
import anonym.chat.core.utils.validator.ValidationResult;
import anonym.chat.core.utils.validator.ValidatorUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomMessageServiceImpl implements RoomMessageService {
    private final RoomMessageRepository roomMessageRepository;
    private final RoomMessageMapper roomMessageMapper;
    private final MessageMapper messageMapper;

    private final PaginationConfigProvider paginationConfigProvider;
    private final DateTimeProvider dateTimeProvider;
    private final Validator validator;
    private final RoomService roomService;
    private final UserService userService;

    public RoomMessageServiceImpl(
            RoomMessageRepository roomMessageRepository,
            RoomMessageMapper roomMessageMapper,
            MessageMapper messageMapper,
            PaginationConfigProvider paginationConfigProvider,
            DateTimeProvider dateTimeProvider,
            Validator validator,
            RoomService roomService,
            UserService userService) {
        this.roomMessageRepository = roomMessageRepository;
        this.roomMessageMapper = roomMessageMapper;
        this.messageMapper = messageMapper;
        this.paginationConfigProvider = paginationConfigProvider;
        this.dateTimeProvider = dateTimeProvider;
        this.validator = validator;
        this.roomService = roomService;
        this.userService = userService;
    }

    @Override
    public ResultT<RoomMessageModel> addPublicRoomMessage(CreateNewRoomMessage createNewRoomMessage) {
        CreateNewRoomMessageValidator validationModel = messageMapper
                .toCreateNewRoomMessageValidatorFromCreateNewRoomMessage(createNewRoomMessage);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<UserModel> existsUserResult = userService.getById(createNewRoomMessage.userId());

        if (existsUserResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsUserResult.getError());
        }

        ResultT<RoomModel> existsRoomResult = roomService.getById(createNewRoomMessage.roomId());

        if (existsRoomResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsUserResult.getError());
        }


        ResultT<Boolean> isConnectedProducerUserResult = roomService.isConnectedToRoom(
                createNewRoomMessage.userId(),
                createNewRoomMessage.roomId());

        boolean isConnected = isConnectedProducerUserResult.getData();

        if (!isConnected) {
            return ResultUtils.fromTSingleError(
                MessageErrorConstants.NOT_CONNECTED_TO_ROOM_TYPE,
                MessageErrorConstants.NOT_CONNECTED_TO_ROOM_TITLE,
                MessageErrorConstants.NOT_CONNECTED_TO_ROOM_STATUS,
                MessageErrorConstants.NOT_CONNECTED_TO_ROOM_DESCRIPTION
            );
        }

        RoomMessageModel createdRoomMessage = roomMessageRepository.addPublicRoomMessage(
                createNewRoomMessage.userId(),
                createNewRoomMessage.roomId(),
                createNewRoomMessage.body(),
                dateTimeProvider.now());

        return ResultUtils.fromTData(createdRoomMessage);
    }

    @Override
    public ResultT<RoomMessageModel> replyToRoomMessage(ReplyRoomMessage replyRoomMessage) {
        ReplyRoomMessageValidator validationModel = roomMessageMapper
                .toReplyRoomMessageValidatorFromReplyRoomMessage(replyRoomMessage);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<UserModel> existsUserResult = userService.getById(replyRoomMessage.userId());

        if (existsUserResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsUserResult.getError());
        }

        ResultT<RoomModel> existsRoomResult = roomService.getById(replyRoomMessage.roomId());

        if (existsRoomResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsRoomResult.getError());
        }

        ResultT<RoomMessageModel> existsMessageResult = getInRoomByIdPublic(new GetInRoomByIdPublic(
                replyRoomMessage.roomId(),
                replyRoomMessage.roomMessageId()));

        if (existsMessageResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsMessageResult.getError());
        }

        RoomMessageModel repliedMessage = roomMessageRepository.replyToRoomMessage(
                replyRoomMessage.userId(),
                replyRoomMessage.roomId(),
                replyRoomMessage.roomMessageId(),
                replyRoomMessage.body(),
                dateTimeProvider.now());

        return ResultUtils.fromTData(repliedMessage);
    }

    @Override
    public ResultT<RoomMessageModel> replyToRoomPrivateMessage(ReplyToRoomPrivateMessage replyToRoomPrivateMessage) {
        ReplyRoomPrivateMessageValidator validationModel = roomMessageMapper
                .toReplyRoomPrivateMessageValidatorFromReplyToRoomPrivateMessage(replyToRoomPrivateMessage);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<UserModel> existsUserResult = userService.getById(replyToRoomPrivateMessage.userId());

        if (existsUserResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsUserResult.getError());
        }

        ResultT<UserModel> existsDestinationUserResult = userService.getById(replyToRoomPrivateMessage.destinationUserId());

        if (existsUserResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsDestinationUserResult.getError());
        }

        ResultT<RoomModel> existsRoomResult = roomService.getById(replyToRoomPrivateMessage.roomId());

        if (existsRoomResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsRoomResult.getError());
        }

        ResultT<RoomMessageModel> existsRoomMessageResult = getInRoomById(new GetInRoomById(
                replyToRoomPrivateMessage.roomId(),
                replyToRoomPrivateMessage.userId(),
                replyToRoomPrivateMessage.roomMessageId()
        ));

        if (existsRoomMessageResult.isFailure()
            || existsRoomMessageResult.isSuccess()
                && existsRoomMessageResult.getData().privateMessageInfo == null ) {
            return ResultUtils.fromTSingleError(
                ModelsErrorConstants.NOT_EXISTS_TYPE,
                ModelsErrorConstants.NOT_EXISTS_TITLE,
                ModelsErrorConstants.NOT_EXISTS_STATUS,
                ModelsErrorConstants.NOT_EXISTS_DESCRIPTION
            );
        }

        RoomMessageModel repliedRoomMessage = roomMessageRepository.replyToRoomPrivateMessage(
                replyToRoomPrivateMessage.userId(),
                replyToRoomPrivateMessage.roomId(),
                replyToRoomPrivateMessage.destinationUserId(),
                replyToRoomPrivateMessage.roomMessageId(),
                replyToRoomPrivateMessage.body(),
                dateTimeProvider.now()
        );

        return ResultUtils.fromTData(repliedRoomMessage);
    }

    @Override
    public ResultT<SectionCollection<RoomMessageModel>> getAllInRoomSection(GetAllInRoomSection get) {
        GetAllInRoomSectionValidator validationModel = roomMessageMapper
                .toGetAllInRoomSectionValidatorFromGetAllInRoomSection(get);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<RoomModel> existsRoomResult = roomService.getById(get.roomId());

        if (existsRoomResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsRoomResult.getError());
        }

        ResultT<UserModel> existsUserResult = userService.getById(get.userIdForPrivates());

        if (existsRoomResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsUserResult.getError());
        }

        ResultT<Boolean> isConnectedProducerUserResult = roomService.isConnectedToRoom(
                get.userIdForPrivates(),
                get.roomId());

        boolean isConnected = isConnectedProducerUserResult.getData();

        if (!isConnected) {
            return ResultUtils.fromTSingleError(
                MessageErrorConstants.NOT_CONNECTED_TO_ROOM_TYPE,
                MessageErrorConstants.NOT_CONNECTED_TO_ROOM_TITLE,
                MessageErrorConstants.NOT_CONNECTED_TO_ROOM_STATUS,
                MessageErrorConstants.NOT_CONNECTED_TO_ROOM_DESCRIPTION
            );
        }

        PaginationConfig paginationConfig = paginationConfigProvider.get();

        List<RoomMessageModel> sectionItems = roomMessageRepository.getAllInRoomSection(
            get.roomId(),
            get.userIdForPrivates(),
            PaginationUtils.getOffset(get.section(), paginationConfigProvider),
            paginationConfig.perPageDefault());

        long sectionItemsTotal = roomMessageRepository.getTotalInRoom(get.roomId(), get.userIdForPrivates());

        SectionCollection<RoomMessageModel> result = new SectionCollection<>(
            get.section(),
            PaginationUtils.getLastSection(sectionItemsTotal, paginationConfigProvider),
            paginationConfig.perPageDefault(),
            sectionItems.size(),
            sectionItems
        );

        return ResultUtils.fromTData(result);
    }

    @Override
    public ResultT<RoomMessageModel> getInRoomById(GetInRoomById get) {
        GetInRoomByIdValidator validationModel = roomMessageMapper
                .toGetInRoomByIdValidatorFromGetInRoomById(get);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<RoomModel> existsRoomResult = roomService.getById(get.roomId());

        if (existsRoomResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsRoomResult.getError());
        }

        ResultT<UserModel> existsUserResult = userService.getById(get.userIdForPrivates());

        if (existsUserResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsUserResult.getError());
        }

        RoomMessageModel existsMessage = roomMessageRepository.getInRoomById(
            get.roomId(),
            get.userIdForPrivates(),
            get.messageId()
        );

        if (existsMessage == null) {
            return ResultUtils.fromTSingleError(
                ModelsErrorConstants.NOT_EXISTS_TYPE,
                ModelsErrorConstants.NOT_EXISTS_TITLE,
                ModelsErrorConstants.NOT_EXISTS_STATUS,
                ModelsErrorConstants.NOT_EXISTS_DESCRIPTION
            );
        }

        return ResultUtils.fromTData(existsMessage);
    }

    @Override
    public ResultT<RoomMessageModel> getInRoomByIdPublic(GetInRoomByIdPublic get) {
        GetInRoomByIdPublicValidator validationModel = roomMessageMapper
                .toGetInRoomByIdPublicValidatorFromGetInRoomByIdPublic(get);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<RoomModel> existsRoomResult = roomService.getById(get.roomId());

        if (existsRoomResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsRoomResult.getError());
        }

        RoomMessageModel existsRoomMessage = roomMessageRepository
                .getInRoomByIdPublic(get.roomId(), get.messageId());

        if (existsRoomMessage == null) {
            return ResultUtils.fromTSingleError(
                ModelsErrorConstants.NOT_EXISTS_TYPE,
                ModelsErrorConstants.NOT_EXISTS_TITLE,
                ModelsErrorConstants.NOT_EXISTS_STATUS,
                ModelsErrorConstants.NOT_EXISTS_DESCRIPTION
            );
        }

        return ResultUtils.fromTData(existsRoomMessage);
    }

    @Override
    public ResultT<RoomMessageModel> addPrivateRoomMessage(CreateNewPrivateRoomMessage createNewPrivateRoomMessage) {
        CreateNewPrivateRoomMessageValidator validationModel = roomMessageMapper
                .toCreateNewPrivateRoomMessageValidatorFromCreateNewPrivateRoomMessages(createNewPrivateRoomMessage);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<UserModel> existsUserResult = userService.getById(createNewPrivateRoomMessage.userId());

        if (existsUserResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsUserResult.getError());
        }

        ResultT<RoomModel> existsRoomResult = roomService.getById(createNewPrivateRoomMessage.roomId());

        if (existsRoomResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsRoomResult.getError());
        }

        ResultT<UserModel> existsDestinationUserResult = userService.getById(createNewPrivateRoomMessage.destinationUserId());

        if (existsDestinationUserResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsDestinationUserResult.getError());
        }

        if (createNewPrivateRoomMessage.userId() == createNewPrivateRoomMessage.destinationUserId()) {
            return ResultUtils.fromTSingleError(
                RoomPrivateMessageErrorsConstants.CANNOT_SEND_TO_YOUR_SELF_PRIVATE_MESSAGE_TYPE,
                RoomPrivateMessageErrorsConstants.CANNOT_SEND_TO_YOUR_SELF_PRIVATE_MESSAGE_TITLE,
                RoomPrivateMessageErrorsConstants.CANNOT_SEND_TO_YOUR_SELF_PRIVATE_MESSAGE_STATUS,
                RoomPrivateMessageErrorsConstants.CANNOT_SEND_TO_YOUR_SELF_PRIVATE_MESSAGE_DESCRIPTION
            );
        }

        RoomMessageModel createdPrivateRoomMessage = roomMessageRepository.addPrivateRoomMessage(
            createNewPrivateRoomMessage.userId(),
            createNewPrivateRoomMessage.roomId(),
            createNewPrivateRoomMessage.destinationUserId(),
            createNewPrivateRoomMessage.body(),
            dateTimeProvider.now()
        );

        return ResultUtils.fromTData(createdPrivateRoomMessage);
    }

    @Override
    public ResultT<SectionCollection<UserModel>> getAllPrivateMessageDestinationUserSection(GetAllPrivateMessageDestinationUserSection get) {
        GetAllPrivateMessageDestinationUserSectionValidator validationModel = roomMessageMapper
                .toGetAllPrivateMessageDestinationUserSectionValidatorFromModel(get);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        PaginationConfig paginationConfig = paginationConfigProvider.get();

        ResultT<RoomModel> existsRoomResult = roomService.getById(get.roomId());

        if (existsRoomResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsRoomResult.getError());
        }

        ResultT<UserModel> existsUserResult = userService.getById(get.userIdForPrivates());

        if (existsUserResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsUserResult.getError());
        }

        List<UserModel> list = roomMessageRepository.getPrivateMessagesDestinationUsers(
            get.roomId(),
            get.userIdForPrivates(),
            PaginationUtils.getOffset(get.section(), paginationConfigProvider),
            paginationConfig.perPageDefault()
        );

        long privateMessageTotal = roomMessageRepository
                .getTotalPrivateMessagesDestinationUsers(get.roomId(),get.userIdForPrivates());

        SectionCollection<UserModel> section = new SectionCollection<>(
            get.section(),
            PaginationUtils.getLastSection(privateMessageTotal, paginationConfigProvider),
            paginationConfig.perPageDefault(),
            list.size(),
            list
        );

        return ResultUtils.fromTData(section);
    }

    @Override
    public ResultT<SectionCollection<RoomMessageModel>> getAllPrivateMessageByDestinationUserSection(GetAllPrivateMessageByDestination get) {
        GetAllPrivateMessageByDestinationValidator validationModel = roomMessageMapper.toGetAllPrivateMessageByDestinationValidatorFromModel(get);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        PaginationConfig paginationConfig = paginationConfigProvider.get();

        ResultT<RoomModel> existsRoomResult = roomService.getById(get.roomId());

        if (existsRoomResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsRoomResult.getError());
        }

        ResultT<UserModel> existsUserForPrivatesResult = userService.getById(get.userIdForPrivates());

        if (existsUserForPrivatesResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsUserForPrivatesResult.getError());
        }

        ResultT<UserModel> existsDestinationUserResult = userService.getById(get.destinationUserId());

        if (existsDestinationUserResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsDestinationUserResult.getError());
        }

        List<RoomMessageModel> sectionList = roomMessageRepository
                .getAllPrivateMessageByDestinationUserSection(
                        get.roomId(),
                        get.userIdForPrivates(),
                        get.destinationUserId(),
                        PaginationUtils.getOffset(get.section(), paginationConfigProvider),
                        paginationConfig.perPageDefault());

        long privateMessageByDestinationTotal = roomMessageRepository
                .getTotalAllPrivateMessageByDestinationUser(
                        get.roomId(),
                        get.userIdForPrivates(),
                        get.destinationUserId());

        SectionCollection<RoomMessageModel> section = new SectionCollection<>(
                get.section(),
                PaginationUtils.getLastSection(privateMessageByDestinationTotal, paginationConfigProvider),
                paginationConfig.perPageDefault(),
                sectionList.size(),
                sectionList);

        return ResultUtils.fromTData(section);
    }
}
