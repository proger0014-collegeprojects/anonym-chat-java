package anonym.chat.application.validators.message;

import anonym.chat.core.models.ValidationModel;
import jakarta.validation.constraints.Min;

public record MessageIdValidator(
    @Min(value = 1, message = "Значение не должно быть меньше, чем 1")
    long messageId
) implements ValidationModel { }
