package anonym.chat.application.mappers;

import anonym.chat.application.validators.avatar.CreateNewAvatarValidator;
import anonym.chat.application.validators.avatar.GetAllOfUserSectionValidator;
import anonym.chat.core.models.avatar.CreateNewAvatar;
import anonym.chat.core.models.avatar.EditAvatar;
import anonym.chat.core.models.avatar.GetAllOfUserSection;
import anonym.chat.core.models.file.temp.FileTempModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.io.InputStream;

@Mapper(componentModel = "spring", implementationName = "AvatarMapperImplApplication")
public interface AvatarMapper {
    CreateNewAvatarValidator toCreateNewAvatarValidatorFromCreateNewAvatar(CreateNewAvatar createNewAvatar);

    @Mapping(target = "tempFile", source = "fileTempModel")
    @Mapping(target = "offsetX", source = "createNewAvatar.offsetX")
    @Mapping(target = "offsetY", source = "createNewAvatar.offsetY")
    @Mapping(target = "size", source = "createNewAvatar.size")
    EditAvatar toEditAvatarFromCreateNewAvatar(CreateNewAvatar createNewAvatar, FileTempModel fileTempModel);

    GetAllOfUserSectionValidator toGetAllOfUserSectionValidatorFromModel(GetAllOfUserSection source);
}
