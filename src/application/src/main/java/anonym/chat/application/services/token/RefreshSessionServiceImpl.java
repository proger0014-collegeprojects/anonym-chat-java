package anonym.chat.application.services.token;

import anonym.chat.application.mappers.RefreshTokenMapper;
import anonym.chat.application.validators.token.refresh.CheckTokenIsValidValidator;
import anonym.chat.application.validators.token.refresh.CreateNewFromExistingTokenValidator;
import anonym.chat.application.validators.token.refresh.RefreshTokenValidator;
import anonym.chat.application.validators.user.UserIdValidator;
import anonym.chat.core.abstractions.providers.DateTimeProvider;
import anonym.chat.core.abstractions.Validator;
import anonym.chat.core.abstractions.providers.TokenGeneratorProvider;
import anonym.chat.core.abstractions.providers.config.RefreshTokenConfigProvider;
import anonym.chat.core.abstractions.token.refresh.RefreshSessionRepository;
import anonym.chat.core.abstractions.token.refresh.RefreshSessionService;
import anonym.chat.core.abstractions.user.UserService;
import anonym.chat.core.constants.errors.RefreshTokenErrorsConstants;
import anonym.chat.core.models.token.refresh.CheckTokenIsValid;
import anonym.chat.core.models.token.refresh.CreateNewFromExistingToken;
import anonym.chat.core.models.token.refresh.RefreshSessionModel;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.core.utils.result.ResultUtils;
import anonym.chat.core.utils.validator.ValidationResult;
import anonym.chat.core.utils.validator.ValidatorUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class RefreshSessionServiceImpl implements RefreshSessionService {
    private final RefreshSessionRepository refreshSessionRepository;

    private final UserService userService;
    private final TokenGeneratorProvider tokenGeneratorProvider;
    private final Validator validator;
    private final RefreshTokenMapper refreshTokenMapper;
    private final DateTimeProvider dateTimeProvider;
    private final RefreshTokenConfigProvider refreshTokenConfigProvider;

    public RefreshSessionServiceImpl(
            RefreshSessionRepository refreshSessionRepository,
            UserService userService,
            TokenGeneratorProvider tokenGeneratorProvider,
            Validator validator,
            RefreshTokenMapper refreshTokenMapper,
            DateTimeProvider dateTimeProvider,
            RefreshTokenConfigProvider refreshTokenConfigProvider) {
        this.refreshSessionRepository = refreshSessionRepository;
        this.userService = userService;
        this.tokenGeneratorProvider = tokenGeneratorProvider;
        this.validator = validator;
        this.refreshTokenMapper = refreshTokenMapper;
        this.dateTimeProvider = dateTimeProvider;
        this.refreshTokenConfigProvider = refreshTokenConfigProvider;
    }

    @Override
    public ResultT<RefreshSessionModel> createNew(long userId) {
        UserIdValidator validationModel = new UserIdValidator(userId);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<UserModel> existsUserResult = userService.getById(userId);

        if (existsUserResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsUserResult.getError());
        }

        LocalDateTime nowDateTime = dateTimeProvider.now();
        LocalDateTime expiresDateTime = nowDateTime.plusDays(refreshTokenConfigProvider.get().expiresInDays());

        RefreshSessionModel createdRefreshSession = refreshSessionRepository.add(
            userId,
            tokenGeneratorProvider.generate(),
            nowDateTime,
            expiresDateTime);

        return ResultUtils.fromTData(createdRefreshSession);
    }

    @Override
    public ResultT<RefreshSessionModel> getByToken(String refreshToken) {
        RefreshTokenValidator validationModel = new RefreshTokenValidator(refreshToken);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        RefreshSessionModel existsRefreshSession = refreshSessionRepository.getByToken(refreshToken);

        if (existsRefreshSession == null) {
            return ResultUtils.fromTSingleError(
                RefreshTokenErrorsConstants.NOT_EXISTS_TYPE,
                RefreshTokenErrorsConstants.NOT_EXISTS_TITLE,
                RefreshTokenErrorsConstants.NOT_EXISTS_STATUS,
                RefreshTokenErrorsConstants.NOT_EXISTS_DESCRIPTION
            );
        }

        return ResultUtils.fromTData(existsRefreshSession);
    }

    @Override
    public ResultT<Boolean> deleteByToken(String refreshToken) {
        RefreshTokenValidator validationModel = new RefreshTokenValidator(refreshToken);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<RefreshSessionModel> existsRefreshSessionResult = getByToken(refreshToken);

        if (existsRefreshSessionResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsRefreshSessionResult.getError());
        }

        RefreshSessionModel existsRefreshSession = existsRefreshSessionResult.getData();

        boolean isDeleted = refreshSessionRepository.deleteByRefreshToken(existsRefreshSession.getRefreshToken());

        return ResultUtils.fromTData(isDeleted);
    }

    @Override
    public ResultT<RefreshSessionModel> createNewFromExistingToken(CreateNewFromExistingToken createNewFromExistingToken) {
        CreateNewFromExistingTokenValidator validationModel = refreshTokenMapper
                .toCreateNewFromExistingTokenValidatorFromCreateNewFromExistingToken(createNewFromExistingToken);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<RefreshSessionModel> existsRefreshSessionResult = getByToken(createNewFromExistingToken.refreshToken());

        if (existsRefreshSessionResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsRefreshSessionResult.getError());
        }

        RefreshSessionModel existsRefreshSession = existsRefreshSessionResult.getData();

        CheckTokenIsValid checkTokenIsValid = new CheckTokenIsValid(existsRefreshSession.getRefreshToken());

        ResultT<Boolean> tokenIsValidResult = checkTokenIsValid(checkTokenIsValid);

        if (tokenIsValidResult.isFailure()) {
            return ResultUtils.fromTSingleError(tokenIsValidResult.getError());
        }

        ResultT<Boolean> deleteRefreshTokenResult = deleteByToken(existsRefreshSession.getRefreshToken());

        if (deleteRefreshTokenResult.isFailure()) {
            return ResultUtils.fromTSingleError(deleteRefreshTokenResult.getError());
        }

        return createNew(existsRefreshSession.getUserId());
    }

    @Override
    public ResultT<Boolean> checkTokenIsValid(CheckTokenIsValid checkTokenIsValid) {
        CheckTokenIsValidValidator validationModel = refreshTokenMapper
                .toCheckTokenIsValidValidatorFromCheckTokenIsValid(checkTokenIsValid);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<RefreshSessionModel> existsRefreshSessionResult = getByToken(checkTokenIsValid.refreshToken());

        if (existsRefreshSessionResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsRefreshSessionResult.getError());
        }

        RefreshSessionModel existsRefreshSession = existsRefreshSessionResult.getData();

        LocalDateTime nowDateTime = dateTimeProvider.now();

        if (existsRefreshSession.getExpiresIn().isBefore(nowDateTime)) {
            return ResultUtils.fromTSingleError(
                RefreshTokenErrorsConstants.EXPIRED_TYPE,
                RefreshTokenErrorsConstants.EXPIRED_TITLE,
                RefreshTokenErrorsConstants.EXPIRED_STATUS,
                RefreshTokenErrorsConstants.EXPIRED_DESCRIPTION
            );
        }

        return ResultUtils.fromTData(true);
    }
}
