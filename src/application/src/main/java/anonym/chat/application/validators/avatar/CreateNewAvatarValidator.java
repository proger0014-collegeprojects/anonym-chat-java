package anonym.chat.application.validators.avatar;

import anonym.chat.core.models.ValidationModel;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;

public record CreateNewAvatarValidator(
        @Min(value = 1, message = "Значение не должно быть меньше, чем 1")
        long userId,

        @Min(value = 0, message = "Значение не должно быть меньше, чем 0")
        @Max(value = 100, message = "значение не должно быть больше, чем 100")
        int offsetX,

        @Min(value = 0, message = "Значение не должно быть меньше, чем 0")
        @Max(value = 100, message = "значение не должно быть больше, чем 100")
        int offsetY,

        @Min(value = 0, message = "Значение не должно быть меньше, чем 0")
        @Max(value = 100, message = "значение не должно быть больше, чем 100")
        int size
) implements ValidationModel {
}
