package anonym.chat.application.validators.user;

import anonym.chat.core.constants.validations.CommonValidationMessages;
import anonym.chat.core.models.ValidationModel;
import anonym.chat.core.models.user.Role;
import jakarta.validation.constraints.NotNull;

public record CreateNewAuthUserValidator(
    @NotNull(message = CommonValidationMessages.FIELD_EMPTY)
    Role role,

    @NotNull(message = CommonValidationMessages.FIELD_EMPTY)
    String login,

    @NotNull(message = CommonValidationMessages.FIELD_EMPTY)
    String password
) implements ValidationModel { }
