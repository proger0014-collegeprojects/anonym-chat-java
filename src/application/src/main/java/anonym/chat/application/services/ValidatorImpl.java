package anonym.chat.application.services;

import anonym.chat.core.abstractions.Validator;
import anonym.chat.core.models.ValidationModel;
import anonym.chat.core.utils.validator.ValidationResult;
import anonym.chat.core.utils.validator.ValidatorUtils;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.executable.ExecutableValidator;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ValidatorImpl implements Validator {

    private final jakarta.validation.Validator validator;

    public ValidatorImpl(jakarta.validation.Validator validator) {
        this.validator = validator;
    }


    @SafeVarargs
    @Override
    public final <T extends ValidationModel> ValidationResult validate(T... target) {
        Set<ConstraintViolation<T>> result = null;

        for (T one : target) {
            if (result == null) {
                result = validator.validate(one);
            } else {
                result.addAll(validator.validate(one));
            }
        }

        if (!result.isEmpty()) {
            var errors = new HashMap<String, List<String>>();

            for (var violation : result) {
                if (errors.containsKey(violation.getPropertyPath().toString())) {
                    var messages = errors.get(violation.getPropertyPath().toString());
                    messages.add(violation.getMessage());
                    errors.replace(violation.getPropertyPath().toString(), messages);
                } else {
                    List<String> messages = new ArrayList<>();
                    messages.add(violation.getMessage());
                    errors.put(violation.getPropertyPath().toString(), messages);
                }
            }

            return ValidatorUtils.invalid(errors);
        }

        return ValidatorUtils.valid();
    }
}
