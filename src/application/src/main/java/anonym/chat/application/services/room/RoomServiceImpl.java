package anonym.chat.application.services.room;

import anonym.chat.application.mappers.RoomMapper;
import anonym.chat.application.utils.PaginationUtils;
import anonym.chat.application.validators.common.IdValidator;
import anonym.chat.application.validators.common.SectionValidator;
import anonym.chat.application.validators.room.ConnectValidator;
import anonym.chat.application.validators.room.CreateNewRoomValidator;
import anonym.chat.application.validators.common.PageValidator;
import anonym.chat.application.validators.room.RoomIdValidator;
import anonym.chat.application.validators.user.UserIdValidator;
import anonym.chat.core.abstractions.providers.DateTimeProvider;
import anonym.chat.core.abstractions.providers.config.PaginationConfigProvider;
import anonym.chat.core.abstractions.Validator;
import anonym.chat.core.abstractions.room.RoomRepository;
import anonym.chat.core.abstractions.room.RoomService;
import anonym.chat.core.abstractions.room.profile.RoomProfileService;
import anonym.chat.core.abstractions.user.UserService;
import anonym.chat.core.constants.errors.ModelsErrorConstants;
import anonym.chat.core.constants.errors.RoomErrorsConstants;
import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.models.room.CreateNewRoom;
import anonym.chat.core.models.room.RoomModel;
import anonym.chat.core.models.room.profile.RoomProfileModel;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.paginations.PaginationConfig;
import anonym.chat.core.utils.result.ResultError;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.core.utils.result.ResultUtils;
import anonym.chat.core.utils.validator.ValidationResult;
import anonym.chat.core.utils.validator.ValidatorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;

@Lazy
@Service
public class RoomServiceImpl implements RoomService {
    private final RoomRepository roomRepository;

    private final Validator validator;
    private final RoomMapper roomMapper;
    private final UserService userService;
    private final DateTimeProvider dateTimeProvider;
    private final PaginationConfigProvider paginationConfigProvider;

    @Autowired
    private RoomProfileService roomProfileService;


    public RoomServiceImpl(
            RoomRepository roomRepository,
            Validator validator,
            RoomMapper roomMapper,
            UserService userService,
            DateTimeProvider dateTimeProvider,
            PaginationConfigProvider paginationConfigProvider) {
        this.roomRepository = roomRepository;
        this.validator = validator;
        this.roomMapper = roomMapper;
        this.userService = userService;
        this.dateTimeProvider = dateTimeProvider;
        this.paginationConfigProvider = paginationConfigProvider;
    }

    @Override
    public ResultT<RoomModel> add(CreateNewRoom create) {
        CreateNewRoomValidator validationModel = roomMapper.toCreateNewRoomValidatorFromCreateNewRoom(create);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<UserModel> existsUserResult = userService.getById(create.userId());

        if (existsUserResult.isFailure()) {
            ResultError error = existsUserResult.getError();
            return ResultUtils.fromTSingleError(error);
        }

        RoomModel roomModel = roomMapper.toRoomModelFromCreateNewRoom(create);

        roomModel.setCreatedAt(dateTimeProvider.now());

        RoomModel createdRoomModel = roomRepository.add(roomModel);

        if (createdRoomModel == null) {
            return ResultUtils.fromTSingleError(
                ModelsErrorConstants.NOT_EXISTS_TYPE,
                ModelsErrorConstants.NOT_EXISTS_TITLE,
                ModelsErrorConstants.NOT_EXISTS_STATUS,
                ModelsErrorConstants.NOT_EXISTS_DESCRIPTION
            );
        }

        return ResultUtils.fromTData(createdRoomModel);
    }

    @Override
    public ResultT<RoomModel> getById(long id) {
        IdValidator validationModel = new IdValidator(id);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        RoomModel existsRoom = roomRepository.getById(id);

        if (existsRoom == null) {
            return ResultUtils.fromTSingleError(
                ModelsErrorConstants.NOT_EXISTS_TYPE,
                ModelsErrorConstants.NOT_EXISTS_TITLE,
                ModelsErrorConstants.NOT_EXISTS_STATUS,
                ModelsErrorConstants.NOT_EXISTS_DESCRIPTION
            );
        }

        return ResultUtils.fromTData(existsRoom);
    }

    @Override
    public ResultT<SectionCollection<RoomModel>> getAllPage(long page) {
        PageValidator validationModel = new PageValidator(page);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        List<RoomModel> list = roomRepository.getAllInSection(
            PaginationUtils.getOffset(page, paginationConfigProvider),
            paginationConfigProvider.get().perPageDefault()
        );

        long totalRooms = roomRepository.getTotal();

        SectionCollection<RoomModel> section = new SectionCollection<>(
            page,
            PaginationUtils.getLastSection(totalRooms, paginationConfigProvider),
            paginationConfigProvider.get().perPageDefault(),
            list.size(),
            list);

        return ResultUtils.fromTData(section);
    }

    @Override
    public ResultT<SectionCollection<RoomModel>> getAllByUserIdInPage(long userId, long page) {
        PaginationConfig paginationConfig = paginationConfigProvider.get();

        UserIdValidator userIdValidationModel = new UserIdValidator(userId);
        PageValidator pageValidationModel = new PageValidator(page);

        ValidationResult validationResult = validator.validate(userIdValidationModel, pageValidationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<UserModel> existsUserResult = userService.getById(userId);

        if (existsUserResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsUserResult.getError());
        }

        List<RoomModel> list = roomRepository.getAllByUserIdInSection(
            userId,
            PaginationUtils.getOffset(page, paginationConfigProvider),
            paginationConfig.perPageDefault());

        long totalRoomsByUserId = roomRepository.getTotalByUserId(userId);

        SectionCollection<RoomModel> section = new SectionCollection<>(
                page,
                PaginationUtils.getLastSection(totalRoomsByUserId, paginationConfigProvider),
                paginationConfig.perPageDefault(),
                list.size(),
                list);

        return ResultUtils.fromTData(section);
    }

    @Override
    public ResultT<Integer> getConnectedUsersCountInRoom(long roomId) {
        RoomIdValidator roomIdValidationModel = new RoomIdValidator(roomId);

        ValidationResult validationResult = validator.validate(roomIdValidationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<RoomModel> existsRoomResult = getById(roomId);

        if (existsRoomResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsRoomResult.getError());
        }

        int count = roomRepository.getConnectedUsersCountInRoom(roomId);

        return ResultUtils.fromTData(count);
    }

    @Override
    public ResultT<SectionCollection<UserModel>> getAllConnectedUsersInRoomSection(long roomId, long section) {
        PaginationConfig paginationConfig = paginationConfigProvider.get();

        RoomIdValidator roomIdValidationModel = new RoomIdValidator(roomId);
        SectionValidator sectionValidationModel = new SectionValidator(section);

        ValidationResult validationResult = validator.validate(roomIdValidationModel, sectionValidationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<RoomModel> existsRoomResult = getById(roomId);

        if (existsRoomResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsRoomResult.getError());
        }

        List<UserModel> connectedUsersSectionList = roomRepository.getAllConnectedUsersInRoomSection(
            roomId,
            PaginationUtils.getOffset(section, paginationConfigProvider),
            paginationConfig.perPageDefault());

        long totalConnectedUsers = roomRepository.getConnectedUsersCountInRoom(roomId);

        SectionCollection<UserModel> connectedUsersSection = new SectionCollection<>(
            section,
            PaginationUtils.getLastSection(totalConnectedUsers, paginationConfigProvider),
            paginationConfig.perPageDefault(),
            connectedUsersSectionList.size(),
            connectedUsersSectionList
        );

        return ResultUtils.fromTData(connectedUsersSection);
    }

    @Override
    public ResultT<Boolean> connectUserToRoom(long userId, long roomId) {
        ConnectValidator validationModel = new ConnectValidator(userId, roomId);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<UserModel> existsUserResult = userService.getById(userId);

        if (existsUserResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsUserResult.getError());
        }

        ResultT<RoomModel> existsRoomResult = getById(roomId);

        if (existsRoomResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsRoomResult.getError());
        }

        ResultT<RoomProfileModel> existsSelectedRoomProfile = roomProfileService.getByRoomIdAndUserId(roomId, userId);

        if (existsSelectedRoomProfile.isFailure()) {
            return ResultUtils.fromTSingleError(
                RoomErrorsConstants.NOT_SELECTED_ROOM_PROFILE_FOR_ROOM_TYPE,
                RoomErrorsConstants.NOT_SELECTED_ROOM_PROFILE_FOR_ROOM_TITLE,
                RoomErrorsConstants.NOT_SELECTED_ROOM_PROFILE_FOR_ROOM_STATUS,
                RoomErrorsConstants.NOT_SELECTED_ROOM_PROFILE_FOR_ROOM_DESCRIPTION
            );
        }

        boolean isConnected = roomRepository.isConnectedToRoom(userId, roomId);

        if (isConnected) {
            return ResultUtils.fromTSingleError(
                RoomErrorsConstants.CONNECT_IS_ALREADY_EXISTS_TYPE,
                RoomErrorsConstants.CONNECT_IS_ALREADY_EXISTS_TITLE,
                RoomErrorsConstants.CONNECT_IS_ALREADY_EXISTS_STATUS,
                RoomErrorsConstants.CONNECT_IS_ALREADY_EXISTS_DESCRIPTION
            );
        }

        boolean connect = roomRepository.connectUserToRoom(userId, roomId);

        return ResultUtils.fromTData(connect);
    }

    @Override
    public ResultT<Boolean> isConnectedToRoom(long userId, long roomId) {
        ConnectValidator validationModel = new ConnectValidator(userId, roomId);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<UserModel> existsUserResult = userService.getById(userId);

        if (existsUserResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsUserResult.getError());
        }

        ResultT<RoomModel> existsRoomResult = getById(roomId);

        if (existsRoomResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsRoomResult.getError());
        }

        boolean isConnected = roomRepository.isConnectedToRoom(userId, roomId);

        return ResultUtils.fromTData(isConnected);
    }

    @Override
    public ResultT<Boolean> disconnectUserFromRoom(long userId, long roomId) {
        ConnectValidator validationModel = new ConnectValidator(userId, roomId);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<UserModel> existsUserResult = userService.getById(userId);

        if (existsUserResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsUserResult.getError());
        }

        ResultT<RoomModel> existsRoomResult = getById(roomId);

        if (existsRoomResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsRoomResult.getError());
        }

        boolean isConnected = roomRepository.isConnectedToRoom(userId, roomId);

        if (!isConnected) {
            return ResultUtils.fromTSingleError(
                RoomErrorsConstants.CONNECT_IS_NOT_EXISTS_TYPE,
                RoomErrorsConstants.CONNECT_IS_NOT_EXISTS_TITLE,
                RoomErrorsConstants.CONNECT_IS_NOT_EXISTS_STATUS,
                RoomErrorsConstants.CONNECT_IS_NOT_EXISTS_DESCRIPTION
            );
        }

        boolean isDisconnected = roomRepository.disconnectUserFromRoom(userId, roomId);

        return ResultUtils.fromTData(isDisconnected);
    }

    @Override
    public ResultT<Long> deleteById(long id) {
        IdValidator validationModel = new IdValidator(id);
        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        long deletedRoom = roomRepository.deleteById(id);

        if (deletedRoom < 0) {
            return ResultUtils.fromTSingleError(
                ModelsErrorConstants.NOT_EXISTS_TYPE,
                ModelsErrorConstants.NOT_EXISTS_TITLE,
                ModelsErrorConstants.NOT_EXISTS_STATUS,
                ModelsErrorConstants.NOT_EXISTS_DESCRIPTION
            );
        }

        return ResultUtils.fromTData(deletedRoom);
    }
}
