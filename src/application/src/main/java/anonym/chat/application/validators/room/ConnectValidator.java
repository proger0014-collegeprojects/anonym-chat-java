package anonym.chat.application.validators.room;

import anonym.chat.core.models.ValidationModel;
import jakarta.validation.constraints.Min;

public record ConnectValidator(
    @Min(value = 1, message = "Значение не должно быть меньше, чем 1")
    long userId,
    @Min(value = 1, message = "Значение не должно быть меньше, чем 1")
    long roomId
) implements ValidationModel { }
