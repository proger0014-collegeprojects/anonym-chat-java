package anonym.chat.application.validators.avatar;

import anonym.chat.core.models.ValidationModel;
import jakarta.validation.constraints.Min;

public record AvatarIdValidator(
    @Min(value = 1, message = "Значение не должно быть меньше, чем 1")
    long avatarId
) implements ValidationModel { }
