plugins {
    id("java")
}

group = "anonym.chat.application"
version = "0.0.1"

dependencies {
    implementation(platform(org.springframework.boot.gradle.plugin.SpringBootPlugin.BOM_COORDINATES))

    implementation(project(":src:core"))

    testImplementation(libs.testng)
    implementation(libs.spring.context)
    implementation(libs.support.validator)
    implementation(libs.support.apache.commons.io)

    compileOnly(libs.mapstruct.mapper)
    annotationProcessor(libs.mapstruct.annotation.processor)
}
