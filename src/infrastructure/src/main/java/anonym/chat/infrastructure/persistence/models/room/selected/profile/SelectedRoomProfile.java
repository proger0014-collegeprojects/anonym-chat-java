package anonym.chat.infrastructure.persistence.models.room.selected.profile;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name = "selected_room_profiles", schema = "public")
public class SelectedRoomProfile {

    @EmbeddedId
    private SelectedRoomProfileId selectedRoomProfileId;


    public SelectedRoomProfileId getSelectedRoomProfileId() {
        return selectedRoomProfileId;
    }

    public void setSelectedRoomProfileId(SelectedRoomProfileId selectedRoomProfileId) {
        this.selectedRoomProfileId = selectedRoomProfileId;
    }
}
