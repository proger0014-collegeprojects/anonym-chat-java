package anonym.chat.infrastructure.persistence.models.message.room;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

import java.io.Serializable;

@Embeddable
public class RoomPrivateMessageId implements Serializable {

    @Column(nullable = false)
    private Long roomMessageId;

    @Column(nullable = false)
    private Long userId;


    public Long getRoomMessageId() {
        return roomMessageId;
    }

    public void setRoomMessageId(Long roomMessageId) {
        this.roomMessageId = roomMessageId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
