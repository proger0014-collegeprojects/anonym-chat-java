package anonym.chat.infrastructure.persistence.repositories.message.room;

import anonym.chat.core.abstractions.message.room.RoomMessageRepository;
import anonym.chat.core.models.message.room.RoomMessageModel;
import anonym.chat.core.models.message.room.RoomPrivateMessageInfo;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.infrastructure.persistence.mappers.MessageMapper;
import anonym.chat.infrastructure.persistence.mappers.UserMapper;
import anonym.chat.infrastructure.persistence.models.message.Message;
import anonym.chat.infrastructure.persistence.models.message.room.RoomMessage;
import anonym.chat.infrastructure.persistence.models.message.room.RoomPrivateMessage;
import anonym.chat.infrastructure.persistence.models.message.room.RoomPrivateMessageId;
import anonym.chat.infrastructure.persistence.models.room.Room;
import anonym.chat.infrastructure.persistence.models.user.User;
import anonym.chat.infrastructure.persistence.repositories.message.MessageRepositoryJpa;
import anonym.chat.infrastructure.persistence.repositories.room.RoomRepositoryJpa;
import anonym.chat.infrastructure.persistence.repositories.user.UserRepositoryJpa;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class RoomMessageRepositoryImpl implements RoomMessageRepository {
    private final RoomMessageRepositoryJpa roomMessageRepositoryJpa;
    private final MessageRepositoryJpa messageRepositoryJpa;
    private final RoomPrivateMessageRepositoryJpa roomPrivateMessageRepositoryJpa;

    private final MessageMapper messageMapper;
    private final UserRepositoryJpa userRepositoryJpa;
    private final RoomRepositoryJpa roomRepositoryJpa;
    private final UserMapper userMapper;

    public RoomMessageRepositoryImpl(
            RoomMessageRepositoryJpa roomMessageRepositoryJpa,
            MessageRepositoryJpa messageRepositoryJpa,
            RoomPrivateMessageRepositoryJpa roomPrivateMessageRepositoryJpa,
            MessageMapper messageMapper,
            UserRepositoryJpa userRepositoryJpa,
            RoomRepositoryJpa roomRepositoryJpa,
            UserMapper userMapper) {
        this.roomMessageRepositoryJpa = roomMessageRepositoryJpa;
        this.messageRepositoryJpa = messageRepositoryJpa;
        this.roomPrivateMessageRepositoryJpa = roomPrivateMessageRepositoryJpa;
        this.messageMapper = messageMapper;
        this.userRepositoryJpa = userRepositoryJpa;
        this.roomRepositoryJpa = roomRepositoryJpa;
        this.userMapper = userMapper;
    }

    @Transactional
    @Override
    public RoomMessageModel addPublicRoomMessage(long userId, long roomId, String body, LocalDateTime now) {
        User existsUser = userRepositoryJpa.getById(userId);
        Room existsRoom = roomRepositoryJpa.findById(roomId).get();

        Message newMessage = new Message();

        newMessage.setUser(existsUser);
        newMessage.setBody(body);
        newMessage.setCreatedAt(now);

        Message savedMessage = messageRepositoryJpa.save(newMessage);

        RoomMessage newRoomMessage = new RoomMessage();

        newRoomMessage.setMessage(savedMessage);
        newRoomMessage.setRoom(existsRoom);

        RoomMessage savedRoomMessage = roomMessageRepositoryJpa.save(newRoomMessage);

        return messageMapper.toRoomMessageModelFromRoomMessage(savedRoomMessage, null);
    }

    @Transactional
    @Override
    public RoomMessageModel replyToRoomMessage(long userId, long roomId, long roomMessageId, String body, LocalDateTime now) {
        User existsUser = userRepositoryJpa.getById(userId);
        Room existsRoom = roomRepositoryJpa.findById(roomId).get();
        RoomMessage existsRoomMessageToReply = roomMessageRepositoryJpa.getByIdInRoomPublic(roomId, roomMessageId).get();

        Message repliedNewMessage = new Message();

        repliedNewMessage.setReply(existsRoomMessageToReply.getMessage());
        repliedNewMessage.setUser(existsUser);
        repliedNewMessage.setBody(body);
        repliedNewMessage.setCreatedAt(now);

        Message savedRepliedNewMessage = messageRepositoryJpa.save(repliedNewMessage);

        RoomMessage toSave = new RoomMessage();

        toSave.setRoom(existsRoom);
        toSave.setMessage(savedRepliedNewMessage);

        RoomMessage savedRoomMessage = roomMessageRepositoryJpa.save(toSave);

        return messageMapper.toRoomMessageModelFromRoomMessage(savedRoomMessage, null);
    }

    @Transactional
    @Override
    public List<RoomMessageModel> getAllInRoomSection(long roomId, long userIdForPrivates, long offset, long count) {
        List<RoomMessage> list = roomMessageRepositoryJpa.getAllInRoomSection(roomId, userIdForPrivates, offset, count);

        return list.stream().map(rm -> {
            Optional<RoomPrivateMessage> existsAsPrivate = roomPrivateMessageRepositoryJpa
                    .getByRoomMessageIdAndUserIdForPrivates(rm.getId(), userIdForPrivates);

            RoomPrivateMessageInfo roomPrivateMessageInfo = null;

            if (existsAsPrivate.isPresent()) {
                roomPrivateMessageInfo = new RoomPrivateMessageInfo();

                roomPrivateMessageInfo.setDestinationUserId(existsAsPrivate.get().getRoomPrivateMessageId().getUserId());
            }


            return messageMapper.toRoomMessageModelFromRoomMessage(rm, roomPrivateMessageInfo);
        }).toList();
    }

    @Override
    public long getTotalInRoom(long roomId, long userIdForPrivates) {
        return roomMessageRepositoryJpa.getTotalInRoom(roomId, userIdForPrivates);
    }

    @Transactional
    @Override
    public RoomMessageModel getInRoomById(long roomId, long userIdForPrivates, long messageId) {
        Optional<RoomMessage> existsRoomMessage = roomMessageRepositoryJpa
                .getByIdInRoom(roomId, userIdForPrivates, messageId);

        if (existsRoomMessage.isEmpty()) return null;

        Optional<RoomPrivateMessage> existsRoomPrivateMessage = roomPrivateMessageRepositoryJpa
                .getByRoomMessageIdAndUserIdForPrivates(messageId, userIdForPrivates);

        RoomPrivateMessageInfo roomPrivateMessageInfo = null;

        if (existsRoomPrivateMessage.isPresent()) {
            roomPrivateMessageInfo = new RoomPrivateMessageInfo();

            roomPrivateMessageInfo.setDestinationUserId(existsRoomPrivateMessage.get().getRoomPrivateMessageId().getUserId());
        }

        return messageMapper.toRoomMessageModelFromRoomMessage(existsRoomMessage.get(), roomPrivateMessageInfo);
    }

    @Override
    public RoomMessageModel getInRoomByIdPublic(long roomId, long messageId) {
        Optional<RoomMessage> existsRoomMessage = roomMessageRepositoryJpa
                .getByIdInRoomPublic(roomId, messageId);

        return existsRoomMessage
                .map(roomMessage -> messageMapper.toRoomMessageModelFromRoomMessage(roomMessage, null))
                .orElse(null);
    }

    @Transactional
    @Override
    public RoomMessageModel addPrivateRoomMessage(long userId, long roomId, long destinationUserId, String body, LocalDateTime now) {
        User existsUser = userRepositoryJpa.getById(userId);
        Room existsRoom = roomRepositoryJpa.findById(roomId).get();
        User existsDestinationUser = userRepositoryJpa.getById(destinationUserId);

        Message newMessage = new Message();

        newMessage.setCreatedAt(now);
        newMessage.setBody(body);
        newMessage.setUser(existsUser);

        Message savedNewMessage = messageRepositoryJpa.save(newMessage);

        RoomMessage newRoomMessage = new RoomMessage();

        newRoomMessage.setMessage(savedNewMessage);
        newRoomMessage.setRoom(existsRoom);

        RoomMessage savedNewRoomMessage = roomMessageRepositoryJpa.save(newRoomMessage);

        RoomPrivateMessage newPrivateRoomMessage = new RoomPrivateMessage();

        RoomPrivateMessageId roomPrivateMessageId = new RoomPrivateMessageId();

        roomPrivateMessageId.setUserId(existsDestinationUser.getId());
        roomPrivateMessageId.setRoomMessageId(savedNewRoomMessage.getId());

        newPrivateRoomMessage.setRoomPrivateMessageId(roomPrivateMessageId);

        RoomPrivateMessage savedNewPrivateRoomMessage = roomPrivateMessageRepositoryJpa.save(newPrivateRoomMessage);

        RoomPrivateMessageInfo roomPrivateMessageInfo = new RoomPrivateMessageInfo();

        roomPrivateMessageInfo.setDestinationUserId(existsDestinationUser.getId());

        return messageMapper.toRoomMessageModelFromRoomMessage(savedNewRoomMessage, roomPrivateMessageInfo);
    }

    @Transactional
    @Override
    public RoomMessageModel replyToRoomPrivateMessage(long userId, long roomId, long destinationUserId, long roomPrivateMessageId, String body, LocalDateTime now) {
        Room existsRoom = roomRepositoryJpa.findById(roomId).get();
        User existsUser = userRepositoryJpa.getById(userId);
        User existsDestinationUser = userRepositoryJpa.getById(destinationUserId);

        RoomMessage existsRoomPrivateMessageToReply = roomMessageRepositoryJpa
                .getByIdInRoom(existsRoom.getId(), userId, roomPrivateMessageId).get();

        Message newMessageToReply = new Message();

        newMessageToReply.setUser(existsUser);
        newMessageToReply.setBody(body);
        newMessageToReply.setReply(existsRoomPrivateMessageToReply.getMessage());
        newMessageToReply.setCreatedAt(now);

        Message savedNewMessageToReply = messageRepositoryJpa.save(newMessageToReply);

        RoomMessage newRoomMessageToReply = new RoomMessage();

        newRoomMessageToReply.setRoom(existsRoom);
        newRoomMessageToReply.setMessage(savedNewMessageToReply);

        RoomMessage savedNewRoomMessageToReply = roomMessageRepositoryJpa.save(newRoomMessageToReply);

        RoomPrivateMessage newRoomPrivateMessageToReply = new RoomPrivateMessage();

        RoomPrivateMessageId roomPrivateMessageIdEntity = new RoomPrivateMessageId();

        roomPrivateMessageIdEntity.setRoomMessageId(savedNewRoomMessageToReply.getId());
        roomPrivateMessageIdEntity.setUserId(destinationUserId);

        newRoomPrivateMessageToReply.setRoomPrivateMessageId(roomPrivateMessageIdEntity);

        RoomPrivateMessage savedNewRoomPrivateMessage = roomPrivateMessageRepositoryJpa.save(newRoomPrivateMessageToReply);

        RoomPrivateMessageInfo roomPrivateMessageInfo = new RoomPrivateMessageInfo();

        roomPrivateMessageInfo.setDestinationUserId(roomPrivateMessageIdEntity.getUserId());

        return messageMapper.toRoomMessageModelFromRoomMessage(savedNewRoomMessageToReply, roomPrivateMessageInfo);
    }

    @Override
    public List<UserModel> getPrivateMessagesDestinationUsers(long roomId, long userIdForPrivates, long offset, long count) {
        List<User> destinations = userRepositoryJpa
                .getPrivateMessageDestinationUsersSection(roomId, userIdForPrivates, offset, count)
                .get()
                .map(uid -> userRepositoryJpa.findById(uid).get())
                .toList();

        return destinations.stream().map(userMapper::from).toList();
    }

    @Override
    public long getTotalPrivateMessagesDestinationUsers(long roomId, long userIdForPrivates) {
        return roomPrivateMessageRepositoryJpa.getTotalPrivateMessagesDestinationUsers(roomId, userIdForPrivates);
    }

    @Override
    public List<RoomMessageModel> getAllPrivateMessageByDestinationUserSection(long roomId, long userIdForPrivates, long destinationUserId, long offset, long count) {
        return roomPrivateMessageRepositoryJpa
                .getAllPrivateMessageByDestinationUserSection(roomId, userIdForPrivates, destinationUserId, offset, count)
                .get()
                .map(rpm -> {
                    RoomMessage rm = roomMessageRepositoryJpa.getByIdInRoom(
                            roomId,
                            userIdForPrivates,
                            rpm.getRoomPrivateMessageId().getRoomMessageId()).get();

                    RoomPrivateMessageInfo roomPrivateMessageInfo = new RoomPrivateMessageInfo();

                    roomPrivateMessageInfo.setDestinationUserId(rpm.getRoomPrivateMessageId().getUserId());

                    return messageMapper.toRoomMessageModelFromRoomMessage(rm, roomPrivateMessageInfo);
                }).toList();
    }

    @Override
    public long getTotalAllPrivateMessageByDestinationUser(long roomId, long userIdForPrivates, long destinationUserId) {
        return roomPrivateMessageRepositoryJpa.getTotalPrivateMessageByDestinationUser(roomId, userIdForPrivates, destinationUserId);
    }
}
