package anonym.chat.infrastructure.persistence.configs;

import org.springframework.context.annotation.Configuration;

@Configuration
public class FileTempConfig {
    public String getTempPath() {
        return System.getProperty("java.io.tmpdir");
    }
}
