package anonym.chat.infrastructure.media.images;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

public final class ImageUtils {
    public static Optional<InputStream> getInputStreamFrom(BufferedImage bufferedImage, String format) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ImageIO.write(bufferedImage, format, byteArrayOutputStream);
            InputStream inputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
            return Optional.of(inputStream);
        } catch (IOException ignored) { }

        return Optional.empty();
    }

    public static int getValueFromPercentage(int value, int percent) {
        return (int)Math.floor(((double) value) * ( ((double) percent ) / 100 ));
    }
}
