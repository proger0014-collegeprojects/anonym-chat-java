package anonym.chat.infrastructure.persistence.constants;

public final class MinioConstants {
    public static final String DEFAULTS_BUCKET = "defaults";
    public static final String FILES_BUCKET = "files";
}
