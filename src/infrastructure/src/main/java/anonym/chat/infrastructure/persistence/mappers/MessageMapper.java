package anonym.chat.infrastructure.persistence.mappers;

import anonym.chat.core.models.message.room.RoomMessageModel;
import anonym.chat.core.models.message.room.RoomPrivateMessageInfo;
import anonym.chat.infrastructure.persistence.models.message.room.RoomMessage;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", implementationName = "MessageMapperImplInfrastructure")
public interface MessageMapper {
    @Mapping(target = "id", source = "source.id")
    @Mapping(target = "userId", source = "source.message.user.id")
    @Mapping(target = "reply", source = "source.message.reply.id")
    @Mapping(target = "createdAt", source = "source.message.createdAt")
    @Mapping(target = "body", source = "source.message.body")
    @Mapping(target = "roomId", source = "source.room.id")
    @Mapping(target = "privateMessageInfo", source = "privateMessageInfo")
    RoomMessageModel toRoomMessageModelFromRoomMessage(RoomMessage source, RoomPrivateMessageInfo privateMessageInfo);
}
