package anonym.chat.infrastructure.persistence.configs;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EntityScan(basePackages = { "anonym.chat.infrastructure.persistence.models" })
@EnableJpaRepositories(
    basePackages = { "anonym.chat.infrastructure.persistence.repositories" }
)
public class DatabaseConfig { }
