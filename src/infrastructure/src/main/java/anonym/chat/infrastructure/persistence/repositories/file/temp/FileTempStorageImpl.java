package anonym.chat.infrastructure.persistence.repositories.file.temp;

import anonym.chat.core.abstractions.file.temp.FileTempStorage;
import anonym.chat.core.models.file.FileExtension;
import anonym.chat.core.models.file.temp.FileTempModel;
import anonym.chat.infrastructure.persistence.configs.FileTempConfig;
import anonym.chat.infrastructure.persistence.utils.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Optional;
import java.util.UUID;

@Component
public class FileTempStorageImpl implements FileTempStorage {
    private final Logger logger = LoggerFactory.getLogger(FileTempStorageImpl.class);

    private final FileTempConfig fileTempConfig;

    public FileTempStorageImpl(FileTempConfig fileTempConfig) {
        this.fileTempConfig = fileTempConfig;
    }

    @Override
    public Optional<FileTempModel> saveTempFileByPath(String filePath) {
        try {
            Path existsFile = Path.of(filePath);

            InputStream toSearchFileExtension = new FileInputStream(existsFile.toString());
            InputStream toSave = new FileInputStream(existsFile.toString());

            IOUtils.closeQuietly(toSearchFileExtension, toSave);
        } catch (Exception ignored) { }

        return Optional.empty();
    }

    @Override
    public Optional<FileTempModel> saveTempFileByStream(InputStream stream) {
        try {
            byte[] buffer = stream.readAllBytes();

            InputStream toSearchFileExtension = new ByteArrayInputStream(buffer);
            InputStream toSave = new ByteArrayInputStream(buffer);

            return saveTempFile(toSearchFileExtension, toSave);
        } catch (Exception ignored) { }

        return Optional.empty();
    }

    private Optional<FileTempModel> saveTempFile(InputStream toSearchFileExtension, InputStream toSave) {
        try {
            Optional<FileExtension> fileExtension = FileUtils.getFileExtensionFromStream(toSearchFileExtension);

            if (fileExtension.isEmpty()) return Optional.empty();

            String tempFileName = UUID.randomUUID() + "." + fileExtension.get().getValue();

            Path toSaveFilePath = Path.of(fileTempConfig.getTempPath(), tempFileName);

            Files.copy(toSave, toSaveFilePath, StandardCopyOption.REPLACE_EXISTING);

            File savedFile = new File(toSaveFilePath.toString());

            FileTempModel fileTempModel = new FileTempModel();

            fileTempModel.setFile(savedFile);
            fileTempModel.setExtension(fileExtension.get());

            return Optional.of(fileTempModel);
        } catch (Exception e){
            logger.error(e.getMessage());
        }

        return Optional.empty();
    }

    @Override
    public boolean deleteTempFile(File toDelete) {
        if (!toDelete.getAbsolutePath().contains(fileTempConfig.getTempPath())) {
            return false;
        }

        return toDelete.delete();
    }
}
