package anonym.chat.infrastructure.persistence.mappers;

import anonym.chat.core.models.room.profile.RoomProfileModel;
import anonym.chat.infrastructure.persistence.models.avatar.Avatar;
import anonym.chat.infrastructure.persistence.models.room.RoomProfile;
import anonym.chat.infrastructure.persistence.models.user.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", implementationName = "RoomProfileMapperImplInfrastructure")
public interface RoomProfileMapper {
    @Mapping(target = "id", source = "source.id")
    @Mapping(target = "user", source = "user")
    @Mapping(target = "avatar", source = "avatar")
    @Mapping(target = "nickname", source = "source.nickname")
    RoomProfile toRoomProfileFromRoomProfileModel(RoomProfileModel source, User user, Avatar avatar);

    @Mapping(target = "userId", source = "user.id")
    @Mapping(target = "avatarId", source = "avatar.id")
    RoomProfileModel toRoomProfileModelFromRoomProfile(RoomProfile source);

    default List<RoomProfileModel> toRoomProfileModelListFromRoomProfileList(List<RoomProfile> source) {
        return source.stream().map(this::toRoomProfileModelFromRoomProfile).toList();
    }
}
