package anonym.chat.infrastructure.persistence.repositories.file;

import anonym.chat.infrastructure.persistence.constants.MinioConstants;
import anonym.chat.infrastructure.persistence.models.file.UploadedMinioFile;
import io.minio.MinioClient;
import io.minio.ObjectWriteResponse;
import io.minio.UploadObjectArgs;
import io.minio.errors.*;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Component
public class FileMinioStorage {
    private final MinioClient minioClient;

    public FileMinioStorage(MinioClient minioClient) {
        this.minioClient = minioClient;
    }

    public UploadedMinioFile saveFile(File file, String name, String extension) throws
            IOException,
            ServerException,
            InsufficientDataException,
            ErrorResponseException,
            NoSuchAlgorithmException,
            InvalidKeyException,
            InvalidResponseException,
            XmlParserException,
            InternalException {
        UploadObjectArgs upload = UploadObjectArgs.builder()
            .bucket(MinioConstants.FILES_BUCKET)
            .object(name + "." + extension)
            .filename(file.getPath())
            .build();

        ObjectWriteResponse response = minioClient.uploadObject(upload);

        return new UploadedMinioFile(
            upload.bucket(),
            upload.object(),
            upload.objectSize(),
            upload.contentType()
        );
    }
}
