package anonym.chat.infrastructure.persistence.utils;

import anonym.chat.core.models.file.FileExtension;
import org.apache.tika.mime.MimeType;
import org.apache.tika.mime.MimeTypes;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.Optional;

public final class FileUtils {
    public static Optional<FileExtension> getFileExtensionFromStream(InputStream from) {
        try(InputStream stream = from) {
            InputStream toCheckMimeType = new BufferedInputStream(stream);

            String mimeType = URLConnection.guessContentTypeFromStream(toCheckMimeType);

            MimeTypes allTypes = MimeTypes.getDefaultMimeTypes();
            MimeType type = allTypes.forName(mimeType);
            String extension = type.getExtension();

            String extensionWithoutDot = extension.substring(1);
            FileExtension fileExtension = FileExtension.fromExtension(extensionWithoutDot);

            return Optional.ofNullable(fileExtension);
        } catch (Exception ignored) { }

        return Optional.empty();
    }
}
