package anonym.chat.infrastructure.media.images;

import anonym.chat.core.abstractions.avatar.AvatarEditor;
import anonym.chat.core.abstractions.file.temp.FileTempStorage;
import anonym.chat.core.models.avatar.EditAvatar;
import anonym.chat.core.models.file.temp.FileTempModel;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Optional;

@Component
public class AvatarEditorImpl implements AvatarEditor {
    private final AvatarConfig avatarConfig;

    private final FileTempStorage fileTempStorage;

    public AvatarEditorImpl(
            AvatarConfig avatarConfig,
            FileTempStorage fileTempStorage) {
        this.avatarConfig = avatarConfig;
        this.fileTempStorage = fileTempStorage;
    }

    @Override
    public FileTempModel editAvatar(EditAvatar forEdit) {
        int defaultImageSize = avatarConfig.getDefaultSizeImage();

        FileTempModel result = null;

        try(ImageInputStream input = ImageIO.createImageInputStream(forEdit.tempFile().getFile())) {

            Iterator<ImageReader> readers = ImageIO.getImageReaders(input);

            if (!readers.hasNext()) {
                return null;
            }

            ImageReader reader = readers.next();

            try {
                reader.setInput(input);

                int imageWidth = reader.getWidth(0);
                int imageHeight = reader.getHeight(0);

                int imageWidthForWork = imageWidth - avatarConfig.getDefaultSizeImage();
                int imageHeightForWork = imageHeight - avatarConfig.getDefaultSizeImage();

                int imageSizeXRemainder = imageWidth - imageWidthForWork;
                int imageSizeYRemainder = imageHeight - imageHeightForWork;

                int imageSizeXPixel = ImageUtils
                        .getValueFromPercentage(imageWidthForWork, forEdit.size()) + imageSizeXRemainder;

                int imageSizeYPixel = ImageUtils
                        .getValueFromPercentage(imageHeightForWork, forEdit.size()) + imageSizeYRemainder;

                int imageOffsetXInPixel = ImageUtils
                        .getValueFromPercentage(imageWidthForWork, forEdit.offsetX());

                int imageOffsetYInPixel = ImageUtils
                        .getValueFromPercentage(imageHeightForWork, forEdit.offsetY());

                int imageSizePixel = Math.min(imageSizeXPixel, imageSizeYPixel);

                ImageReadParam params = reader.getDefaultReadParam();

                Rectangle rectangle = new Rectangle(
                    imageOffsetXInPixel,
                    imageOffsetYInPixel,
                    imageSizePixel,
                    imageSizePixel);

                params.setSourceRegion(rectangle);

                BufferedImage croppedImage = reader.read(0, params);

                BufferedImage scaledImage = new BufferedImage(avatarConfig.getDefaultSizeImage(), avatarConfig.getDefaultSizeImage(), croppedImage.getType());

                Graphics2D graphics = scaledImage.createGraphics();
                graphics.drawImage(croppedImage, 0, 0, avatarConfig.getDefaultSizeImage(), avatarConfig.getDefaultSizeImage(), null);
                graphics.dispose();

                Optional<InputStream> imageStream = ImageUtils.getInputStreamFrom(scaledImage, reader.getFormatName());

                if (imageStream.isEmpty()) return null;

                Optional<FileTempModel> savedTempFile = fileTempStorage.saveTempFileByStream(imageStream.get());

                if (savedTempFile.isEmpty()) return null;

                result = savedTempFile.get();
            } catch (IOException ignored) {
                return null;
            }
        } catch (IOException ignored) {
            return null;
        }

        return result;
    }
}
