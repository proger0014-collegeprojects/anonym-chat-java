package anonym.chat.infrastructure.persistence.repositories.message.room;

import anonym.chat.infrastructure.persistence.models.message.room.RoomPrivateMessage;
import anonym.chat.infrastructure.persistence.models.message.room.RoomPrivateMessageId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoomPrivateMessageRepositoryJpa extends JpaRepository<RoomPrivateMessage, RoomPrivateMessageId> {
    @Query(value =
            "SELECT rpm.* FROM room_private_messages rpm " +
            "RIGHT JOIN room_messages rm ON rm.id = rpm.room_message_id " +
            "INNER JOIN messages m ON m.id = rm.message_id " +
            "INNER JOIN users u ON u.id = m.user_id " +
            "WHERE rpm.room_message_id = ?1 " +
            " AND ( ( rpm.user_id = ?2 ) " +
            "  OR ( m.user_id = ?2 " +
            "   AND rm.message_id = m.id " +
            "   AND rpm.room_message_id = rm.id ) )",
            nativeQuery = true)
    Optional<RoomPrivateMessage> getByRoomMessageIdAndUserIdForPrivates(
            long roomMessageId, long userIdForPrivates);

    @Query(
            value = "SELECT DISTINCT count(*)" +
                    "FROM users u " +
                    "INNER JOIN messages m ON m.user_id = u.id " +
                    "INNER JOIN room_messages rm ON rm.message_id = m.id " +
                    "INNER JOIN room_private_messages rpm ON rpm.room_message_id = rm.id " +
                    "INNER JOIN rooms r ON r.id = rm.room_id " +
                    "WHERE r.id = ?1 " +
                    " AND ( ( rpm.user_id = ?2 AND m.user_id != ?2 ) " +
                    "  OR ( m.user_id = ?2 AND rpm.user_id != ?2 ) )",
            nativeQuery = true)
    Long getTotalPrivateMessagesDestinationUsers(long roomId, long userIdForPrivates);

    @Query(
            value = "SELECT rpm.* FROM room_private_messages rpm " +
                    "INNER JOIN room_messages rm ON rm.id = rpm.room_message_id " +
                    "INNER JOIN messages m ON m.id = rm.message_id " +
                    "INNER JOIN rooms r ON r.id = rm.room_id " +
                    "WHERE r.id = ?1 " +
                    " AND ( ( rpm.user_id = ?3 AND m.user_id = ?2 ) " +
                    "  OR ( rpm.user_id = ?2 AND m.user_id = ?3 ) ) " +
                    "LIMIT ?5 OFFSET ?4",
            nativeQuery = true)
    Streamable<RoomPrivateMessage> getAllPrivateMessageByDestinationUserSection(long roomId, long userIdForPrivates, long destinationUserId, long offset, long count);

    @Query(
            value = "SELECT count(rpm.*) FROM room_private_messages rpm " +
                    "INNER JOIN room_messages rm ON rm.id = rpm.room_message_id " +
                    "INNER JOIN messages m ON m.id = rm.message_id " +
                    "INNER JOIN rooms r ON r.id = rm.room_id " +
                    "WHERE r.id = ?1 " +
                    " AND ( ( rpm.user_id = ?3 AND m.user_id = ?2 ) " +
                    "  OR ( rpm.user_id = ?2 AND m.user_id = ?3 ) )",
            nativeQuery = true)
    Long getTotalPrivateMessageByDestinationUser(long roomId, long userIdForPrivates, long destinationUserId);
}
