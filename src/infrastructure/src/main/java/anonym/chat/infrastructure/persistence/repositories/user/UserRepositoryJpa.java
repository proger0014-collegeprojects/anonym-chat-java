package anonym.chat.infrastructure.persistence.repositories.user;

import anonym.chat.infrastructure.persistence.models.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepositoryJpa extends JpaRepository<User, Long> {

    @Query(value = "SELECT u.* FROM users u WHERE u.id = :id", nativeQuery = true)
    User getById(@Param("id") long id);

    @Query(value = "SELECT u.* FROM users u INNER JOIN user_auth ua ON u.id = ua.user_id WHERE ua.login = :login", nativeQuery = true)
    User getByLogin(@Param("login") String login);

    @Query(
        value = "SELECT u.* FROM users u " +
                "INNER JOIN room_profiles rp ON rp.user_id = u.id " +
                "WHERE rp.id = ?1",
        nativeQuery = true)
    User getByRoomProfileId(long roomProfileId);

    @Query(
            value = "SELECT DISTINCT " +
                    " CASE " +
                    "  WHEN rpm.user_id = ?2 then m.user_id " +
                    "  ELSE rpm.user_id " +
                    " END " +
                    "FROM users u " +
                    "INNER JOIN messages m ON m.user_id = u.id " +
                    "INNER JOIN room_messages rm ON rm.message_id = m.id " +
                    "INNER JOIN room_private_messages rpm ON rpm.room_message_id = rm.id " +
                    "INNER JOIN rooms r ON r.id = rm.room_id " +
                    "WHERE r.id = ?1 " +
                    " AND ( ( rpm.user_id = ?2 AND m.user_id != ?2 ) " +
                    "  OR ( m.user_id = ?2 AND rpm.user_id != ?2 ) ) " +
                    "LIMIT ?4 OFFSET ?3",
            nativeQuery = true)
    Streamable<Long> getPrivateMessageDestinationUsersSection(long roomId, long userIdForPrivates, long offset, long count);

    @Query(
            value = "SELECT DISTINCT " +
                    " CASE " +
                    "  WHEN rpm.user_id = ?2 then m.user_id " +
                    "  ELSE rpm.user_id " +
                    " END " +
                    "FROM users u " +
                    "INNER JOIN messages m ON m.user_id = u.id " +
                    "INNER JOIN room_messages rm ON rm.message_id = m.id " +
                    "INNER JOIN room_private_messages rpm ON rpm.room_message_id = rm.id " +
                    "INNER JOIN rooms r ON r.id = rm.room_id " +
                    "WHERE r.id = ?1 " +
                    " AND ( ( rpm.user_id = ?2 AND m.user_id != ?2 ) " +
                    "  OR ( m.user_id = ?2 AND rpm.user_id != ?2 ) ) ",
            nativeQuery = true)
    Optional<Long> getPrivateMessageDestinationUser(long roomId, long userIdForPrivates, long messageId);
}
