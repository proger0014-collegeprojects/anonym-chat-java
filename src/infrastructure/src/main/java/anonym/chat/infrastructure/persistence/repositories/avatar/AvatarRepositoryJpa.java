package anonym.chat.infrastructure.persistence.repositories.avatar;

import anonym.chat.infrastructure.persistence.models.avatar.Avatar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AvatarRepositoryJpa extends JpaRepository<Avatar, Long> {
    @Query(value = "SELECT * FROM avatars WHERE id = ?1 AND user_id = ?2", nativeQuery = true)
    Optional<Avatar> getOfUserById(long id, long userId);

    @Query(
            value = "SELECT * FROM avatars WHERE user_id = ?1 LIMIT ?3 OFFSET ?2",
            nativeQuery = true)
    List<Avatar> getAllOfUserSection(long userId, long offset, long count);

    @Query(
            value = "SELECT count(*) FROM avatars WHERE user_id = ?1",
            nativeQuery = true)
    Long getTotalOfUser(long userId);
}
