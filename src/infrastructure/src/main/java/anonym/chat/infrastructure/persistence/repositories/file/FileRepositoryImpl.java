package anonym.chat.infrastructure.persistence.repositories.file;

import anonym.chat.core.abstractions.file.FileRepository;
import anonym.chat.core.models.file.FileModel;
import anonym.chat.core.models.file.temp.FileTempModel;
import anonym.chat.core.utils.FileModelUtils;
import anonym.chat.infrastructure.persistence.mappers.FileMapper;
import anonym.chat.infrastructure.persistence.models.file.UploadedMinioFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.util.Optional;
import java.util.UUID;

@Repository
public class FileRepositoryImpl implements FileRepository {
    private final Logger logger = LoggerFactory.getLogger(FileRepositoryImpl.class);

    private final FileRepositoryJpa fileRepositoryJpa;
    private final FileMinioStorage fileMinioStorage;

    private final FileMapper fileMapper;

    public FileRepositoryImpl(
            FileRepositoryJpa fileRepositoryJpa,
            FileMinioStorage fileMinioStorage,
            FileMapper fileMapper) {
        this.fileRepositoryJpa = fileRepositoryJpa;
        this.fileMinioStorage = fileMinioStorage;
        this.fileMapper = fileMapper;
    }

    @Override
    public FileModel add(FileTempModel tempFile) {
        try {
            UploadedMinioFile uploadedMinioFile = fileMinioStorage.saveFile(
                    tempFile.getFile(),
                    UUID.randomUUID().toString(),
                    tempFile.getExtension().getValue());

            anonym.chat.infrastructure.persistence.models.file.File fileToPersist =
                    new anonym.chat.infrastructure.persistence.models.file.File();

            fileToPersist.setSize(uploadedMinioFile.size());
            fileToPersist.setType(FileModelUtils.getFileTypeFromMimeType(uploadedMinioFile.contentType()).getValue());
            fileToPersist.setUrl(uploadedMinioFile.bucket() + "/" + uploadedMinioFile.filename());

            anonym.chat.infrastructure.persistence.models.file.File savedFile = fileRepositoryJpa.save(fileToPersist);

            return fileMapper.toFileModelFromFileEntity(savedFile);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        return null;
    }

    @Override
    public FileModel getById(long id) {
        Optional<anonym.chat.infrastructure.persistence.models.file.File> existsFile = fileRepositoryJpa.findById(id);

        return existsFile.map(fileMapper::toFileModelFromFileEntity).orElse(null);
    }
}
