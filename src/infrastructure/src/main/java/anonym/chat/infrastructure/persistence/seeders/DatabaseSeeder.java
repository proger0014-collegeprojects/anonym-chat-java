package anonym.chat.infrastructure.persistence.seeders;

import anonym.chat.core.abstractions.room.RoomService;
import anonym.chat.core.abstractions.user.UserService;
import anonym.chat.core.models.room.CreateNewRoom;
import anonym.chat.core.models.user.CreateNewAuthUser;
import anonym.chat.core.models.user.Role;
import anonym.chat.core.models.user.UserAuth;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.infrastructure.persistence.configs.DatabaseSeederConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class DatabaseSeeder {
    private final Logger logger = LoggerFactory.getLogger(DatabaseSeeder.class);

    private UserService userService;
    private DatabaseSeederConfig databaseSeederConfig;
    private RoomService roomService;

    public DatabaseSeeder(
            UserService userService,
            DatabaseSeederConfig databaseSeederConfig,
            RoomService roomService) {
        this.userService = userService;
        this.databaseSeederConfig = databaseSeederConfig;
        this.roomService = roomService;
    }

    public void seed() {
        logger.info("Запуск инициализации базы данных!");

        ResultT<UserModel> existsUser = userService.getByLogin(databaseSeederConfig.getAdminLogin());

        if (existsUser.isSuccess()) {
            logger.info("Инициализация отменена: база данных уже имеет данные.");
            return;
        }

        UserModel createdAdmin = userService.addAuthUser(new CreateNewAuthUser(Role.ADMIN,
                new UserAuth(databaseSeederConfig.getAdminLogin(), databaseSeederConfig.getAdminPassword()))).getData();

        roomService.add(new CreateNewRoom(
            createdAdmin.getId(),
            databaseSeederConfig.getMainRoomName(),
            databaseSeederConfig.getMainRoomDescription()
        ));

        logger.info("База данных была инициализирована");
    }
}
