package anonym.chat.infrastructure.persistence.models.room.selected.profile;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

import java.io.Serializable;

@Embeddable
public class SelectedRoomProfileId implements Serializable {
    @Column(nullable = false)
    private Long roomProfileId;

    @Column(nullable = false)
    private Long roomId;


    public Long getRoomProfileId() {
        return roomProfileId;
    }

    public void setRoomProfileId(Long roomProfileId) {
        this.roomProfileId = roomProfileId;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }
}
