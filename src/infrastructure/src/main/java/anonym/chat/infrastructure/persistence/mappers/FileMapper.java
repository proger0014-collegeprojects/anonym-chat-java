package anonym.chat.infrastructure.persistence.mappers;

import anonym.chat.core.models.file.FileModel;
import anonym.chat.infrastructure.persistence.models.file.File;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", implementationName = "FileMapperImplInfrastructure")
public interface FileMapper {
    @Mapping(target = "type", expression = "java(anonym.chat.core.models.file.FileType.fromInt(file.getType()))")
    FileModel toFileModelFromFileEntity(File file);
}
