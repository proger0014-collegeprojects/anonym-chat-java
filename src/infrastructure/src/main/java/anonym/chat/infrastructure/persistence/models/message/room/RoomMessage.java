package anonym.chat.infrastructure.persistence.models.message.room;

import anonym.chat.infrastructure.persistence.models.message.Message;
import anonym.chat.infrastructure.persistence.models.room.Room;
import jakarta.persistence.*;

@Entity
@Table(name = "room_messages", schema = "public")
public class RoomMessage {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "message_id", referencedColumnName = "id")
    private Message message;

    @OneToOne
    @JoinColumn(name = "room_id", referencedColumnName = "id")
    private Room room;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
}
