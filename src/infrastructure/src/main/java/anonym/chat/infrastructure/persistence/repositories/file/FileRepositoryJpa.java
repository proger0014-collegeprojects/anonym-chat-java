package anonym.chat.infrastructure.persistence.repositories.file;

import anonym.chat.infrastructure.persistence.models.file.File;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRepositoryJpa extends JpaRepository<File, Long> { }
