package anonym.chat.infrastructure.media.images;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AvatarConfig {
    @Value("${image.defaults.size}")
    private Integer defaultSizeImage;

    public Integer getDefaultSizeImage() {
        return defaultSizeImage;
    }
}
