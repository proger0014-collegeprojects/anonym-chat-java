package anonym.chat.infrastructure.persistence.mappers;

import anonym.chat.core.models.token.refresh.RefreshSessionModel;
import anonym.chat.infrastructure.persistence.models.token.refresh.RefreshSession;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

@Mapper(
        componentModel = MappingConstants.ComponentModel.SPRING,
        implementationName = "RefreshSessionMapperImplInfrastructure"
)
public interface RefreshSessionMapper {
    @Mapping(target = "userId", expression = "java(source.getUser().getId())")
    RefreshSessionModel toModelFromEntity(RefreshSession source);
}
