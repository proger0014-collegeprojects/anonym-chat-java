package anonym.chat.infrastructure.persistence.repositories.room;

import anonym.chat.infrastructure.persistence.models.room.Room;
import anonym.chat.infrastructure.persistence.models.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoomRepositoryJpa extends JpaRepository<Room, Long> {
    @Query(value = "SELECT * FROM rooms LIMIT ?2 OFFSET ?1", nativeQuery = true)
    List<Room> getAllInSection(long offset, long count);

    @Query(
            value = "SELECT count(*) FROM rooms",
            nativeQuery = true)
    Long getTotal();

    @Query(
            value = "SELECT count(*) FROM rooms WHERE user_id = ?1",
            nativeQuery = true)
    Long getTotalByUserId(long userId);

    @Query(value = "SELECT * FROM rooms WHERE user_id = ?1 LIMIT ?3 OFFSET ?2", nativeQuery = true)
    List<Room> getAllUsersRoomInSection(long userId, long offset, long count);
}
