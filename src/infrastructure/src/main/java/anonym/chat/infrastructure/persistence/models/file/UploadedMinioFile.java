package anonym.chat.infrastructure.persistence.models.file;

public record UploadedMinioFile(String bucket, String filename, Long size, String contentType) { }
