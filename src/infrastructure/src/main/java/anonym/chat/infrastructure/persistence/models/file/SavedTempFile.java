package anonym.chat.infrastructure.persistence.models.file;

import java.io.File;

public record SavedTempFile(
    File file,
    String extension
) { }
