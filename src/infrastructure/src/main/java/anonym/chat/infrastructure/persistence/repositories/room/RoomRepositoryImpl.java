package anonym.chat.infrastructure.persistence.repositories.room;

import anonym.chat.core.abstractions.room.RoomRepository;
import anonym.chat.core.abstractions.user.UserRepository;
import anonym.chat.core.abstractions.user.UserService;
import anonym.chat.core.models.room.RoomModel;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.infrastructure.persistence.mappers.RoomMapper;
import anonym.chat.infrastructure.persistence.mappers.UserMapper;
import anonym.chat.infrastructure.persistence.models.room.ConnectedUsersToRoomRedis;
import anonym.chat.infrastructure.persistence.models.room.Room;
import anonym.chat.infrastructure.persistence.models.user.User;
import anonym.chat.infrastructure.persistence.repositories.user.ConnectedUsersToRoomRepositoryRedis;
import anonym.chat.infrastructure.persistence.repositories.user.UserRepositoryJpa;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public class RoomRepositoryImpl implements RoomRepository {
    private final RoomRepositoryJpa roomRepositoryJpa;
    private final ConnectedUsersToRoomRepositoryRedis connectedUsersToRoomRepositoryRedis;
    private final RoomMapper roomMapper;
    private final UserMapper userMapper;

    private final UserRepository userRepository;

    public RoomRepositoryImpl(
            RoomRepositoryJpa roomRepositoryJpa,
            ConnectedUsersToRoomRepositoryRedis connectedUsersToRoomRepositoryRedis,
            UserRepositoryJpa userRepositoryJpa,
            RoomMapper roomMapper,
            UserMapper userMapper,
            UserRepository userRepository) {
        this.roomRepositoryJpa = roomRepositoryJpa;
        this.connectedUsersToRoomRepositoryRedis = connectedUsersToRoomRepositoryRedis;
        this.roomMapper = roomMapper;
        this.userMapper = userMapper;
        this.userRepository = userRepository;
    }

    @Override
    public RoomModel add(RoomModel roomModel) {
        UserModel existsUser = userRepository.getById(roomModel.getUserId());

        User user = userMapper.from(existsUser);
        Room room = roomMapper.toRoomFromRoomModelAndUser(roomModel, user);
        Room createdRoom = roomRepositoryJpa.save(room);

        return roomMapper.toRoomModelFromRoom(createdRoom);
    }

    @Override
    public RoomModel getById(long id) {
        Optional<Room> existsRoom = roomRepositoryJpa.findById(id);

        return existsRoom.map(roomMapper::toRoomModelFromRoom).orElse(null);

    }

    @Override
    public List<RoomModel> getAllInSection(long offset, long count) {
        List<Room> section = roomRepositoryJpa.getAllInSection(offset, count);

        return roomMapper.toRoomModelListFromRoomList(section);
    }

    @Override
    public long getTotal() {
        return roomRepositoryJpa.getTotal();
    }

    @Override
    public List<RoomModel> getAllByUserIdInSection(long userId, long offset, long count) {
        List<Room> userRoomsList = roomRepositoryJpa.getAllUsersRoomInSection(userId, offset, count);

        return roomMapper.toRoomModelListFromRoomList(userRoomsList);
    }

    @Override
    public long getTotalByUserId(long userId) {
        return roomRepositoryJpa.getTotalByUserId(userId);
    }

    @Override
    public int getConnectedUsersCountInRoom(long roomId) {
        return (int)connectedUsersToRoomRepositoryRedis.findAllByRoomId(roomId).get().count();
    }

    @Override
    public List<UserModel> getAllConnectedUsersInRoomSection(long roomId, long offset, long count) {
        return connectedUsersToRoomRepositoryRedis
                .findAllByRoomId(roomId)
                .get()
                .skip(offset)
                .limit(count)
                .map(c -> userRepository.getById(c.getUserId()))
                .toList();
    }

    @Override
    public boolean connectUserToRoom(long userId, long roomId) {
        ConnectedUsersToRoomRedis connect = new ConnectedUsersToRoomRedis();
        connect.setUserId(userId);
        connect.setRoomId(roomId);

        ConnectedUsersToRoomRedis savedConnect = connectedUsersToRoomRepositoryRedis.save(connect);

        return true;
    }

    @Override
    public long deleteById(long id) {
        Optional<Room> existsRoom = roomRepositoryJpa.findById(id);

        if (existsRoom.isEmpty()) return -1;

        roomRepositoryJpa.deleteById(id);

        return existsRoom.get().getId();
    }

    @Override
    public boolean isConnectedToRoom(long userId, long roomId) {
        Optional<ConnectedUsersToRoomRedis> existsConnect = connectedUsersToRoomRepositoryRedis
                .findByUserIdAndRoomId(userId, roomId);

        return existsConnect.isPresent();
    }

    @Override
    public boolean disconnectUserFromRoom(long userId, long roomId) {
        Optional<ConnectedUsersToRoomRedis> existsConnect = connectedUsersToRoomRepositoryRedis
                .findByUserIdAndRoomId(userId, roomId);

        if (existsConnect.isEmpty()) return false;

        connectedUsersToRoomRepositoryRedis.delete(existsConnect.get());

        return true;
    }
}
