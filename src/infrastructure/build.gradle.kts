import java.util.*

plugins {
    id("java")
    alias(libs.plugins.migration.liquibase)
}

group = "anonym.chat.infrastructure"
version = "0.0.1"

fun getPlaceByEnv(env: String): String {
    return if (env == "testing") "test" else "main";
}

fun getChangelogFilePath(env: String): String {
    val place = getPlaceByEnv(env);

    return "src/infrastructure/build/resources/${place}/db/migrations/changelog.xml";
}

fun replaceEnvInProperties(prop: Properties) {
    for (propertyName in prop.stringPropertyNames()) {
        val propValue = prop.get(propertyName) as String;

        if (!propValue.startsWith("\${") || !propValue.endsWith("}")) continue;

        val envName = propValue.substring(2, propValue.length - 1);

        val envValue: String? = System.getenv(envName);

        if (envValue != null) {
            prop.setProperty(propertyName, envValue);
        }
    }
}

fun loadProperties(env: String): Properties {
    val place = getPlaceByEnv(env);

    val props =  Properties().apply {
        load(file("${project(":src:web").projectDir}/src/${place}/resources/application-${env}.properties").reader())
    }

    replaceEnvInProperties(props);

    return props;
}

fun registerLiquibase(env: String) {
    val props = loadProperties(env);

    liquibase {
        if (activities.contains(activities.findByName("main"))) return@liquibase;

        activities.register("main") {
            val url = props["spring.datasource.url"]
            val username = props["spring.datasource.username"]
            val password = props["spring.datasource.password"]
            val changelogFile = getChangelogFilePath(env)
            val classpath = "/"
            arguments = mapOf(
                "logLevel" to "info",
                "changelogFile" to changelogFile,
                "url" to url,
                "username" to username,
                "password" to password,
                "classpath" to classpath
            )
        }
    }
}

fun getEnvByProp(prop: String?): String {
    return prop ?: "development"
}

dependencies {
    implementation(platform(org.springframework.boot.gradle.plugin.SpringBootPlugin.BOM_COORDINATES))

    implementation(project(":src:core"))

    implementation(libs.spring.context)
    implementation(libs.spring.boot.starter.data.jpa)
    implementation(libs.spring.boot.starter.data.redis)
    runtimeOnly(libs.db.postgresql)
    implementation(libs.db.redis.jedis)
    implementation(libs.db.`object`.minio)
    implementation(libs.support.ffmpeg)
    implementation(libs.support.image.imageio.core)
    implementation(libs.support.image.imageio.metadata)
    implementation(libs.support.image.imageio.jpeg)
    implementation(libs.support.apache.tika)

    liquibaseRuntime(libs.migration.liquibase.core)
    liquibaseRuntime(libs.support.picocli)
    liquibaseRuntime(libs.db.postgresql)

    testImplementation(libs.testng)

    compileOnly(libs.mapstruct.mapper)
    annotationProcessor(libs.mapstruct.annotation.processor)
}

val env: String? by project

tasks.register("updateDb") {
    dependsOn("update")

    val envComputed = getEnvByProp(env);

    registerLiquibase(envComputed);
}

tasks.register("dropDb") {
    dependsOn("dropAll");

    val envComputed = getEnvByProp(env);

    registerLiquibase(envComputed)
}

//tasks.register("migrateTest") {
//    dependsOn("update")
//
//    registerLiquibase("test")
//}
//
//tasks.register("migrateApp") {
//    dependsOn("update")
//
//    registerLiquibase("dev")
//}
//
//tasks.register("dropAllTest") {
//    dependsOn("dropAll")
//
//    registerLiquibase("test")
//}
//
//tasks.register("dropAllApp") {
//    dependsOn("dropAll")
//
//    registerLiquibase("dev")
//}