package anonym.chat.core.abstractions;

public interface PasswordEncoder {
    String encode(String password);
}
