package anonym.chat.core.models.avatar;

import java.io.InputStream;

public record CreateNewAvatar(
    long userId,
    InputStream file,
    int offsetX,
    int offsetY,
    int size // percentage
) { }
