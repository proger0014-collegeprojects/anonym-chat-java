package anonym.chat.core.abstractions.token.refresh;

import anonym.chat.core.models.token.refresh.CheckTokenIsValid;
import anonym.chat.core.models.token.refresh.CreateNewFromExistingToken;
import anonym.chat.core.models.token.refresh.RefreshSessionModel;
import anonym.chat.core.utils.result.ResultT;

public interface RefreshSessionService {
    ResultT<RefreshSessionModel> createNew(long userId);
    ResultT<RefreshSessionModel> getByToken(String refreshToken);
    ResultT<Boolean> deleteByToken(String refreshToken);
    ResultT<RefreshSessionModel> createNewFromExistingToken(CreateNewFromExistingToken createNewFromExistingToken);
    ResultT<Boolean> checkTokenIsValid(CheckTokenIsValid checkTokenIsValid);
}
