package anonym.chat.core.models.message.room;

public record GetAllInRoomSection(
    long roomId,
    long section,
    long userIdForPrivates
) { }
