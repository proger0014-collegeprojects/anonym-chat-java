package anonym.chat.core.utils.claim;

import anonym.chat.core.models.token.Claim;

import java.util.List;
import java.util.Optional;

public interface Claims {
    <TClaim> Optional<Claim<TClaim>> getByKey(String key);
    List<Claim<?>> getAll();
}
