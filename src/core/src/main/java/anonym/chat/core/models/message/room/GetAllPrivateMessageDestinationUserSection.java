package anonym.chat.core.models.message.room;

public record GetAllPrivateMessageDestinationUserSection(
    long roomId,
    long userIdForPrivates,
    long section
) { }
