package anonym.chat.core.models.message.room;

public class RoomPrivateMessageInfo {
    private long destinationUserId;

    public long getDestinationUserId() {
        return destinationUserId;
    }

    public void setDestinationUserId(long destinationUserId) {
        this.destinationUserId = destinationUserId;
    }
}
