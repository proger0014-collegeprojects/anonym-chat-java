package anonym.chat.core.abstractions.providers.config;

import anonym.chat.core.utils.token.RefreshTokenConfig;

public interface RefreshTokenConfigProvider extends ConfigProvider<RefreshTokenConfig> { }
