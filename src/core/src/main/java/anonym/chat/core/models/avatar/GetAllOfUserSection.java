package anonym.chat.core.models.avatar;

public record GetAllOfUserSection(
    long userId,
    long section
) { }
