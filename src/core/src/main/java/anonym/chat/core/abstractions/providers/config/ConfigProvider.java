package anonym.chat.core.abstractions.providers.config;

public interface ConfigProvider<TConfigDto> {
    TConfigDto get();
}
