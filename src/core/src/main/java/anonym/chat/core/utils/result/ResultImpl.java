package anonym.chat.core.utils.result;

sealed class ResultImpl<TData, TError> implements Result<TData, TError> permits ResultTImpl {
    private final TData data;
    private final TError error;

    ResultImpl(TData data, TError error) {
        this.data = data;
        this.error = error;
    }

    @Override
    public TData getData() {
        return data;
    }

    @Override
    public TError getError() {
        return error;
    }

    @Override
    public boolean isSuccess() {
        return data != null;
    }

    @Override
    public boolean isFailure() {
        return !isSuccess();
    }
}
