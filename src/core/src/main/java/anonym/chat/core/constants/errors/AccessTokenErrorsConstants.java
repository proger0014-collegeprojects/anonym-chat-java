package anonym.chat.core.constants.errors;

public final class AccessTokenErrorsConstants {
    public static final String TYPE = CommonErrorsConstants.TYPE + "/access-token";

    public static final String NOT_EXISTS_CLAIM_BY_KEY_TYPE = TYPE + "/not-exists-claim-by-key";
    public static final String NOT_EXISTS_CLAIM_BY_KEY_TITLE = "По данному ключу не сущесвует данного утверждения!";
    public static final int NOT_EXISTS_CLAIM_BY_KEY_STATUS = 404;
    public static final String NOT_EXISTS_CLAIM_BY_KEY_DESCRIPTION = CommonErrorsConstants.DESCRIPTION_VALID;

    public static final String WRONG_TOKEN_TYPE = TYPE + "/wrong-token";
    public static final String WRONG_TOKEN_TITLE = "Неверный формат токена";
    public static final int WRONG_TOKEN_STATUS = 400;
    public static final String WRONG_TOKEN_DESCRIPTION = CommonErrorsConstants.INCORRECT_REQUEST_DESCRIPTION;

    public static final String EXPIRED_TOKEN_TYPE = TYPE + "/expired-token";
    public static final String EXPIRED_TOKEN_TITLE = "Истек срок токена";
    public static final int EXPIRED_TOKEN_STATUS = 400;
    public static final String EXPIRED_TOKEN_DESCRIPTION = CommonErrorsConstants.INCORRECT_REQUEST_DESCRIPTION;

}
