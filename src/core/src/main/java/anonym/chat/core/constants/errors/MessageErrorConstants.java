package anonym.chat.core.constants.errors;

public final class MessageErrorConstants {
    public static final String TYPE = CommonErrorsConstants.TYPE + "/messages";

    public static final String NOT_CONNECTED_TO_ROOM_TYPE = TYPE + "/not-connected-to-room";
    public static final String NOT_CONNECTED_TO_ROOM_TITLE = "Вы не подключены к комнате, чтобы читать или писать сообщения";
    public static final int NOT_CONNECTED_TO_ROOM_STATUS = 400;
    public static final String NOT_CONNECTED_TO_ROOM_DESCRIPTION = "Подключитесь в комнату";
}
