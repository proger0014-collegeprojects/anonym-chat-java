package anonym.chat.core.models.room;

public record CreateNewRoom(
    long userId,
    String name,
    String description
) { }
