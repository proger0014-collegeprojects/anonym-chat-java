package anonym.chat.core.abstractions.room.profile;

import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.models.room.profile.RoomProfileModel;

import java.util.List;

public interface RoomProfileRepository {
    RoomProfileModel add(RoomProfileModel roomProfileModel);
    List<RoomProfileModel> getAllByUserIdInSection(long userId, long offset, long count);
    long getTotalByUserId(long userId);
    RoomProfileModel getById(long id, long userIdTarget);
    Boolean setRoomProfileToRoom(long roomProfileId, long roomId, long userIdTarget);
    RoomProfileModel getByRoomIdAndUserId(long roomId, long userId);
    RoomProfileModel getInstalled(long roomId, long userProfileId, long userId);
}
