package anonym.chat.core.models.avatar;

import anonym.chat.core.models.file.temp.FileTempModel;

public record EditAvatar(
    FileTempModel tempFile,
    int offsetX,
    int offsetY,
    int size
) { }
