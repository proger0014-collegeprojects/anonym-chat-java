package anonym.chat.core.models.room.profile;

public record SetRoomProfileToRoom(
    long roomProfileId,
    long roomId,
    long userIdTarget
) { }
