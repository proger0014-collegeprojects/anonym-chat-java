package anonym.chat.core.models.file.temp;

import java.io.File;

public record DeleteTempFile(
    File toDelete
) { }
