package anonym.chat.core.constants;

public final class UsedFileMimeTypes {
    public static final String IMAGE_PNG = "image/png";
    public static final String IMAGE_JPEG = "image/jpeg";
    public static final String IMAGE_JPG = "image/jpg";
    public static final String IMAGE_GIF = "image/gif";

    public static final String VIDEO_MP4 = "video/mp4";
}
