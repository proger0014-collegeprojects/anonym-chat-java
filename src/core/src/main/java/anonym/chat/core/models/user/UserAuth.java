package anonym.chat.core.models.user;

public record UserAuth(
    String login,
    String password
) { }
