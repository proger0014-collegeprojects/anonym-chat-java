package anonym.chat.core.models.user;

public record CreateNewAuthUser(
    Role role,
    UserAuth userAuth
) { }
