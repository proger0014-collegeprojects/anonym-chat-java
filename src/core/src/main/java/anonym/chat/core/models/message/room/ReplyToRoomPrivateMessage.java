package anonym.chat.core.models.message.room;

public record ReplyToRoomPrivateMessage(
    long userId,
    long destinationUserId,
    long roomId,
    long roomMessageId,
    String body
) { }
