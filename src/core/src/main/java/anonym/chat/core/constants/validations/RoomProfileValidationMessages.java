package anonym.chat.core.constants.validations;

import anonym.chat.core.constants.RoomProfileConstants;

public final class RoomProfileValidationMessages {
    public static final String NICKNAME_MIN = "Не должен быть меньше, чем " + RoomProfileConstants.NICKNAME_MIN_LENGTH;
    public static final String NICKNAME_MAX = "Не должен быть больше, чем " + RoomProfileConstants.NICKNAME_MAX_LENGTH;
}
