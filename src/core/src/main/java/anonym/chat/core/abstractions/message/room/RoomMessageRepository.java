package anonym.chat.core.abstractions.message.room;

import anonym.chat.core.models.message.room.RoomMessageModel;
import anonym.chat.core.models.user.UserModel;

import java.time.LocalDateTime;
import java.util.List;

public interface RoomMessageRepository {
    RoomMessageModel addPublicRoomMessage(long userId, long roomId, String body, LocalDateTime now);
    RoomMessageModel replyToRoomMessage(long userId, long roomId, long roomMessageId, String body, LocalDateTime now);
    List<RoomMessageModel> getAllInRoomSection(long roomId, long userIdForPrivates, long offset, long count);
    long getTotalInRoom(long roomId, long userIdForPrivates);
    RoomMessageModel getInRoomById(long roomId, long userIdForPrivates, long messageId);
    RoomMessageModel getInRoomByIdPublic(long roomId, long messageId);
    RoomMessageModel addPrivateRoomMessage(long userId, long roomId, long destinationUserId, String body, LocalDateTime now);
    RoomMessageModel replyToRoomPrivateMessage(long userId, long roomId, long destinationUserId, long roomPrivateMessageId, String body, LocalDateTime now);
    List<UserModel> getPrivateMessagesDestinationUsers(long roomId, long userIdForPrivates, long offset, long count);
    long getTotalPrivateMessagesDestinationUsers(long roomId, long userIdForPrivates);
    List<RoomMessageModel> getAllPrivateMessageByDestinationUserSection(long roomId, long userIdForPrivates, long destinationUserId, long offset, long count);
    long getTotalAllPrivateMessageByDestinationUser(long roomId, long userIdForPrivates, long destinationUserId);
}
