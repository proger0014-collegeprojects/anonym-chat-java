package anonym.chat.core.abstractions.providers;

public interface TokenGeneratorProvider {
    String generate();
}
