package anonym.chat.core.abstractions.file;

import anonym.chat.core.models.file.FileModel;
import anonym.chat.core.models.file.temp.FileTempModel;
import anonym.chat.core.utils.result.ResultT;

import java.io.InputStream;

public interface FileService {
    ResultT<FileModel> add(InputStream file);

    ResultT<FileModel> getById(long id);

    // Добавить сохраненный в файловой системе файл
    ResultT<FileModel> addExistsFile(FileTempModel existsFile);
}
