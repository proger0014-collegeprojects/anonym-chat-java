package anonym.chat.core.utils;

import java.time.*;
import java.time.temporal.TemporalAccessor;
import java.util.Date;

public final class DateAndTimeUtils {
    public static Date toDate(LocalDateTime source) {
        return Date.from(source.atZone(ZoneId.systemDefault())
                .toInstant());
    }

    public static Date toDateUTC(LocalDateTime source) {
        return Date.from(source.atZone(Clock.systemUTC().getZone())
                .toInstant());
    }

    public static LocalDateTime toLocalDateTime(Date source) {
        return source.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }
}
