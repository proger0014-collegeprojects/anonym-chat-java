package anonym.chat.core.models.user;

public record CreateNewUser(
    Role role
) { }
