package anonym.chat.core.abstractions.providers;

import java.time.LocalDateTime;

public interface DateTimeProvider {
    LocalDateTime now();
    LocalDateTime nowUTC();
}
