package anonym.chat.core.models.message.room;

public record ReplyRoomMessage(
    long userId,
    long roomId,
    long roomMessageId,
    String body
) { }
