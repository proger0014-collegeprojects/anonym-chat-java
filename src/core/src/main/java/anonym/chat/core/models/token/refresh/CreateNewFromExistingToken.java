package anonym.chat.core.models.token.refresh;

public record CreateNewFromExistingToken(
    String refreshToken
) { }
