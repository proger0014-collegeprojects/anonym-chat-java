package anonym.chat.core.models.message.room;

import anonym.chat.core.models.message.BaseMessage;


public class RoomMessageModel extends BaseMessage {
    private long roomId;
    public RoomPrivateMessageInfo privateMessageInfo;


    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public RoomPrivateMessageInfo getPrivateMessageInfo() {
        return privateMessageInfo;
    }

    public void setPrivateMessageInfo(RoomPrivateMessageInfo privateMessageInfo) {
        this.privateMessageInfo = privateMessageInfo;
    }
}
