package anonym.chat.core.abstractions;

import anonym.chat.core.models.ValidationModel;
import anonym.chat.core.utils.validator.ValidationResult;

public interface Validator {
    <T extends ValidationModel> ValidationResult validate(T... target);
}
