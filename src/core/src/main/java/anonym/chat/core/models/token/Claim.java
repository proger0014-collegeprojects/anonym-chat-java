package anonym.chat.core.models.token;

public record Claim<T>(
    String key,
    T value
) { }
