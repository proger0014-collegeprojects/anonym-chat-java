package anonym.chat.core.models.message.room;

public record CreateNewRoomMessage(
    long userId, // owner
    long roomId, // to
    String body
) { }
