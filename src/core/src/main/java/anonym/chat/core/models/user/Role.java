package anonym.chat.core.models.user;

public enum Role {
    ADMIN(0),
    MODERATOR(1),
    USER(2);

    private final int value;

    Role(final int newValue) {
        this.value = newValue;
    }

    public int getValue() {
        return value;
    }

    public static Role fromInt(int value) {
        for (Role role : Role.values()) {
            if (role.getValue() == value) {
                return role;
            }
        }

        return null;
    }
}
