package anonym.chat.core.abstractions.room.profile;

import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.models.room.profile.CreateNewRoomProfile;
import anonym.chat.core.models.room.profile.IsInstalledToRoom;
import anonym.chat.core.models.room.profile.RoomProfileModel;
import anonym.chat.core.models.room.profile.SetRoomProfileToRoom;
import anonym.chat.core.utils.result.ResultT;

import java.util.Optional;

public interface RoomProfileService {
    ResultT<RoomProfileModel> add(CreateNewRoomProfile create);
    ResultT<SectionCollection<RoomProfileModel>> getAllByUserIdInPage(long userId, long page);
    ResultT<RoomProfileModel> getById(long id, long userIdTarget);
    ResultT<Boolean> setRoomProfileToRoom(SetRoomProfileToRoom set);
    ResultT<RoomProfileModel> getByRoomIdAndUserId(long roomId, long userId);
    ResultT<Boolean> isInstalledToRoom(IsInstalledToRoom isInstalled);
}
