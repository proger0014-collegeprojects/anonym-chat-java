package anonym.chat.core.constants.errors;

public class RefreshTokenErrorsConstants {
    public static final String TYPE = CommonErrorsConstants.AUTH_TYPE + "/refresh-token";

    public static final String NOT_EXISTS_TYPE = TYPE + "/not-exists";
    public static final String NOT_EXISTS_TITLE = "Не существует данного рефреш токена!";
    public static final int NOT_EXISTS_STATUS = 400;
    public static final String NOT_EXISTS_DESCRIPTION = CommonErrorsConstants.INTERNAL_SERVER_ERROR_DESCRIPTION;

    public static final String EXPIRED_TYPE = TYPE + "/expired";
    public static final String EXPIRED_TITLE = "Рефреш токен истек!";
    public static final int EXPIRED_STATUS = 400;
    public static final String EXPIRED_DESCRIPTION = "Войдите снова в систему!";
}
