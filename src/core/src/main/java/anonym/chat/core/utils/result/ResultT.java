package anonym.chat.core.utils.result;

public interface ResultT<TData> extends Result<TData, ResultError> { }
