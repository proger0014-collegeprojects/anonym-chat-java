package anonym.chat.core.constants.errors;

public final class RoomPrivateMessageErrorsConstants {
    public static final String TYPE = RoomErrorsConstants.TYPE + "/privates/messages";

    public static final String CANNOT_SEND_TO_YOUR_SELF_PRIVATE_MESSAGE_TYPE = TYPE + "/cannot-send-to-your-self";
    public static final String CANNOT_SEND_TO_YOUR_SELF_PRIVATE_MESSAGE_TITLE = "Вы не можете отправлять самому себе приватные сообщения";
    public static final int CANNOT_SEND_TO_YOUR_SELF_PRIVATE_MESSAGE_STATUS = 400;
    public static final String CANNOT_SEND_TO_YOUR_SELF_PRIVATE_MESSAGE_DESCRIPTION = CommonErrorsConstants.INCORRECT_REQUEST_DESCRIPTION;

}
