package anonym.chat.core.abstractions.user;

import anonym.chat.core.models.user.*;
import anonym.chat.core.utils.result.Result;
import anonym.chat.core.utils.result.ResultT;

public interface UserService {
    ResultT<UserModel> add();
    ResultT<UserModel> addAuthUser(CreateNewAuthUser add);
    ResultT<UserModel> getById(long userId);
    ResultT<UserModel> getByLogin(String login);
    ResultT<UserModel> update(UpdateUser update);
    ResultT<UserModel> updateAuthUser(UpdateAuthUser update);
}
