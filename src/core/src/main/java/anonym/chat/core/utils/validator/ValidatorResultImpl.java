package anonym.chat.core.utils.validator;

import java.util.List;
import java.util.Map;

final class ValidatorResultImpl implements ValidationResult {
    private final boolean isValid;
    private final Map<String, List<String>> errors;

    ValidatorResultImpl(boolean isValid, Map<String, List<String>> errors) {
        this.isValid = isValid;
        this.errors = errors;
    }

    @Override
    public boolean isValid() {
        return isValid;
    }

    @Override
    public Map<String, List<String>> getErrors() {
        return errors;
    }
}
