package anonym.chat.core.constants.errors;

public final class UserErrorsConstants {
    public static final String TYPE = CommonErrorsConstants.TYPE + "/users";

    public static final String LOGIN_IS_USED_TYPE = TYPE + "/login-is-already-used";
    public static final String LOGIN_IS_USED_TITLE = "Логин уже занят другим аккаунтом!";
    public static final int LOGIN_IS_USED_STATUS = 400;
    public static final String LOGIN_IS_USED_DESCRIPTION = "Попробуйте повторить запрос с другим логином";
}
