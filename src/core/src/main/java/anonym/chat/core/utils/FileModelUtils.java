package anonym.chat.core.utils;

import anonym.chat.core.constants.UsedFileMimeTypes;
import anonym.chat.core.models.file.FileType;

import java.util.*;

public final class FileModelUtils {
    public static FileType getFileTypeFromMimeType(String mimeType) {
        HashMap<FileType, List<String>> mimeTypes = new HashMap<>();

        List<String> images = new ArrayList<>();

        images.add(UsedFileMimeTypes.IMAGE_PNG);
        images.add(UsedFileMimeTypes.IMAGE_JPEG);
        images.add(UsedFileMimeTypes.IMAGE_JPG);
        images.add(UsedFileMimeTypes.IMAGE_GIF);

        List<String> videos = new ArrayList<>();

        videos.add(UsedFileMimeTypes.VIDEO_MP4);

        mimeTypes.put(FileType.IMAGE, images);
        mimeTypes.put(FileType.VIDEO, videos);

        Optional<Map.Entry<FileType, List<String>>> result = mimeTypes.entrySet().stream()
                .filter(entry -> entry.getValue().contains(mimeType))
                .findFirst();

        return result.map(Map.Entry::getKey).orElse(null);
    }
}
