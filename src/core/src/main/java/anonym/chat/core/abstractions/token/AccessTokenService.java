package anonym.chat.core.abstractions.token;

import anonym.chat.core.models.token.Claim;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.claim.Claims;
import anonym.chat.core.utils.result.ResultT;

public interface AccessTokenService {
    ResultT<String> generateToken(long userId);
    ResultT<UserModel> getUserByToken(String token);
    <TClaim> ResultT<Claim<TClaim>> extractClaim(String token, String key);
    ResultT<Claims> extractAllClaims(String token);
    ResultT<Boolean> checkTokenValid(String token);
}
