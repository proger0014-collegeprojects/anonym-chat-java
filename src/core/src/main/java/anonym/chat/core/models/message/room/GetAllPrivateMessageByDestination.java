package anonym.chat.core.models.message.room;

public record GetAllPrivateMessageByDestination(
    long section,
    long roomId,
    long userIdForPrivates,
    long destinationUserId
) { }
