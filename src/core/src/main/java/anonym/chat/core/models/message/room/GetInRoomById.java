package anonym.chat.core.models.message.room;

public record GetInRoomById(
    long roomId,
    long userIdForPrivates,
    long messageId
) { }
