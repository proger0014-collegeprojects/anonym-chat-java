package anonym.chat.core.models.file;

public enum FileType {
    IMAGE(0),
    VIDEO(1);

    private final int value;

    FileType(final int newValue) {
        this.value = newValue;
    }

    public int getValue() {
        return value;
    }

    public static FileType fromInt(int value) {
        for (FileType fileType : FileType.values()) {
            if (fileType.getValue() == value) {
                return fileType;
            }
        }

        return null;
    }
}
