package anonym.chat.core.models.file.temp;

import anonym.chat.core.models.file.FileExtension;

import java.io.File;

public class FileTempModel {
    private File file;
    private FileExtension extension;


    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public FileExtension getExtension() {
        return extension;
    }

    public void setExtension(FileExtension extension) {
        this.extension = extension;
    }
}
