package anonym.chat.core.constants.errors;

public final class RoomMessageErrorsConstants {
    public static final String TYPE = RoomErrorsConstants.TYPE + "/messages";
}
