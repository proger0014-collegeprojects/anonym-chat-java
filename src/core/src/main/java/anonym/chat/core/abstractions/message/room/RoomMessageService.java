package anonym.chat.core.abstractions.message.room;

import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.models.message.room.ReplyRoomMessage;
import anonym.chat.core.models.message.room.*;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;

public interface RoomMessageService {
    ResultT<RoomMessageModel> addPublicRoomMessage(CreateNewRoomMessage createNewRoomMessage);
    ResultT<RoomMessageModel> replyToRoomMessage(ReplyRoomMessage replyRoomMessage);
    ResultT<RoomMessageModel> replyToRoomPrivateMessage(ReplyToRoomPrivateMessage replyToRoomPrivateMessage);
    // TODO: Подумать о датах и получения по датам списка
    ResultT<SectionCollection<RoomMessageModel>> getAllInRoomSection(GetAllInRoomSection get);
    ResultT<RoomMessageModel> getInRoomById(GetInRoomById get);
    ResultT<RoomMessageModel> getInRoomByIdPublic(GetInRoomByIdPublic get);
    ResultT<RoomMessageModel> addPrivateRoomMessage(CreateNewPrivateRoomMessage createNewPrivateRoomMessage);
    ResultT<SectionCollection<UserModel>> getAllPrivateMessageDestinationUserSection(GetAllPrivateMessageDestinationUserSection get);
    ResultT<SectionCollection<RoomMessageModel>> getAllPrivateMessageByDestinationUserSection(GetAllPrivateMessageByDestination get);
}
