package anonym.chat.core.utils.result;

import java.util.List;
import java.util.Map;

class ResultErrorImpl implements ResultError {
    private final String type;
    private final String title;
    private final int status;
    private final Map<String, List<String>> errors;

    ResultErrorImpl(String type, String title, int status, Map<String, List<String>> errors) {
        this.type = type;
        this.title = title;
        this.status = status;
        this.errors = errors;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public Map<String, List<String>> getErrors() {
        return errors;
    }
}
