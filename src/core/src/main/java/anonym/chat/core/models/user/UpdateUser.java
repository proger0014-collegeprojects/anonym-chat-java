package anonym.chat.core.models.user;

public record UpdateUser(
    long userId,
    Role role
) { }
