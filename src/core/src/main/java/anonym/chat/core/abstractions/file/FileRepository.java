package anonym.chat.core.abstractions.file;

import anonym.chat.core.models.file.FileModel;
import anonym.chat.core.models.file.temp.FileTempModel;

import java.io.File;

public interface FileRepository {
    FileModel add(FileTempModel tempFile);

    FileModel getById(long id);
}
