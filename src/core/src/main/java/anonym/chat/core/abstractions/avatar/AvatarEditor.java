package anonym.chat.core.abstractions.avatar;

import anonym.chat.core.models.avatar.EditAvatar;
import anonym.chat.core.models.file.temp.FileTempModel;

public interface AvatarEditor {
    FileTempModel editAvatar(EditAvatar forEdit);
}
