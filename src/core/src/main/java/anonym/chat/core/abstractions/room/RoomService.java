package anonym.chat.core.abstractions.room;

import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.models.room.CreateNewRoom;
import anonym.chat.core.models.room.RoomModel;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;

public interface RoomService {
    ResultT<RoomModel> add(CreateNewRoom create);
    ResultT<RoomModel> getById(long id);
    ResultT<SectionCollection<RoomModel>> getAllPage(long page);
    ResultT<SectionCollection<RoomModel>> getAllByUserIdInPage(long userId, long page);
    ResultT<Integer> getConnectedUsersCountInRoom(long roomId);
    ResultT<SectionCollection<UserModel>> getAllConnectedUsersInRoomSection(long roomId, long section);
    ResultT<Boolean> connectUserToRoom(long userId, long roomId);
    ResultT<Boolean> isConnectedToRoom(long userId, long roomId);
    ResultT<Boolean> disconnectUserFromRoom(long userId, long roomId);
    ResultT<Long> deleteById(long id);
}
