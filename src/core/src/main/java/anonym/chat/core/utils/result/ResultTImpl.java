package anonym.chat.core.utils.result;

public final class ResultTImpl<TData> extends ResultImpl<TData, ResultError> implements ResultT<TData> {
    public ResultTImpl(TData data, ResultError error) {
        super(data, error);
    }
}
