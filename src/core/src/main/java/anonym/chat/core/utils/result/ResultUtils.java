package anonym.chat.core.utils.result;

import anonym.chat.core.constants.ResultConstants;

import java.util.*;

public final class ResultUtils {
    private static ResultError createResultError(String type, String title, int status, Map<String, List<String>> errors) {
        return new ResultErrorImpl(type, title, status, errors);
    }

    public static <TData> Result<TData, ResultError> fromResultError(String type, String title, int status, Map<String, List<String>> errors) {
        var resultError = createResultError(type, title, status, errors);

        return fromError(resultError);
    }

    public static <TData, TError> Result<TData, TError> fromError(TError error) {
        return new ResultImpl<>(null, error);
    }

    public static <TData> ResultT<TData> fromTError(String type, String title, int status, Map<String, List<String>> errors) {
        var resultError = createResultError(type, title, status, errors);

        return new ResultTImpl<>(null, resultError);
    }

    public static <TData> ResultT<TData> fromTSingleError(String type, String title, int status, String singleError) {
        var singleErrorMap = new HashMap<String, List<String>>();
        var singleErrorList = new ArrayList<String>();

        singleErrorList.add(singleError);
        singleErrorMap.put("error", singleErrorList);

        return fromTError(type, title, status, singleErrorMap);
    }

    public static <TData> ResultT<TData> fromTSingleError(ResultError error) {
        String singleError = "";
        Map<String, List<String>> errors = error.getErrors();

        Optional<String> firstError = errors.keySet().stream().findFirst();

        if (firstError.isEmpty()) {
            singleError = "Неизвестная ошибка!";
        }

        singleError = errors.get(firstError.get()).getFirst();

        return fromTSingleError(error.getType(), error.getTitle(), error.getStatus(), singleError);
    }

    public static <TData> ResultT<TData> fromTData(TData data) {
        return new ResultTImpl<>(data, null);
    }

    public static <TData, TError> Result<TData, TError> fromData(TData data) {
        return new ResultImpl<>(data, null);
    }

    public static <TData> boolean isSingleError(ResultError error) {
        var errors = error.getErrors();

        if (errors.size() > 1) return false;

        try {
            var singleErrorValue = errors.get(ResultConstants.SINGLE_ERROR_KEY);

            if (singleErrorValue == null) return false;
        } catch (Exception e) {
            return false;
        }

        return true;
    }
}
