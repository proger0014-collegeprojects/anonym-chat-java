package anonym.chat.core.constants.errors;

// TODO: типизировать ошибки
public final class ModelsErrorConstants {
    public static final String TYPE = CommonErrorsConstants.TYPE + "/models";

    public static final String NOT_EXISTS_TYPE = TYPE + "/not-exists";
    public static final String NOT_EXISTS_TITLE = "Данной сущности не существует";
    public static final int NOT_EXISTS_STATUS = 404;
    public static final String NOT_EXISTS_DESCRIPTION = CommonErrorsConstants.DESCRIPTION_VALID;

    public static final String EXISTS_TYPE = TYPE + "/exists";
    public static final String EXISTS_TITLE = "Данная сущность уже существует";
    public static final int EXISTS_STATUS = 400;
    public static final String EXISTS_DESCRIPTION = CommonErrorsConstants.DESCRIPTION_VALID;
}
