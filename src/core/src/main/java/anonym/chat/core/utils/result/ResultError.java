package anonym.chat.core.utils.result;

import java.util.List;
import java.util.Map;

public interface ResultError {
    String getType();
    String getTitle();
    int getStatus();
    Map<String, List<String>> getErrors();
}
