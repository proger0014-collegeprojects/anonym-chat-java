package anonym.chat.core.abstractions.token.refresh;

import anonym.chat.core.models.token.refresh.RefreshSessionModel;

import java.time.LocalDateTime;

public interface RefreshSessionRepository {
    RefreshSessionModel add(long userId, String token, LocalDateTime createdAt, LocalDateTime expiresIn);
    RefreshSessionModel getByToken(String refreshToken);
    boolean deleteByRefreshToken(String refreshToken);
}
