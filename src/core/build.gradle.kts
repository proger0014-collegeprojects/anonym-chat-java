plugins {
    id("java")
}

group = "anonym.chat.core"
version = "0.0.1"

dependencies {
    testImplementation(libs.testng)
}